{-|
Module      : Data.Result.Optics
Description : Optics from lens for the multi results
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Data.Result.Optics (
    -- * Result Prisms
    _Success,
    _Failure,
    _Continue,

    -- * Result Lenses
    _result,
    _errors,

    -- * Result Predicates
    isSuccess,
    isFailure,
    isContinue
  ) where

import           Control.Lens.Fold  (has)
import           Control.Lens.Lens  (Lens, lens)
import           Control.Lens.Prism (Prism', prism')
import           Data.DList         (DList)
import           Data.Result        (Result, fromContinue, fromFailure,
                                     fromSuccess, getErrors, getResult,
                                     setErrors, setResult, toContinue,
                                     toFailure, toSuccess)

-- | Prism for 'Success' results.
_Success :: Prism' (Result e a) a
_Success = prism' toSuccess fromSuccess

-- | Prism for 'Failure' results.
_Failure :: Prism' (Result e a) (DList e)
_Failure = prism' toFailure fromFailure

-- | Prism for 'Continue' results.
_Continue :: Prism' (Result e a) (DList e, a)
_Continue = prism' (uncurry toContinue) fromContinue

-- | Lens for handling the optional result value of a 'Result'.
_result :: Lens (Result e a) (Result e b) (Maybe a) b
_result = lens getResult setResult

-- | Lens for handling the optional error 'DList' of a 'Result'.
_errors :: Lens (Result e1 a) (Result e2 a) (DList e1) (DList e2)
_errors = lens getErrors setErrors

-- | Predicate to determine whether a given 'Result' is 'Success'.
isSuccess
  :: Result e a  -- ^ Result to test.
  -> Bool        -- ^ True when 'Success', otherwise False.
isSuccess = has _Success

-- | Predicate to determine whether a given 'Result' is 'Failure'.
isFailure
  :: Result e a -- ^ Result to test.
  -> Bool       -- ^ True when 'Failure', otherwise False.
isFailure = has _Failure

-- | Predicate to determine whether a given 'Result' is 'Continue'.
isContinue
  :: Result e a -- ^ Result to test.
  -> Bool       -- ^ True when 'Continue', otherwise False.
isContinue = has _Continue
