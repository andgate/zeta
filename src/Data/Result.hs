{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-|
Module      : Data.Result
Description : Multi-error result type
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Data.Result where

import           Data.Bifunctor
import           Data.DList     (DList)

-- | Multi-error result type
-- Designed to safely and efficiently accumulate multiple non-fatal errors
-- during a computation. For efficiency, 'DList' is used as an error container.
data Result e a
  = Success a
  | Failure (DList e)
  | Continue (DList e) a
  deriving (Show)
instance (Eq e, Eq a) => Eq (Result e a) where
  (==) r1 r2 = case (r1, r2) of
    (Success a1, Success a2)               -> a1 == a2
    (Failure errs1, Failure errs2)         -> errs1 == errs2
    (Continue errs1 a1, Continue errs2 a2) -> a1 == a2 && errs1 == errs2
    _                                      -> False

-- | Create a successful result from some value.
toSuccess
  :: a            -- ^ Some value.
  -> Result e a   -- ^ A successful result containing the give value.
toSuccess = Success

-- | Attempt to extract a 'Success' from a given 'Result.
fromSuccess
  :: Result e a   -- ^ Some result.
  -> Maybe a      -- ^ A result value when the result is successful, otherwise Nothing.
fromSuccess = \case
  Success a -> Just a
  _         -> Nothing

-- | Create a failure from some errors.
toFailure
  :: DList e      -- ^ Some errors.
  -> Result e a   -- ^ A failed result, carrying the given errors.
toFailure = Failure

-- | Attempt to extract a 'Failure' from a given 'Result.
fromFailure
  :: Result e a      -- ^ Some result.
  -> Maybe (DList e) -- ^ A list of failures when the result is failed, otherwise Nothing.
fromFailure = \case
  Failure errs -> Just errs
  _            -> Nothing

-- | Create a continuing result from some errors and a value.
toContinue
  :: DList e    -- ^ Some errors.
  -> a          -- ^ A value to continue with.
  -> Result e a -- ^ A result carrying the errors and the value.
toContinue = Continue

-- | Attempt to extract a 'Continue' from a given 'Result.
fromContinue
  :: Result e a         -- ^ Some result.
  -> Maybe (DList e, a) -- ^ A list of failures paired with a result value when the result is continuing, otherwise Nothing.
fromContinue = \case
  Continue errs a -> Just (errs, a)
  _               -> Nothing

-- | Attempt to extract the result value from a given 'Result'.
-- Succeeds when the result is successful or continued.
getResult
  :: Result e a  -- ^ Some result.
  -> Maybe a     -- ^ An extracted result value, or Nothing in the case of failure.
getResult = \case
  Success a    -> Just a
  Failure _    -> Nothing
  Continue _ a -> Just a

-- | Set the result value of a 'Result'.
setResult
  :: Result e a  -- ^ Current result
  -> b           -- ^ New result value
  -> Result e b  -- ^ New result, carrying the new result value.
setResult r b = case r of
  Success _       -> Success b
  Failure errs    -> Failure errs
  Continue errs _ -> Continue errs b

-- | Get the errors from a 'Result'.
getErrors :: Result e a -> DList e
getErrors = \case
  Success _       -> mempty
  Failure errs    -> errs
  Continue errs _ -> errs

-- | Get the errors of a 'Result'.
setErrors
  :: Result e1 a  -- ^ Current result
  -> DList e2     -- ^ New 'DList' of errors
  -> Result e2 a  -- ^ New result, carrying the new errors.
setErrors r errs = case r of
  Success a    -> Success a
  Failure _    -> Failure errs
  Continue _ a -> Continue errs a

instance Functor (Result e) where
  fmap f = \case
    Success a    -> Success (f a)
    Failure e    -> Failure e
    Continue e a -> Continue e (f a)

instance Bifunctor Result where
  bimap f g = \case
    Success a     -> Success (g a)
    Failure es    -> Failure (f <$> es)
    Continue es a -> Continue (f <$> es) (g a)

  first f = \case
    Success a     -> Success a
    Failure es    -> Failure (f <$> es)
    Continue es a -> Continue (f <$> es) a

  second g = \case
    Success a     -> Success (g a)
    Failure es    -> Failure es
    Continue es a -> Continue es (g a)
