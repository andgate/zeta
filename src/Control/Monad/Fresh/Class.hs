{-# LANGUAGE MultiParamTypeClasses #-}
{-|
Module      : Control.Monad.Fresh.Class
Description : MonadFresh class and instances.
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Control.Monad.Fresh.Class where

import           Classy.HasGeneration        (HasGeneration (..))
import           Classy.HasName              (HasName (..))
import           Control.Lens.At             (at)
import           Control.Lens.Operators      ((&), (.~), (?~), (^.))
import           Control.Monad.Except        (ExceptT, mapExceptT)
import           Control.Monad.Reader        (MonadReader (..), ReaderT (..),
                                              asks, mapReaderT)
import           Control.Monad.State         (state)
import qualified Control.Monad.State.Lazy    as Lazy (StateT (..), mapStateT)
import qualified Control.Monad.State.Strict  as Strict (StateT (..), mapStateT)
import           Control.Monad.Trans         (MonadTrans (..))
import           Control.Monad.Trans.Fresh   (FreshLocalT (..), FreshT (..))
import           Control.Monad.Trans.Maybe   (MaybeT, mapMaybeT)
import qualified Control.Monad.Writer.Lazy   as Lazy (WriterT (..), mapWriterT)
import qualified Control.Monad.Writer.Strict as Strict (WriterT (..),
                                                        mapWriterT)
import qualified Data.Map.Strict             as Map
import           Data.Text                   (Text)

-- | Type class for monads that can generate globablly fresh values.
class Monad m => MonadFresh m where
  -- | Generate some fresher value under some supporting monadic context
  fresh
    :: (HasName a, HasGeneration a)
    => a   -- ^ Some named, generational object to freshen.
    -> m a -- ^ Fresh object, generated under a global monadic context.

instance Monad m => MonadFresh (FreshT m) where
  fresh x = FreshT . state $
    \ns ->
      let n = x ^. _name in
      case ns ^. at n of
        Nothing ->
          ( x  & generation .~ 0
          , ns & at n ?~ 1
          )
        Just i ->
          ( x  & generation .~ i
          , ns & at n ?~ i + 1
          )
  {-# INLINABLE fresh #-}

instance MonadFresh m => MonadFresh (ExceptT e m) where
  fresh = lift . fresh
  {-# INLINABLE fresh #-}

instance MonadFresh m => MonadFresh (MaybeT m) where
  fresh = lift . fresh
  {-# INLINABLE fresh #-}

instance MonadFresh m => MonadFresh (ReaderT r m) where
  fresh = lift . fresh
  {-# INLINABLE fresh #-}

instance MonadFresh m => MonadFresh (Lazy.StateT s m) where
  fresh = lift . fresh
  {-# INLINABLE fresh #-}

instance MonadFresh m => MonadFresh (Strict.StateT s m) where
  fresh = lift . fresh
  {-# INLINABLE fresh #-}

instance (Monoid w, MonadFresh m) => MonadFresh (Lazy.WriterT w m) where
  fresh = lift . fresh
  {-# INLINABLE fresh #-}

instance (Monoid w, MonadFresh m) => MonadFresh (Strict.WriterT w m) where
  fresh = lift . fresh
  {-# INLINABLE fresh #-}

-- | Type class for monads that can generate locally fresh values.
class Monad m => MonadLocallyFresh m where
  -- | Generate some fresher value under some supporting monadic context
  freshLocally
    :: (HasGeneration a, HasName a)
    => a   -- ^ Some variable to freshen.
    -> m a -- ^ Fresh value, generated under a local monadic context.

  -- | Avoid the given list of names.
  avoid
    :: (HasGeneration a, HasName a)
    => [a] -- ^ List of names to avoid.
    -> m b -- ^ Subcomputation to avoid names in.
    -> m b -- ^ Results in a new subcomputation where the given names are taken.

  -- | Get the list of avoided names in the current subcomputations.
  getAvoids :: m [(Text, Integer)]

instance Monad m => MonadLocallyFresh (FreshLocalT m) where
  freshLocally v = FreshLocalT . reader $
    \ns ->
      let n = v ^. _name in
      case ns ^. at n of
        Nothing -> v & generation .~ 0
        Just i  -> v & generation .~ i + 1
  {-# INLINABLE freshLocally #-}

  avoid vs = FreshLocalT . local f . unFreshLocalT
    where
      ns2 = Map.fromList [ (v ^. _name, v ^. generation)| v <- vs]
      f ns1 = ns2 <> ns1 -- entries in ns2 must replace overlapping entries in ns1
      {-# INLINE f #-}
  {-# INLINABLE avoid #-}

  getAvoids = FreshLocalT . asks $ Map.toList
  {-# INLINABLE getAvoids #-}

instance MonadLocallyFresh m => MonadLocallyFresh (ExceptT e m) where
  freshLocally = lift . freshLocally
  {-# INLINABLE freshLocally #-}
  avoid = mapExceptT . avoid
  {-# INLINABLE avoid #-}
  getAvoids = lift getAvoids
  {-# INLINABLE getAvoids #-}

instance MonadLocallyFresh m => MonadLocallyFresh (MaybeT m) where
  freshLocally = lift . freshLocally
  {-# INLINABLE freshLocally #-}
  avoid = mapMaybeT . avoid
  {-# INLINABLE avoid #-}
  getAvoids = lift getAvoids
  {-# INLINABLE getAvoids #-}

instance MonadLocallyFresh m => MonadLocallyFresh (ReaderT r m) where
  freshLocally = lift . freshLocally
  {-# INLINABLE freshLocally #-}
  avoid = mapReaderT . avoid
  {-# INLINABLE avoid #-}
  getAvoids = lift getAvoids
  {-# INLINABLE getAvoids #-}

instance MonadLocallyFresh m => MonadLocallyFresh (Lazy.StateT s m) where
  freshLocally = lift . freshLocally
  {-# INLINABLE freshLocally #-}
  avoid = Lazy.mapStateT . avoid
  {-# INLINABLE avoid #-}
  getAvoids = lift getAvoids
  {-# INLINABLE getAvoids #-}

instance MonadLocallyFresh m => MonadLocallyFresh (Strict.StateT s m) where
  freshLocally = lift . freshLocally
  {-# INLINABLE freshLocally #-}
  avoid = Strict.mapStateT . avoid
  {-# INLINABLE avoid #-}
  getAvoids = lift getAvoids
  {-# INLINABLE getAvoids #-}

instance (Monoid w, MonadLocallyFresh m) => MonadLocallyFresh (Lazy.WriterT w m) where
  freshLocally = lift . freshLocally
  {-# INLINABLE freshLocally #-}
  avoid = Lazy.mapWriterT . avoid
  {-# INLINABLE avoid #-}
  getAvoids = lift getAvoids
  {-# INLINABLE getAvoids #-}

instance (Monoid w, MonadLocallyFresh m) => MonadLocallyFresh (Strict.WriterT w m) where
  freshLocally = lift . freshLocally
  {-# INLINABLE freshLocally #-}
  avoid = Strict.mapWriterT . avoid
  {-# INLINABLE avoid #-}
  getAvoids = lift getAvoids
  {-# INLINABLE getAvoids #-}
