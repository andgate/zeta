{-# LANGUAGE RankNTypes   #-}
{-# LANGUAGE ViewPatterns #-}
{-|
Module      : Control.Monad.Span.Class
Description : Span monad class.
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Control.Monad.Span.Class where

import           Prelude                  hiding (span)

import           Control.Lens.Operators   ((^?))
import           Control.Monad.Trans.Span
import           Document.Classy.HasSpan  (HasSpan (..))

-- | Class for lifted Span Monad
-- This is essentially a /WriterT m (Maybe Span)/.
class Monad m => MonadSpan m where
  -- | Include span from some object into the span monad context.
  include :: forall a. HasSpan a => a -> m ()

instance MonadSpan SpanM where
  include ((^? span) -> s) = SpanM (s, ())
  {-# INLINE include #-}
