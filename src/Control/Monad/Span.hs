{-|
Module      : Control.Monad.Span
Description : The Span Monad
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX

The span monad is accumulates spans into a /Maybe Span/ context,
using a monoid instance to combine spans it encounters.

Normally, since a Span is defined only on Naturals, there is no empty value,
and thus no monoid instance. However, wrapping a Span in a Maybe supplies
us with an additive identity, and we get a monoid for free. With the /Maybe Span/
monoid, we can then build Applicative and Monad instances for our Span monad.

Ultimately, 'SpanT m' is equivalent to /WriterT m (Maybe Span)/.
-}
module Control.Monad.Span (
    -- * Span Monad Transformer Types
    module Trans,
    -- * Span Monad Class
    module Class
  ) where

import           Control.Monad.Span.Class as Class
import           Control.Monad.Trans.Span as Trans
