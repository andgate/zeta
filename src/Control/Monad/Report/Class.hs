{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE LambdaCase             #-}
{-|
Module      : Control.Monad.Report.Class
Description : Report monad class
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Control.Monad.Report.Class (
    MonadReport(..)
  ) where

import           Control.Monad.Trans.Report (ReportT (..))
import           Data.Result                (Result (..))

-- | An interface for multi-error handling under some context, such as a Monad
class MonadReport e m | m -> e where
  {-# MINIMAL allFatal, report #-}

  -- | Records some error, but doesn't cause failure.
  witness
    :: e    -- ^ Some error to accumulate
    -> m () -- ^ Results in a unit
  witness e = nonfatal e ()

  -- | Records some error and returns some default value,
  -- but doesn't cause
  nonfatal
    :: e    -- ^ Some error to accumulate
    -> a    -- ^ Some default value to return
    -> m a  -- ^ Results in the given default value
  nonfatal e = report . Continue (pure e)

  -- | Records a fatal error, terminating any further actions.
  fatal
    :: e   -- ^ Some error to terminate with
    -> m a -- ^ Results in a failed computation
  fatal = report . Failure . pure

  -- | Executes some inner report, treating all accumulated errors as fatal.
  allFatal
    :: m a  -- ^ Some inner report to compute
    -> m a  -- ^ Result of the inner report, treating any accumulated errors as fatal.

  -- | Reports some `Result` to the `Report` monad.
  -- This can force a failure, accumulate new errors,
  -- or simply change the current return value.
  report
    :: Result e a  -- ^ Some result to report
    -> m a         -- ^ Results in a context updated by the report.

instance Monad m => MonadReport e (ReportT e m) where
  witness = report . (`Continue` ()) . pure

  nonfatal e a = report $ Continue (pure e) a

  fatal = report . Failure . pure

  allFatal m = ReportT $ \case
      Failure es    -> return $ Failure es
      Continue es _ -> do
          ja <- unReportT m $ Success ()
          return $ case ja of
            Failure es'    -> let es'' = es' <> es in es'' `seq` Failure es''
            Continue es' _ -> let es'' = es' <> es in es'' `seq` Failure es''
            Success     a  -> Continue es a
      Success _   -> do
          ja <- unReportT m $ Success ()
          return $ case ja of
              Failure es'    -> Failure es'
              Continue es' _ -> Failure es'
              Success     a  -> Success a


  report ra = ReportT $ \r ->
    return $ case r of
      Success _ -> ra
      Failure es ->
        case ra of
          Success _      -> Failure es
          Failure es'    -> let es'' = es <> es' in es'' `seq` Failure es''
          Continue es' _ -> let es'' = es <> es' in es'' `seq` Failure es''
      Continue es _ ->
        case ra of
          Success a      -> Continue es a
          Failure es'    -> let es'' = es <> es' in es'' `seq` Failure es''
          Continue es' a -> let es'' = es <> es' in es'' `seq` Continue es'' a
