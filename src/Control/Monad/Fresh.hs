{-# OPTIONS_GHC -Wno-unused-top-binds #-}
{-|
Module      : Control.Monad.Fresh
Description : Monad for generating fresh variable names.
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Control.Monad.Fresh (module X) where

import           Control.Monad.Fresh.Class as X
import           Control.Monad.Trans.Fresh as X
