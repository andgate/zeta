{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE UndecidableInstances  #-}
{-|
Module      : Control.Monad.Trans.Fresh
Description : MonadFresh class and instances.
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Control.Monad.Trans.Fresh where

import           Control.Monad.Except   (MonadError (..))
import           Control.Monad.IO.Class (MonadIO (..))
import           Control.Monad.Reader   (MonadReader (..), ReaderT, mapReaderT)
import           Control.Monad.State    (MonadState (..), StateT, evalStateT)
import           Control.Monad.Trans    (MonadTrans (..))
import           Control.Monad.Writer   (MonadWriter (..))
import           Data.Functor.Identity  (Identity, runIdentity)
import           Data.Map.Strict        (Map)
import qualified Data.Map.Strict        as Map
import           Data.Text              (Text)

type NameMap = Map Text Integer

-- | Fresh monad transformer.
newtype FreshT m a = FreshT { unFreshT :: StateT NameMap m a }

runFreshT :: Monad m => FreshT m a -> m a
runFreshT m = contFreshT m mempty
{-# INLINE runFreshT #-}

contFreshT :: Monad m => FreshT m a -> [(Text, Integer)] -> m a
contFreshT (FreshT ma) = evalStateT ma . Map.fromList
{-# INLINE contFreshT #-}

-- | Fresh monad, which is just a 'FreshT Identity a'.
type Fresh a = FreshT Identity a

runFresh :: Fresh a -> a
runFresh = runIdentity . runFreshT
{-# INLINE runFresh #-}

contFresh :: Fresh a -> [(Text, Integer)] -> a
contFresh m = runIdentity . contFreshT m
{-# INLINE contFresh #-}

instance Monad m => Functor (FreshT m) where
  fmap f = FreshT . fmap f . unFreshT
  {-# INLINEABLE fmap #-}

instance Monad m => Applicative (FreshT m) where
  pure = FreshT . pure
  {-# INLINEABLE pure #-}

  (FreshT r1) <*> (FreshT r2) =
    FreshT (r1 <*> r2)
  {-# INLINEABLE (<*>) #-}

instance Monad m => Monad (FreshT m) where
  return = FreshT . return
  {-# INLINEABLE return #-}

  (FreshT ma) >>= f = FreshT (ma >>= (unFreshT . f))
  {-# INLINEABLE (>>=) #-}

instance MonadIO m => MonadIO (FreshT m) where
  liftIO = FreshT . liftIO
  {-# INLINEABLE liftIO #-}

instance MonadError e m => MonadError e (FreshT m) where
  throwError = FreshT . throwError
  {-# INLINEABLE throwError #-}

  catchError ma f = FreshT $ catchError (unFreshT ma) (unFreshT . f)
  {-# INLINEABLE catchError #-}

instance MonadWriter w m => MonadWriter w (FreshT m) where
  tell = lift . tell
  {-# INLINEABLE tell #-}

  listen = FreshT . listen . unFreshT
  {-# INLINEABLE listen #-}

  pass = FreshT . pass . unFreshT
  {-# INLINEABLE pass #-}

instance MonadReader r m => MonadReader r (FreshT m) where
  ask = lift ask
  {-# INLINEABLE ask #-}
  local f = FreshT . local f . unFreshT
  {-# INLINEABLE local #-}

instance MonadState s m => MonadState s (FreshT m) where
  get = lift get
  {-# INLINEABLE get #-}
  put = lift . put
  {-# INLINEABLE put #-}

instance MonadTrans FreshT where
  lift = FreshT . lift
  {-# INLINEABLE lift #-}

-- | Local variant of the fresh monad transformer.
newtype FreshLocalT m a =
  FreshLocalT { unFreshLocalT :: ReaderT (Map Text Integer) m a }

-- | Local variant of the fresh monad
-- which is just a 'FreshLocalT Identity a'.
type FreshLocal m a = FreshLocalT Identity a

instance Monad m => Functor (FreshLocalT m) where
  fmap f = FreshLocalT . fmap f . unFreshLocalT
  {-# INLINEABLE fmap #-}

instance Monad m => Applicative (FreshLocalT m) where
  pure = FreshLocalT . pure
  {-# INLINEABLE pure #-}

  (FreshLocalT r1) <*> (FreshLocalT r2) =
    FreshLocalT (r1 <*> r2)
  {-# INLINEABLE (<*>) #-}

instance Monad m => Monad (FreshLocalT m) where
  return = FreshLocalT . return
  {-# INLINEABLE return #-}

  (FreshLocalT ma) >>= f =
    FreshLocalT (ma >>= (unFreshLocalT . f))
  {-# INLINEABLE (>>=) #-}

instance MonadIO m => MonadIO (FreshLocalT m) where
  liftIO = FreshLocalT . liftIO
  {-# INLINEABLE liftIO #-}

instance MonadError e m => MonadError e (FreshLocalT m) where
  throwError =
    FreshLocalT . throwError
  {-# INLINEABLE throwError #-}
  catchError ma f =
    FreshLocalT $ catchError (unFreshLocalT ma) (unFreshLocalT . f)
  {-# INLINEABLE catchError #-}

instance MonadWriter w m => MonadWriter w (FreshLocalT m) where
  tell = lift . tell
  {-# INLINEABLE tell #-}
  listen = FreshLocalT . listen . unFreshLocalT
  {-# INLINEABLE listen #-}
  pass = FreshLocalT . pass . unFreshLocalT
  {-# INLINEABLE pass #-}

instance MonadReader r m => MonadReader r (FreshLocalT m) where
  ask = lift ask
  {-# INLINEABLE ask #-}
  local f = FreshLocalT . mapReaderT (local f) . unFreshLocalT
  {-# INLINEABLE local #-}

instance MonadState s m => MonadState s (FreshLocalT m) where
  get = lift get
  {-# INLINEABLE get #-}
  put = lift . put
  {-# INLINEABLE put #-}

instance MonadTrans FreshLocalT where
  lift = FreshLocalT . lift
  {-# INLINEABLE lift #-}
