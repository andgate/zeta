{-|
Module      : Control.Monad.Trans.Span
Description : Span monad transformer.
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Control.Monad.Trans.Span where

import           Prelude                 hiding (span)

import           Control.Comonad
import           Document.Classy.HasSpan (HasSpan (..))
import           Document.Data.Span      (Span)

-- | Span Monad
newtype SpanM a = SpanM { runSpan :: (Maybe Span, a) }

instance (Show a) => Show (SpanM a) where
  show (SpanM (l, a)) = show l <> ":" <> show a

instance (Eq a) => Eq (SpanM a) where
  (==) (SpanM (l1, a1)) (SpanM (l2, a2)) =
    l1 == l2 && a1 == a2

instance {-# OVERLAPS #-} HasSpan (SpanM a) where
  span f (SpanM (s, a)) = fmap SpanM . (,) <$> span f s <*> pure a
  {-# INLINE span #-}

instance Semigroup a => Semigroup (SpanM a) where
  (SpanM (s1, a1)) <> (SpanM (s2, a2)) = SpanM (s1 <> s2, a1 <> a2)
  {-# INLINE (<>) #-}

instance (Monoid a) => Monoid (SpanM a) where
  mempty = SpanM (Nothing, mempty)
  {-# INLINE mempty #-}

instance Functor SpanM where
  fmap f (SpanM (s, a)) = SpanM (s, f a)
  {-# INLINE fmap #-}

instance Applicative SpanM where
  pure a = SpanM (mempty, a)
  {-# INLINE pure #-}
  (SpanM (s1, f)) <*> (SpanM (s2, a)) = SpanM (s1 <> s2, f a)
  {-# INLINE (<*>) #-}

instance Monad SpanM where
  (SpanM (s1, a)) >>= f =
    let (SpanM (s2, b)) = f a
    in SpanM (s1 <> s2, b)
  {-# INLINE (>>=) #-}

instance Comonad SpanM where
  extract (SpanM (_, a))     = a
  {-# INLINE extract #-}
  duplicate s@(SpanM (l, _)) = SpanM (l, s)
  {-# INLINE duplicate #-}
  extend f s@(SpanM (l, _))  = SpanM (l, f s)
  {-# INLINE extend #-}
