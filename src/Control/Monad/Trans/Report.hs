{-# LANGUAGE CPP                   #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances  #-}
{-|
Module      : Control.Monad.Trans.Report
Description : Report monad transformer types
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Control.Monad.Trans.Report (
    -- * The Report Monad Transformer
    ReportT(..),
    runReportT,

    -- * The Report Monad
    Report,
    runReport
  ) where

import           Control.Monad              (liftM)
import           Control.Monad.Reader.Class (MonadReader (..))
import           Control.Monad.Trans        (MonadTrans (..))
import qualified Data.DList                 as DL
import           Data.Functor.Identity      (Identity, runIdentity)
import           Data.Result                (Result (..))

-- | Report monad transformer, essentially ErrorT extended with multi-errors.
-- Unlike other error reporting monads, this one does not space-leak.
newtype ReportT e m a
  = ReportT { unReportT :: Result e () -> m (Result e a) }

-- | Run some given 'ReportT' monad.
runReportT
  :: Monad m
  => ReportT e m a    -- ^ Report monad transformer to run.
  -> m (Either [e] a) -- ^ Either a list of fatal errors or some result value, under the transformer's given monad.
runReportT m = do
  r <- unReportT m (Success ())
  case r of
    Success   a  -> return $ Right a
    Failure  es  -> return $ Left $ DL.toList es
    Continue _ a -> return $ Right a

-- | Report monad, which just 'ReportT' on the 'Identity' monad.
type Report e a = ReportT e Identity a

-- | Run some given 'Report' monad.
runReport
  :: Report e a   -- ^ Report monad to run.
  -> Either [e] a -- ^ Either a list of fatal errors or some result value.
runReport = runIdentity . runReportT

instance (Show e, Show a) => Show (ReportT e Identity a) where
  show = show . runReport

instance Functor m => Functor (ReportT e m) where
  fmap f (ReportT r) = ReportT $ \es -> fmap f <$> r es

instance (Functor m, Monad m) => Applicative (ReportT e m) where
  pure a = ReportT $ \case
    Success _     -> return $ Success a
    Failure es    -> return $ Failure es
    Continue es _ -> return $ Continue es a

  (ReportT mf) <*> (ReportT mx) = ReportT $ \es ->
    do  rf <- mf es
        case rf of
          Success f ->
            do  rx <- mx (Success ())
                case rx of
                  Success      x  -> return $ Success      (f x)
                  Failure  es2    -> return $ Failure  es2
                  Continue  es2 x -> return $ Continue  es2 (f x)

          Failure  es'  -> return $ Failure es'

          Continue es' f ->
            do  rx <- mx $ Continue es' ()
                case rx of
                  Success       x  -> return $ Success       (f x)
                  Failure  es''    -> return $ Failure  es''
                  Continue  es'' x -> return $ Continue  es'' (f x)

instance Monad m => Monad (ReportT e m) where
#if !(MIN_VERSION_base(4,8,9))
  return a = ReportT $ \case
    Success    _   -> return $ Success    a
    Failure es     -> return $ Failure es
    Continue  es _ -> return $ Continue  es a
#endif

  (ReportT ma) >>= k = ReportT $ \es -> do
    ra <- ma es
    case ra of
      Success     a  -> unReportT (k a) (Success ())
      Failure es'    -> return $ Failure es'
      Continue es' a -> unReportT (k a) (Continue es' ())

instance MonadTrans (ReportT e) where
  lift m = ReportT $ \case
      Success _     -> Success `liftM` m
      Failure es    -> return $ Failure es
      Continue es _ -> Continue es `liftM` m

instance (MonadReader r m) => MonadReader r (ReportT e m) where
  ask = lift ask
  local f (ReportT m)
    = ReportT $ \r -> local f (m r)
  reader = lift . reader
