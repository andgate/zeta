{-|
Module      : Control.Monad.Report
Description : A classy report monad
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Control.Monad.Report (
    -- * Report Monad Class
    module Class,
    -- * Report Monad Types
    module Trans
  ) where

import           Control.Monad.Report.Class as Class
import           Control.Monad.Trans.Report as Trans
