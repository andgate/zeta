{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasLocation.Loc where

import           Document.Classy.HasLocation.Class (HasLocation (..))
import           Document.Data.Location.Type       (Loc)

instance HasLocation Loc where
  location = id
  {-# INLINABLE location #-}
