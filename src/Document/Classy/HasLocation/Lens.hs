module Document.Classy.HasLocation.Lens where

import           Control.Lens.Traversal            (Traversal')
import           Document.Classy.HasLocation.Class (HasLocation (..))
import           Document.Data.Location.Lens       (_locFilePath, _locSpan)
import           Document.Data.Span.Type           (Span)

-- | Classy Traversal to the source filepath of a source location carrier.
locFilePath :: HasLocation a => Traversal' a FilePath
locFilePath = location . _locFilePath

-- | Classy Traversal to the source document span of a source location carrier.
locSpan :: HasLocation a => Traversal' a Span
locSpan = location . _locSpan
