module Document.Classy.HasLocation.Class where

import           Control.Lens.Traversal      (Traversal')
import           Document.Data.Location.Type (Loc)

-- | Class for types which are 'Loc' carriers.
class HasLocation a where
  -- | Traverse to the source location carried by \(a\).
  location :: Traversal' a Loc
