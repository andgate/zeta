{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasLocation.Traversable where

import           Document.Classy.HasLocation.Class (HasLocation (..))

instance (Traversable f, HasLocation a) => HasLocation (f a) where
  location f = traverse (location f)
  {-# INLINABLE location #-}
