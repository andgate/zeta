{-|
Module      : Document.Classy.HasLocation
Description : Classy lenses for "Document.Data.Location#Loc"
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Document.Classy.HasLocation (module X) where

import           Document.Classy.HasLocation.Class       as X
import           Document.Classy.HasLocation.Lens        as X
import           Document.Classy.HasLocation.Loc         ()
import           Document.Classy.HasLocation.Traversable ()
