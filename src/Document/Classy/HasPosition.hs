{-|
Module      : Document.Classy.HasPosition
Description : Classy lenses for "Document.Data.Position#Position"
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX

Classy lenses for "Document.Data.Position#Position"
-}
module Document.Classy.HasPosition (
    -- * HasPosition Class
    module Class,
    -- * Lifted Lenses for Position
    module Lens
  ) where

import           Document.Classy.HasPosition.Class       as Class
import           Document.Classy.HasPosition.Lens        as Lens
import           Document.Classy.HasPosition.NaturalPair ()
import           Document.Classy.HasPosition.Patterns    ()
import           Document.Classy.HasPosition.Position    ()
import           Document.Classy.HasPosition.V2          ()
