{-|
Module      : Document.Classy.HasSpan
Description : Classy lenses for "Document.Data.Span#Span"
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX

Classy lenses for "Document.Data.Span#Span".
-}
module Document.Classy.HasSpan (
    -- * HasSpan Class
    module Class,
    -- * Lifted Lenses for Span
    module Lens
  ) where

import           Document.Classy.HasSpan.Class           as Class
import           Document.Classy.HasSpan.HasLocation     ()
import           Document.Classy.HasSpan.HasPositionPair ()
import           Document.Classy.HasSpan.Lens            as Lens
import           Document.Classy.HasSpan.Maybe           ()
import           Document.Classy.HasSpan.Patterns        ()
import           Document.Classy.HasSpan.PositionPair    ()
import           Document.Classy.HasSpan.Span            ()
import           Document.Classy.HasSpan.Traversable     ()
import           Document.Classy.HasSpan.V4              ()
