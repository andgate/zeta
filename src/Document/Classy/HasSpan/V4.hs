{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasSpan.V4 where

import           Document.Classy.HasSpan.Class (HasSpan (..))
import           Document.Data.Span.Type       (Span (..), getSpanV)
import           Linear.V4                     (V4)
import           Numeric.Natural               (Natural)

instance HasSpan (V4 Natural) where
  span f v =
    getSpanV <$> f (S v)
  {-# INLINE span #-}
