{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasSpan.Traversable where

import           Document.Classy.HasSpan.Class (HasSpan (..))
import           Prelude                       hiding (span)

instance {-# OVERLAPPABLE #-} (Traversable t, HasSpan a) => HasSpan (t a) where
  span f = traverse (span f)
  {-# INLINE span #-}
