{-# LANGUAGE RankNTypes #-}
module Document.Classy.HasSpan.Class where

import           Prelude                      hiding (span)

import           Control.Lens.Operators       ((^?))
import           Control.Lens.Traversal       (Traversal')
import           Document.Data.Span.Semigroup ()
import           Document.Data.Span.Type      (Span)

-- | Class for types which are 'Span'-carriers.
class HasSpan a where
  -- | "Control.Lens.Traversal" to a 'Span' in some value of type /a/.
  span :: Traversal' a Span

infixr 6 <++>
(<++>) :: (HasSpan a, HasSpan b) => a -> b -> Maybe Span
(<++>) a b = (a ^? span) <> ( b ^? span)
