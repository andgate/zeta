{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasSpan.HasLocation where

import           Document.Classy.HasLocation.Class (HasLocation (..))
import           Document.Classy.HasLocation.Lens  (locSpan)
import           Document.Classy.HasSpan.Class     (HasSpan (..))

instance {-# OVERLAPPABLE #-} (HasLocation a) => HasSpan a where
  span = locSpan
  {-# INLINABLE span #-}
