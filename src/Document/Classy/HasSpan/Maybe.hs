{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase        #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasSpan.Maybe where

import           Document.Classy.HasSpan.Class (HasSpan (..))
import           Prelude                       hiding (span)

instance {-# OVERLAPS #-} (HasSpan a) => HasSpan (Maybe a) where
  span f = \case
    Just a  -> Just <$> span f a
    Nothing -> pure Nothing
  {-# INLINE span #-}
