{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasSpan.PositionPair where

import           Data.Functor                  (($>))
import           Document.Classy.HasSpan.Class (HasSpan (..))
import           Document.Data.Position.Type   (Position)
import           Document.Data.Span.Type       (newSpan)

instance {-# OVERLAPS #-} HasSpan (Position, Position) where
  span f (s, e) = f (newSpan s e) $> (s, e)
  {-# INLINE span #-}
