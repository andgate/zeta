module Document.Classy.HasSpan.Lens where

import           Prelude                       hiding (span)

import           Control.Lens.Traversal        (Traversal')
import           Document.Classy.HasSpan.Class (HasSpan (..))
import           Document.Data.Position.Type   (Position)
import           Document.Data.Span.Lens       (_end, _start)

-- | "Control.Lens.Traversal" to the start 'Position' of a 'Span'-carrier.
start :: HasSpan a => Traversal' a Position
start = span . _start
{-# INLINE start #-}

-- | "Control.Lens.Traversal" to the end 'Position' of a 'Span'-carrier.
end :: HasSpan a => Traversal' a Position
end = span . _end
{-# INLINE end #-}
