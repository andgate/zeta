{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasSpan.Span where

import           Document.Classy.HasSpan.Class (HasSpan (..))
import           Document.Data.Span.Type       (Span)

instance HasSpan Span where
  span = id
  {-# INLINE span #-}
