{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns    #-}
module Document.Classy.HasSpan.Patterns where

import           Prelude                       hiding (span)

import           Control.Lens.Operators        ((^?!))
import           Document.Classy.HasSpan.Class (HasSpan (..))
import           Document.Classy.HasSpan.Lens  (end, start)
import           Document.Data.Position.Type   (Position)
import           Document.Data.Span.Patterns
import           Document.Data.Span.Type       (Span (..))

pattern Span' :: (HasSpan a) => Position -> Position -> a
pattern Span' s e <- ((^?! span) -> Span s e)
{-# COMPLETE Span' :: Span #-}

pattern Start' :: (HasSpan a) => Position -> a
pattern Start' s <- ((^?! start) -> s)
{-# COMPLETE Start' :: Span #-}

pattern End' :: (HasSpan a) => Position -> a
pattern End' e <- ((^?! end) -> e)
{-# COMPLETE End' :: Span #-}
