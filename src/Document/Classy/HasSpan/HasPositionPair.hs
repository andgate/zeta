{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasSpan.HasPositionPair where

import           Prelude                           hiding (span)

import           Control.Lens.Operators            ((^?))
import           Data.Foldable                     (traverse_)
import           Data.Functor                      (($>))
import           Document.Classy.HasPosition.Class (HasPosition (..))
import           Document.Classy.HasSpan.Class     (HasSpan (..))
import           Document.Data.Span                (newSpan)

instance {-# OVERLAPPABLE#-} (HasPosition a, HasPosition b) => HasSpan (a, b) where
  span f (a, b) = traverseTempSpan $> (a, b)
    where
      tmp_sp = newSpan <$> (a ^? position) <*> (b ^? position)
      traverseTempSpan = traverse_ f tmp_sp
  {-# INLINABLE span #-}
