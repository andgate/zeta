module Document.Classy.HasPosition.Lens where

import           Control.Lens.Traversal            (Traversal')
import           Document.Classy.HasPosition.Class (HasPosition (..))
import           Document.Data.Position.Lens       (_column, _line)
import           Numeric.Natural                   (Natural)

-- | "Control.Lens.Traversal" for the lines of a position-carrier.
line :: HasPosition a => Traversal' a Natural
line = position . _line
{-# INLINE line #-}

-- | "Control.Lens.Traversal" for the columns of a position-carrier.
column :: HasPosition p => Traversal' p Natural
column = position . _column
{-# INLINE column #-}
