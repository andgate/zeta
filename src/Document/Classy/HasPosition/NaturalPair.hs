{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasPosition.NaturalPair where

import           Control.Lens.Lens                 (lens)
import           Document.Classy.HasPosition.Class (HasPosition (..))
import           Document.Data.Position.Patterns
import           Document.Data.Position.Type       (newPosition)
import           Numeric.Natural                   (Natural)

instance HasPosition (Natural, Natural) where
  position = lens (uncurry newPosition) (\_ (Position l c) -> (l, c))
  {-# INLINE position #-}
