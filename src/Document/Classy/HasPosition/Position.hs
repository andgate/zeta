{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasPosition.Position where

import           Document.Classy.HasPosition.Class (HasPosition (..))
import           Document.Data.Position.Type       (Position)

instance HasPosition Position where
  position = id
  {-# INLINE position #-}
