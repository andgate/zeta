{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Classy.HasPosition.V2 where

import           Control.Lens.Lens                 (lens)
import           Document.Classy.HasPosition.Class (HasPosition (..))
import           Document.Data.Position.Type       (Position (..), getPositionV)
import           Linear.V2                         (V2)
import           Numeric.Natural                   (Natural)

instance HasPosition (V2 Natural) where
  position = lens P (\_ -> getPositionV)
  {-# INLINE position #-}
