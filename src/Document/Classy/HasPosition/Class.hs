module Document.Classy.HasPosition.Class where

import           Control.Lens.Traversal      (Traversal')
import           Document.Data.Position.Type (Position)

-- | Class for types which are 'Position'-carriers.
class HasPosition a where
  -- | "Control.Lens.Traversal" to a 'Position' in some value of type /a/.
  position :: Traversal' a Position
