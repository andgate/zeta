{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns    #-}
module Document.Classy.HasPosition.Patterns where

import           Control.Lens.Operators            ((^?!))
import           Document.Classy.HasPosition.Class (HasPosition (..))
import           Document.Classy.HasPosition.Lens  (column, line)
import           Document.Data.Position.Patterns
import           Document.Data.Position.Type       (Position)
import           Numeric.Natural                   (Natural)

pattern Position' :: (HasPosition a) => Natural -> Natural -> a
pattern Position' l c <- ((^?! position) -> Position l c)
{-# COMPLETE Position' :: Position #-}

pattern Line' :: (HasPosition a) => Natural -> a
pattern Line' l <- ((^?! line) -> l)
{-# COMPLETE Line' :: Position #-}

pattern Column' :: (HasPosition a) => Natural -> a
pattern Column' c <- ((^?! column) -> c)
{-# COMPLETE Column' :: Position #-}
