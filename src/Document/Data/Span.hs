{-|
Module      : Document.Data.Span
Description : Document span
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX

A document 'Span' is a start/end 'Document.Data.Position#Position' pair.
This locationally represents a slice of text in a document string.
-}
module Document.Data.Span (
    -- * Span Type
    module Type,

    -- * Span Lenses
    module Lens,

    -- * Span Patterns
    module Patterns,

    -- * Span Extras
    module Extra
  ) where

import           Document.Data.Span.Eq        ()
import           Document.Data.Span.Extra     as Extra
import           Document.Data.Span.Lens      as Lens
import           Document.Data.Span.Ord       ()
import           Document.Data.Span.Patterns  as Patterns
import           Document.Data.Span.Pretty    ()
import           Document.Data.Span.Semigroup ()
import           Document.Data.Span.Type      as Type
