{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Location.Semigroup where

import           Document.Data.Location.Type  (Loc (..))
import           Document.Data.Span.Semigroup ()

instance Semigroup Loc where
  (<>) (L fp s1) (L _ s2) =
    L fp (s1 <> s2)
