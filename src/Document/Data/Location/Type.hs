module Document.Data.Location.Type where

import           Document.Data.Span.Eq   ()
import           Document.Data.Span.Type (Span, containsSpan)

-- | A document location is 'FilePath' paired with a 'Span'.
data Loc =
  L -- ^ Constructor for source locations.
  { -- | Get the filepath of a source location.
    getLocFilePath :: FilePath
    -- | Get the document span of a source location.
  , getLocSpan     :: Span
  } deriving (Eq, Show)

-- | Construct a new source location.
newLocation
  :: FilePath -- ^ Source location filepath.
  -> Span     -- ^ Source location document span.
  -> Loc      -- ^ A new source location.
newLocation = L

-- | Set the filepath of a source location.
setLocFilePath :: Loc -> FilePath -> Loc
setLocFilePath l fp = l { getLocFilePath = fp }

-- | Set the document span of a source location.
setLocSpan :: Loc -> Span -> Loc
setLocSpan l sp = l { getLocSpan = sp }

-- | Test if a 'Span' is contained by the source location.
doesLocContainSpan
  :: Loc  -- ^ Source location to use as container.
  -> Span -- ^ 'Span' to test for containment.
  -> Bool -- ^ 'True' if the span was contained by the source location, otherwise 'False'.
doesLocContainSpan l =
  containsSpan (getLocSpan l)
