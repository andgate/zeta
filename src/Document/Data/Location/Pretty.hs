{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Location.Pretty where

import           Prettyprinter
import           Document.Data.Location.Type (Loc (..))
import           Document.Data.Span.Pretty   ()

instance Pretty Loc where
  pretty (L fp s) =
    pretty fp <> ":" <> pretty s
