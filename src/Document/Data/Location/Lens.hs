module Document.Data.Location.Lens where

import Control.Lens.Lens (Lens', lens)
import Document.Data.Location.Type (Loc (..), setLocFilePath, setLocSpan)
import Document.Data.Span.Type (Span)

_locFilePath :: Lens' Loc FilePath
_locFilePath = lens getLocFilePath setLocFilePath

_locSpan :: Lens' Loc Span
_locSpan = lens getLocSpan setLocSpan
