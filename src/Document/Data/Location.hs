{-|
Module      : Document.Data.Location
Description : Document location
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX

A document 'Loc' pairs 'FilePath' with "Document.Data.Span#Span", in order to represent
the complete location information of some text section in a document.
-}
module Document.Data.Location (module X) where

import           Document.Data.Location.Lens      as X
import           Document.Data.Location.Pretty    ()
import           Document.Data.Location.Semigroup ()
import           Document.Data.Location.Type      as X
