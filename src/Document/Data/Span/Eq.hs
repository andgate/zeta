{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Span.Eq where

import           Document.Data.Span.Type (Span (..))

instance Eq Span where
  (S m) == (S n) = m == n
  {-# INLINE (==) #-}
