{-# LANGUAGE ViewPatterns #-}
module Document.Data.Span.Type where

import           Document.Data.Position.Ord  ()
import           Document.Data.Position.Type (Position (..))
import           Linear.V2                   (V2 (..))
import           Linear.V4                   (V4 (..))
import           Numeric.Natural             (Natural)

-- | Span of text in a document, represented by a start/end 'Position' pair.
-- Internally, Span is implemented as newtype wrapper around a 'V4' of 'Natural's.
newtype Span = S (V4 Natural)
  deriving (Read, Show)

-- | The initial span, 1:1-1:1.
-- >>> initialSpan
-- S (V4 1 1 1 1)
initialSpan :: Span
initialSpan = S (V4 1 1 1 1)
{-# INLINE initialSpan #-}

-- | Construct a new 'Span' from a start/end 'Position' pair.
-- >>> newSpan (P (V2 1 2)) (P (V2 3 4))
-- S (V4 1 2 3 4)
newSpan :: Position -> Position -> Span
newSpan (P (V2 l1 c1)) (P (V2 l2 c2)) = S (V4 l1 c1 l2 c2)
{-# INLINE newSpan #-}

newSpan' :: Natural -> Natural -> Natural -> Natural -> Span
newSpan' l1 c1 l2 c2 = S (V4 l1 c1 l2 c2)
{-# INLINE newSpan' #-}

-- | Get the start/end 'Position' pair from a 'Span'.
getSpan :: Span -> (Position, Position)
getSpan s = (getStart s, getEnd s)
{-# INLINE getSpan #-}

-- | Get the internal start/end 'V4' from a 'Span'.
getSpanV :: Span -> V4 Natural
getSpanV (S m) = m
{-# INLINE getSpanV #-}

-- | Set the internal start/end 'V4' for a 'Span'.
-- >>> setSpanV (S (V4 1 2 3 4)) (V4 5 6 7 8)
-- S (V4 5 6 7 8)
setSpanV :: Span -> V4 Natural -> Span
setSpanV _ = S
{-# INLINE setSpanV #-}

-- | Get the start 'Position' of a 'Span'.
-- >>> getStart (S (V4 1 2 3 4))
-- P (V2 1 2)
getStart :: Span -> Position
getStart (S (V4 x y _ _)) = P (V2 x y)
{-# INLINE getStart #-}

-- | Set the start 'Position' of a 'Span'.
-- >>> setStart (S (V4 1 2 3 4)) (P (V2 5 6))
-- S (V4 5 6 3 4)
setStart :: Span -> Position -> Span
setStart (S (V4 _ _ z w)) (P (V2 x y)) = S (V4 x y z w)
{-# INLINE setStart #-}

-- | Get the end 'Position' of a 'Span'.
-- >>> getEnd (S (V4 1 2 3 4))
-- P (V2 3 4)
getEnd :: Span -> Position
getEnd (S (V4 _ _ z w)) = P (V2 z w)
{-# INLINE getEnd #-}

-- | Set the end 'Position' of a 'Span'.
-- >>> setEnd (S (V4 1 2 3 4)) (P (V2 5 6))
-- S (V4 1 2 5 6)
setEnd :: Span -> Position -> Span
setEnd (S (V4 x y _ _)) (P (V2 z w)) = S (V4 x y z w)
{-# INLINE setEnd #-}

-- | Test a 'Span' to see if a given position lies between its end points.
containsPosition
  :: Span     -- ^ The container span
  -> Position -- ^ The bounded position to check
  -> Bool     -- ^ True is the position is within the span, otherwise False.
containsPosition (getSpan -> (s, e)) p =
  minimum [s, e] <= p && p <= maximum [s, e]
{-# INLINE containsPosition #-}

-- | Test a 'Span' to see if it contains a given span.
containsSpan
  :: Span  -- ^ The container span
  -> Span  -- ^ The bounded span
  -> Bool  -- ^ True if the bounded span is inclusively within the container, otherwise False.
containsSpan l (getSpan -> (s, e)) =
  containsPosition l s && containsPosition l e
{-# INLINE containsSpan #-}
