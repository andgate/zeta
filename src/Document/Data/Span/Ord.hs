{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Span.Ord where

import           Document.Data.Position.Ord  ()
import           Document.Data.Span.Eq       ()
import           Document.Data.Span.Patterns
import           Document.Data.Span.Type     (Span)

instance Ord Span where
  (Start s1) <= (Start s2) = s1 <= s2
  {-# INLINE (<=) #-}
