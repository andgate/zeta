{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Span.Pretty where

import           Prettyprinter
import           Document.Data.Position.Eq     ()
import           Document.Data.Position.Pretty ()
import           Document.Data.Span.Patterns
import           Document.Data.Span.Type       (Span)

instance Pretty Span where
  pretty (Span s e)
    | s == e = pretty s
    | otherwise = pretty s <> "-" <> pretty e
