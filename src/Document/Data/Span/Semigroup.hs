{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Span.Semigroup where

import           Document.Data.Span.Ord      ()
import           Document.Data.Span.Patterns
import           Document.Data.Span.Type     (Span, newSpan)

instance Semigroup Span where
  (<>) (Span s1 e1) (Span s2 e2) =
    newSpan (minimum [s1, s2, e1, e2]) (maximum [s1, s2, e1, e2])
