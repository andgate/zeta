{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns    #-}
module Document.Data.Span.Patterns where

import           Document.Data.Position.Type (Position (..))
import           Document.Data.Span.Type     (Span (..), getEnd, getSpan,
                                              getStart)
import           Linear.V2                   (V2 (..))
import           Linear.V4                   (V4 (..))

-- | Matches on the start/end 'Position's in a 'Span',
-- avoiding the span's internal 'V4' vector. 
pattern Span :: Position -> Position -> Span
pattern Span s e <- (getSpan -> (s, e)) where
  Span (P (V2 l1 c1)) (P (V2 l2 c2)) = S (V4 l1 c1 l2 c2)
{-# COMPLETE Span #-}

-- | Matches on the start 'Position' of a 'Span'.
pattern Start :: Position -> Span
pattern Start s <- (getStart -> s)
{-# COMPLETE Start #-}

-- | Matches on the end 'Position' of a 'Span'.
pattern End :: Position -> Span
pattern End e <- (getEnd -> e)
{-# COMPLETE End #-}
