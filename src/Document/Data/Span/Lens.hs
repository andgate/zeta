module Document.Data.Span.Lens where

import           Control.Lens.Lens           (Lens', lens)
import           Document.Data.Position.Type (Position)
import           Document.Data.Span.Type     (Span, getEnd, getStart, setEnd,
                                              setStart)

-- | "Control.Lens.Lens" for the start 'Position' of a Span
_start :: Lens' Span Position
_start = lens getStart setStart
{-# INLINE _start #-}

-- | "Control.Lens.Lens" for the end 'Position' of a Span
_end :: Lens' Span Position
_end = lens getEnd setEnd
{-# INLINE _end #-}
