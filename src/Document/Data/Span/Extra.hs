module Document.Data.Span.Extra where

import           Document.Data.Position.Extra    (movePositionColumn,
                                                  toNextLineStartPosition)
import           Document.Data.Position.Patterns
import           Document.Data.Position.Type     (Position, newPosition)
import           Document.Data.Span.Patterns
import           Document.Data.Span.Type         (Span, newSpan)
import           Numeric.Natural                 (Natural)

-- | /Stretches/ a 'Position' into a 'Span' by some 'Natural' offset.
stretch :: Position -> Natural -> Span
stretch p1@(Position l c) n =
  let p2 = newPosition l (c + n)
  in newSpan p1 p2
{-# INLINE stretch #-}

-- | Move a given 'Span' by some 'Natural' offset.
moveSpan :: Span -> Natural -> Span
moveSpan (End p2) d =
  newSpan p2 (movePositionColumn p2 d)
{-# INLINE moveSpan #-}

-- | Grow a given 'Span' rightward by some 'Natural' amount.
growSpan :: Span -> Natural -> Span
growSpan (Span p1 p2) d =
  newSpan p1 (movePositionColumn p2 d)
{-# INLINE growSpan #-}

-- | Shift the start/end 'Position's of a 'Span' downward by one line.
startNextLineSpan :: Span -> Span
startNextLineSpan (Span p1 p2) =
  newSpan (toNextLineStartPosition p1) (toNextLineStartPosition p2)
{-# INLINE startNextLineSpan #-}

-- | Grow a given 'Span' downward by some 'Natural' amount.
growLineSpan :: Span -> Span
growLineSpan (Span p1 p2) =
  newSpan p1 (toNextLineStartPosition p2)
{-# INLINE growLineSpan #-}
