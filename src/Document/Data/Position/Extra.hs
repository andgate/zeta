module Document.Data.Position.Extra where

import           Control.Lens.Operators      ((&), (+~), (.~))
import           Document.Data.Position.Lens (_column, _line)
import           Document.Data.Position.Type (Position)
import           Numeric.Natural             (Natural)

-- | Move a 'Position's line downward, by some 'Natural' offset.
movePositionLine :: Position -> Natural -> Position
movePositionLine p l = p & _line +~ l
{-# INLINE movePositionLine #-}

-- | Move a 'Position's column rightward, by some 'Natural'. offset
movePositionColumn :: Position -> Natural -> Position
movePositionColumn p c = p & _column +~ c
{-# INLINE movePositionColumn #-}

-- | Move a 'Position' down by one line.
toNextLinePosition :: Position -> Position
toNextLinePosition p = movePositionLine p 1
{-# INLINE toNextLinePosition #-}

-- | Move a 'Position' to the start of a line (such that column=1).
toColumnStartPosition :: Position -> Position
toColumnStartPosition p = p & _column .~ 1
{-# INLINE toColumnStartPosition #-}

-- | Move a 'Position' to the start of the next line.
toNextLineStartPosition :: Position -> Position
toNextLineStartPosition = toColumnStartPosition . toNextLinePosition
{-# INLINE toNextLineStartPosition #-}
