{-# OPTIONS_HADDOCK show-extensions #-}
module Document.Data.Position.Type where

import           Linear.V2       (V2 (..))
import           Numeric.Natural (Natural)
import           Prelude         hiding (getLine)

{- | Cursor position in a document.
     This is represented by a pair of 'Natural's, line and column.
     'Position's are implemented as newtype wrappers around 'V2'.
-}
newtype Position = P (V2 Natural)
  deriving (Read, Show)

-- | The initial cursor position, 1:1.
-- >>> initialPosition
-- P (V2 1 1)
initialPosition :: Position
initialPosition = P (V2 1 1)
{-# INLINE initialPosition #-}

-- | Create a new 'Position' from two 'Natural's, which represent line/column respectively.
newPosition :: Natural -> Natural -> Position
newPosition l c = P (V2 l c)
{-# INLINE newPosition #-}

-- | Get 'Natural' pair representing the line/column of a given 'Position'.
getPosition :: Position -> (Natural, Natural)
getPosition (P (V2 l c)) = (l, c)
{-# INLINE getPosition #-}

-- | Get 'V2' representing a given 'Position'.
getPositionV :: Position -> V2 Natural
getPositionV (P v) = v
{-# INLINE getPositionV #-}

-- | Set the 'V2' representing a given 'Position'.
setPositionV :: Position -> V2 Natural -> Position
setPositionV _ = P
{-# INLINE setPositionV #-}

-- | Get a 'Natural' representing the line of a given 'Position'.
getLine :: Position -> Natural
getLine (P (V2 l _)) = l
{-# INLINE getLine #-}

-- | Set the 'Natural' representing the line of a given 'Position'.
setLine :: Position -> Natural -> Position
setLine (P (V2 _ c)) l = P (V2 l c)
{-# INLINE setLine #-}

-- | Get a 'Natural' representing the column of a given 'Position'.
getColumn :: Position -> Natural
getColumn (P (V2 _ c)) = c
{-# INLINE getColumn #-}

-- | Set the 'Natural' representing the column of a given 'Position'.
setColumn :: Position -> Natural -> Position
setColumn (P (V2 l _)) c = P (V2 l c)
{-# INLINE setColumn #-}
