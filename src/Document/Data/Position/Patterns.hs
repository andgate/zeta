{-# LANGUAGE PatternSynonyms #-}
module Document.Data.Position.Patterns where

import           Document.Data.Position.Type (Position (..))
import           Linear.V2                   (V2 (..))
import           Numeric.Natural             (Natural)

-- | Matches on the line/column 'Natural's in a 'Position',
-- avoiding the position's internal 'V2' vector. 
pattern Position :: Natural -> Natural -> Position
pattern Position l c = P (V2 l c)
{-# COMPLETE Position :: Position #-}

-- | Matches on the line 'Natural' of a 'Position'.
pattern Line :: Natural -> Position
pattern Line l <- P (V2 l _)
{-# COMPLETE Line :: Position #-}

-- | Matches on the column 'Natural' of a 'Position'.
pattern Column :: Natural -> Position
pattern Column c <- P (V2 _ c)
{-# COMPLETE Column :: Position #-}
