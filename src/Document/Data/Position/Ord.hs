{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Position.Ord where

import           Document.Data.Position.Eq       ()
import           Document.Data.Position.Patterns
import           Document.Data.Position.Type     (Position)
import           Numeric.Natural                 ()

instance Ord Position where
  (Position l1 c1) <= (Position l2 c2)
    | l1 == l2  = c1 <= c2
    | otherwise = l1 < l2
  {-# INLINE (<=) #-}
