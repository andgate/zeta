{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Position.Eq where

import           Document.Data.Position.Type (Position (..))
import           Linear.V2                   ()

instance Eq Position where
  (P v) == (P u) = v == u
  {-# INLINE (==) #-}
