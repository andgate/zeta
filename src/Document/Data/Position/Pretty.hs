{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Position.Pretty where

import           Document.Data.Position.Patterns
import           Document.Data.Position.Type     (Position)
import           Prettyprinter

instance Pretty Position where
  pretty (Position l c) =
    pretty l <> ":" <> pretty c
  {-# INLINE pretty #-}
