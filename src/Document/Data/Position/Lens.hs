module Document.Data.Position.Lens where

import           Control.Lens.Lens           (Lens', lens)
import           Document.Data.Position.Type (Position, getColumn, getLine,
                                              getPosition, newPosition,
                                              setColumn, setLine)
import           Numeric.Natural             (Natural)
import           Prelude                     hiding (getLine)

-- | "Control.Lens.Lens" for the line/column pair of a 'Position'
_position :: Lens' Position (Natural, Natural)
_position = lens getPosition (\_ -> uncurry newPosition)
{-# INLINE _position #-}

-- | "Control.Lens.Lens" for the line of a 'Position'
_line :: Lens' Position Natural
_line = lens getLine setLine
{-# INLINE _line #-}

-- | "Control.Lens.Lens" for the column of a 'Position'
_column :: Lens' Position Natural
_column = lens getColumn setColumn
{-# INLINE _column #-}
