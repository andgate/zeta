{-|
Module      : Document.Data.Position
Description : Document cursor position
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX

A document 'Position' represents a cursor position by line and column.
-}
module Document.Data.Position (
    -- * Position Type
    module Type,

    -- * Position Lenses
    module Lens,

    -- * Position Patterns
    module Patterns,

    -- * Position Extras
    module Extra
  ) where

import           Document.Data.Position.Eq       ()
import           Document.Data.Position.Extra    as Extra
import           Document.Data.Position.Lens     as Lens
import           Document.Data.Position.Ord      ()
import           Document.Data.Position.Patterns as Patterns
import           Document.Data.Position.Pretty   ()
import           Document.Data.Position.Type     as Type
