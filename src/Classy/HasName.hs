{-|
Module      : Classy.HasName
Description : Classy lens for name carriers.
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Classy.HasName where

import           Control.Lens.Lens (Lens')
import           Data.Text         (Text)

-- | Class of types with objects that carry names.
class HasName a where
  -- | Lens into the name of some object.
  _name :: Lens' a Text
