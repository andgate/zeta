{-|
Module      : Classy.HasGeneration
Description : Classy lens for generation carriers.
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Classy.HasGeneration where

import           Control.Lens.Lens (Lens')

-- | Class of types with objects that carry a generation index.
class HasGeneration a where
  -- | Lens into the generation index of some object.
  generation :: Lens' a Integer
