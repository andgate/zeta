{
{-# LANGUAGE   OverloadedStrings
             , TupleSections
             , FlexibleContexts
             , GeneralizedNewtypeDeriving
  #-}

module Language.Zeta.Lex (lex, lexUnsafe, LexError) where

import           Prelude                      hiding (lex)

import           Control.Monad.Except
import           Data.Bits                    (shiftR, (.&.))
import           Data.Char                    (ord)
import           Data.Text                    (Text)
import qualified Data.Text                    as T
import           Data.Word                    (Word8)
import           Language.Zeta.Document.Token (Token, TokenClass (..))
import           Language.Zeta.Lex.Error
import           Language.Zeta.Lex.Helpers
import           Language.Zeta.Lex.LexAction
import           Language.Zeta.Lex.Monad
import           Safe                         (headDef)
import           Prettyprinter

}

-- -----------------------------------------------------------------------------
-- Alex "Character set macros"

$digit = 0-9


$opchar = [\!\#\$\%\&\*\+\/\<\=\>\?\@\\\^\|\-\~\:]

$small        = [a-z]
$large        = [A-Z]
$alphachar    = [A-za-z]
$idchar       = [A-Za-z0-9]
$idcharsym    = [A-Za-z0-9\_\']

$nonwhite       = ~$white
$whiteNoNewline = $white # \n


-- -----------------------------------------------------------------------------
-- Alex "Regular expression macros"

-- Basic Ids
@varid = $alphachar $idcharsym*
@primid = \# $small $idcharsym*

@qual = (@varid \.)+
@qvarid = @qual @varid

-- -----------------------------------------------------------------------------
-- Alex "Identifier"

zeta :-

-- -----------------------------------------------------------------------------
-- Alex "Rules"

<0> {
  -- Skip whitespace everywhere
  $whiteNoNewline                 { skipBreak }
  [\n\r]                          { \ _ _ -> nextLineBreak }
  "/*"                            { beginComment commentSC }
  "//"                            { beginLineComment lineCommentSC }
  
  \"                              { beginString stringSC }
  \' .* \'                        { handleChar }
  

  \\                              { rsvp }
  \-\>                            { rsvp }
  \<\-                            { rsvp }
  \:                              { rsvp }
  \;                              { rsvp }
  \,                              { rsvp }
  \.                              { rsvp }
  \=                              { rsvp }
  \_                              { rsvp }
  \*                              { rsvp }
  \&                              { rsvp }
  \~                              { rsvp }
  \|                              { rsvp }

  \(                              { rsvp }
  \)                              { rsvp }
  \[                              { rsvp }
  \]                              { rsvp }
  \{                              { rsvp }
  \}                              { rsvp }
  \<                              { rsvp }
  \>                              { rsvp }

  "forall"                        { rsvp }
  
  "i1"                            { rsvp }
  "i8"                            { rsvp }
  "i16"                           { rsvp }
  "i32"                           { rsvp }
  "i64"                           { rsvp }

  "u8"                            { rsvp }
  "u16"                           { rsvp }
  "u32"                           { rsvp }
  "u64"                           { rsvp }

  "f16"                           { rsvp } 
  "f32"                           { rsvp }
  "f64"                           { rsvp }
  "f128"                          { rsvp }

  "null"                          { rsvp }

  "module"                        { rsvp }
  "import"                        { rsvp }

  "extern"                        { rsvp }
  "data"                          { rsvp }

  "infix"                         { rsvp }
  "infixl"                        { rsvp }
  "infixr"                        { rsvp }

  "return"                        { rsvp }
  "break"                         { rsvp }
  "continue"                      { rsvp }

  "as"                            { rsvp }
  "case"                          { rsvp }

  "if"                            { rsvp }
  "else"                          { rsvp }
  "elif"                          { rsvp }

  "while"                         { rsvp }
  "do"                            { rsvp }
  "for"                           { rsvp }

  "free"                           { rsvp }

  "True"                          { \_ len -> yieldTokAt (TokenBool True) len }
  "False"                         { \_ len -> yieldTokAt (TokenBool False) len }

  @primid                         { \text len -> yieldTokAt (TokenPrimId text) len }
  @varid                          { \text len -> yieldTokAt (TokenVarId text) len }

  $digit* \. $digit+              { \text len -> yieldTokAt (TokenDouble $ readDbl text) len }
  $digit+ \. $digit*              { \text len -> yieldTokAt (TokenDouble $ readDbl text) len }
  
  ($digit)+                       { \text len -> yieldTokAt (TokenInteger $ readInt text) len }
}

<stringSC> {
  \\[nt\"]                        { escapeString }
  \"                              { endString }
  [.]                             { appendString }
}

<commentSC> {
  "/*"                            { continueComment }
  "*/"                            { endComment commentSC }
  [\n\r]                          { \_ _ -> nextLineContinue }
  [.]                             { skipContinue }
}

<lineCommentSC> {
  [\n\r]                          { endLineComment }
  [.]                             { skipContinue }
}

{

-- This was lifted almost intact from the @alex@ source code
encode :: Char -> (Word8, [Word8])
encode c = (fromIntegral h, map fromIntegral t)
  where
    (h, t) = go (ord c)
    go n
        | n <= 0x7f   = (n, [])
        | n <= 0x7ff  = (0xc0 + (n `shiftR` 6), [0x80 + n .&. 0x3f])
        | n <= 0xffff =
            (   0xe0 + (n `shiftR` 12)
            ,   [   0x80 + ((n `shiftR` 6) .&. 0x3f)
                ,   0x80 + n .&. 0x3f
                ]
            )
        | otherwise   =
            (   0xf0 + (n `shiftR` 18)
            ,   [   0x80 + ((n `shiftR` 12) .&. 0x3f)
                ,   0x80 + ((n `shiftR` 6) .&. 0x3f)
                ,   0x80 + n .&. 0x3f
                ]
            )


{- @alex@ does not provide a `Text` wrapper, so the following code just modifies
   the code from their @basic@ wrapper to work with `Text`

   I could not get the @basic-bytestring@ wrapper to work; it does not correctly
   recognize Unicode regular expressions.
-}
data AlexInput = AlexInput
  { prevChar  :: Char
  , currBytes :: [Word8]
  , currInput :: Text
  }

alexGetByte :: AlexInput -> Maybe (Word8, AlexInput)
alexGetByte (AlexInput c bytes text) = case bytes of
  b:ytes -> Just (b, AlexInput c ytes text)
  []     -> case T.uncons text of
    Nothing       -> Nothing
    Just (t, ext) -> case encode t of
        (b, ytes) -> Just (b, AlexInput t ytes ext)

alexInputPrevChar :: AlexInput -> Char
alexInputPrevChar = prevChar

lex :: FilePath -> Text -> Except LexError [Token]
lex fp text = do
  runLexer fp start

  where
    start = go $ AlexInput '\n' [] text

    go input = do
      sc <- getLexStartcode
      case alexScan input sc of
        AlexEOF                         -> do
            yieldTaggedTok TokenEof
            reverse <$> getLexTokAcc

        AlexError (AlexInput p _ text') -> do
            l <- getLexLocation
            throwError $ UnrecognizedToken l (headDef (show p) $ words $ show text')

        AlexSkip  _ _           -> do
            l <- getLexLocation
            throwError $ IllegalLexerSkip l

        AlexToken input' len act       -> do
            act (T.take (fromIntegral len) (currInput input)) (fromIntegral len)
            go input'

lexUnsafe :: Text -> [Token]
lexUnsafe src =
  case runExcept (lex "" src) of
    Left errs   -> error . show . pretty $ errs
    Right etoks -> etoks

}
