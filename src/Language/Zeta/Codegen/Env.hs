{-# LANGUAGE OverloadedStrings #-}
module Language.Zeta.Codegen.Env where

import           Data.List              (foldl')
import           Data.Map.Strict        (Map)
import qualified Data.Map.Strict        as Map
import           Document.Data.Location (Loc)
import           LLVM.AST               (Operand, Type)
import           Prettyprinter          (Pretty (..))
import qualified Prettyprinter          as PP

data Env = Env { envTypes    :: Map String Type
               , envFuncs    :: Map String Operand
               , envLocals   :: Map String Operand
               , envSizes    :: Map String Int
               , envTypeDefs :: Map String Type
               , envConstrs  :: Map String (String, Type, Type, Int, [Type])
               , envMembers  :: Map String (Type, Int)
               , envLoc      :: Loc
               }

mkEnv :: Loc -> Env
mkEnv l = Env { envTypes = mempty
               , envFuncs = mempty
               , envLocals = mempty
               , envSizes = mempty
               , envTypeDefs = mempty
               , envConstrs = mempty
               , envMembers = mempty
               , envLoc = l
               }

envGetLoc :: Env -> Loc
envGetLoc = envLoc

envInsertTypes :: [(String, Type)] -> Env -> Env
envInsertTypes types env = foldl' go env types
  where go env' (n, ty) = envInsertType n ty env'

envInsertType :: String -> Type -> Env -> Env
envInsertType n ty env = env { envTypes = Map.insert n ty (envTypes env) }

envLookupType :: String -> Env -> Maybe Type
envLookupType n env = Map.lookup n (envTypes env)

envSetLoc :: Loc -> Env -> Env
envSetLoc l env = env { envLoc = l }

envInsertLocals :: [(String, Operand)] -> Env -> Env
envInsertLocals locals env = foldl' go env locals
  where go env' (n, op) = envInsertLocal n op env'

envInsertLocal :: String -> Operand -> Env -> Env
envInsertLocal n op env = env { envLocals = Map.insert n op (envLocals env) }

envLookupLocal :: String -> Env -> Maybe Operand
envLookupLocal n env = Map.lookup n (envLocals env)


envInsertFunc :: String -> Operand -> Env -> Env
envInsertFunc n op env = env { envFuncs = Map.insert n op (envFuncs env) }

envLookupFunc :: String -> Env -> Operand
envLookupFunc n env =
  case Map.lookup n (envFuncs env) of
    Just op -> op
    Nothing ->
      error $ show $ PP.vsep [ PP.line <> pretty (envLoc env) PP.<+> "error:"
                             , PP.indent 4 $ "envLookupFunc - Couldn't find function:" PP.<+> pretty n
                             , PP.line
                             ]


envInsertSize :: String -> Int -> Env -> Env
envInsertSize n s env = env { envSizes = Map.insert n s (envSizes env) }

envLookupSize :: String -> Env -> Maybe Int
envLookupSize n env = Map.lookup n (envSizes env)

envInsertTypeDef :: String -> Type -> Env -> Env
envInsertTypeDef n ty env = env { envTypeDefs = Map.insert n ty (envTypeDefs env) }

envLookupTypeDef :: String -> Env -> Maybe Type
envLookupTypeDef n env = Map.lookup n (envTypeDefs env)


envInsertConstrs :: [(String, Type, String, Type, [Type])] -> Env -> Env
envInsertConstrs ns env = foldl' go env (zip ns [0..])
  where go env' ((ty_n, ty, con_n, con_ty, ty_params), i)
          = envInsertConstr con_n ty_n ty con_ty i ty_params env'

envInsertConstr :: String -> String -> Type -> Type -> Int -> [Type] -> Env -> Env
envInsertConstr con_n ty_n ty con_ty i argty env
  = env { envConstrs = Map.insert con_n (ty_n, ty, con_ty, i, argty) (envConstrs env) }

envLookupConstr :: String -> Env -> Maybe (String, Type, Type, Int, [Type])
envLookupConstr con_n env = Map.lookup con_n (envConstrs env)

envInsertMembers :: Type -> [Maybe String] -> Env -> Env
envInsertMembers con_ty mem_ns env = foldl' go env (zip mem_ns [1..])
  where go env' (Just mem_n, i) = envInsertMember mem_n con_ty i env'
        go env' (Nothing, _)    = env'

envInsertMember :: String -> Type -> Int -> Env -> Env
envInsertMember mem_n con_ty i env
  = env { envMembers = Map.insert mem_n (con_ty, i) (envMembers env) }

envLookupMember :: String -> Env -> Maybe (Type, Int)
envLookupMember mem_n env = Map.lookup mem_n (envMembers env)
