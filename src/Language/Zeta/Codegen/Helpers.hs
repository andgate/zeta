module Language.Zeta.Codegen.Helpers where

import qualified Data.ByteString.Char8 as S8
import           Data.ByteString.Short (ShortByteString)
import qualified Data.ByteString.Short as SBS
import           LLVM.AST.Name         (Name (..))
import           LLVM.IRBuilder.Module (ParameterName (..))

str2Name :: String -> Name
str2Name = Name . str2sbs

str2Param :: String -> ParameterName
str2Param = ParameterName . str2sbs

str2sbs :: String -> ShortByteString
str2sbs = SBS.toShort . S8.pack
