{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Language.Zeta.Codegen.Classy.AsModule.Class where

import           Control.Monad.Fix         (MonadFix)
import           Control.Monad.State       (MonadState)
import           LLVM.IRBuilder.Module     (MonadModuleBuilder)
import           Language.Zeta.Codegen.Env (Env)

type MonadModuleGen m = (MonadFix m, MonadModuleBuilder m, MonadState Env m)

class AsModule a b where
  llvmModule :: (MonadModuleGen m) => a -> m b
