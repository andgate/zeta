{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Codegen.Classy.AsModule.Module where

import           Language.Zeta.Codegen.Classy.AsIR           ()
import           Language.Zeta.Codegen.Classy.AsModule.Class (AsModule (..))
import           Language.Zeta.Syntax                        (Module (..))

instance AsModule Module () where
  llvmModule = \case
    _ -> _llmodule
