module Language.Zeta.Codegen.Classy.AsModule (module X) where

import           Language.Zeta.Codegen.Classy.AsModule.Class  as X
import           Language.Zeta.Codegen.Classy.AsModule.Module ()
