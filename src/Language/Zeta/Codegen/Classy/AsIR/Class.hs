{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Language.Zeta.Codegen.Classy.AsIR.Class where

import           Control.Monad.Fix         (MonadFix)
import           Control.Monad.State       (MonadState)
import           LLVM.IRBuilder.Module     (MonadModuleBuilder)
import           LLVM.IRBuilder.Monad      (MonadIRBuilder)
import           Language.Zeta.Codegen.Env (Env)

type MonadIRGen m = (MonadFix m, MonadModuleBuilder m, MonadIRBuilder m, MonadState Env m)

class AsIR a b where
  llvmIR :: (MonadIRGen m) => a -> m b
