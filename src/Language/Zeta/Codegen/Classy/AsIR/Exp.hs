{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Codegen.Classy.AsIR.Exp where

import           Language.Zeta.Codegen.Classy.AsIR.Class (AsIR (..))
import           Language.Zeta.Syntax                    (Exp (..))

instance AsIR Exp () where
  llvmIR = \case
    _ -> _ir
