module Language.Zeta.Project.Type where

import           Data.Text (Text)

data Project = Project
  { projectName :: Text }
  deriving (Show)
