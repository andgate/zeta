{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
module Language.Zeta.Parse.Error where

import           Prettyprinter
import           Language.Zeta.Lex.Error
import           Language.Zeta.Document.Token

data ParseError
    = UnexpectedToken [Token] [String]
    | PLexErr LexError
    deriving(Show)

instance Pretty ParseError where
    pretty = \case
        UnexpectedToken unexpected expected ->
            vsep [ "Unexpected tokens:" <+> dquotes (pretty unexpected)
                 , "Expected tokens:" <+> dquotes (pretty expected)
                 ]

        PLexErr err ->
            pretty err
