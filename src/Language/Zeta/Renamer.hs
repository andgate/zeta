{-|
Module      : Language.Zeta.Renamer
Description : Syntax renamer for the Zeta programming language.
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Renamer (renameModule, renameExp, renameType) where

import           Prelude                            hiding (span)

import           Control.Lens.Operators             ((^?))
import           Document.Classy.HasSpan            (HasSpan (..))
import           Document.Data.Location             (Loc (..))
import           Language.Zeta.Compiler.NameSet     (NameSet)
import           Language.Zeta.Renamer.Monad        (runRenamer)
import           Language.Zeta.Renamer.Monad.Class  ()
import           Language.Zeta.Renamer.Renamable    (Renamable (..))
import           Language.Zeta.Renamer.RenamerEnv   (newRenamerEnv)
import           Language.Zeta.Renamer.RenamerError (RenamerErr)
import           Language.Zeta.Syntax               (Exp, Module, Type,
                                                     getModuleLocation)

-- | Rename a module.
renameModule :: NameSet -> Module -> Either [RenamerErr] Module
renameModule ns m = runRenamer (rename m) env
  where
    l = getModuleLocation m
    env = newRenamerEnv ns l

-- | Rename an expression.
renameExp :: FilePath -> NameSet -> Exp -> Either [RenamerErr] Exp
renameExp fp ns e = runRenamer (rename e) env
  where
    sp = e ^? span
    env = newRenamerEnv ns (L fp <$> sp)

-- | Rename a type.
renameType :: FilePath -> NameSet -> Type -> Either [RenamerErr] Type
renameType fp ns t = runRenamer (rename t) env
  where
    sp = t ^? span
    env = newRenamerEnv ns (L fp <$> sp)
