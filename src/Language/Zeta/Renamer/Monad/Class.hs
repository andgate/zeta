{-# LANGUAGE ConstraintKinds  #-}
{-# LANGUAGE FlexibleContexts #-}
module Language.Zeta.Renamer.Monad.Class where

import           Control.Lens.Fold                  (preview)
import           Control.Lens.Getter                (view, views)
import           Control.Lens.Operators             ((.~), (?~), (^.))
import           Control.Monad.Reader               (MonadReader (..))
import           Control.Monad.Report               (MonadReport (nonfatal))
import qualified Data.Set                           as Set
import           Document.Data.Location             (Loc)
import           Document.Data.Span                 (Span)
import           Language.Zeta.Compiler.NameSet     (NameSet, VarName (..),
                                                     lookupVar,
                                                     nameSetClassNames,
                                                     nameSetConstructorNames,
                                                     nameSetFieldNames,
                                                     nameSetTypeNames)
import           Language.Zeta.Renamer.RenamerEnv   (RenamerEnv (..),
                                                     envInsertNames,
                                                     renamerEnvFilePath,
                                                     renamerEnvLoc,
                                                     renamerEnvNames,
                                                     renamerEnvSpan)
import           Language.Zeta.Renamer.RenamerError (RenamerErr (..))
import           Language.Zeta.Syntax               (ClassName, ConstructorName,
                                                     Exp (..), FieldName, TVar,
                                                     Type (TCon, TVar),
                                                     TypeName, Var,
                                                     toConstructorName,
                                                     toFieldName, toTypeName)

-- | The Renamer Monad
type MonadRenamer m = (MonadReader RenamerEnv m, MonadReport RenamerErr m)

getNames :: MonadRenamer m => m NameSet
getNames = view renamerEnvNames

getLoc :: MonadRenamer m => m (Maybe Loc)
getLoc = view renamerEnvLoc

getFilePath :: MonadRenamer m => m FilePath
getFilePath = view renamerEnvFilePath

getSpan :: MonadRenamer m => m (Maybe Span)
getSpan = preview renamerEnvSpan

withNames :: MonadRenamer m => NameSet -> m a -> m a
withNames dict = local (`envInsertNames` dict)

withLoc :: MonadRenamer m => Loc -> m a -> m a
withLoc l = local (renamerEnvLoc ?~ l)

withLoc' :: MonadRenamer m => Maybe Loc -> m a -> m a
withLoc' (Just l) = withLoc l
withLoc' Nothing  = id

withSpan :: MonadRenamer m => Span -> m a -> m a
withSpan sp = local (renamerEnvSpan .~ sp)

withSpan' :: MonadRenamer m => Maybe Span -> m a -> m a
withSpan' (Just sp) = withSpan sp
withSpan' Nothing   = id

lookupConstructorName :: MonadRenamer m => Var -> m (Maybe ConstructorName)
lookupConstructorName v = do
  let n = v ^. toConstructorName
  ns <- view (renamerEnvNames . nameSetConstructorNames)
  return $
    if Set.member n ns then
      Just n
    else
      Nothing

lookupFieldName :: MonadRenamer m => Var -> m (Maybe FieldName)
lookupFieldName v = do
  let n = v ^. toFieldName
  ns <- view (renamerEnvNames . nameSetFieldNames)
  return $
    if Set.member n ns then
      Just n
    else
      Nothing

lookupTypeName :: MonadRenamer m => TVar -> m (Maybe TypeName)
lookupTypeName tv = do
  let n = tv ^. toTypeName
  ns <- view (renamerEnvNames . nameSetTypeNames)
  return $
    if Set.member n ns then
      Just n
    else
      Nothing

containsClassName :: MonadRenamer m => ClassName -> m Bool
containsClassName n = do
  ns <- view (renamerEnvNames . nameSetClassNames)
  return $ Set.member n ns

containsConstructorName :: MonadRenamer m => ConstructorName -> m Bool
containsConstructorName n = do
  ns <- view (renamerEnvNames . nameSetConstructorNames)
  return $ Set.member n ns

containsFieldName :: MonadRenamer m => FieldName -> m Bool
containsFieldName n = do
  ns <- view (renamerEnvNames . nameSetFieldNames)
  return $ Set.member n ns

containsTypeName :: MonadRenamer m => TypeName -> m Bool
containsTypeName n = do
  ns <- view (renamerEnvNames . nameSetTypeNames)
  return $ Set.member n ns

undefinedVar :: MonadRenamer m => Var -> m Var
undefinedVar v = do
  l <- getLoc
  let err = UndefinedVar l v
  nonfatal err v

undefinedTVar :: MonadRenamer m => TVar -> m TVar
undefinedTVar tv = do
  l <- getLoc
  let err = UndefinedTVar l tv
  nonfatal err tv

undefinedClassName :: MonadRenamer m => ClassName -> m ClassName
undefinedClassName n = do
  l <- getLoc
  let err = UndefinedClassName l n
  nonfatal err n

undefinedConstructorName :: MonadRenamer m => ConstructorName -> m ConstructorName
undefinedConstructorName n = do
  l <- getLoc
  let err = UndefinedConstructorName l n
  nonfatal err n

undefinedFieldName :: MonadRenamer m => FieldName -> m FieldName
undefinedFieldName n = do
  l <- getLoc
  let err = UndefinedFieldName l n
  nonfatal err n

undefinedTypeName :: MonadRenamer m => TypeName -> m TypeName
undefinedTypeName n = do
  l <- getLoc
  let err = UndefinedTypeName l n
  nonfatal err n

witnessVar :: MonadRenamer m => Var -> m Exp
witnessVar v = do
  may_n <- views renamerEnvNames (lookupVar v)
  case may_n of
    Just (VarConstructorName n) ->
      return $ ECon n
    Just (VarFieldName n) ->
      return $ EField n
    Nothing ->
      EVar <$> undefinedVar v

witnessTVar :: MonadRenamer m => TVar -> m Type
witnessTVar tv = do
  may_n <- lookupTypeName tv
  case may_n of
    Just n  -> return $ TCon n
    Nothing -> TVar <$> undefinedTVar tv

witnessClassName :: MonadRenamer m => ClassName -> m ClassName
witnessClassName n = do
  hasName <- containsClassName n
  if hasName then
    return n
  else
    undefinedClassName n

witnessConstructorName :: MonadRenamer m => ConstructorName -> m ConstructorName
witnessConstructorName n = do
  hasName <- containsConstructorName n
  if hasName then
    return n
  else
    undefinedConstructorName n

witnessFieldName :: MonadRenamer m => FieldName -> m FieldName
witnessFieldName n = do
  hasName <- containsFieldName n
  if hasName then
    return n
  else
    undefinedFieldName n

witnessTypeName :: MonadRenamer m => TypeName -> m TypeName
witnessTypeName n = do
  hasName <- containsTypeName n
  if hasName then
    return n
  else
    undefinedTypeName n
