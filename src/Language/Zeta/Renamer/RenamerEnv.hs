module Language.Zeta.Renamer.RenamerEnv where

import           Prelude                        hiding (span)

import           Control.Lens.Lens              (Lens', lens)
import           Control.Lens.Prism             (_Just)
import           Control.Lens.Traversal         (Traversal')
import           Document.Data.Location         (Loc, _locFilePath, _locSpan)
import           Document.Data.Span             (Span)
import           Language.Zeta.Compiler.NameSet (NameSet)

-- | Renamer Environment
data RenamerEnv = RenamerEnv
  { getRenamerEnvNames :: NameSet
  , getRenamerEnvLoc   :: Maybe Loc
  }

emptyRenamerEnv :: RenamerEnv
emptyRenamerEnv =
  RenamerEnv
  { getRenamerEnvNames = mempty
  , getRenamerEnvLoc   = mempty
  }

newRenamerEnv :: NameSet -> Maybe Loc -> RenamerEnv
newRenamerEnv namemap l =
  RenamerEnv
  { getRenamerEnvNames = namemap
  , getRenamerEnvLoc = l
  }

setRenamerEnvNames :: RenamerEnv -> NameSet -> RenamerEnv
setRenamerEnvNames env ns = env { getRenamerEnvNames = ns }

setRenamerEnvLoc :: RenamerEnv -> Maybe Loc -> RenamerEnv
setRenamerEnvLoc env l = env { getRenamerEnvLoc = l }

envInsertNames :: RenamerEnv -> NameSet -> RenamerEnv
envInsertNames env ns =
  setRenamerEnvNames env (getRenamerEnvNames env <> ns)

renamerEnvNames :: Lens' RenamerEnv NameSet
renamerEnvNames = lens getRenamerEnvNames setRenamerEnvNames

renamerEnvLoc :: Lens' RenamerEnv (Maybe Loc)
renamerEnvLoc = lens getRenamerEnvLoc setRenamerEnvLoc

renamerEnvFilePath :: Traversal' RenamerEnv FilePath
renamerEnvFilePath = renamerEnvLoc . _Just . _locFilePath

renamerEnvSpan :: Traversal' RenamerEnv Span
renamerEnvSpan = renamerEnvLoc . _Just . _locSpan

instance Semigroup RenamerEnv where
  (<>) env1 env2 =
    RenamerEnv
    { getRenamerEnvNames = getRenamerEnvNames env1 <> getRenamerEnvNames env2
    , getRenamerEnvLoc = getRenamerEnvLoc env1 <> getRenamerEnvLoc env2
    }

instance Monoid RenamerEnv where
  mempty = emptyRenamerEnv
