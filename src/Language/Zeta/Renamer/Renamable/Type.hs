{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.Type where

import           Language.Zeta.Renamer.Monad.Class           (withSpan,
                                                              witnessTVar,
                                                              witnessTypeName)
import           Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import           Language.Zeta.Renamer.Renamable.Traversable ()
import           Language.Zeta.Renamer.Renamable.TypeContext ()
import           Language.Zeta.Syntax                        (Type (..),
                                                              hasTVarBinder)

instance Renamable Type where
  rename ty = case ty of
    TVar tv
      | hasTVarBinder tv -> return ty
      | otherwise -> witnessTVar tv
    TFunc paramtys retty ->
      TFunc <$> rename paramtys <*> rename retty
    TApp t ts ->
      TApp <$> rename t <*> rename ts
    TCon n  -> TCon <$> witnessTypeName n
    TInt _  -> pure ty
    TUInt _ -> pure ty
    TFp _   -> pure ty
    TTuple t ne ->
      TTuple <$> rename t <*> rename ne
    TArray n t ->
      TArray n <$> rename t
    TPtr t ->
      TPtr <$> rename t
    TCtx ctx t ->
      TCtx <$> rename ctx <*> rename t
    TCache t tc ->
      TCache <$> rename t <*> pure tc
    TLoc t sp -> withSpan sp $
      TLoc <$> rename t <*> pure sp
    TParens t ->
      TParens <$> rename t
