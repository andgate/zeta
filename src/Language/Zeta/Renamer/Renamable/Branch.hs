{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE LambdaCase       #-}
module Language.Zeta.Renamer.Renamable.Branch where

import                          Language.Zeta.Renamer.Monad.Class           (withSpan')
import                          Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import {-# SOURCE #-}           Language.Zeta.Renamer.Renamable.Exp         ()
import                          Language.Zeta.Renamer.Renamable.Traversable ()
import                          Language.Zeta.Syntax                        (Branch (..))

instance Renamable Branch where
  rename = \case
    ElseBranch m_sp e ->
      withSpan' m_sp $
        ElseBranch m_sp <$> rename e
    ElifBranch m_sp e e' m_br ->
      withSpan' m_sp $
        ElifBranch m_sp <$> rename e <*> rename e' <*> rename m_br
