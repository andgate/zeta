{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.ImportStmt where

import           Language.Zeta.Renamer.Renamable.Class (Renamable (..))
import           Language.Zeta.Syntax                  (ImportStmt (..))

instance Renamable ImportStmt where
  rename = pure
