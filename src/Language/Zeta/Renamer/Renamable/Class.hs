{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
module Language.Zeta.Renamer.Renamable.Class where

import           Language.Zeta.Renamer.Monad.Class (MonadRenamer)

class Renamable s where
  rename :: forall m. MonadRenamer m => s -> m s
