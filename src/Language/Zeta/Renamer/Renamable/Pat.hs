{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.Pat where

import                          Language.Zeta.Renamer.Monad.Class           (withSpan,
                                                                             witnessConstructorName)
import                          Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import {-# SOURCE #-}           Language.Zeta.Renamer.Renamable.Exp         ()
import                          Language.Zeta.Renamer.Renamable.Traversable ()
import                          Language.Zeta.Renamer.Renamable.Type        ()
import                          Language.Zeta.Syntax                        (Pat (..))

instance Renamable Pat where
  rename pat = case pat of
    PVar _          -> pure pat
    PCon cn ps      -> PCon <$> witnessConstructorName cn <*> rename ps
    PTuple p ne     -> PTuple <$> rename p <*> rename ne
    PObj ob         -> PObj <$> rename ob
    PLabel pn p     -> PLabel pn <$> rename p
    PView e p       -> PView <$> rename e <*> rename p
    PWild           -> pure pat
    PType p ty      -> PType <$> rename p <*> rename ty
    PExpCache p ec  -> PExpCache <$> rename p <*> pure ec
    PTypeCache p tc -> PTypeCache <$> rename p <*> pure tc
    PLoc p sp       -> withSpan sp $ PLoc <$> rename p <*> pure sp
    PParens p       -> PParens <$> rename p
