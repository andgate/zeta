{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.Exp where

import           Language.Zeta.Renamer.Monad.Class           (withSpan,
                                                              witnessConstructorName,
                                                              witnessFieldName,
                                                              witnessVar)
import           Language.Zeta.Renamer.Renamable.Branch      ()
import           Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import           Language.Zeta.Renamer.Renamable.Clause      ()
import           Language.Zeta.Renamer.Renamable.Pat         ()
import           Language.Zeta.Renamer.Renamable.Traversable ()
import           Language.Zeta.Renamer.Renamable.Type        ()
import           Language.Zeta.Syntax                        (Exp (..),
                                                              hasVarBinder)

instance Renamable Exp where
  rename ex = case ex of
    EVar v
      | hasVarBinder v -> return ex
      | otherwise -> witnessVar v
    ELit _ -> pure ex
    ECall e es ->
      ECall <$> rename e <*> rename es
    ELam ps m_ty sc ->
      ELam <$> rename ps <*> rename m_ty <*> rename sc
    EType e ty ->
      EType <$> rename e <*> rename ty
    ECast e ty ->
      ECast <$> rename e <*> rename ty
    ECache e ec ->
      ECache <$> rename e <*> pure ec
    ETypeCache e tc ->
      ETypeCache <$> rename e <*> pure tc
    ELoc e sp -> withSpan sp $
      ELoc <$> rename e <*> pure sp
    EParens e ->
      EParens <$> rename e
    ERef e ->
      ERef <$> rename e
    EDeref e ->
      EDeref <$> rename e
    ETuple e ne ->
      ETuple <$> rename e <*> rename ne
    ECon cn ->
      ECon <$> witnessConstructorName cn
    EArray es ->
      EArray <$> rename es
    EObj ob ->
      EObj <$> rename ob
    EFree e ->
      EFree <$> rename e
    EField fn ->
      EField <$> witnessFieldName fn
    EGet e fn ->
      EGet <$> rename e <*> witnessFieldName fn
    EGetI e e' ->
      EGetI <$> rename e <*> rename e'
    ESet e e' ->
      ESet <$> rename e <*> rename e'
    EUnOp uo e ->
      EUnOp uo <$> rename e
    EBinOp bo e e' ->
      EBinOp bo <$> rename e <*> rename e'
    EBlock e ->
      EBlock <$> rename e
    ESeq e e' ->
      ESeq <$> rename e <*> rename e'
    EBind p e sc ->
      EBind <$> rename p <*> rename e <*> rename sc
    EIf e e' m_br ->
      EIf <$> rename e <*> rename e' <*> rename m_br
    ECase e cb ->
      ECase <$> rename e <*> rename cb
    EFor i c u e ->
      EFor <$> rename i <*> rename c <*> rename u <*> rename e
    EWhile e e' ->
      EWhile <$> rename e <*> rename e'
    EDoWhile e e' ->
      EDoWhile <$> rename e <*> rename e'
    EReturn e ->
      EReturn <$> rename e
    EBreak ->
      pure ex
    EContinue ->
      pure ex
