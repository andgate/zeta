{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.InstanceDefn where

import           Language.Zeta.Renamer.Monad.Class           (withSpan,
                                                              witnessClassName)
import           Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import           Language.Zeta.Renamer.Renamable.FuncDefn    ()
import           Language.Zeta.Renamer.Renamable.Traversable ()
import           Language.Zeta.Renamer.Renamable.Type        ()
import           Language.Zeta.Syntax                        (InstanceDefn (..))
import           Language.Zeta.Syntax.Data                   (InstanceDefnBlock (InstanceDefnBlock))

instance Renamable InstanceDefn where
  rename (InstanceDefn l ctx n tys body) =
    withSpan l $
      InstanceDefn l
        <$> rename ctx
        <*> witnessClassName n
        <*> rename tys
        <*> rename body

instance Renamable InstanceDefnBlock where
  rename (InstanceDefnBlock fns) =
    InstanceDefnBlock <$> rename fns
