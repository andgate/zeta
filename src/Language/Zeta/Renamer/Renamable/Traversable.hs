{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes        #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.Traversable where

import           Language.Zeta.Renamer.Renamable.Class (Renamable (..))

instance (Traversable f, Renamable a) => Renamable (f a) where
  rename = traverse rename
