{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.MorphismDefn where

import           Language.Zeta.Renamer.Monad.Class           (withSpan',
                                                              witnessClassName)
import           Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import           Language.Zeta.Renamer.Renamable.FuncDefn    ()
import           Language.Zeta.Renamer.Renamable.Type        ()
import           Language.Zeta.Renamer.Renamable.TypeContext ()
import           Language.Zeta.Syntax                        (MorphismDefn (..),
                                                              MorphismDefnBlock (MorphismDefnBlock))

instance Renamable MorphismDefn where
  rename (MorphismDefn l ctx n1 args1 n2 args2 body) =
    withSpan' l $
      MorphismDefn l
        <$> rename ctx
        <*> witnessClassName n1
        <*> rename args1
        <*> witnessClassName n2
        <*> rename args2
        <*> rename body

instance Renamable MorphismDefnBlock where
  rename (MorphismDefnBlock fns) =
    MorphismDefnBlock <$> rename fns
