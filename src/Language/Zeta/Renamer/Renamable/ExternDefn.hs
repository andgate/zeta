{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.ExternDefn where

import           Language.Zeta.Renamer.Monad.Class           (withSpan')
import           Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import           Language.Zeta.Renamer.Renamable.Traversable ()
import           Language.Zeta.Renamer.Renamable.Type        ()
import           Language.Zeta.Syntax                        (ExternDefn (..),
                                                              newExternDefn)

instance Renamable ExternDefn where
  rename (ExternDefn l n paramtys retty) =
    withSpan' l $
      newExternDefn l n <$> rename paramtys <*> rename retty
