{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase       #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.ModuleStmt where

import           Language.Zeta.Renamer.Renamable.Class        (Renamable (..))
import           Language.Zeta.Renamer.Renamable.ClassDefn    ()
import           Language.Zeta.Renamer.Renamable.ExternDefn   ()
import           Language.Zeta.Renamer.Renamable.FuncDefn     ()
import           Language.Zeta.Renamer.Renamable.ImportStmt   ()
import           Language.Zeta.Renamer.Renamable.InstanceDefn ()
import           Language.Zeta.Renamer.Renamable.MorphismDefn ()
import           Language.Zeta.Renamer.Renamable.TypeDefn     ()
import           Language.Zeta.Syntax                         (ModuleStmt (..))

instance Renamable ModuleStmt where
  rename = \case
    ModuleImportStmt ims  -> ModuleImportStmt <$> rename ims
    ModuleFuncDefn fd     -> ModuleFuncDefn <$> rename fd
    ModuleExternDefn ed   -> ModuleExternDefn <$> rename ed
    ModuleTypeDefn td     -> ModuleTypeDefn <$> rename td
    ModuleClassDefn cd    -> ModuleClassDefn <$> rename cd
    ModuleInstanceDefn i  -> ModuleInstanceDefn <$> rename i
    ModuleMorphismDefn md -> ModuleMorphismDefn <$> rename md
