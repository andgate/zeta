{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase       #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.FuncDefn where

import           Language.Zeta.Renamer.Monad.Class           (withSpan')
import           Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import           Language.Zeta.Renamer.Renamable.Exp         ()
import           Language.Zeta.Renamer.Renamable.Pat         ()
import           Language.Zeta.Renamer.Renamable.Traversable ()
import           Language.Zeta.Renamer.Renamable.Type        ()
import           Language.Zeta.Renamer.Renamable.TypeContext ()
import           Language.Zeta.Syntax                        (FuncDefn (..),
                                                              FuncDefnBody (..))

instance Renamable FuncDefn where
  rename (FuncDefn l tyctx n args retty body) =
    withSpan' l $
      FuncDefn l
        <$> rename tyctx
        <*> pure n
        <*> rename args
        <*> rename retty
        <*> rename body

instance Renamable FuncDefnBody where
  rename = \case
    FuncDefnBodyBlock m_sp sb ->
      withSpan' m_sp $
        FuncDefnBodyBlock m_sp <$> rename sb
    FuncDefnBodyExp m_sp e ->
      withSpan' m_sp $
        FuncDefnBodyExp m_sp <$> rename e
