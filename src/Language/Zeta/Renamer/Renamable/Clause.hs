{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.Clause where

import           Language.Zeta.Renamer.Monad.Class           (withSpan')
import           Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import           Language.Zeta.Renamer.Renamable.Pat         ()
import           Language.Zeta.Renamer.Renamable.Traversable ()
import           Language.Zeta.Syntax                        (Clause (..),
                                                              ClauseBlock (..))

instance Renamable ClauseBlock where
  rename (ClauseBlock m_sp cs) =
    withSpan' m_sp $
      ClauseBlock m_sp <$> rename cs

instance Renamable Clause where
  rename (Clause m_sp ps sc) =
    withSpan' m_sp $
      Clause m_sp <$> rename ps <*> rename sc
