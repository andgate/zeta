{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.Module where

import           Language.Zeta.Renamer.Monad.Class           (withLoc')
import           Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import           Language.Zeta.Renamer.Renamable.ModuleStmt  ()
import           Language.Zeta.Renamer.Renamable.Traversable ()
import           Language.Zeta.Syntax                        (Module (..),
                                                              getModuleLocation,
                                                              setModuleStmtList)

instance Renamable Module where
  rename m =
    withLoc' (getModuleLocation m) $
      setModuleStmtList m <$> rename (getModuleStmtList m)
