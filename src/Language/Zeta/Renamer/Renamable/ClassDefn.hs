{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE LambdaCase       #-}
module Language.Zeta.Renamer.Renamable.ClassDefn where

import           Language.Zeta.Renamer.Monad.Class           (withSpan)
import           Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import           Language.Zeta.Renamer.Renamable.Traversable ()
import           Language.Zeta.Renamer.Renamable.FuncDefn ()
import           Language.Zeta.Renamer.Renamable.TypeContext ()
import           Language.Zeta.Syntax                        (ClassDefn (..),
                                                              ClassDefnBlock (..),
                                                              ClassDefnStmt (..))

instance Renamable ClassDefn where
  rename (ClassDefn l ctx n tvs body) =
    withSpan l $
      ClassDefn l
        <$> rename ctx
        <*> pure n
        <*> pure tvs
        <*> rename body

instance Renamable ClassDefnBlock where
  rename (ClassDefnBlock stmts) =
    ClassDefnBlock <$> rename stmts

instance Renamable ClassDefnStmt where
  rename = \case
    ClassDefnSig sp mn tys ->
      withSpan sp $
        ClassDefnSig sp mn <$> rename tys
    ClassDefnMinimalMethod fd ->
      ClassDefnMinimalMethod <$> rename fd
