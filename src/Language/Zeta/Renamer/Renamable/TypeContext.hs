{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase       #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.TypeContext where

import                          Language.Zeta.Renamer.Monad.Class           (withSpan',
                                                                             witnessClassName)
import                          Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import                          Language.Zeta.Renamer.Renamable.Traversable ()
import {-# SOURCE #-}           Language.Zeta.Renamer.Renamable.Type        ()
import                          Language.Zeta.Syntax                        (ClassConstraint (..),
                                                                             ClassInstance (..),
                                                                             TypeContext (..),
                                                                             TypeContextElement (..),
                                                                             TypeSchema (..))

instance Renamable TypeContext where
  rename (TypeContext l es) =
    withSpan' l $
      TypeContext l <$> rename es

instance Renamable TypeContextElement where
  rename = \case
    TypeContextSchema ts ->
      TypeContextSchema <$> rename ts
    TypeContextConstraint cc ->
      TypeContextConstraint <$> rename cc

instance Renamable TypeSchema where
  rename = pure

instance Renamable ClassConstraint where
  rename (ClassConstraint l cs) =
    withSpan' l $
      ClassConstraint l <$> rename cs

instance Renamable ClassInstance where
  rename (ClassInstance l n tys) =
    withSpan' l $
      ClassInstance l <$> witnessClassName n <*> rename tys
