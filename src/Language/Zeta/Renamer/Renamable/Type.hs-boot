{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.Type where

import           Language.Zeta.Renamer.Renamable.Class (Renamable)
import           Language.Zeta.Syntax                  (Type)

instance Renamable Type
