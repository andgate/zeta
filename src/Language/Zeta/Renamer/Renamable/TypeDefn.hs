{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase       #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.TypeDefn where

import           Language.Zeta.Renamer.Monad.Class           (withSpan)
import           Language.Zeta.Renamer.Renamable.Class       (Renamable (..))
import           Language.Zeta.Renamer.Renamable.Exp         ()
import           Language.Zeta.Renamer.Renamable.FuncDefn    ()
import           Language.Zeta.Renamer.Renamable.Pat         ()
import           Language.Zeta.Renamer.Renamable.Traversable ()
import           Language.Zeta.Renamer.Renamable.Type        ()
import           Language.Zeta.Renamer.Renamable.TypeContext ()
import           Language.Zeta.Syntax                        (TypeDefn (..),
                                                              TypeDefnBlock (..),
                                                              TypeDefnStmt (..))

instance Renamable TypeDefn where
  rename (TypeDefn l m_ctx n tvs b) =
    withSpan l $
      TypeDefn l <$> rename m_ctx <*> pure n <*> pure tvs <*> rename b

instance Renamable TypeDefnBlock where
  rename (TypeDefnBlock ss) =
    TypeDefnBlock <$> rename ss

instance Renamable TypeDefnStmt where
  rename = \case
    TypeDefnConstr sp cn tys ->
      withSpan sp $
        TypeDefnConstr sp cn <$> rename tys
    TypeDefnProp sp p e ->
      withSpan sp $
        TypeDefnProp sp <$> rename p <*> rename e
    TypeDefnMethod fd ->
      TypeDefnMethod <$> rename fd
