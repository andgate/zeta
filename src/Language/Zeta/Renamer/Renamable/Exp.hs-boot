{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Renamer.Renamable.Exp where

import           Language.Zeta.Renamer.Renamable.Class (Renamable)
import           Language.Zeta.Syntax                  (Exp)

instance Renamable Exp
