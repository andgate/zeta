{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
module Language.Zeta.Renamer.RenamerError where

import           Document.Data.Location (Loc)
import           Language.Zeta.Syntax   (ClassName, ConstructorName, FieldName,
                                         TVar, TypeName, Var)
import           Prettyprinter          (Pretty (pretty), indent, line, vsep,
                                         (<+>))

data RenamerErr
  = UndefinedVar (Maybe Loc) Var
  | UndefinedTVar (Maybe Loc) TVar
  | UndefinedClassName (Maybe Loc) ClassName
  | UndefinedConstructorName (Maybe Loc) ConstructorName
  | UndefinedFieldName (Maybe Loc) FieldName
  | UndefinedTypeName (Maybe Loc) TypeName

instance Pretty RenamerErr where
  pretty = \case
    UndefinedVar l v ->
      vsep [ line <> pretty l <+> "error:"
           , indent 4 $ "Undefined variable encountered: " <+> pretty v
           , line
           ]

    UndefinedTVar l tv ->
      vsep [ line <> pretty l <+> "error:"
           , indent 4 $ "Undefined type variable encountered: " <+> pretty tv
           , line
           ]

    UndefinedClassName l n ->
      vsep [ line <> pretty l <+> "error:"
           , indent 4 $ "Undefined class name encountered: " <+> pretty n
           , line
           ]

    UndefinedConstructorName l n ->
      vsep [ line <> pretty l <+> "error:"
           , indent 4 $ "Undefined constructor name encountered: " <+> pretty n
           , line
           ]

    UndefinedFieldName l n ->
      vsep [ line <> pretty l <+> "error:"
           , indent 4 $ "Undefined field name encountered: " <+> pretty n
           , line
           ]

    UndefinedTypeName l n ->
      vsep [ line <> pretty l <+> "error:"
           , indent 4 $ "Undefined type name encountered: " <+> pretty n
           , line
           ]
