{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Language.Zeta.Renamer.Monad where

import           Control.Monad.Reader               (MonadReader, Reader,
                                                     runReader)
import           Control.Monad.Report               (MonadReport, ReportT,
                                                     runReportT)
import           Language.Zeta.Renamer.RenamerEnv   (RenamerEnv)
import           Language.Zeta.Renamer.RenamerError (RenamerErr)

-- newtype Renamer t = Fold t (Either [RenamerError] t)

-- runNamecheck :: Monad m => Namecheck t -> m [NameCheckError]

newtype Renamer a = NC { unRenamer :: ReportT RenamerErr (Reader RenamerEnv) a }
  deriving (Functor, Applicative, Monad, MonadReader RenamerEnv, MonadReport RenamerErr)

runRenamer :: Renamer a -> RenamerEnv -> Either [RenamerErr] a
runRenamer m
  = runReader (runReportT (unRenamer m))
