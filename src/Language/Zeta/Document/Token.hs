{-|
Module      : Language.Zeta.Document.Token
Description : Token type that lexer uses.
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Document.Token (
    -- * Token Datatype
    module TokenType,

    -- * Token Optics
    module TokenOptics
  ) where

import           Language.Zeta.Document.Token.HasSpan ()
import           Language.Zeta.Document.Token.Optics  as TokenOptics
import           Language.Zeta.Document.Token.Pretty  ()
import           Language.Zeta.Document.Token.Type    as TokenType
