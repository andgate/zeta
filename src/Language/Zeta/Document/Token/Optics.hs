module Language.Zeta.Document.Token.Optics (
    -- ** Lenses
    tokenClass,
    tokenSpan,

    -- ** Prisms
    _TokenRsvp,
    _TokenVarId,
    _TokenPrimId,
    _TokenInteger,
    _TokenDouble,
    _TokenChar,
    _TokenString,
    _TokenBool,
    _TokenLineOpen,
    _TokenLineClose,
    _TokenBlockOpen,
    _TokenBlockClose,
    _TokenEof,

    -- ** Traversals
    tokenRsvp,
    tokenVarId,
    tokenPrimId,
    tokenInteger,
    tokenDouble,
    tokenChar,
    tokenString,
    tokenBool,
    tokenLineOpen,
    tokenLineClose,
    tokenBlockOpen,
    tokenBlockClose,
    tokenEof,

    -- ** Token Class Predicates on Tokens
    hasTokenRsvp,
    hasTokenVarId,
    hasTokenPrimId,
    hasTokenInteger,
    hasTokenDouble,
    hasTokenChar,
    hasTokenString,
    hasTokenBool,
    hasTokenLineOpen,
    hasTokenLineClose,
    hasTokenBlockOpen,
    hasTokenBlockClose,
    hasTokenEof
  ) where

import           Control.Lens.Fold                 (has)
import           Control.Lens.Lens                 (Lens', lens)
import           Control.Lens.Prism                (Prism', prism')
import           Control.Lens.Traversal            (Traversal')
import           Data.Text                         (Text)
import           Document.Data.Span                (Span)
import           Language.Zeta.Document.Token.Type (Token (..), TokenClass (..),
                                                    getTokenBlockClose,
                                                    getTokenBlockOpen,
                                                    getTokenBool, getTokenChar,
                                                    getTokenDouble, getTokenEof,
                                                    getTokenInteger,
                                                    getTokenLineClose,
                                                    getTokenLineOpen,
                                                    getTokenPrimId,
                                                    getTokenRsvp, getTokenSpan,
                                                    getTokenString,
                                                    getTokenVarId,
                                                    setTokenClass, setTokenSpan)

-- | Lens on the 'TokenClass' of a 'Token'.
tokenClass :: Lens' Token TokenClass
tokenClass = lens getTokenClass setTokenClass

-- | Lens on the 'Span' of a 'Token'.
tokenSpan :: Lens' Token Span
tokenSpan = lens getTokenSpan setTokenSpan

-- | Prism on the 'TokenRsvp' constructor of a 'TokenClass'.
_TokenRsvp :: Prism' TokenClass Text
_TokenRsvp = prism' TokenRsvp getTokenRsvp

-- | Prism on the 'TokenVarId' constructor of a 'TokenClass'.
_TokenVarId :: Prism' TokenClass Text
_TokenVarId = prism' TokenVarId getTokenVarId

-- | Prism on the 'TokenPrimId' constructor of a 'TokenClass'.
_TokenPrimId :: Prism' TokenClass Text
_TokenPrimId = prism' TokenPrimId getTokenPrimId

-- | Prism on the 'TokenInteger' constructor of a 'TokenClass'.
_TokenInteger :: Prism' TokenClass Integer
_TokenInteger = prism' TokenInteger getTokenInteger

-- | Prism on the 'TokenDouble' constructor of a 'TokenClass'.
_TokenDouble :: Prism' TokenClass Double
_TokenDouble = prism' TokenDouble getTokenDouble

-- | Prism on the 'TokenChar' constructor of a 'TokenClass'.
_TokenChar :: Prism' TokenClass Char
_TokenChar = prism' TokenChar getTokenChar

-- | Prism on the 'TokenString' constructor of a 'TokenClass'.
_TokenString :: Prism' TokenClass String
_TokenString = prism' TokenString getTokenString

-- | Prism on the 'TokenBool' constructor of a 'TokenClass'.
_TokenBool :: Prism' TokenClass Bool
_TokenBool = prism' TokenBool getTokenBool

-- | Prism on the 'TokenLineOpen' constructor of a 'TokenClass'.
_TokenLineOpen :: Prism' TokenClass ()
_TokenLineOpen = prism' (const TokenLineOpen) getTokenLineOpen

-- | Prism on the 'TokenLineClose' constructor of a 'TokenClass'.
_TokenLineClose :: Prism' TokenClass ()
_TokenLineClose = prism' (const TokenLineClose) getTokenLineClose

-- | Prism on the 'TokenBlockOpen' constructor of a 'TokenClass'.
_TokenBlockOpen :: Prism' TokenClass ()
_TokenBlockOpen = prism' (const TokenBlockOpen) getTokenBlockOpen

-- | Prism on the 'TokenBlockClose' constructor of a 'TokenClass'.
_TokenBlockClose :: Prism' TokenClass ()
_TokenBlockClose = prism' (const TokenBlockClose) getTokenBlockClose

-- | Prism on the 'TokenEof' constructor of a 'TokenClass'.
_TokenEof :: Prism' TokenClass ()
_TokenEof = prism' (const TokenEof) getTokenEof

-- | Traversal on the 'TokenRspv' text of a 'Token'.
tokenRsvp :: Traversal' Token Text
tokenRsvp = tokenClass . _TokenRsvp

-- | Traversal on the 'TokenVarId' text of a 'Token'.
tokenVarId :: Traversal' Token Text
tokenVarId = tokenClass . _TokenVarId

-- | Traversal on the 'TokenPrimid' text of a 'Token'.
tokenPrimId :: Traversal' Token Text
tokenPrimId = tokenClass . _TokenPrimId

-- | Traversal on the 'TokenInteger' value of a 'Token'.
tokenInteger :: Traversal' Token Integer
tokenInteger = tokenClass . _TokenInteger

-- | Traversal on the 'TokenDouble' value of a 'Token'.
tokenDouble :: Traversal' Token Double
tokenDouble = tokenClass . _TokenDouble

-- | Traversal on the 'TokenChar' value of a 'Token'.
tokenChar :: Traversal' Token Char
tokenChar = tokenClass . _TokenChar

-- | Traversal on the 'TokenString' value of a 'Token'.
tokenString :: Traversal' Token String
tokenString = tokenClass . _TokenString

-- | Traversal to 'TokenBool' value of a 'Token'.
tokenBool :: Traversal' Token Bool
tokenBool = tokenClass . _TokenBool

-- | Traversal to 'TokenLineOpen' in a 'Token'.
tokenLineOpen :: Traversal' Token ()
tokenLineOpen = tokenClass . _TokenLineOpen

-- | Traversal to 'TokenLineClose' in a 'Token'.
tokenLineClose :: Traversal' Token ()
tokenLineClose = tokenClass . _TokenLineClose

-- | Traversal to 'TokenBlockOpen' in a 'Token'.
tokenBlockOpen :: Traversal' Token ()
tokenBlockOpen = tokenClass . _TokenBlockOpen

-- | Traversal to 'TokenBlockClose' in a 'Token'.
tokenBlockClose :: Traversal' Token ()
tokenBlockClose = tokenClass . _TokenBlockClose

-- | Traversal to 'TokenEof' in a 'Token'.
tokenEof :: Traversal' Token ()
tokenEof = tokenClass . _TokenEof

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenRsvp'.
hasTokenRsvp :: Token -> Bool
hasTokenRsvp = has tokenRsvp

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenVarId'.
hasTokenVarId :: Token -> Bool
hasTokenVarId = has tokenVarId

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenPrimId'.
hasTokenPrimId :: Token -> Bool
hasTokenPrimId = has tokenPrimId

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenInteger'.
hasTokenInteger :: Token -> Bool
hasTokenInteger = has tokenInteger

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenDouble'.
hasTokenDouble :: Token -> Bool
hasTokenDouble = has tokenDouble

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenChar'.
hasTokenChar :: Token -> Bool
hasTokenChar = has tokenChar

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenString'.
hasTokenString :: Token -> Bool
hasTokenString = has tokenString

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenBool'.
hasTokenBool :: Token -> Bool
hasTokenBool = has tokenBool

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenEof'.
hasTokenEof :: Token -> Bool
hasTokenEof = has tokenEof

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenLineOpen'.
hasTokenLineOpen :: Token -> Bool
hasTokenLineOpen = has tokenLineOpen

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenLineClose'.
hasTokenLineClose :: Token -> Bool
hasTokenLineClose = has tokenLineClose

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenBlockOpen'.
hasTokenBlockOpen :: Token -> Bool
hasTokenBlockOpen = has tokenBlockOpen

-- | Test if the 'TokenClass' of a 'Token' is a 'TokenBlockClose'.
hasTokenBlockClose :: Token -> Bool
hasTokenBlockClose = has tokenBlockClose
