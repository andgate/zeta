{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Document.Token.Pretty where

import           Language.Zeta.Document.Token.Type
import           Prettyprinter

instance Pretty Token where
  pretty t =
    pretty (getTokenClass t) <> "@" <> pretty (getTokenSpan t)

instance Pretty TokenClass where
  pretty = pretty . show
