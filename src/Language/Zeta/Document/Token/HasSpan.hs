{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Document.Token.HasSpan where

import           Document.Classy.HasSpan             (HasSpan (..))
import           Language.Zeta.Document.Token.Optics (tokenSpan)
import           Language.Zeta.Document.Token.Type   (Token)

instance HasSpan Token where
    span = tokenSpan
