{-# LANGUAGE LambdaCase #-}
module Language.Zeta.Document.Token.Type where

import           Data.Text          (Text)
import           Document.Data.Span (Span)

-- | A lexical `Token`, used to parse Zeta source code.
data Token = Token -- ^ Constructor for a 'Token'.
    { getTokenClass :: !TokenClass -- ^ Get the token class.
    , getTokenSpan  :: !Span       -- ^ Get the span of the token's source string.
    } deriving (Eq, Show, Ord)

-- | Set the 'TokenClass' of some 'Token'.
setTokenClass
  :: Token      -- ^ Current token.
  -> TokenClass -- ^ New token class.
  -> Token      -- ^ Updated token, with new token class.
setTokenClass t c = t { getTokenClass = c }

-- | Set the 'Span' of some 'Token'.
setTokenSpan
  :: Token  -- ^ Current token.
  -> Span   -- ^ New token location.
  -> Token  -- ^ Updated token, with new location.
setTokenSpan t s = t { getTokenSpan = s }

-- | Token class, which differentiates tokens into different types.
-- Token classes may also carry class-specific data, such as variable names or integers.
-- A variety of ways have been provided to safely extract this data.
data TokenClass
  = TokenRsvp Text          -- ^ Reserved token
  | TokenVarId Text         -- ^ Variable id token
  | TokenPrimId Text        -- ^ Primitive id token

  | TokenInteger Integer    -- ^ Integer token
  | TokenDouble Double      -- ^ Double token
  | TokenChar Char          -- ^ Character token
  | TokenString String      -- ^ String token
  | TokenBool Bool          -- ^ Boolean token (true/false)

  | TokenLineOpen           -- ^ Line open token
  | TokenLineClose          -- ^ Line close token
  | TokenBlockOpen          -- ^ Block open token
  | TokenBlockClose         -- ^ Block close token
  | TokenEof                -- ^ End-of-file token
  deriving (Eq, Show, Ord)

-- | Attempts to get the data from a rsvp id token.
getTokenRsvp
  :: TokenClass -- ^ TokenClass to attempt extraction on.
  -> Maybe Text -- ^ Results in some rsvp id or 'Nothing' when the class doesn't match.
getTokenRsvp = \case
  TokenRsvp n -> Just n
  _           -> Nothing

-- | Attempts to get the data from a variable id 'TokenClass'.
getTokenVarId
  :: TokenClass -- ^ TokenClass to attempt extraction on.
  -> Maybe Text -- ^ Results in some variable id or 'Nothing' when the class doesn't match.
getTokenVarId = \case
  TokenVarId n -> Just n
  _            -> Nothing

-- | Attempts to get the data from a primitive id 'TokenClass'.
getTokenPrimId
  :: TokenClass -- ^ TokenClass to attempt extraction on.
  -> Maybe Text -- ^ Results in some primitive id or 'Nothing' when the class doesn't match.
getTokenPrimId = \case
  TokenPrimId n -> Just n
  _             -> Nothing

-- | Attempts to get the data from an integer 'TokenClass'.
getTokenInteger
  :: TokenClass     -- ^ TokenClass to attempt extraction on.
  -> Maybe Integer  -- ^ Results in some 'Integer' or 'Nothing' when the class doesn't match.
getTokenInteger (TokenInteger v) = Just v
getTokenInteger _                = Nothing

-- | Attempts to get the data from a double 'TokenClass'.
getTokenDouble
  :: TokenClass   -- ^ TokenClass to attempt extraction on.
  -> Maybe Double -- ^ Results in some 'Double' or 'Nothing' when the class doesn't match.
getTokenDouble (TokenDouble v) = Just v
getTokenDouble _               = Nothing

-- | Attempts to get the data from a text character 'TokenClass'.
getTokenChar
  :: TokenClass -- ^ TokenClass to attempt extraction on.
  -> Maybe Char -- ^ Results in some 'Char' or 'Nothing' when the class doesn't match.
getTokenChar (TokenChar v) = Just v
getTokenChar _             = Nothing

-- | Attempts to get the data from a string 'TokenClass'.
getTokenString
  :: TokenClass   -- ^ TokenClass to attempt extraction on.
  -> Maybe String -- ^ Results in some 'String' or 'Nothing' when the class doesn't match.
getTokenString (TokenString v) = Just v
getTokenString _               = Nothing

-- | Attempts to get the data from a boolean (true/false) 'TokenClass'.
getTokenBool
  :: TokenClass -- ^ TokenClass to attempt extraction on.
  -> Maybe Bool -- ^ Results in some 'Bool' or 'Nothing' when the class doesn't match.
getTokenBool (TokenBool v) = Just v
getTokenBool _             = Nothing

-- | Attempts to get the data from a line open 'TokenClass'.
getTokenLineOpen
  :: TokenClass -- ^ TokenClass to attempt extraction on.
  -> Maybe ()   -- ^ Results in some '()' or 'Nothing' when the class doesn't match.
getTokenLineOpen TokenLineOpen = Just ()
getTokenLineOpen _             = Nothing

-- | Attempts to get the data from a line close 'TokenClass'.
getTokenLineClose
  :: TokenClass -- ^ TokenClass to attempt extraction on.
  -> Maybe ()   -- ^ Results in some '()' or 'Nothing' when class doesn't match.
getTokenLineClose TokenLineClose = Just ()
getTokenLineClose _              = Nothing

-- | Attempts to get the data from a block open 'TokenClass'.
getTokenBlockOpen
  :: TokenClass -- ^ TokenClass to attempt extraction on.
  -> Maybe ()   -- ^ Results in some '()' or 'Nothing' when class doesn't match.
getTokenBlockOpen TokenBlockOpen = Just ()
getTokenBlockOpen _              = Nothing

-- | Attempts to get the data from a block close 'TokenClass'.
getTokenBlockClose
  :: TokenClass -- ^ TokenClass to attempt extraction on.
  -> Maybe ()   -- ^ Results in some '()' or 'Nothing' when class doesn't match.
getTokenBlockClose TokenBlockClose = Just ()
getTokenBlockClose _               = Nothing

-- | Attempts to get the data from an end-of-file 'TokenClass'.
getTokenEof
  :: TokenClass -- ^ TokenClass to attempt extraction on.
  -> Maybe ()   -- ^ Results in some '()' or 'Nothing' when class doesn't match.
getTokenEof TokenEof = Just ()
getTokenEof _        = Nothing
