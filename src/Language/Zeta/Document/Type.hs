module Language.Zeta.Document.Type where

-- import Data.DList (DList)
-- import Language.Zeta.Document.Token

-- data Doc = Doc {
--   docRootBlock :: Block,
--   docInfo :: DocInfo
-- }

-- data DocInfo = DocInfo {
--   docInfoFilePath :: FilePath
-- }

-- data Block = Block (DList LineFold)

-- data LineFold = LineFold (DList LineItem)

-- data LineItem
--   = TokenItem Token
--   | BlockItem Block


-- appendToken :: Token -> Doc -> Doc
-- appendToken _ _ = undefined

-- class IsTokenSequence a where
--   toTokenSeq :: a -> [Token]

-- instance IsTokenSequence Doc where
--   toTokenSeq :: Doc -> [Token]
--   toTokenSeq d = toTokenSeq . docRootBlock

-- instance IsTokenSequence Block where
--   toTokenSeq :: Block -> [Token]
--   toTokenSeq (Block ls) = mconcat (toTokenSeq <$> ls)

-- instance IsTokenSequence LineFold where
--   toTokenSeq :: LineFold -> [Token]
--   toTokenSeq (LineFold tks) = mconcat (toTokenSeq <$> tks)

-- instance IsTokenSequence LineItem where
--   toTokenSeq :: LineItem -> [Token]
--   toTokenSeq (TokenItem t) = [t]
--   toTokenSeq (BlockItem b) = toTokenSeq b


-- data DocCursor =
--   DocCursor DocInfo BlockContext LineBreakContext LineItem

-- data BlockContext = BlockContext [LineBreak] [LineBreak]

-- data LineBreakContext = LineBreakContext [LineItem] [LineItem]

-- openDocCursor :: Doc -> DocCursor
-- openDocCursor d =
--   let Block ((LineBreak (i:l2)):b2) = docRootBlock d
--       m = docInfo d
--   in DocCursor m (BlockContext [] b2) (LineBreakContext [] l2) i

-- closeDocCursor :: DocCursor -> Doc
-- closeDocCursor (DocCursor m (BlockContext b1 b2) (LineBreakContext l1 l2) i) =
--   let l3 = LineBreak (l1 <> (i:l2))
--       b3 = Block (b1 <> (l3:b2))
--   in Doc {
--     docRootBlock = b3
--   , docInfo = m
--   }

-- insertTokenAtCursor :: Token -> DocCursor -> DocCursor
-- insertTokenAtCursor _ _ = undefined
