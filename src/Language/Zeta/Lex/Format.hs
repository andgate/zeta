{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE KindSignatures    #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes        #-}
module Language.Zeta.Lex.Format where

import           Control.Lens.Getter          (view)
import           Control.Lens.Operators       ((^.))
import           Control.Monad                (unless, void, when)
import           Control.Monad.State.Strict   (State, evalState, gets, modify)
import           Document.Data.Position
import           Document.Data.Span
import           Language.Zeta.Document.Token
import           Numeric.Natural              (Natural)
import           Safe                         (headDef)

blkTriggers :: [TokenClass]
blkTriggers = [TokenRsvp "of", TokenRsvp "let"]

blkEndTriggers :: [TokenClass]
blkEndTriggers = [TokenRsvp "in"]

-- -----------------------------------------------------------------------------
-- Cell Type

data CellType =
    Block
  | LineFold
  deriving (Eq, Ord, Show)

data Cell =
  Cell
  { cellIndent :: !Natural
  , cellType   :: CellType
  } deriving (Eq, Ord, Show)

setCellIndent :: Natural -> Cell -> Cell
setCellIndent i c = c { cellIndent = i }

setCellType :: CellType -> Cell -> Cell
setCellType ty c = c { cellType = ty }

defCell :: Cell
defCell = Cell 0 Block

-- -----------------------------------------------------------------------------
-- Layout Types

data LayoutState =
    LayoutState
    { laySpan      :: Span
    , layStack     :: [Cell]
    , blkTriggered :: Bool
    , layToks      :: [Token]
    , layToks'     :: [Token]
    , layResults   :: [[Token]]
    } deriving (Eq, Ord, Show)

type Layout = State LayoutState

mkLayout :: [Token] -> LayoutState
mkLayout input = LayoutState
  { laySpan = initialSpan
  , layStack = [defCell]
  , blkTriggered = False
  , layToks = input
  , layToks' = []
  , layResults = []
  }

setLaySpan :: Span -> Layout ()
setLaySpan s = modify (\st -> st { laySpan = s })

setLayStack :: [Cell] -> Layout ()
setLayStack stk = modify (\st -> st { layStack = stk })

modifyLayStack :: ([Cell] -> [Cell]) -> Layout ()
modifyLayStack f = modify (\st -> st { layStack = f (layStack st) })

modifyCurrCell :: (Cell -> Cell) -> Layout ()
modifyCurrCell f = modifyLayStack $ \case
  c1:cn -> f c1 : cn
  []    -> []

setCurrCellIndent :: Natural -> Layout ()
setCurrCellIndent i = modifyCurrCell (setCellIndent i)

setBlkTriggered :: Bool -> Layout ()
setBlkTriggered t = modify (\st -> st { blkTriggered = t })

setLayToks :: [Token] -> Layout ()
setLayToks s = modify (\st -> st { layToks = s })

setLayToks' :: [Token] -> Layout ()
setLayToks' toks = modify (\st -> st { layToks' = toks })

modifyLayToks' :: ([Token] -> [Token]) -> Layout ()
modifyLayToks' f = modify (\st -> st { layToks' = f (layToks' st) })

setLayResults :: [[Token]] -> Layout ()
setLayResults r = modify (\st -> st { layResults = r })

modifyLayResults :: ([[Token]] -> [[Token]]) -> Layout ()
modifyLayResults f = modify (\st -> st { layResults = f (layResults st) })

-- -----------------------------------------------------------------------------
-- Layout Driver


-- This would be more performant with a foldM in the State monad
-- Since this was originally implemented with conduit, using recursion in a state monad that
-- maintains input/out lists was easier.
layout :: [Token] -> [Token]
layout toks = mconcat $ reverse $ evalState layoutDriver (mkLayout toks)


layoutDriver :: Layout [[Token]]
layoutDriver = do
  ts <- gets layToks
  case ts of
    (t:ts') -> do
      setLayToks ts'
      updateLocation t
      handleTok t
      layoutDriver

    [] -> do
      closeStack
      gets layResults


handleTok :: Token -> Layout ()
handleTok t
  | hasTokenEof t = closeStack

  | otherwise = do
      -- Blocks triggered on last token need to be handled
      emitBlk <- gets blkTriggered
      when emitBlk $ do
        setBlkTriggered False
        open Block

      when ((t ^. tokenClass) `elem` blkEndTriggers)
           closeBlk

      closeInvalid
      yieldTok t

      -- Colons trigger block emission for the next token
      when ((t ^. tokenClass) `elem` blkTriggers) $
        setBlkTriggered True


-- -----------------------------------------------------------------------------
-- Driver Helpers


yieldTok :: Token -> Layout ()
yieldTok t = do
  modifyLayToks' (t:)

  d <- length <$> gets layStack
  let isTopLevel = (d == 1) && hasTokenLineClose t
      isEof = hasTokenEof t

   -- Is it time to split it up?
  when (isTopLevel || isEof)
       $ do ts <- reverse <$> gets layToks'
            modifyLayResults (ts:)
            setLayToks' []




closeStack :: Layout ()
closeStack = do
  cl <- peekCell
  unless (cl == defCell)
         (close >> closeStack)


closeInvalid :: Layout ()
closeInvalid = do
  go =<< getCurrIndent
  fillBlock
  where
    go i = do
      cl <- peekCell
      unless (isValidIndent i cl)
             (close >> go i)


fillBlock :: Layout ()
fillBlock = do
  (Cell _ ct) <- peekCell
  when (ct == Block)
       (open LineFold)


open :: CellType -> Layout ()
open ct = do
  l <- gets laySpan
  i <- getCurrIndent

  let cl = Cell i ct
  pushCell cl
  yieldTok $ openTok l cl


close :: Layout ()
close = do
  cl <- peekCell
  l <- gets laySpan
  void popCell
  yieldTok $ closeTok l cl


-- | This will close a block layout, if there is one.
-- | Otherwise, it will just close a linefold, if there is one.
closeBlk :: Layout ()
closeBlk = do
  -- Peek two cells off the stack
  (Cell i1 ct1) <- peekCell
  (Cell i2 ct2) <- headDef defCell . tail <$> gets layStack

  -- Close twice when there is a linefold followed by a block that isn't root
  when (ct1 == LineFold && ct2 == Block && i2 /= 0)
       (close >> close)

  -- Close once when there is a block that isn't root
  when (ct1 == Block && i1 /= 0)
       (close)


-- -----------------------------------------------------------------------------
-- Layout Helpers

updateLocation :: Token -> Layout ()
updateLocation = setLaySpan . view tokenSpan

getCellIndent :: Layout Natural
getCellIndent =
  cellIndent <$> peekCell

getCurrIndent :: Layout Natural
getCurrIndent =
  gets (getColumn . getStart . laySpan)

pushCell :: Cell -> Layout ()
pushCell l =
  modifyLayStack (l:)

popCell :: Layout Cell
popCell = do
  cn <- peekCell
  modifyLayStack tail
  return cn

peekCell :: Layout Cell
peekCell =
  headDef defCell <$> gets layStack

openTok :: Span -> Cell -> Token
openTok l cl =
  case cellType cl of
      Block    -> Token TokenBlockOpen l
      LineFold -> Token TokenLineOpen l

closeTok :: Span -> Cell -> Token
closeTok l cl =
  case cellType cl of
      Block    -> Token TokenBlockClose l
      LineFold -> Token TokenLineClose l

isValidIndent :: Natural -> Cell -> Bool
isValidIndent i (Cell ci ct) =
  case ct of
    Block ->
      ci <= i

    LineFold ->
      ci < i
