module Language.Zeta.Lex.Helpers where

import           Data.Either    (fromRight)
import           Data.Text      (Text)
import qualified Data.Text.Read as T

readInt :: Text -> Integer
readInt = fst . fromRight undefined . T.decimal

readSignedInt :: Text -> Integer
readSignedInt = fst . fromRight undefined . T.signed T.decimal

readDbl :: Text -> Double
readDbl = fst . fromRight undefined . T.double

readSignedDbl :: Text -> Double
readSignedDbl = fst . fromRight undefined . T.signed T.double
