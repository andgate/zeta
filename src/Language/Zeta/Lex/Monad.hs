{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Language.Zeta.Lex.Monad where

import           Prelude                      hiding (span)

import           Control.Lens.Fold            (preuse)
import           Control.Monad.Except         (Except, MonadError)
import           Control.Monad.State          (MonadState, StateT (StateT),
                                               evalStateT, gets, modify)
import           Data.Maybe                   (fromJust)
import           Document.Classy.HasSpan      (HasSpan (span))
import           Document.Data.Location       (Loc (..))
import           Document.Data.Span           (Span, growLineSpan, growSpan,
                                               moveSpan, startNextLineSpan)
import           Language.Zeta.Document.Token (Token (..), TokenClass)
import           Language.Zeta.Lex.Error      (LexError)
import           Language.Zeta.Lex.State      (LexState (lexCommentDepth, lexFilePath, lexSpan, lexStartcode, lexStringBuf, lexTokAcc),
                                               initialLexState)

newtype Lex a = Lex { unLex :: StateT LexState (Except LexError) a }
  deriving ( Functor, Applicative, Monad
           , MonadState LexState
           , MonadError LexError
           )

runLexer :: FilePath -> Lex a -> Except LexError a
runLexer fp lexer = evalStateT (unLex lexer) (initialLexState fp)

getLexTokAcc :: Lex [Token]
getLexTokAcc = gets lexTokAcc

setLexTokAcc :: [Token] -> Lex ()
setLexTokAcc acc = modify (\st -> st { lexTokAcc = acc })

pushLexTokAcc :: Token -> Lex ()
pushLexTokAcc t = modify (\st -> st { lexTokAcc = t:(lexTokAcc st) })

getLexSpan :: Lex Span
getLexSpan = fromJust <$> preuse span

setLexSpan :: Span -> Lex ()
setLexSpan s = modify (\st -> st { lexSpan = s })

modifyLexSpan :: (Span -> Span) -> Lex ()
modifyLexSpan f = modify (\st -> st { lexSpan = f (lexSpan st) })

getLexStartcode :: Lex Int
getLexStartcode = gets lexStartcode

setLexStartcode :: Int -> Lex ()
setLexStartcode sc = modify (\st -> st { lexStartcode = sc })

getLexCommentDepth :: Lex Int
getLexCommentDepth = gets lexCommentDepth

setLexCommentDepth :: Int -> Lex ()
setLexCommentDepth d = modify (\st -> st { lexCommentDepth = d })

addLexCommentDepth :: Int -> Lex ()
addLexCommentDepth d = modify (\st -> st { lexCommentDepth = (lexCommentDepth st) + d })

getLexStringBuf :: Lex String
getLexStringBuf = gets lexStringBuf

setLexStringBuf :: String -> Lex ()
setLexStringBuf sbuf = modify (\st -> st { lexStringBuf = sbuf })

pushLexStringBuf :: Char -> Lex ()
pushLexStringBuf c = modify (\st -> st { lexStringBuf = c:(lexStringBuf st) })

setLexFilePath :: FilePath -> Lex ()
setLexFilePath fp = modify (\st -> st { lexFilePath = fp })

getLexFilePath :: Lex FilePath
getLexFilePath = gets lexFilePath

getLexLocation :: Lex Loc
getLexLocation = L <$> getLexFilePath <*> getLexSpan

tag :: TokenClass -> Lex Token
tag tc = Token tc <$> getLexSpan

moveLexSpan :: Int -> Lex ()
moveLexSpan len = do
  modifyLexSpan (`moveSpan` fromIntegral len)

growLexSpan :: Int -> Lex ()
growLexSpan len =
  modifyLexSpan (`growSpan` fromIntegral len)

nextLineBreak :: Lex ()
nextLineBreak =
  modifyLexSpan startNextLineSpan

nextLineContinue :: Lex ()
nextLineContinue = do
  modifyLexSpan growLineSpan

yieldTaggedTok :: TokenClass -> Lex ()
yieldTaggedTok c = do
  t <- tag c
  yieldTok t

yieldTok :: Token -> Lex ()
yieldTok t = pushLexTokAcc t
