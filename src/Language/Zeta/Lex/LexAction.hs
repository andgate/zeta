module Language.Zeta.Lex.LexAction where

import           Control.Monad.Except         (MonadError (..))
import           Data.Text                    (Text)
import qualified Data.Text                    as T
import           Language.Zeta.Document.Token
import           Language.Zeta.Lex.Error
import           Language.Zeta.Lex.Monad

type LexAction = Text -> Int -> Lex ()

yieldTokAt :: TokenClass -> Int -> Lex ()
yieldTokAt c len = do
  moveLexSpan len
  yieldTaggedTok c

rsvp :: LexAction
rsvp text len =
  yieldTokAt (TokenRsvp text) len

skipBreak :: LexAction
skipBreak _ len = do
  moveLexSpan len

skipContinue :: LexAction
skipContinue _ len = do
  growLexSpan len

beginString :: Int -> LexAction
beginString stringSC _ len =
  do
    moveLexSpan len
    setLexStartcode stringSC

endString :: LexAction
endString _ len = do
  buf <- do
    growLexSpan len
    getLexStringBuf

  yieldTaggedTok (TokenString $ reverse buf)

  do
    setLexStringBuf ""
    setLexStartcode 0

appendString :: LexAction
appendString text len = do
    growLexSpan len
    pushLexStringBuf (T.head text)

escapeString :: LexAction
escapeString text len = do
  let c = T.head $ T.tail text
  unesc <- case c of
    'n' -> return '\n'
    't' -> return '\t'
    '"' -> return '"'
    _  -> do
      l <- getLexLocation
      throwError $ InvalidEscapeChar l c
  growLexSpan len
  pushLexStringBuf unesc

handleChar :: LexAction
handleChar text len = do
  let trim = T.unpack . T.tail . T.init
      yieldCharAt ch = yieldTokAt (TokenChar ch) len
  case (trim text) of
    ([])   -> yieldCharAt '\0'
    "\\0"   -> yieldCharAt '\0'
    "\\t"   -> yieldCharAt '\t'
    "\\n"   -> yieldCharAt '\n'
    "\\r"   -> yieldCharAt '\r'
    "\'"   -> yieldCharAt '\''
    (c:[])  -> yieldCharAt c
    _      -> do
      l <- getLexLocation
      throwError $ InvalidCharLit l (trim text)

beginComment :: Int -> LexAction
beginComment commentSC _ len = do
  moveLexSpan len
  setLexStartcode commentSC
  setLexCommentDepth 1

continueComment :: LexAction
continueComment _ len = do
  growLexSpan len
  addLexCommentDepth 1

endComment :: Int -> LexAction
endComment commentSC _ len = do
  growLexSpan len

  addLexCommentDepth (-1)
  cd <- getLexCommentDepth
  let sc = if cd == 0 then 0 else commentSC
  setLexStartcode sc

beginLineComment :: Int -> LexAction
beginLineComment lineCommentSC _ len = do
  moveLexSpan len
  setLexStartcode lineCommentSC

endLineComment :: LexAction
endLineComment _ _ = do
  nextLineContinue
  setLexStartcode 0
