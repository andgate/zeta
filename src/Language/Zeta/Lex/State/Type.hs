module Language.Zeta.Lex.State.Type where

import           Document.Data.Span (Span, initialSpan)
import           Language.Zeta.Document.Token (Token)

data LexState =
  LexState  { lexTokAcc       :: [Token]
            , lexSpan         :: Span
            , lexStartcode    :: Int
            , lexCommentDepth :: Int
            , lexStringBuf    :: String
            , lexFilePath     :: FilePath
            } deriving Show

initialLexState :: FilePath -> LexState
initialLexState fp =
  LexState
    { lexTokAcc = []
    , lexSpan = initialSpan
    , lexStartcode = 0
    , lexCommentDepth = 0
    , lexStringBuf = ""
    , lexFilePath = fp
    }

setLexSpan :: LexState -> Span -> LexState
setLexSpan s l = s { lexSpan = l }
