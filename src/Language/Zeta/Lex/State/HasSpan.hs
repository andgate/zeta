{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Lex.State.HasSpan where

import           Document.Classy.HasSpan      (HasSpan (..))
import           Language.Zeta.Lex.State.Type (LexState (lexSpan), setLexSpan)

instance HasSpan LexState where
  span f s = setLexSpan s <$> f (lexSpan s)
