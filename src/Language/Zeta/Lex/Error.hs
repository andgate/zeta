{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns      #-}
module Language.Zeta.Lex.Error where

import           Document.Data.Location (Loc)
import           Prettyprinter

data LexError
  = UnrecognizedToken Loc String
  | InvalidCharLit Loc String
  | InvalidEscapeChar Loc Char
  | IllegalLexerSkip Loc
  deriving (Eq, Show)

instance Pretty LexError where
    pretty = \case
      UnrecognizedToken (pretty -> l) (pretty -> cs) ->
        vsep [ line <> l <+> "error:"
             , indent 4 $ "Lexer has encountered an unrecognized token: " <+> dquotes cs
             ]

      InvalidCharLit (pretty -> l) (pretty -> str) ->
        vsep [ line <> l <+> "error:"
             , indent 4 $ "Lexer has encountered an invalid literal character:" <+> squotes str
             ]

      InvalidEscapeChar (pretty -> l) (pretty -> str) ->
        vsep [ line <> l <+> "error:"
             , indent 4 $ "Lexer has encountered an invalid literal character:" <+> squotes str
             ]

      IllegalLexerSkip (pretty -> l) ->
        vsep [ line <> l <+> "error:"
             , indent 4 $ "Lexer performed an illegal skip operation."
             ]
