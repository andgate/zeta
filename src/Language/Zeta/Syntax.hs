{-|
Module      : Language.Zeta.Syntax
Description : Zeta syntax types and functions
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax (
    -- * Syntax Types
    module Data,

    -- * Syntax Optics
    module Optics,

    -- * Classy Syntax
    module Classy
  ) where

import           Language.Zeta.Syntax.Classy as Classy
import           Language.Zeta.Syntax.Data   as Data
import           Language.Zeta.Syntax.Optics as Optics
