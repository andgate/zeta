{-|
Module      : Language.Zeta.Syntax.Classy
Description : Classy Zeta syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Classy (module X) where

import           Language.Zeta.Syntax.Classy.Abstracts    as X
import           Language.Zeta.Syntax.Classy.Alpha        as X
import           Language.Zeta.Syntax.Classy.Bound        as X
import           Language.Zeta.Syntax.Classy.HasExpCache  as X
import           Language.Zeta.Syntax.Classy.HasNames     as X
import           Language.Zeta.Syntax.Classy.HasSize      as X
import           Language.Zeta.Syntax.Classy.HasSpan      ()
import           Language.Zeta.Syntax.Classy.HasType      as X
import           Language.Zeta.Syntax.Classy.HasTypeCache as X
import           Language.Zeta.Syntax.Classy.HasVars      as X
import           Language.Zeta.Syntax.Classy.Instantiable as X
import           Language.Zeta.Syntax.Classy.Pretty       ()
import           Language.Zeta.Syntax.Classy.Ranked       as X
import           Language.Zeta.Syntax.Classy.Replaceable  as X
