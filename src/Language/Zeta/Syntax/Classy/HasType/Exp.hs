{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasType.Exp where

import           Language.Zeta.Syntax.Classy.HasType.Class (HasType (..))
import           Language.Zeta.Syntax.Data.Exp             (Exp (..))

instance HasType Exp where
  typeOf f = \case
    EType e t -> EType e <$> f t
    ELoc e l  -> ELoc <$> typeOf f e <*> pure l
    EParens e -> EParens <$> typeOf f e
    e         -> pure e
  {-# INLINE typeOf #-}
