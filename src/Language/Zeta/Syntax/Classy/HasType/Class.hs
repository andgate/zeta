module Language.Zeta.Syntax.Classy.HasType.Class where

import           Control.Lens.Traversal         (Traversal')
import           Language.Zeta.Syntax.Data.Type (Type)

-- | Classy typed carriers.
class HasType a where
  -- | Traversal to the types embedded in some object.
  typeOf :: Traversal' a Type
