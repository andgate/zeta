{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasType.Pat where

import           Language.Zeta.Syntax.Classy.HasType.Class (HasType (..))
import           Language.Zeta.Syntax.Data.Pat             (Pat (..))

instance HasType Pat where
  typeOf f = \case
    PType p t -> PType p <$> f t
    PLoc p l  -> PLoc <$> typeOf f p <*> pure l
    PParens p -> PParens <$> typeOf f p
    p         -> pure p
  {-# INLINE typeOf #-}
