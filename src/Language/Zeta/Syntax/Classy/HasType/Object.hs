{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasType.Object where

import           Language.Zeta.Syntax.Classy.HasType.Class (HasType (..))
import           Language.Zeta.Syntax.Data.Object          (Object (..),
                                                            ObjectField (..))

instance HasType t => HasType (Object t) where
  typeOf f (Object l fs) =
    Object l <$> traverse (typeOf f) fs
  {-# INLINE typeOf #-}

instance HasType t => HasType (ObjectField t) where
  typeOf f (ObjectField l n t) =
    ObjectField l n <$> typeOf f t
  {-# INLINE typeOf #-}
