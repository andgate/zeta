module Language.Zeta.Syntax.Classy.HasVars (module X) where

import           Language.Zeta.Syntax.Classy.HasVars.Branch      ()
import           Language.Zeta.Syntax.Classy.HasVars.Class       as X
import           Language.Zeta.Syntax.Classy.HasVars.Clause      ()
import           Language.Zeta.Syntax.Classy.HasVars.Exp         ()
import           Language.Zeta.Syntax.Classy.HasVars.Object      ()
import           Language.Zeta.Syntax.Classy.HasVars.Pat         ()
import           Language.Zeta.Syntax.Classy.HasVars.Scope       ()
import           Language.Zeta.Syntax.Classy.HasVars.TVar        ()
import           Language.Zeta.Syntax.Classy.HasVars.Traversable ()
import           Language.Zeta.Syntax.Classy.HasVars.Type        ()
import           Language.Zeta.Syntax.Classy.HasVars.TypeContext ()
