{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections         #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Abstracts.Exp where

import           Control.Lens.Fold                             (folded)
import           Control.Lens.Operators                        ((%~), (&), (+~),
                                                                (.~), (^.),
                                                                (^..))
import           Control.Lens.Prism                            (_Just)
import           Control.Lens.Traversal                        (Traversal)
import qualified Data.Foldable                                 as Fold
import           Data.Map.Strict                               (Map)
import qualified Data.Map.Strict                               as Map
import           Data.Text                                     (Text)
import           Language.Zeta.Syntax.Classy.Abstracts.Class   (Abstracts (..))
import           Language.Zeta.Syntax.Classy.Replaceable.Class (Replaceable (..))
import           Language.Zeta.Syntax.Classy.Replaceable.Exp   ()
import           Language.Zeta.Syntax.Data.Exp                 (Exp (..))
import           Language.Zeta.Syntax.Data.ExpCache            (getExpCacheHasFree)
import           Language.Zeta.Syntax.Data.Pat                 (Pat)
import           Language.Zeta.Syntax.Data.Scope               (newScope)
import           Language.Zeta.Syntax.Data.Var                 (Var (getVarStr),
                                                                newFreeVar,
                                                                toBoundVars)
import           Language.Zeta.Syntax.Optics.Pat               (patternVars)
import           Language.Zeta.Syntax.Optics.Var               (varBinder)

instance Abstracts Exp Text where
  abstract vs = abstract (newFreeVar Nothing <$> vs)

instance Abstracts Exp Pat where
  abstract ps = abstract vs
    where
      vs = ps ^.. folded . patternVars

instance Abstracts Exp Var where
  abstract vs e = newScope n e'
    where
      n :: Int
      n = length vs

      vList = Fold.toList vs

      vmap :: Map Text Var
      vmap = Map.fromList $ zip (getVarStr <$> vList) (toBoundVars vList)

      bindVar :: (Int, Var) -> Exp
      bindVar (outer, v) =
        case Map.lookup (getVarStr v) vmap of
          Just v' ->
            EVar $
              v & varBinder .~ (v' ^. varBinder)
                & varBinder . _Just +~ outer
          Nothing ->
            EVar v

      replaceFreeOf :: Traversal Exp Exp (Int, Var) Exp
      replaceFreeOf f = replace (const getExpCacheHasFree) f . (0,)

      e' :: Exp
      e' = e & replaceFreeOf %~ bindVar
