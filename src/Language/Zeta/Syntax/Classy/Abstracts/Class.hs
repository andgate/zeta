{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
module Language.Zeta.Syntax.Classy.Abstracts.Class where

import           Language.Zeta.Syntax.Data.Scope (Scope)

-- | Class of types that can perform variable abstraction.
class Abstracts t v where
  -- | Perform variable abstraction.
  abstract :: forall f. (Functor f, Foldable f) => f v -> t -> Scope t
