{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Abstracts.Exp where

import           Data.Text                                   (Text)
import           Language.Zeta.Syntax.Classy.Abstracts.Class (Abstracts (..))
import           Language.Zeta.Syntax.Data.Exp               (Exp)
import           Language.Zeta.Syntax.Data.Pat               (Pat)
import           Language.Zeta.Syntax.Data.Var               (Var)

instance Abstracts Exp Text
instance Abstracts Exp Pat
instance Abstracts Exp Var
