{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections         #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Abstracts.Type where


import           Control.Lens.Operators                        ((%~), (&), (+~),
                                                                (.~), (^.))
import           Control.Lens.Prism                            (_Just)
import           Control.Lens.Traversal                        (Traversal)
import qualified Data.Foldable                                 as Fold
import           Data.Map.Strict                               (Map)
import qualified Data.Map.Strict                               as Map
import           Data.Text                                     (Text)
import           Language.Zeta.Syntax.Classy.Abstracts.Class   (Abstracts (..))
import           Language.Zeta.Syntax.Classy.Replaceable.Class (Replaceable (replace))
import           Language.Zeta.Syntax.Classy.Replaceable.Type  ()
import           Language.Zeta.Syntax.Data.Scope               (newScope)
import           Language.Zeta.Syntax.Data.TVar                (TVar (..),
                                                                newFreeTVar,
                                                                toBoundTVars)
import           Language.Zeta.Syntax.Data.Type                (Type (TVar))
import           Language.Zeta.Syntax.Data.TypeCache           (getTypeCacheHasFree)
import           Language.Zeta.Syntax.Optics.TVar              (tvarBinder)

instance Abstracts Type Text where
  abstract ns = abstract tvs
    where
      tvs = newFreeTVar Nothing <$> ns

instance Abstracts Type TVar where
  abstract tvs t = newScope n t'
    where
      n :: Int
      n = length tvs

      tvList = Fold.toList tvs

      tvmap :: Map Text TVar
      tvmap = Map.fromList $ zip (getTVarStr <$> tvList) (toBoundTVars tvList)

      bindTVar :: (Int, TVar) -> Type
      bindTVar (outer, tv) =
        case Map.lookup (getTVarStr tv) tvmap of
          Just tv' ->
            TVar $
              tv & tvarBinder .~ (tv' ^. tvarBinder)
                 & tvarBinder . _Just +~ outer
          Nothing  ->
            TVar tv

      replaceFreeOf :: Traversal Type Type (Int, TVar) Type
      replaceFreeOf f = replace (const getTypeCacheHasFree) f . (0,)

      t' :: Type
      t' = t & replaceFreeOf %~ bindTVar
