{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.Replaceable.Clause where

import           Language.Zeta.Syntax.Classy.Replaceable.Class       (Replaceable (..))
import           Language.Zeta.Syntax.Classy.Replaceable.Pat         ()
import           Language.Zeta.Syntax.Classy.Replaceable.Scope       ()
import           Language.Zeta.Syntax.Classy.Replaceable.Traversable ()
import           Language.Zeta.Syntax.Data.Clause                    (Clause (..),
                                                                      ClauseBlock (..))
import           Language.Zeta.Syntax.Data.Exp                       (Exp)
import           Language.Zeta.Syntax.Data.ExpCache                  (ExpCache)
import           Language.Zeta.Syntax.Data.Var                       (Var)

instance Replaceable ClauseBlock Exp Var ExpCache where
  replace p f (outer, ClauseBlock m_sp cs) =
    ClauseBlock m_sp <$> replace p f (outer, cs)

instance Replaceable Clause Exp Var ExpCache where
  replace p f (outer, Clause m_sp ps sc) =
    Clause m_sp <$> replace p f (outer, ps) <*> replace p f (outer, sc)
