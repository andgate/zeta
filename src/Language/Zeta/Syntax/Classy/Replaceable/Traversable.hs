{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections         #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Replaceable.Traversable where

import           Language.Zeta.Syntax.Classy.Replaceable.Class (Replaceable (..))

instance {-# OVERLAPPABLE #-} (Traversable f, Replaceable s t v c) => Replaceable (f s) t v c where
  replace p f (outer, t) = traverse (replace p f . (outer,)) t
