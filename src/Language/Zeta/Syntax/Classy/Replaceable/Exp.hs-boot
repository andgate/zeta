{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Replaceable.Exp where

import           Language.Zeta.Syntax.Classy.Replaceable.Class (Replaceable (..))
import           Language.Zeta.Syntax.Data.Exp                 (Exp (..))
import           Language.Zeta.Syntax.Data.ExpCache            (ExpCache)
import           Language.Zeta.Syntax.Data.Var                 (Var)

instance Replaceable Exp Exp Var ExpCache
