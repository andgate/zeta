{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
module Language.Zeta.Syntax.Classy.Replaceable.Class where

import           Control.Lens.Traversal (Traversal)


-- | Term predicate.
-- This is a boolean test on some term t.
type TermPred t = Int -> t -> Bool

-- | Term replacement, with a predicate to guard the traversal.
class Replaceable s t v c where
  -- | Traversal to replace some embedded terms.
  -- Only traverses into terms which satisfy the given predicate.
  replace
    :: TermPred c        -- ^ Term predicate
    -> Traversal (Int, s) s (Int, v) t -- ^ Traversal to replace terms, optimized by the given term predicate.
