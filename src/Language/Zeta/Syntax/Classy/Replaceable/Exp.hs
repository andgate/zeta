{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Replaceable.Exp where

import           Prelude                                             hiding
                                                                     (exp)

import           Control.Lens.Review                                 (review)
import           Language.Zeta.Syntax.Classy.Replaceable.Branch      ()
import           Language.Zeta.Syntax.Classy.Replaceable.Class       (Replaceable (..))
import           Language.Zeta.Syntax.Classy.Replaceable.Clause      ()
import           Language.Zeta.Syntax.Classy.Replaceable.Object      ()
import           Language.Zeta.Syntax.Classy.Replaceable.Pat         ()
import           Language.Zeta.Syntax.Classy.Replaceable.Scope       ()
import           Language.Zeta.Syntax.Classy.Replaceable.Traversable ()
import           Language.Zeta.Syntax.Data.Exp                       (Exp (..))
import           Language.Zeta.Syntax.Data.ExpCache                  (ExpCache)
import           Language.Zeta.Syntax.Data.Var                       (Var)
import           Language.Zeta.Syntax.Optics.Exp                     (_ECache)

instance Replaceable Exp Exp Var ExpCache where
  replace p f (outer, ex) =
    case ex of
      EVar v -> f (outer, v)
      ELit _ -> pure ex
      ECall e xs -> ECall <$> replace p f (outer, e) <*> replace p f (outer,xs)
      ELam ps m_ty sc ->
        ELam
          <$> replace p f (outer, ps)
          <*> pure m_ty
          <*> replace p f (outer, sc)
      EType e ty -> EType <$> replace p f (outer, e) <*> pure ty
      ECast e ty -> ECast <$> replace p f (outer, e) <*> pure ty
      ECache e ec
        | p outer ec ->
            review _ECache <$> replace p f (outer, e)
        | otherwise ->
            pure ex
      ETypeCache e tc -> ETypeCache <$> replace p f (outer, e) <*> pure tc
      ELoc e sp -> ELoc <$> replace p f (outer, e) <*> pure sp
      EParens e -> EParens <$> replace p f (outer, e)
      ERef e -> ERef <$> replace p f (outer, e)
      EDeref e -> EDeref <$> replace p f (outer, e)
      ETuple e ne -> ETuple <$> replace p f (outer, e) <*> replace p f (outer, ne)
      ECon _ -> pure ex
      EArray es -> EArray <$> replace p f (outer, es)
      EObj ob -> EObj <$> replace p f (outer, ob)
      EFree e -> EFree <$> replace p f (outer, e)
      EField _ -> pure ex
      EGet e fn -> EGet <$> replace p f (outer, e) <*> pure fn
      EGetI e e' -> EGetI <$> replace p f (outer, e) <*> replace p f (outer, e')
      ESet e e' -> ESet <$> replace p f (outer, e) <*> replace p f (outer, e')
      EUnOp uo e -> EUnOp uo <$> replace p f (outer, e)
      EBinOp bo e e'  -> EBinOp bo <$> replace p f (outer, e) <*> replace p f (outer, e')
      EBlock e        -> EBlock <$> replace p f (outer, e)
      ESeq e e'       -> ESeq <$> replace p f (outer, e) <*> replace p f (outer, e')
      EBind pt e sc   -> EBind <$> replace p f (outer, pt) <*> replace p f (outer, e) <*> replace p f (outer, sc)
      EIf e e' m_br   -> EIf <$> replace p f (outer, e) <*> replace p f (outer, e') <*> replace p f (outer, m_br)
      ECase e cb      -> ECase <$> replace p f (outer, e) <*> replace p f (outer, cb)
      EFor i c u e    -> EFor <$> replace p f (outer, i) <*> replace p f (outer, c) <*> replace p f (outer, u) <*> replace p f (outer, e)
      EWhile e e'     -> EWhile <$> replace p f (outer, e) <*> replace p f (outer, e')
      EDoWhile e e'   -> EDoWhile <$> replace p f (outer, e) <*> replace p f (outer, e')
      EReturn e       -> EReturn <$> replace p f (outer, e)
      EBreak          -> pure ex
      EContinue       -> pure ex
