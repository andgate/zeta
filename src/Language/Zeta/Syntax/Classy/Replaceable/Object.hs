{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Replaceable.Object where

import           Language.Zeta.Syntax.Classy.Replaceable.Class       (Replaceable (..))
import           Language.Zeta.Syntax.Classy.Replaceable.Traversable ()
import           Language.Zeta.Syntax.Data.Object                    (Object (..),
                                                                      ObjectField (..))

instance {-# OVERLAPS #-} Replaceable s t v c => Replaceable (Object s) t v c where
  replace p f (outer, Object l ofields) =
    Object l <$> replace p f (outer, ofields)

instance {-# OVERLAPS #-} Replaceable s t v c => Replaceable (ObjectField s) t v c where
  replace p f (outer, ObjectField l n t) =
    ObjectField l n <$> replace p f (outer, t)
