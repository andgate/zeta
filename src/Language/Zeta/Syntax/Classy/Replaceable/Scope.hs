{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Replaceable.Scope where

import                          Language.Zeta.Syntax.Classy.Replaceable.Class (Replaceable (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.Replaceable.Exp   ()
import                          Language.Zeta.Syntax.Data.Scope               (Scope (..))

instance {-# OVERLAPS #-} Replaceable t t v c => Replaceable (Scope t) t v c where
  replace p f (outer, Scope lvl t) =
    Scope lvl <$> replace p f (outer + lvl, t)
