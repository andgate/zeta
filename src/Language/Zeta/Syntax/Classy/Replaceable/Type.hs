{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Replaceable.Type where

import           Language.Zeta.Syntax.Classy.Replaceable.Class       (Replaceable (..))
import           Language.Zeta.Syntax.Classy.Replaceable.Scope       ()
import           Language.Zeta.Syntax.Classy.Replaceable.Traversable ()
import           Language.Zeta.Syntax.Data.TVar                      (TVar)
import           Language.Zeta.Syntax.Data.Type                      (Type (..))
import           Language.Zeta.Syntax.Data.TypeCache                 (TypeCache)


instance Replaceable Type Type TVar TypeCache where
  replace p f (outer, typ0) = case typ0 of
    TVar tv      -> f (outer, tv)
    TFunc tys ty -> TFunc <$> replace p f (outer, tys) <*> replace p f (outer, ty)
    TApp ty tys  -> TApp <$> replace p f (outer, ty) <*> replace p f (outer, tys)
    TCon _       -> pure typ0
    TInt _       -> pure typ0
    TUInt _      -> pure typ0
    TFp _        -> pure typ0
    TTuple ty ne -> TTuple <$> replace p f (outer, ty) <*> replace p f (outer, ne)
    TArray n ty  -> TArray n <$> replace p f (outer, ty)
    TPtr ty      -> TPtr <$> replace p f (outer, ty)
    TCtx tc ty   -> TCtx tc <$> replace p f (outer, ty)
    TCache ty tc
      | p outer tc      -> TCache <$> replace p f (outer, ty) <*> pure tc
      | otherwise -> pure typ0
    TLoc ty sp   -> TLoc <$> replace p f (outer, ty) <*> pure sp
    TParens ty   -> TParens <$> replace p f (outer, ty)

