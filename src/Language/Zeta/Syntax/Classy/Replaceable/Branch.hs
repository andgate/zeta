{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.Replaceable.Branch where

import                          Language.Zeta.Syntax.Classy.Replaceable.Class       (Replaceable (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.Replaceable.Exp         ()
import                          Language.Zeta.Syntax.Classy.Replaceable.Traversable ()
import                          Language.Zeta.Syntax.Data.Branch                    (Branch (..))
import                          Language.Zeta.Syntax.Data.Exp                       (Exp)
import                          Language.Zeta.Syntax.Data.ExpCache                  (ExpCache)
import                          Language.Zeta.Syntax.Data.Var                       (Var)

instance Replaceable Branch Exp Var ExpCache where
  replace p f (outer, br) = case br of
    ElseBranch m_sp e ->
      ElseBranch m_sp <$> replace p f (outer, e)
    ElifBranch m_sp e e' m_br ->
      ElifBranch m_sp
        <$> replace p f (outer, e)
        <*> replace p f (outer, e')
        <*> replace p f (outer, m_br)
