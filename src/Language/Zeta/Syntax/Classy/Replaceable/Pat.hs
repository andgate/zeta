{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Replaceable.Pat where

import                          Prelude                                             hiding
                                                                                    (pred)

import                          Control.Lens.Review                                 (review)
import                          Language.Zeta.Syntax.Classy.Replaceable.Class       (Replaceable (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.Replaceable.Exp         ()
import                          Language.Zeta.Syntax.Classy.Replaceable.Object      ()
import                          Language.Zeta.Syntax.Classy.Replaceable.Traversable ()
import                          Language.Zeta.Syntax.Data.Exp                       (Exp)
import                          Language.Zeta.Syntax.Data.ExpCache                  (ExpCache)
import                          Language.Zeta.Syntax.Data.Pat                       (Pat (..))
import                          Language.Zeta.Syntax.Data.Var                       (Var)
import                          Language.Zeta.Syntax.Optics.Pat                     (_PExpCache)

instance Replaceable Pat Exp Var ExpCache where
  replace pred f (outer, pat) = replacedPat
    where
      replacedPat = case pat of
        PVar _      -> pure pat
        PCon pn ps  -> PCon pn <$> replace pred f (outer, ps)
        PTuple p ne -> PTuple <$> replace pred f (outer, p) <*> replace pred f (outer, ne)
        PObj ob     -> PObj <$> replace pred f (outer, ob)
        PLabel pn p -> PLabel pn <$> replace pred f (outer, p)
        PView e p   -> PView <$> replace pred f (outer, e) <*> replace pred f (outer, p)
        PWild       -> pure pat
        PType p ty  -> PType <$> replace pred f (outer, p) <*> pure ty
        PExpCache p ec
          | pred outer ec -> review _PExpCache <$> replace pred f (outer, p)
          | otherwise -> pure pat
        PTypeCache p tc -> PTypeCache <$> replace pred f (outer, p) <*> pure tc
        PLoc p sp   -> PLoc <$> replace pred f (outer, p) <*> pure sp
        PParens p   -> PParens <$> replace pred f (outer, p)
