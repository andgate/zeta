module Language.Zeta.Syntax.Classy.HasSize (module X) where

import           Language.Zeta.Syntax.Classy.HasSize.Class    as X
import           Language.Zeta.Syntax.Classy.HasSize.TypeDefn ()
