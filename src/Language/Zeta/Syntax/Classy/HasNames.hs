module Language.Zeta.Syntax.Classy.HasNames (module X) where

import           Language.Zeta.Syntax.Classy.HasNames.Branch       ()
import           Language.Zeta.Syntax.Classy.HasNames.Class        as X
import           Language.Zeta.Syntax.Classy.HasNames.Clause       ()
import           Language.Zeta.Syntax.Classy.HasNames.Exp          ()
import           Language.Zeta.Syntax.Classy.HasNames.ExternDefn   ()
import           Language.Zeta.Syntax.Classy.HasNames.FuncDefn     ()
import           Language.Zeta.Syntax.Classy.HasNames.Module       ()
import           Language.Zeta.Syntax.Classy.HasNames.ModuleHeader ()
import           Language.Zeta.Syntax.Classy.HasNames.ModuleStmt   ()
import           Language.Zeta.Syntax.Classy.HasNames.Object       ()
import           Language.Zeta.Syntax.Classy.HasNames.Operator     ()
import           Language.Zeta.Syntax.Classy.HasNames.Pat          ()
import           Language.Zeta.Syntax.Classy.HasNames.Traversable  ()
import           Language.Zeta.Syntax.Classy.HasNames.Type         ()
import           Language.Zeta.Syntax.Classy.HasNames.TypeContext  ()
import           Language.Zeta.Syntax.Classy.HasNames.TypeDefn     ()
