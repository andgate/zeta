{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasExpCache.Pat where

import                          Language.Zeta.Syntax.Classy.HasExpCache.Class    (HasExpCache (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasExpCache.Exp      ()
import                          Language.Zeta.Syntax.Classy.HasExpCache.Foldable ()
import                          Language.Zeta.Syntax.Classy.HasExpCache.Object   ()
import                          Language.Zeta.Syntax.Data.Pat                    (Pat (..))

instance HasExpCache Pat where
  getExpCache pt =
    case pt of
      PVar _           -> mempty
      PCon _ pats      -> getExpCache pats
      PTuple pat ne    -> getExpCache pat <> getExpCache ne
      PObj ob          -> getExpCache ob
      PLabel _ pat     -> getExpCache pat
      PView e pat      -> getExpCache e <> getExpCache pat
      PWild            -> mempty
      PType pat _      -> getExpCache pat
      PExpCache _ ec   -> ec
      PTypeCache pat _ -> getExpCache pat
      PLoc pat _       -> getExpCache pat
      PParens pat      -> getExpCache pat
