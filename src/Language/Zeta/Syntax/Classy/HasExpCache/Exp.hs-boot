{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasExpCache.Exp where

import           Language.Zeta.Syntax.Classy.HasExpCache.Class (HasExpCache (..))
import           Language.Zeta.Syntax.Data.Exp                 (Exp)

instance HasExpCache Exp
