{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasExpCache.Var where

import           Language.Zeta.Syntax.Classy.HasExpCache.Class (HasExpCache (..))
import           Language.Zeta.Syntax.Data.ExpCache            (newExpCache)
import           Language.Zeta.Syntax.Data.Var                 (Var (..))

instance HasExpCache Var where
  getExpCache v =
    case getVarBinder v of
      Nothing -> newExpCache True (-1)
      Just b  -> newExpCache False b
