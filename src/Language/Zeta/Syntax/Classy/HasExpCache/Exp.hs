{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasExpCache.Exp where

import           Language.Zeta.Syntax.Classy.HasExpCache.Branch   ()
import           Language.Zeta.Syntax.Classy.HasExpCache.Class    (HasExpCache (..))
import           Language.Zeta.Syntax.Classy.HasExpCache.Clause   ()
import           Language.Zeta.Syntax.Classy.HasExpCache.Foldable ()
import           Language.Zeta.Syntax.Classy.HasExpCache.Pat      ()
import           Language.Zeta.Syntax.Classy.HasExpCache.Var      ()
import           Language.Zeta.Syntax.Data.Exp                    (Exp (..))

instance HasExpCache Exp where
  getExpCache ex =
    case ex of
      EVar var       -> getExpCache var
      ELit _         -> mempty
      ECall e es     -> getExpCache e <> getExpCache es
      ELam pats _ sc -> getExpCache pats <> getExpCache sc
      EType e _      -> getExpCache e
      ECast e _      -> getExpCache e
      ECache _ ec    -> ec
      ETypeCache e _ -> getExpCache e
      ELoc e _       -> getExpCache e
      EParens e      -> getExpCache e
      ERef e         -> getExpCache e
      EDeref e       -> getExpCache e
      ETuple e ne    -> getExpCache e <> getExpCache ne
      ECon _         -> mempty
      EArray es      -> getExpCache es
      EObj ob        -> getExpCache ob
      EFree e        -> getExpCache e
      EField _       -> mempty
      EGet e _       -> getExpCache e
      EGetI e e'     -> getExpCache e <> getExpCache e'
      ESet e e'      -> getExpCache e <> getExpCache e'
      EUnOp _ e      -> getExpCache e
      EBinOp _ e e'  -> getExpCache e <> getExpCache e'
      EBlock e       -> getExpCache e
      ESeq e e'      -> getExpCache e <> getExpCache e'
      EBind p e sc   -> getExpCache p <> getExpCache e <> getExpCache sc
      EIf p e m_br   -> getExpCache p <> getExpCache e <> getExpCache m_br
      ECase e cb     -> getExpCache e <> getExpCache cb
      EFor i c u e   -> getExpCache i <> getExpCache c <> getExpCache u <> getExpCache e
      EWhile e e'    -> getExpCache e <> getExpCache e'
      EDoWhile e e'  -> getExpCache e <> getExpCache e'
      EReturn e      -> getExpCache e
      EBreak         -> mempty
      EContinue      -> mempty
