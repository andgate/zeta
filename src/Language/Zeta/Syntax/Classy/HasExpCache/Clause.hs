{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasExpCache.Clause where

import                          Language.Zeta.Syntax.Classy.HasExpCache.Class    (HasExpCache (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasExpCache.Exp      ()
import                          Language.Zeta.Syntax.Classy.HasExpCache.Foldable ()
import                          Language.Zeta.Syntax.Classy.HasExpCache.Pat      ()
import                          Language.Zeta.Syntax.Data.Clause                 (Clause (..),
                                                                                  ClauseBlock (..))

instance HasExpCache ClauseBlock where
  getExpCache (ClauseBlock _ cs) =
    getExpCache cs

instance HasExpCache Clause where
  getExpCache (Clause _ ps e) =
    getExpCache ps <> getExpCache e
