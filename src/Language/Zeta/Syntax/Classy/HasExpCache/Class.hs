{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Language.Zeta.Syntax.Classy.HasExpCache.Class where

import           Language.Zeta.Syntax.Data.ExpCache (ExpCache)

class HasExpCache t where
  getExpCache :: t -> ExpCache
