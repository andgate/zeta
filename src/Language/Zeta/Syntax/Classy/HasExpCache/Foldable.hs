{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasExpCache.Foldable where

import           Language.Zeta.Syntax.Classy.HasExpCache.Class (HasExpCache (..))

instance {-# OVERLAPPABLE #-} (Foldable f, HasExpCache t) => HasExpCache (f t) where
  getExpCache = foldMap getExpCache
