{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasExpCache.Object where

import           Language.Zeta.Syntax.Classy.HasExpCache.Class (HasExpCache (..))
import           Language.Zeta.Syntax.Data.Object              (Object (..),
                                                                ObjectField (..))

instance (HasExpCache t) => HasExpCache (Object t) where
  getExpCache (Object _ flds) = foldMap getExpCache flds

instance (HasExpCache t) => HasExpCache (ObjectField t) where
  getExpCache (ObjectField _ _ t) = getExpCache t
