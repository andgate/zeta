{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasExpCache.Branch where

import                          Language.Zeta.Syntax.Classy.HasExpCache.Class    (HasExpCache (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasExpCache.Exp      ()
import                          Language.Zeta.Syntax.Classy.HasExpCache.Foldable ()
import                          Language.Zeta.Syntax.Data.Branch                 (Branch (..))

instance HasExpCache Branch where
  getExpCache = \case
    ElseBranch _ e ->
      getExpCache e
    ElifBranch _ p e m_br ->
      getExpCache p <> getExpCache e <> getExpCache m_br
