module Language.Zeta.Syntax.Classy.Alpha (module X) where

import           Language.Zeta.Syntax.Classy.Alpha.Class as X
import           Language.Zeta.Syntax.Classy.Alpha.Exp   ()
import           Language.Zeta.Syntax.Classy.Alpha.Pat   ()
import           Language.Zeta.Syntax.Classy.Alpha.Type  ()
