module Language.Zeta.Syntax.Classy.Instantiable (module X) where

import           Language.Zeta.Syntax.Classy.Instantiable.Class as X
import           Language.Zeta.Syntax.Classy.Instantiable.Exp   ()
import           Language.Zeta.Syntax.Classy.Instantiable.Type  ()
