module Language.Zeta.Syntax.Classy.Replaceable (module X) where

import           Language.Zeta.Syntax.Classy.Replaceable.Branch      ()
import           Language.Zeta.Syntax.Classy.Replaceable.Class       as X
import           Language.Zeta.Syntax.Classy.Replaceable.Clause      ()
import           Language.Zeta.Syntax.Classy.Replaceable.Exp         ()
import           Language.Zeta.Syntax.Classy.Replaceable.Object      ()
import           Language.Zeta.Syntax.Classy.Replaceable.Pat         ()
import           Language.Zeta.Syntax.Classy.Replaceable.Scope       ()
import           Language.Zeta.Syntax.Classy.Replaceable.Traversable ()
import           Language.Zeta.Syntax.Classy.Replaceable.Type        ()
