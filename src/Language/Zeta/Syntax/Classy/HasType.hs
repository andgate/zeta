module Language.Zeta.Syntax.Classy.HasType (module X) where

import           Language.Zeta.Syntax.Classy.HasType.Class  as X
import           Language.Zeta.Syntax.Classy.HasType.Exp    ()
import           Language.Zeta.Syntax.Classy.HasType.Object ()
import           Language.Zeta.Syntax.Classy.HasType.Pat    ()
