{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE RankNTypes      #-}
module Language.Zeta.Syntax.Classy.Bound.Class where

import           Language.Zeta.Syntax.Classy.Abstracts.Class    (Abstracts (..))
import           Language.Zeta.Syntax.Classy.Instantiable.Class (Instantiable (..))

-- | Bound class constraint.
-- This is anything that implements the 'Abstract' and 'Instantiable' class.
type Bound t v = (Abstracts t v, Instantiable t t)

-- | Generalized substition on 'Bound' instances.
subst
  :: forall t v. (Bound t v)
  => [(v, t)] -- ^ Some list of equations to substitute.
  -> t        -- ^ Some term to perform substitution on.
  -> t        -- ^ New term with equations substituted in.
subst eqs = instantiate ts . abstract vs
  where
    (vs, ts) = unzip eqs
