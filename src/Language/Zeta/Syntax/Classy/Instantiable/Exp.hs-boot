{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Instantiable.Exp where

import           Language.Zeta.Syntax.Classy.Instantiable.Class (Instantiable (..))
import           Language.Zeta.Syntax.Data.Exp                  (Exp)
import           Language.Zeta.Syntax.Data.Pat                  (Pat)
import           Language.Zeta.Syntax.Data.Var                  (Var)

instance Instantiable Exp Pat
instance Instantiable Exp Var
instance Instantiable Exp Exp
