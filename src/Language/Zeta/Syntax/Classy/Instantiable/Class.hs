{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
module Language.Zeta.Syntax.Classy.Instantiable.Class where

import           Language.Zeta.Syntax.Data.Scope (Scope)

-- | Class of types that can perform some sort of value instantiation
-- on bound variables in a scoped term.
class Instantiable t v where
  -- | Instantiate some values inside a scoped term.
  instantiate :: forall f. (Functor f, Foldable f) => f v -> Scope t -> t
