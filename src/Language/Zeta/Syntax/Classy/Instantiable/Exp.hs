{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections         #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Instantiable.Exp where

import           Control.Lens.At                                (ix)
import           Control.Lens.Fold                              (folded)
import           Control.Lens.Operators                         ((%~), (&),
                                                                 (^..), (^?))
import           Control.Lens.Review                            (re)
import           Control.Lens.Traversal                         (Traversal)
import qualified Data.Foldable                                  as Fold
import           Data.Map.Strict                                (Map)
import qualified Data.Map.Strict                                as Map
import           Data.Text                                      (Text)
import           Language.Zeta.Syntax.Classy.Instantiable.Class (Instantiable (..))
import           Language.Zeta.Syntax.Classy.Replaceable.Class  (Replaceable (..))
import           Language.Zeta.Syntax.Classy.Replaceable.Exp    ()
import           Language.Zeta.Syntax.Data.Exp                  (Exp (EVar))
import           Language.Zeta.Syntax.Data.ExpCache             (ExpCache,
                                                                 isInExpCacheBounds)
import           Language.Zeta.Syntax.Data.Pat                  (Pat)
import           Language.Zeta.Syntax.Data.Scope                (Scope (..))
import           Language.Zeta.Syntax.Data.Var                  (Var (..),
                                                                 newFreeVar)
import           Language.Zeta.Syntax.Optics.Exp                (_EVar)
import           Language.Zeta.Syntax.Optics.Pat                (patternVars)

instance Instantiable Exp Text where
  instantiate vs = instantiate (newFreeVar Nothing <$> vs)
  {-# INLINABLE instantiate #-}

instance Instantiable Exp Pat where
  instantiate ps = instantiate vs
    where
      vs = ps ^.. folded . patternVars . re _EVar
  {-# INLINABLE instantiate #-}

instance Instantiable Exp Var where
  instantiate vs =
    instantiate (EVar <$> vs)
  {-# INLINABLE instantiate #-}

instance Instantiable Exp Exp where
  instantiate es sc = e'
    where
      bmap :: Map Int Exp
      bmap = Map.fromList $ zip [0..] (Fold.toList es)

      bigB :: Int
      bigB = getScopeLevel sc

      e' :: Exp
      e' = getScopeBody sc & replaceBoundsOf %~ substBoundVar

      replaceBoundsOf :: Traversal Exp Exp (Int, Var) Exp
      replaceBoundsOf f = replace isInBounds f . (0,)

      isInBounds :: Int -> ExpCache -> Bool
      isInBounds outer ec = isInExpCacheBounds ec outer (outer + bigB)

      substBoundVar :: (Int, Var) -> Exp
      substBoundVar (outer, v) =
        case getVarBinder v of
          Just b
            | b >= outer  ->
              substBinder v (b - outer)
            | otherwise ->
              EVar v
          Nothing -> EVar v

      substBinder :: Var -> Int -> Exp
      substBinder v b =
        case bmap ^? ix b of
          Just ex -> ex
          Nothing -> EVar v
