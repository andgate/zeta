{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections         #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Instantiable.Type where

import           Control.Lens.At                                (ix)
import           Control.Lens.Operators                         ((%~), (&),
                                                                 (^?))
import           Control.Lens.Traversal                         (Traversal)
import qualified Data.Foldable                                  as Fold
import           Data.Map.Strict                                (Map)
import qualified Data.Map.Strict                                as Map
import           Data.Text                                      (Text)
import           Language.Zeta.Syntax.Classy.Instantiable.Class (Instantiable (..))
import           Language.Zeta.Syntax.Classy.Replaceable.Class  (Replaceable (..))
import           Language.Zeta.Syntax.Classy.Replaceable.Type   ()
import           Language.Zeta.Syntax.Data.Scope                (getScopeBody,
                                                                 getScopeLevel)
import           Language.Zeta.Syntax.Data.TVar                 (TVar (getTVarBinder),
                                                                 newFreeTVar)
import           Language.Zeta.Syntax.Data.Type                 (Type (TVar))
import           Language.Zeta.Syntax.Data.TypeCache            (TypeCache,
                                                                 isInTypeCacheBounds)


instance Instantiable Type Text where
  instantiate vs = instantiate (newFreeTVar Nothing <$> vs)
  {-# INLINABLE instantiate #-}

instance Instantiable Type TVar where
  instantiate tvs = instantiate (TVar <$> tvs)

instance Instantiable Type Type where
  instantiate ts sc = t'
    where
      bmap :: Map Int Type
      bmap = Map.fromList $ zip [0..] (Fold.toList ts)

      bigB :: Int
      bigB = getScopeLevel sc

      t' :: Type
      t' = getScopeBody sc & replaceBoundsOf %~ substBoundTVar

      replaceBoundsOf :: Traversal Type Type (Int, TVar) Type
      replaceBoundsOf f = replace isInBounds f . (0,)

      isInBounds :: Int -> TypeCache -> Bool
      isInBounds outer tc = isInTypeCacheBounds tc outer (outer + bigB)

      substBoundTVar :: (Int, TVar) -> Type
      substBoundTVar (outer, tv) =
        case getTVarBinder tv of
          Just b
            | b >= outer ->
              substBinder tv b
            | otherwise ->
              TVar tv
          Nothing -> TVar tv

      substBinder :: TVar -> Int -> Type
      substBinder tv b =
        case bmap ^? ix b of
          Just ex -> ex
          Nothing -> TVar tv
