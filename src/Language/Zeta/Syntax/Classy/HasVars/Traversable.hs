{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasVars.Traversable where

import           Language.Zeta.Syntax.Classy.HasVars.Class (HasVars (..))

instance {-# OVERLAPPABLE #-} (Traversable f, HasVars t v) => HasVars (f t) v where
  vars f = traverse (vars f)
