{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasVars.TypeContext where

import                          Language.Zeta.Syntax.Classy.HasVars.Class       (HasVars (..))
import                          Language.Zeta.Syntax.Classy.HasVars.TVar        ()
import                          Language.Zeta.Syntax.Classy.HasVars.Traversable ()
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasVars.Type        ()
import                          Language.Zeta.Syntax.Data.TVar                  (TVar)
import                          Language.Zeta.Syntax.Data.TypeContext           (ClassConstraint (..),
                                                                                 ClassInstance (..),
                                                                                 TypeContext (..),
                                                                                 TypeContextElement (..),
                                                                                 TypeSchema (..))

instance HasVars TypeContext TVar where
  vars f (TypeContext l es) =
    TypeContext l <$> vars f es

instance HasVars TypeContextElement TVar where
  vars f = \case
    TypeContextSchema sc ->
      TypeContextSchema <$> vars f sc
    TypeContextConstraint cc ->
      TypeContextConstraint <$> vars f cc

instance HasVars TypeSchema TVar where
  vars f (TypeSchema l tvs) =
    TypeSchema l <$> vars f tvs

instance HasVars ClassConstraint TVar where
  vars f (ClassConstraint l insts) =
    ClassConstraint l <$> vars f insts

instance HasVars ClassInstance TVar where
  vars f (ClassInstance l n tys) =
    ClassInstance l n <$> vars f tys
