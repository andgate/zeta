{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasVars.Object where

import           Language.Zeta.Syntax.Classy.HasVars.Class       (HasVars (..))
import           Language.Zeta.Syntax.Classy.HasVars.Traversable ()
import           Language.Zeta.Syntax.Data.Object                (Object (..),
                                                                  ObjectField (..))

instance (HasVars t v) => HasVars (Object t) v where
  vars f (Object l fs) =
    Object l <$> vars f fs

instance (HasVars t v) => HasVars (ObjectField t) v where
  vars f (ObjectField l n t) =
    ObjectField l n <$> vars f t
