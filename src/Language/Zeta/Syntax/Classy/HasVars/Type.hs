{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasVars.Type where

import           Language.Zeta.Syntax.Classy.HasVars.Class       (HasVars (..))
import           Language.Zeta.Syntax.Classy.HasVars.Traversable ()
import           Language.Zeta.Syntax.Classy.HasVars.TypeContext ()
import           Language.Zeta.Syntax.Data.TVar                  (TVar)
import           Language.Zeta.Syntax.Data.Type                  (Type (..))

instance HasVars Type TVar where
  vars f typ = case typ of
    TVar tv      -> TVar <$> f tv
    TFunc tys ty -> TFunc <$> vars f tys <*> vars f ty
    TApp ty tys  -> TApp <$> vars f ty <*> vars f tys
    TCon _       -> pure typ
    TInt _       -> pure typ
    TUInt _      -> pure typ
    TFp _        -> pure typ
    TTuple ty ne -> TTuple <$> vars f ty <*> vars f ne
    TArray n ty  -> TArray n <$> vars f ty
    TPtr ty      -> TPtr <$> vars f ty
    TCtx tc ty   -> TCtx <$> vars f tc <*> vars f ty
    TCache ty tc -> TCache <$> vars f ty <*> pure tc
    TLoc ty sp   -> TLoc <$> vars f ty <*> pure sp
    TParens ty   -> TParens <$> vars f ty
