{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances  #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasVars.Pat where

import                          Data.List.NonEmpty                              ()
import                          Language.Zeta.Syntax.Classy.HasVars.Class       (HasVars (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasVars.Exp         ()
import                          Language.Zeta.Syntax.Classy.HasVars.Object      ()
import                          Language.Zeta.Syntax.Classy.HasVars.Traversable ()
import                          Language.Zeta.Syntax.Data.Pat                   (Pat (..))
import                          Language.Zeta.Syntax.Data.Var                   (Var)

instance HasVars Pat Var where
  vars f = \case
    PVar n ->
      pure $ PVar n
    PCon n ps ->
      PCon n <$> vars f ps
    PTuple p ps ->
      PTuple <$> vars f p <*> vars f ps
    PObj o ->
      PObj <$> vars f o
    PLabel n p ->
      PLabel n <$> vars f p
    PView e p ->
      PView <$> vars f e <*> vars f p
    PWild ->
      pure PWild
    PType p t ->
      PType <$> vars f p <*> pure t
    PExpCache p ec ->
      PExpCache <$> vars f p <*> pure ec
    PTypeCache p tc ->
      PTypeCache <$> vars f p <*> pure tc
    PLoc p l ->
      PLoc <$> vars f p <*> pure l
    PParens p ->
      PParens <$> vars f p
