{-# LANGUAGE MultiParamTypeClasses #-}
module Language.Zeta.Syntax.Classy.HasVars.Class where

import           Control.Lens.Traversal (Traversal')

-- | Classy variable carriers.
class HasVars t v where
  -- | Traversal to the variables embedded in some object.
  vars :: Traversal' t v
