{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasVars.Clause where

import           Language.Zeta.Syntax.Classy.HasVars.Class       (HasVars (..))
import           Language.Zeta.Syntax.Classy.HasVars.Pat         ()
import           Language.Zeta.Syntax.Classy.HasVars.Scope       ()
import           Language.Zeta.Syntax.Classy.HasVars.Traversable ()
import           Language.Zeta.Syntax.Data.Clause                (Clause (..),
                                                                  ClauseBlock (..))
import           Language.Zeta.Syntax.Data.Var                   (Var)


instance HasVars ClauseBlock Var where
  vars f (ClauseBlock m_sp cs) =
    ClauseBlock m_sp <$> vars f cs

instance HasVars Clause Var where
  vars f (Clause m_sp ps sc) =
    Clause m_sp <$> vars f ps <*> vars f sc
