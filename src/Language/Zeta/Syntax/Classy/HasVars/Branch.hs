{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasVars.Branch where


import                          Language.Zeta.Syntax.Classy.HasVars.Class       (HasVars (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasVars.Exp         ()
import                          Language.Zeta.Syntax.Classy.HasVars.Traversable ()
import                          Language.Zeta.Syntax.Data.Branch                (Branch (..))
import                          Language.Zeta.Syntax.Data.Var                   (Var)

instance HasVars Branch Var where
  vars f = \case
    ElseBranch m_sp e ->
      ElseBranch m_sp <$> vars f e
    ElifBranch m_sp e e' m_br ->
      ElifBranch m_sp <$> vars f e <*> vars f e' <*> vars f m_br
