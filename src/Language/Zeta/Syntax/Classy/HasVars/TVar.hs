{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasVars.TVar where

import           Language.Zeta.Syntax.Classy.HasVars.Class (HasVars (..))
import           Language.Zeta.Syntax.Data.TVar            (TVar)

instance HasVars TVar TVar where
  vars = id
