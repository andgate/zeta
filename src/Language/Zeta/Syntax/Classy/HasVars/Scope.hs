{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections         #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasVars.Scope where

import           Language.Zeta.Syntax.Classy.HasVars.Class (HasVars (..))
import           Language.Zeta.Syntax.Data.Scope           (Scope (..))

instance HasVars t v => HasVars (Scope t) v where
  vars f (Scope bnd body) = Scope bnd <$> vars f body
