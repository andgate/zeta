{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasVars.Exp where

import           Data.List.NonEmpty                              ()
import           Language.Zeta.Syntax.Classy.HasVars.Branch      ()
import           Language.Zeta.Syntax.Classy.HasVars.Class       (HasVars (..))
import           Language.Zeta.Syntax.Classy.HasVars.Clause      ()
import           Language.Zeta.Syntax.Classy.HasVars.Object      ()
import           Language.Zeta.Syntax.Classy.HasVars.Pat         ()
import           Language.Zeta.Syntax.Classy.HasVars.Scope       ()
import           Language.Zeta.Syntax.Classy.HasVars.Traversable ()
import           Language.Zeta.Syntax.Data.Exp                   (Exp (..))
import           Language.Zeta.Syntax.Data.Var                   (Var)

instance HasVars Exp Var where
  vars f ex =
    case ex of
      EVar v          -> EVar <$> f v
      ELit _          -> pure ex
      ECall fn es     -> ECall <$> vars f fn <*> vars f es
      ELam ps mty s   -> ELam <$> vars f ps <*> pure mty <*> vars f s
      EType e ty      -> EType <$> vars f e <*> pure ty
      ECast e ty      -> ECast <$> vars f e <*> pure ty
      ECache e ec     -> ECache <$> vars f e <*> pure ec
      ETypeCache e tc -> ETypeCache <$> vars f e <*> pure tc
      ELoc e sp       -> ELoc <$> vars f e <*> pure sp
      EParens e       -> EParens <$> vars f e
      ERef e          -> ERef <$> vars f e
      EDeref e        -> EDeref <$> vars f e
      ETuple e ne     -> ETuple <$> vars f e <*> vars f ne
      ECon _          -> pure ex
      EArray es       -> EArray <$> vars f es
      EObj o          -> EObj <$> vars f o
      EFree e         -> EFree <$> vars f e
      EField _        -> pure ex
      EGet e fn       -> EGet <$> vars f e <*> pure fn
      EGetI e e'      -> EGetI <$> vars f e <*> vars f e'
      ESet e e'       -> ESet <$> vars f e <*> vars f e'
      EUnOp uo e      -> EUnOp uo <$> vars f e
      EBinOp bo e e'  -> EBinOp bo <$> vars f e <*> vars f e'
      EBlock e        -> EBlock <$> vars f e
      ESeq e e'       -> ESeq <$> vars f e <*> vars f e'
      EBind p e sc    -> EBind <$> vars f p <*> vars f e <*> vars f sc
      EIf e e' m_br   -> EIf <$> vars f e <*> vars f e' <*> vars f m_br
      ECase e cb      -> ECase <$> vars f e <*> vars f cb
      EFor i c u e    -> EFor <$> vars f i <*> vars f c <*> vars f u <*> vars f e
      EWhile e e'     -> EWhile <$> vars f e <*> vars f e'
      EDoWhile e e'   -> EDoWhile <$> vars f e <*> vars f e'
      EReturn e       -> EReturn <$> vars f e
      EBreak          -> pure ex
      EContinue       -> pure ex
