{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasVars.Exp where

import           Language.Zeta.Syntax.Classy.HasVars.Class (HasVars)
import           Language.Zeta.Syntax.Data.Exp             (Exp)
import           Language.Zeta.Syntax.Data.Var             (Var)

instance HasVars Exp Var
