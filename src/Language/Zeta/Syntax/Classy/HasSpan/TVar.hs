{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.TVar where

import           Prelude                        hiding (span)

import           Document.Classy.HasSpan        (HasSpan (..))
import           Language.Zeta.Syntax.Data.TVar (TVar (getTVarLoc), setTVarLoc)

instance HasSpan TVar where
  span f tv =
    setTVarLoc tv <$> span f (getTVarLoc tv )
  {-# INLINABLE span #-}
