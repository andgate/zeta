{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Branch where

import                          Prelude                                 hiding
                                                                        (span)

import                          Document.Classy.HasSpan                 (HasSpan (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasSpan.Exp ()
import                          Language.Zeta.Syntax.Data.Branch        (Branch (..))

instance HasSpan Branch where
  {-# INLINABLE span #-}
  span f = \case
    ElseBranch m_sp e ->
      ElseBranch <$> span f m_sp <*> span f e
    ElifBranch m_sp e e' m_br ->
      ElifBranch
        <$> span f m_sp
        <*> span f e
        <*> span f e'
        <*> span f m_br
