{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Name.ImportName where

import           Prelude                                   hiding (span)

import           Document.Classy.HasSpan                   (HasSpan (..))
import           Language.Zeta.Syntax.Data.Name.ImportName (ImportName (getImportNameLoc),
                                                            setImportNameLoc)

instance HasSpan ImportName where
  span f n =
    setImportNameLoc n <$> span f (getImportNameLoc n)
  {-# INLINABLE span #-}
