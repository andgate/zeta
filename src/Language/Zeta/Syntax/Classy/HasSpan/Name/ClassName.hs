{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Name.ClassName where

import           Prelude                                  hiding (span)

import           Document.Classy.HasSpan                  (HasSpan (..))
import           Language.Zeta.Syntax.Data.Name.ClassName (ClassName (getClassNameLoc),
                                                           setClassNameLoc)

instance HasSpan ClassName where
  span f n =
    setClassNameLoc n <$> span f (getClassNameLoc n)
  {-# INLINABLE span #-}
