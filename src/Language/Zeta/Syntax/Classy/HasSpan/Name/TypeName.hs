{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Name.TypeName where

import           Prelude                                 hiding (span)

import           Document.Classy.HasSpan                 (HasSpan (..))
import           Language.Zeta.Syntax.Data.Name.TypeName (TypeName (getTypeNameLoc),
                                                          setTypeNameLoc)

instance HasSpan TypeName where
  span f n =
    setTypeNameLoc n <$> span f (getTypeNameLoc n)
  {-# INLINABLE span #-}
