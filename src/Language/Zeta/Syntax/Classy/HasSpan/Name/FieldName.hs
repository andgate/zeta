{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Name.FieldName where

import           Prelude                                  hiding (span)

import           Document.Classy.HasSpan                  (HasSpan (..))
import           Language.Zeta.Syntax.Data.Name.FieldName (FieldName (getFieldNameLoc),
                                                           setFieldNameLoc)

instance HasSpan FieldName where
  span f n =
    setFieldNameLoc n <$> span f (getFieldNameLoc n)
  {-# INLINABLE span #-}
