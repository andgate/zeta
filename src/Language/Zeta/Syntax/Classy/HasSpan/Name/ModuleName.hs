{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Name.ModuleName where

import           Prelude                                   hiding (span)

import           Document.Classy.HasSpan                   (HasSpan (..))
import           Language.Zeta.Syntax.Data.Name.ModuleName (ModuleName (getModuleNameLoc),
                                                            setModuleNameLoc)

instance HasSpan ModuleName where
  span f n =
    setModuleNameLoc n <$> span f (getModuleNameLoc n)
  {-# INLINABLE span #-}
