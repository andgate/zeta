{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Name.FunctionName where

import           Prelude                                     hiding (span)

import           Document.Classy.HasSpan                     (HasSpan (..))
import           Language.Zeta.Syntax.Data.Name.FunctionName (FunctionName (getFunctionNameLoc),
                                                              setFunctionNameLoc)

instance HasSpan FunctionName where
  span f n =
    setFunctionNameLoc n <$> span f (getFunctionNameLoc n)
  {-# INLINABLE span #-}
