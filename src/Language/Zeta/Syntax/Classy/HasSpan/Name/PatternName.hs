{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Name.PatternName where

import           Prelude                                    hiding (span)

import           Document.Classy.HasSpan                    (HasSpan (..))
import           Language.Zeta.Syntax.Data.Name.PatternName (PatternName (getPatternNameLoc),
                                                             setPatternNameLoc)

instance HasSpan PatternName where
  span f n =
    setPatternNameLoc n <$> span f (getPatternNameLoc n)
  {-# INLINABLE span #-}
