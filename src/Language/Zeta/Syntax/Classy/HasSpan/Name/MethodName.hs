{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Name.MethodName where

import           Prelude                                   hiding (span)

import           Document.Classy.HasSpan                   (HasSpan (..))
import           Language.Zeta.Syntax.Data.Name.MethodName (MethodName (getMethodNameLoc),
                                                            setMethodNameLoc)

instance HasSpan MethodName where
  span f n =
    setMethodNameLoc n <$> span f (getMethodNameLoc n)
  {-# INLINABLE span #-}
