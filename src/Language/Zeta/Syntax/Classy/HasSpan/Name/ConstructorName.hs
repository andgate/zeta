{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Name.ConstructorName where

import           Prelude                                        hiding (span)

import           Document.Classy.HasSpan                        (HasSpan (..))
import           Language.Zeta.Syntax.Data.Name.ConstructorName (ConstructorName (getConstructorNameLoc),
                                                                 setConstructorNameLoc)

instance HasSpan ConstructorName where
  span f n =
    setConstructorNameLoc n <$> span f (getConstructorNameLoc n)
  {-# INLINABLE span #-}
