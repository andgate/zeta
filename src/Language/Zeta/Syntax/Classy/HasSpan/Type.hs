{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Type where

import           Prelude                                         hiding (span)

import           Document.Classy.HasSpan                         (HasSpan (..))
import           Language.Zeta.Syntax.Classy.HasSpan.TVar        ()
import           Language.Zeta.Syntax.Classy.HasSpan.TypeContext ()
import           Language.Zeta.Syntax.Data.Type                  (Type (..))

instance HasSpan Type where
  span f typ = case typ of
    TVar tv      -> TVar <$> span f tv
    TFunc tys ty -> TFunc <$> span f tys <*> span f ty
    TApp ty tys  -> TApp <$> span f ty <*> span f tys
    TCon _       -> pure typ
    TInt _       -> pure typ
    TUInt _      -> pure typ
    TFp _        -> pure typ
    TTuple ty ne -> TTuple <$> span f ty <*> span f ne
    TArray n ty  -> TArray n <$> span f ty
    TPtr ty      -> TPtr <$> span f ty
    TCtx tc ty   -> TCtx <$> span f tc <*> span f ty
    TCache ty tc -> TCache <$> span f ty <*> pure tc
    TLoc ty sp   -> TLoc ty <$> f sp
    TParens ty   -> TParens <$> span f ty
  {-# INLINABLE span #-}
