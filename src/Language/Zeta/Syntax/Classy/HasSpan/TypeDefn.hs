{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.TypeDefn where

import           Document.Classy.HasSpan            (HasSpan (..))
import           Language.Zeta.Syntax.Data.TypeDefn (TypeDefn (..))

instance HasSpan TypeDefn where
  span = undefined
  {-# INLINABLE span #-}

-- instance HasSpan ConstrDefn where
--   locOf = \case
--     ConstrDefn l _ _ -> l
--     RecordDefn l _ _ -> l
