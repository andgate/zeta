{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Exp where

import           Document.Classy.HasSpan       (HasSpan (..))
import           Language.Zeta.Syntax.Data.Exp (Exp)

instance HasSpan Exp
