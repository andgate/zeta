{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Var where

import           Prelude                       hiding (span)

import           Document.Classy.HasSpan       (HasSpan (..))
import           Language.Zeta.Syntax.Data.Var (Var (getVarLoc), setVarLoc)

instance HasSpan Var where
  span f v =
    setVarLoc v <$> span f (getVarLoc v)
  {-# INLINABLE span #-}
