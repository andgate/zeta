{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Object where

import           Prelude                                            hiding
                                                                    (span)

import           Document.Classy.HasSpan                            (HasSpan (..))
import           Language.Zeta.Syntax.Classy.HasSpan.Name.FieldName ()
import           Language.Zeta.Syntax.Data.Object                   (Object (..),
                                                                     ObjectField (..))

instance HasSpan (Object t) where
  span f (Object sp flds) =
    Object <$> span f sp <*> pure flds
  {-# INLINABLE span #-}

instance HasSpan (ObjectField t) where
  span f (ObjectField sp fn t) =
    ObjectField <$> span f sp <*> pure fn <*> pure t
  {-# INLINABLE span #-}
