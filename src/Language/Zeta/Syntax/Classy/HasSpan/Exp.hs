{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Exp where

import           Prelude                                                  hiding
                                                                          (span)

import           Document.Classy.HasSpan                                  (HasSpan (..))
import           Language.Zeta.Syntax.Classy.HasSpan.Branch               ()
import           Language.Zeta.Syntax.Classy.HasSpan.Clause               ()
import           Language.Zeta.Syntax.Classy.HasSpan.Name.ConstructorName ()
import           Language.Zeta.Syntax.Classy.HasSpan.Name.FieldName       ()
import           Language.Zeta.Syntax.Classy.HasSpan.Object               ()
import           Language.Zeta.Syntax.Classy.HasSpan.Pat                  ()
import           Language.Zeta.Syntax.Classy.HasSpan.Type                 ()
import           Language.Zeta.Syntax.Classy.HasSpan.Var                  ()
import           Language.Zeta.Syntax.Data.Exp                            (Exp (..))

instance HasSpan Exp where
  {-# INLINABLE span #-}
  span f ex = case ex of
      EVar v            -> EVar <$> span f v
      ELit _            -> pure ex
      ECall e es        -> ECall <$> span f e <*> span f es
      ELam pats m_ty sc -> ELam <$> span f pats <*> span f m_ty <*> span f sc
      EType e ty        -> EType <$> span f e <*> span f ty
      ECast e ty        -> ECast <$> span f e <*> span f ty
      ECache e ec       -> ECache <$> span f e <*> pure ec
      ETypeCache e tc   -> ETypeCache <$> span f e <*> pure tc
      ELoc e sp         -> ELoc e <$> f sp
      EParens e         -> EParens <$> span f e
      ERef e            -> ERef <$> span f e
      EDeref e          -> EDeref <$> span f e
      ETuple e ne       -> ETuple <$> span f e <*> span f ne
      ECon cn           -> ECon <$> span f cn
      EArray es         -> EArray <$> span f es
      EObj ob           -> EObj <$> span f ob
      EFree e           -> EFree <$> span f e
      EField fn         -> EField <$> span f fn
      EGet e fn         -> EGet <$> span f e <*> span f fn
      EGetI e e'        -> EGetI <$> span f e <*> span f e'
      ESet e e'         -> ESet <$> span f e <*> span f e'
      EUnOp uo e        -> EUnOp uo <$> span f e
      EBinOp bo e e'    -> EBinOp bo <$> span f e <*> span f e'
      EBlock e          -> EBlock <$> span f e
      ESeq e e'         -> ESeq <$> span f e <*> span f e'
      EBind p e sc      -> EBind <$> span f p <*> span f e <*> span f sc
      EIf e e' m_br     -> EIf <$> span f e <*> span f e' <*> span f m_br
      ECase e cb        -> ECase <$> span f e <*> span f cb
      EFor i c u e      -> EFor <$> span f i <*> span f c <*> span f u <*> span f e
      EWhile e e'       -> EWhile <$> span f e <*> span f e'
      EDoWhile e e'     -> EDoWhile <$> span f e <*> span f e'
      EReturn e         -> EReturn <$> span f e
      EBreak            -> pure ex
      EContinue         -> pure ex
