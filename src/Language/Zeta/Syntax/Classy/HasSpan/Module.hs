{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Module where

import           Document.Classy.HasSpan          (HasSpan (..))
import           Language.Zeta.Syntax.Data.Module (Module (getModuleLoc),
                                                   setModuleLoc)

instance HasSpan Module where
  span f m = case getModuleLoc m of
    Just l  -> setModuleLoc m . Just <$> f l
    Nothing -> pure m
  {-# INLINABLE span #-}
