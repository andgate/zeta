{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.ModuleStmt where

import           Prelude                              hiding (span)

import           Document.Classy.HasSpan              (HasSpan (..))
import           Language.Zeta.Syntax.Data.ModuleStmt (ModuleStmt (..))

instance HasSpan ModuleStmt where
  span _f = \case
    _         -> _fs2
  {-# INLINABLE span #-}
