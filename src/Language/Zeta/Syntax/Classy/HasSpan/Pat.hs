{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Pat where

import                          Prelude                                                  hiding
                                                                                         (span)

import                          Document.Classy.HasSpan                                  (HasSpan (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasSpan.Exp                  ()
import                          Language.Zeta.Syntax.Classy.HasSpan.Name.ConstructorName ()
import                          Language.Zeta.Syntax.Classy.HasSpan.Name.PatternName     ()
import                          Language.Zeta.Syntax.Classy.HasSpan.Object               ()
import                          Language.Zeta.Syntax.Classy.HasSpan.Type                 ()
import                          Language.Zeta.Syntax.Data.Pat                            (Pat (..))

instance HasSpan Pat where
  span f pat =
    case pat of
      PVar pn         -> PVar <$> span f pn
      PCon cn ps      -> PCon <$> span f cn <*> span f ps
      PTuple p ne     -> PTuple <$> span f p <*> span f ne
      PObj ob         -> PObj <$> span f ob
      PLabel pn p     -> PLabel <$> span f pn <*> span f p
      PView e p       -> PView <$> span f e <*> span f p
      PWild           -> pure pat
      PType p ty      -> PType <$> span f p <*> span f ty
      PExpCache p ec  -> PExpCache <$> span f p <*> pure ec
      PTypeCache p tc -> PTypeCache <$> span f p <*> pure tc
      PLoc p sp       -> PLoc p <$> f sp
      PParens p       -> PParens <$> span f p
  {-# INLINABLE span #-}
