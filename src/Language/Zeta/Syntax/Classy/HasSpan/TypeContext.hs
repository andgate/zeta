{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.TypeContext where

import           Prelude                               hiding (span)

import           Document.Classy.HasSpan               (HasSpan (..))
import           Language.Zeta.Syntax.Data.TypeContext (ClassConstraint (..),
                                                        ClassInstance (..),
                                                        TypeContext (..),
                                                        TypeContextElement (..),
                                                        TypeSchema (..))

instance HasSpan TypeContext where
  span f (TypeContext l es) = TypeContext <$> span f l <*> pure es
  {-# INLINABLE span #-}

instance HasSpan TypeContextElement where
  span f = \case
    TypeContextSchema s     -> TypeContextSchema <$> span f s
    TypeContextConstraint c -> TypeContextConstraint <$> span f c
  {-# INLINABLE span #-}

instance HasSpan TypeSchema where
  span f (TypeSchema l tvs) = TypeSchema <$> span f l <*> pure tvs
  {-# INLINABLE span #-}

instance HasSpan ClassConstraint where
  span f (ClassConstraint l insts) = ClassConstraint <$> span f l <*> pure insts
  {-# INLINABLE span #-}

instance HasSpan ClassInstance where
  span f (ClassInstance l n xs) = ClassInstance <$> span f l <*> pure n <*> pure xs
  {-# INLINABLE span #-}
