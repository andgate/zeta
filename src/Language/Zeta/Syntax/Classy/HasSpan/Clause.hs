{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.Clause where

import                          Prelude                                 hiding
                                                                        (span)

import                          Document.Classy.HasSpan                 (HasSpan (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasSpan.Exp ()
import                          Language.Zeta.Syntax.Classy.HasSpan.Pat ()
import                          Language.Zeta.Syntax.Data.Clause        (Clause (..),
                                                                         ClauseBlock (..))

instance HasSpan ClauseBlock where
  span f (ClauseBlock l cs) =
    ClauseBlock <$> span f l <*> span f cs
  {-# INLINABLE span #-}

instance HasSpan Clause where
  span f (Clause l ps e) =
    Clause <$> span f l <*> span f ps <*> span f e
  {-# INLINABLE span #-}
