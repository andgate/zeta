{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.ModuleHeader where

import           Document.Classy.HasSpan                (HasSpan (..))
import           Language.Zeta.Syntax.Data.ModuleHeader (ModuleHeader (getModuleHeaderLoc),
                                                         setModuleHeaderLoc)

instance HasSpan ModuleHeader where
  span f m = case getModuleHeaderLoc m of
    Just l  -> setModuleHeaderLoc m . Just <$> f l
    Nothing -> pure m
  {-# INLINABLE span #-}
