{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSpan.FuncDefn where

import           Prelude                            hiding (span)

import           Document.Classy.HasSpan            (HasSpan (..))
import           Language.Zeta.Syntax.Data.FuncDefn (FuncDefn (getFuncDefnLoc),
                                                     FuncDefnBody (..),
                                                     setFuncDefnLoc)

instance HasSpan FuncDefn where
  span f def = case getFuncDefnLoc def of
    Just l  -> setFuncDefnLoc def . Just <$> f l
    Nothing -> pure def
  {-# INLINABLE span #-}

instance HasSpan FuncDefnBody where
  span f = \case
    FuncDefnBodyBlock l sblk -> FuncDefnBodyBlock <$> span f l <*> pure sblk
    FuncDefnBodyExp l e      -> FuncDefnBodyExp <$> span f l <*> pure e
  {-# INLINABLE span #-}
