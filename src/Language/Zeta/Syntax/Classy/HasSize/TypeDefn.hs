{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasSize.TypeDefn where

-- import           Data.Map                                  (Map)
-- import qualified Data.Map                                  as Map

import           Language.Zeta.Syntax.Classy.HasSize.Class (HasSize (..))
import           Language.Zeta.Syntax.Data.TypeDefn        (TypeDefn (..))

instance HasSize TypeDefn where
  sizeOf _ = undefined

-- sizeDataType :: Map String Int -> DataType -> Int
-- sizeDataType sizes (DataType _ _ cs)
--   = maximum $ sizeConstrDefn sizes <$> cs

-- sizeConstrDefn :: Map String Int -> ConstrDefn -> Int
-- sizeConstrDefn sizes = \case
--   ConstrDefn _ _ tys -> sum $ sizeType sizes <$> tys
--   RecordDefn _ _ ens -> sum $ sizeEntry sizes <$> ens

-- sizeEntry :: Map String Int -> Entry -> Int
-- sizeEntry sizes (Entry _ _ ty) = sizeType sizes ty

-- sizeType :: Map String Int -> Type -> Int
-- sizeType sizes = \case
--   TVar _ -> 8 -- size of pointer, since its boxed. Might change later
--   TCon n -> case Map.lookup n sizes of
--     Nothing -> error "DataType not registered"
--     Just i  -> i
--   TInt  s -> ceiling $ (fromIntegral s) / (8.0 :: Double)
--   TUInt s -> ceiling $ (fromIntegral s) / (8.0 :: Double)
--   TFp   s -> ceiling $ (fromIntegral s) / (8.0 :: Double)
--   TTuple t1 ts -> sizeType sizes t1 + foldl (\s t -> sizeType sizes t + s) (0 :: Int) ts
--   TArray n ty -> n * sizeType sizes ty
--   TVect  n ty -> n * sizeType sizes ty
--   TPtr _ -> 8 -- asssume 64-bit system
--   TFunc _ _ -> 8 -- size of pointer
--   TLoc t _ -> sizeType sizes t
--   TParens t -> sizeType sizes t
