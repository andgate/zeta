module Language.Zeta.Syntax.Classy.HasSize.Class where

-- | Classy size carriers.
class HasSize a where
  -- | Get the size of some object.
  sizeOf :: a -> Int
