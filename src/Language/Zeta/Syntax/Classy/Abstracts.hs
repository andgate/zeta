module Language.Zeta.Syntax.Classy.Abstracts (module X) where

import           Language.Zeta.Syntax.Classy.Abstracts.Class as X
import           Language.Zeta.Syntax.Classy.Abstracts.Exp   ()
import           Language.Zeta.Syntax.Classy.Abstracts.Type  ()
