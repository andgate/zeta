module Language.Zeta.Syntax.Classy.HasTypeCache (module X) where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Branch               ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Class                as X
import           Language.Zeta.Syntax.Classy.HasTypeCache.Clause               ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Exp                  ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Foldable             ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Name.ConstructorName ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Name.FieldName       ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Name.PatternName     ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Object               ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Pat                  ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Scope                ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.TVar                 ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Type                 ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.TypeContext          ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Var                  ()
