module Language.Zeta.Syntax.Classy.Ranked (module X) where

import           Language.Zeta.Syntax.Classy.Ranked.Class as X
import           Language.Zeta.Syntax.Classy.Ranked.Exp   ()
import           Language.Zeta.Syntax.Classy.Ranked.Pat   ()
import           Language.Zeta.Syntax.Classy.Ranked.Type  ()
