{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Type where

import           Prettyprinter      (Pretty)
import           Language.Zeta.Syntax.Data.Type (Type)

instance Pretty Type
