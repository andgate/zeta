{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Name.ClassName where

import           Prettyprinter
import           Language.Zeta.Syntax.Data.Name.ClassName (ClassName (getClassNameText))

instance Pretty ClassName where
  pretty = pretty . getClassNameText
