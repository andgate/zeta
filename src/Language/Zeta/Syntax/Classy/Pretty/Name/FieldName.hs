{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Name.FieldName where

import           Prettyprinter
import           Language.Zeta.Syntax.Data.Name.FieldName (FieldName (getFieldNameText))

instance Pretty FieldName where
  pretty = pretty . getFieldNameText
