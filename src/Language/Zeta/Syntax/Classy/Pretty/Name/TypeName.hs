{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Name.TypeName where

import           Prettyprinter
import           Language.Zeta.Syntax.Data.Name.TypeName (TypeName (getTypeNameText))

instance Pretty TypeName where
  pretty = pretty . getTypeNameText
