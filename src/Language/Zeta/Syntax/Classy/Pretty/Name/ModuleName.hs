{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Name.ModuleName where

import           Prettyprinter
import           Language.Zeta.Syntax.Data.Name.ModuleName (ModuleName (getModuleNameText, getModuleNameQualifiers))

instance Pretty ModuleName where
  pretty n
    = concatWith (surround dot) (pretty <$> (getModuleNameQualifiers n <> [getModuleNameText n]))
