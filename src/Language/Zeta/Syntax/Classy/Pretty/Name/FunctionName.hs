{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Name.FunctionName where

import           Prettyprinter
import           Language.Zeta.Syntax.Data.Name.FunctionName (FunctionName (getFunctionNameText))

instance Pretty FunctionName where
  pretty = pretty . getFunctionNameText
