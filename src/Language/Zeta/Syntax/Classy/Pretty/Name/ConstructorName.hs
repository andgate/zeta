{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Name.ConstructorName where

import           Language.Zeta.Syntax.Data.Name.ConstructorName (ConstructorName (getConstructorNameText))
import           Prettyprinter

instance Pretty ConstructorName where
  pretty = pretty . getConstructorNameText
