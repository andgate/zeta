{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Name.PatternName where

import           Prettyprinter
import           Language.Zeta.Syntax.Data.Name.PatternName (PatternName (getPatternNameText))

instance Pretty PatternName where
  pretty = pretty . getPatternNameText
