{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.FuncDefn where

import           Language.Zeta.Syntax.Classy.Pretty.Exp               ()
import           Language.Zeta.Syntax.Classy.Pretty.Name.FunctionName ()
import           Language.Zeta.Syntax.Classy.Pretty.Pat               ()
import           Language.Zeta.Syntax.Classy.Pretty.Type              ()
import           Language.Zeta.Syntax.Classy.Pretty.TypeContext       ()
import           Language.Zeta.Syntax.Data.FuncDefn                   (FuncDefn (..),
                                                                       FuncDefnBody (..))
import           Prettyprinter

instance Pretty FuncDefn where
  pretty (FuncDefn _ mctx n ps mretty body)
    = vsep [
      pretty mctx,
      pretty n <> tupled (pretty <$> ps) <> rty <+> pretty body
    ]
    where rty = case mretty of
            Just retty -> ":" <> pretty retty
            Nothing    -> mempty

instance Pretty FuncDefnBody where
  pretty = \case
    FuncDefnBodyBlock _ blk -> pretty blk
    FuncDefnBodyExp _ e     -> "=" <> line <> indent 2 (pretty e)
