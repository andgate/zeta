{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Operator where

import           Prettyprinter
import           Language.Zeta.Syntax.Data.Operator (BinaryOperator (..),
                                                     UnaryOperator (..))

instance Pretty UnaryOperator where
  pretty unop = "#" <> pretty (show unop)

instance Pretty BinaryOperator where
  pretty binop = "#" <> pretty (show binop)
