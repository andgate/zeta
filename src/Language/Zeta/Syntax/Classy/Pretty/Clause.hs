{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns      #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Clause where

import                qualified Data.List.NonEmpty                       as NE
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.Pretty.Exp   ()
import                          Language.Zeta.Syntax.Classy.Pretty.Pat   ()
import                          Language.Zeta.Syntax.Classy.Pretty.Scope ()
import                          Language.Zeta.Syntax.Data.Clause         (Clause (..),
                                                                          ClauseBlock (..))
import                          Prettyprinter

instance Pretty ClauseBlock where
  pretty (ClauseBlock _ cs) =
    vcat
      [ "{"
      , (indent 2 . vcat) (pretty <$> cs)
      , "}"
      ]

instance Pretty Clause where
  pretty (Clause _ (NE.toList -> pats) body)
    = vcat [ hsep (pretty <$> pats) <+> "->"
           , indent 2 (pretty body)
           , mempty
           ]
