{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Var where

import           Language.Zeta.Syntax.Data.Var (Var (getVarStr))
import           Prettyprinter

instance Pretty Var where
  pretty = pretty . getVarStr
