{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns      #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Pat where

import                qualified Data.List.NonEmpty                                      as NE
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.Pretty.Exp                  ()
import                          Language.Zeta.Syntax.Classy.Pretty.Name.ConstructorName ()
import                          Language.Zeta.Syntax.Classy.Pretty.Name.PatternName     ()
import                          Language.Zeta.Syntax.Classy.Pretty.Object               ()
import                          Language.Zeta.Syntax.Classy.Pretty.Type                 ()
import                          Language.Zeta.Syntax.Data.Pat                           (Pat (..))
import                          Language.Zeta.Syntax.Data.PatRank                       (PatRank (..),
                                                                                         patRank)
import                          Prettyprinter

instance Pretty Pat where
  pretty = \case
    PVar v -> pretty v
    PCon n [] -> pretty n
    PCon n ps ->
      let ps' = do
            p <- ps
            return $ case patRank p of
                      APat -> pretty p
                      _    -> parens $ pretty p
      in pretty n <+> hsep ps'

    PTuple p (NE.toList -> ps) ->
      tupled $ pretty <$> (p:ps)
    PObj o -> pretty o
    PLabel vId p -> pretty vId <> "@" <> pretty p
    PView e p -> pretty e <+> "->" <+> pretty p
    PWild -> "_"
    PType p ty -> parens (pretty p <> ":" <+> pretty ty)
    PExpCache p _ -> pretty p
    PTypeCache p _ -> pretty p
    PLoc p _ -> pretty p
    PParens p -> parens $ pretty p
