{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Object where

import           Language.Zeta.Syntax.Classy.Pretty.Name.FieldName ()
import           Language.Zeta.Syntax.Data.Object                  (Object (..),
                                                                    ObjectField (..))
import           Prettyprinter

instance (Pretty t) => Pretty (Object t) where
  pretty (Object _ fs) =
    encloseSep lbrace rbrace comma (pretty <$> fs)

instance (Pretty t) => Pretty (ObjectField t) where
  pretty (ObjectField _ vId p) =
    pretty vId <> ":" <+> pretty p
