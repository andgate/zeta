{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.TypeContext where

import                          Data.List.NonEmpty                                (NonEmpty (..))
import                qualified Data.List.NonEmpty                                as NE
import                          Prettyprinter
import                          Language.Zeta.Syntax.Classy.Pretty.Name.ClassName ()
import                          Language.Zeta.Syntax.Classy.Pretty.TVar           ()
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.Pretty.Type           ()
import                          Language.Zeta.Syntax.Data.TypeContext             (ClassConstraint (..),
                                                                                   ClassInstance (..),
                                                                                   TypeContext (..),
                                                                                   TypeContextElement (..),
                                                                                   TypeSchema (..))

instance Pretty TypeContext where
  pretty (TypeContext _ es) =
    hsep (pretty <$> es)

instance Pretty TypeContextElement where
  pretty = \case
    TypeContextSchema s      -> pretty s
    TypeContextConstraint cc -> pretty cc

instance Pretty TypeSchema where
  pretty (TypeSchema _ vs) =
    "forall" <+> hsep (pretty <$> NE.toList vs) <> "."

instance Pretty ClassConstraint where
  pretty = \case
    ClassConstraint _ (inst:|[]) -> pretty inst <+> "=>"
    ClassConstraint _ insts      -> tupled (pretty <$> NE.toList insts) <+> "=>"

instance Pretty ClassInstance where
  pretty (ClassInstance _ vId tys) =
    pretty vId <> tupled (pretty <$> tys)
