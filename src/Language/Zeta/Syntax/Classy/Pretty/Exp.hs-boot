{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Exp where

import           Prettyprinter     (Doc, Pretty)
import           Language.Zeta.Syntax.Data.Exp (Exp)

wrapBExp :: Exp -> Doc ann

instance Pretty Exp
