{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Scope where

import           Language.Zeta.Syntax.Data.Scope (Scope (..))
import           Prettyprinter                   (Pretty (..))

instance (Pretty t) => Pretty (Scope t) where
  pretty = pretty . getScopeBody
