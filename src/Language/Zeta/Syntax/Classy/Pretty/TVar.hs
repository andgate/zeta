{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.TVar where

import           Prettyprinter
import           Language.Zeta.Syntax.Data.TVar (TVar (getTVarStr))

instance Pretty TVar where
  pretty = pretty . getTVarStr
