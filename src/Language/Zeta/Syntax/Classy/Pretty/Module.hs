{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Module where

import           Document.Data.Location                          (Loc (..))
import           Language.Zeta.Syntax.Classy.Pretty.ModuleHeader ()
import           Language.Zeta.Syntax.Classy.Pretty.ModuleStmt   ()
import           Language.Zeta.Syntax.Data.Module                (Module (..))
import           Prettyprinter

instance Pretty Module where
  pretty (Module l fp h ds)
    = vsep [ pretty (L <$> fp <*> l),
             pretty h,
             line,
             vsep (pretty <$> ds) ]
