{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.ExternDefn where

import           Prettyprinter
import           Language.Zeta.Syntax.Classy.Pretty.Name.FunctionName ()
import           Language.Zeta.Syntax.Classy.Pretty.Type              ()
import           Language.Zeta.Syntax.Data.ExternDefn                 (ExternDefn (..))
import           Language.Zeta.Syntax.Data.Type                       (Type (..))

instance Pretty ExternDefn where
  pretty (ExternDefn _ n paramtys retty)
    = hsep ["extern", pretty n, ":", pretty (TFunc paramtys retty)]
