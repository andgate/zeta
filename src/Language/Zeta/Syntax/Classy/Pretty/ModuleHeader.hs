{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.ModuleHeader where

import           Prettyprinter
import           Language.Zeta.Syntax.Classy.Pretty.Name.ModuleName ()
import           Language.Zeta.Syntax.Data.ModuleHeader             (ModuleHeader (..))

instance Pretty ModuleHeader where
  pretty (ModuleHeader n _)
    = "module" <+> pretty n
