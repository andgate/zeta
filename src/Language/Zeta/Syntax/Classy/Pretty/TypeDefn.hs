{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.TypeDefn where

import           Prettyprinter
import           Language.Zeta.Syntax.Classy.Pretty.Exp                  ()
import           Language.Zeta.Syntax.Classy.Pretty.FuncDefn             ()
import           Language.Zeta.Syntax.Classy.Pretty.Name.ConstructorName ()
import           Language.Zeta.Syntax.Classy.Pretty.Name.TypeName        ()
import           Language.Zeta.Syntax.Classy.Pretty.Pat                  ()
import           Language.Zeta.Syntax.Classy.Pretty.Type                 ()
import           Language.Zeta.Syntax.Classy.Pretty.TypeContext          ()
import           Language.Zeta.Syntax.Data.TypeDefn                      (TypeDefn (..),
                                                                          TypeDefnBlock (..),
                                                                          TypeDefnStmt (..))

instance Pretty TypeDefn where
  pretty (TypeDefn _ mctx n tvs blk)
    = vsep [
        pretty mctx,
        "type" <+> pretty n <> tupled (pretty <$> tvs) <> pretty blk
      ]

instance Pretty TypeDefnBlock where
  pretty (TypeDefnBlock stmts) =
    (braces . indent 2 . vsep) (pretty <$> stmts)

instance Pretty TypeDefnStmt where
  pretty = \case
    TypeDefnConstr _ n args -> pretty n <> tupled (pretty <$> args)
    TypeDefnProp _ p e      -> pretty p <> "<-" <> pretty e
    TypeDefnMethod defn     -> pretty defn
