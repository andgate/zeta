{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns      #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Exp where

import qualified Data.List.NonEmpty                                      as NE
import           Language.Zeta.Syntax.Classy.Pretty.Branch               ()
import           Language.Zeta.Syntax.Classy.Pretty.Clause               ()
import           Language.Zeta.Syntax.Classy.Pretty.Lit                  ()
import           Language.Zeta.Syntax.Classy.Pretty.Name.ConstructorName ()
import           Language.Zeta.Syntax.Classy.Pretty.Name.FieldName       ()
import           Language.Zeta.Syntax.Classy.Pretty.Object               ()
import           Language.Zeta.Syntax.Classy.Pretty.Operator             ()
import           Language.Zeta.Syntax.Classy.Pretty.Pat                  ()
import           Language.Zeta.Syntax.Classy.Pretty.Var                  ()
import           Language.Zeta.Syntax.Data.Exp                           (Exp (..))
import           Language.Zeta.Syntax.Data.ExpRank                       (isAExp)
import           Language.Zeta.Syntax.Data.Scope                         (Scope (getScopeBody))
import           Prettyprinter

wrapBExp :: Exp -> Doc ann
wrapBExp e
  | isAExp e  = pretty e
  | otherwise = parens $ pretty e

instance Pretty Exp where
  pretty = \case
    EVar n ->
      pretty n

    ELit l ->
      pretty l

    ECall f xs ->
      let f' = wrapBExp f
          xs' = wrapBExp <$> xs
      in hsep (f':xs')

    ELam ps _mty (getScopeBody -> body) ->
      tupled (pretty <$> ps) <+> "->" <+> pretty body

    EType e ty ->
      pretty e <> ":" <+> pretty ty

    ECast e ty ->
      pretty e <+> "as" <+> pretty ty

    ECache e _ ->
      pretty e

    ETypeCache e _ ->
      pretty e

    ELoc e _ ->
      pretty e

    EParens e ->
      parens $ pretty e

    ERef e ->
      let e' = pretty e
      in if isAExp e
          then "&" <> e'
          else "&" <> parens e'

    EDeref e ->
      let e' = pretty e
      in if isAExp e
          then "*" <> e'
          else "*" <> parens e'

    ETuple x (NE.toList -> xs) ->
      tupled $ pretty <$> (x:xs)

    ECon n ->
      pretty n

    EArray xs ->
      list (pretty <$> xs)

    EObj obj ->
      pretty obj

    EFree e ->
      "free" <+> wrapBExp e

    EField n ->
      pretty n

    EGet e fn ->
      wrapBExp e <> "." <> pretty fn

    EGetI e i ->
      wrapBExp e <> brackets (pretty i)

    ESet lhs rhs ->
      wrapBExp lhs <+> "<-" <+> wrapBExp rhs

    EUnOp op a ->
      pretty op <> parens (pretty a)

    EBinOp op a b ->
      pretty op <> tupled (pretty <$> [a, b])

    EBlock e ->
      vcat [ "{", pretty e, "}"]

    ESeq e e' ->
      vcat [ pretty e <> ";", pretty e']

    EBind p e sc ->
      vcat
        [ pretty p <+> "<-" <+> pretty e <> ";"
        , pretty sc
        ]

    EIf e e' m_br ->
      vcat
        [ "if" <> parens (pretty e)
        , indent 2 (pretty e')
        , pretty m_br
        ]

    ECase e cb ->
      "case" <> parens (pretty e) <> pretty cb

    EFor i c u e ->
      vcat
        [ "for" <> parens (pretty i <> ";" <+> pretty c <> ";" <+> pretty u)
        , indent 2 (pretty e)
        ]

    EWhile e e' ->
      vcat
        [ "while" <> parens (pretty e)
        , indent 2 (pretty e')
        ]

    EDoWhile e e' ->
      vcat
        [ "do"
        , indent 2 (pretty e)
        , "while" <> parens (pretty e')
        ]

    EReturn e ->
      "return" <> pretty e

    EBreak ->
      "break"

    EContinue ->
      "continue"
