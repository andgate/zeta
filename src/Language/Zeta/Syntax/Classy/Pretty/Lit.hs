{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Lit where

import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.Pretty.Exp ()
import                          Language.Zeta.Syntax.Data.Lit          (Lit (..))
import                          Prettyprinter

instance Pretty Lit where
  pretty = \case
    LNull       -> "null"
    LInt i      -> pretty i
    LDouble d   -> pretty d
    LBool b     -> pretty b
    LChar c     -> squotes $ pretty c

    LString str -> dquotes $ pretty str

    LArray xs   -> list $ pretty <$> xs
    LArrayI i   -> "Array" <> brackets (pretty i)

    LGetI e i   -> pretty e <> brackets (pretty i)
