{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns      #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Type where

import qualified Data.List.NonEmpty                               as NE
import           Language.Zeta.Syntax.Classy.Pretty.Name.TypeName ()
import           Language.Zeta.Syntax.Classy.Pretty.TypeContext   ()
import           Language.Zeta.Syntax.Data.Type                   (Type (..))
import           Language.Zeta.Syntax.Data.TypeRank               (isAType)
import           Prettyprinter

instance Pretty Type where
  pretty = \case
    TVar n -> pretty n
    TApp tc args -> pretty tc <+> hsep (pretty <$> args)
    TCon n -> pretty n
    TInt s -> "i" <> pretty s
    TUInt s -> "u" <> pretty s
    TFp s -> "f" <> pretty s
    TTuple t (NE.toList -> ts) ->
      tupled $ pretty <$> (t:ts)
    TArray i ty
      | isAType ty -> pretty ty <> brackets (pretty i)
      | True       -> parens (pretty ty) <> brackets (pretty i)
    TPtr ty
      | isAType ty -> "*" <> pretty ty
      | True       -> "*" <> parens (pretty ty)
    TFunc [paramty] retty ->
      pretty paramty <+> "->" <+> pretty retty
    TFunc paramtys retty ->
      tupled (pretty <$> paramtys) <+> "->" <+> pretty retty
    TCtx ctx t -> pretty ctx <+> pretty t
    TCache t _ -> pretty t
    TLoc t _ -> pretty t
    TParens t -> parens $ pretty t
