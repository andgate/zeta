{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.ModuleStmt where

import           Prettyprinter
import           Language.Zeta.Syntax.Classy.Pretty.ExternDefn ()
import           Language.Zeta.Syntax.Classy.Pretty.FuncDefn   ()
import           Language.Zeta.Syntax.Classy.Pretty.TypeDefn   ()
import           Language.Zeta.Syntax.Data.ModuleStmt          (ModuleStmt (..))

instance Pretty ModuleStmt where
  pretty = \case
    ModuleFuncDefn f    -> pretty f <> line
    ModuleExternDefn ex -> pretty ex <> line
    ModuleTypeDefn dt   -> pretty dt <> line
    _ -> undefined
