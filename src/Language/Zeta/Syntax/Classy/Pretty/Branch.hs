{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Pretty.Branch where

import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.Pretty.Exp ()
import                          Language.Zeta.Syntax.Data.Branch       (Branch (..))
import                          Prettyprinter

instance Pretty Branch where
  pretty = \case
    ElseBranch _ e ->
      vcat
        [ "else"
        , indent 2 (pretty e)
        ]
    ElifBranch _ p e m_br ->
      vcat
        [ "elif" <> parens (pretty p)
        , indent 2 (pretty e)
        , pretty m_br
        ]
