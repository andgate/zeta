{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Alpha.Pat where

import           Language.Zeta.Syntax.Classy.Alpha.Class (Alpha (..))
import           Language.Zeta.Syntax.Data.Pat           (Pat)

instance Alpha Pat where
  aeq = _patAlpha
