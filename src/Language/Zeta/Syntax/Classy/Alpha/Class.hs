module Language.Zeta.Syntax.Classy.Alpha.Class where

class Alpha a where
  aeq :: a -> a -> Bool
