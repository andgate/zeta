{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Alpha.Exp where

import           Language.Zeta.Syntax.Classy.Alpha.Class (Alpha (..))
import           Language.Zeta.Syntax.Data.Exp           (Exp)

instance Alpha Exp where
  aeq = _expAlpha
