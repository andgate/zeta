{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Alpha.Type where

import           Language.Zeta.Syntax.Classy.Alpha.Class (Alpha (..))
import           Language.Zeta.Syntax.Data.Type          (Type)

instance Alpha Type where
  aeq = _typeAlpha
