{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.TypeDefn.TypeName where

import           Language.Zeta.Syntax.Classy.HasNames.Class (HasNames (..))
import           Language.Zeta.Syntax.Data.Name.TypeName    (TypeName)
import           Language.Zeta.Syntax.Data.TypeDefn         (TypeDefn (..))

instance HasNames TypeDefn TypeName where
  names = \f (TypeDefn l mctx n tvs blk) ->
    TypeDefn l mctx <$> f n <*> pure tvs <*> pure blk
