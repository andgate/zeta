{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.TypeDefn.ConstructorName where

import           Language.Zeta.Syntax.Classy.HasNames.Class     (HasNames (..))
import           Language.Zeta.Syntax.Data.Name.ConstructorName (ConstructorName)
import           Language.Zeta.Syntax.Data.TypeDefn             (TypeDefn (..),
                                                                 TypeDefnBlock (..),
                                                                 TypeDefnStmt (..))

instance HasNames TypeDefn ConstructorName where
  names f (TypeDefn l mctx n tvs blk) =
    TypeDefn l mctx n tvs <$> names f blk

instance HasNames TypeDefnBlock ConstructorName where
  names f (TypeDefnBlock stmts) =
    TypeDefnBlock <$> traverse (names f) stmts

instance HasNames TypeDefnStmt ConstructorName where
  names f = \case
    TypeDefnConstr l n args ->
      TypeDefnConstr l <$> f n <*> pure args
    TypeDefnProp l p e ->
      pure $ TypeDefnProp l p e
    TypeDefnMethod fn ->
      pure $ TypeDefnMethod fn
