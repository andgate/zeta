{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.Exp where

import           Language.Zeta.Syntax.Classy.HasNames.Branch      ()
import           Language.Zeta.Syntax.Classy.HasNames.Class       (HasNames (..))
import           Language.Zeta.Syntax.Classy.HasNames.Clause      ()
import           Language.Zeta.Syntax.Classy.HasNames.Pat         ()
import           Language.Zeta.Syntax.Classy.HasNames.Traversable ()
import           Language.Zeta.Syntax.Classy.HasNames.Type        ()
import           Language.Zeta.Syntax.Data.Exp                    (Exp (..))
import           Language.Zeta.Syntax.Data.Name.ClassName         (ClassName)
import           Language.Zeta.Syntax.Data.Name.ConstructorName   (ConstructorName)
import           Language.Zeta.Syntax.Data.Name.FieldName         (FieldName)
import           Language.Zeta.Syntax.Data.Name.PatternName       (PatternName)
import           Language.Zeta.Syntax.Data.Name.TypeName          (TypeName)


instance HasNames Exp ClassName where
  names f ex = case ex of
    EVar _          -> pure ex
    ELit _          -> pure ex
    ECall e es      -> ECall <$> names f e <*> names f es
    ELam ps m_ty sc -> ELam <$> names f ps <*> names f m_ty <*> names f sc
    EType e ty      -> EType <$> names f e <*> names f ty
    ECast e ty      -> ECast <$> names f e <*> names f ty
    ECache e ec     -> ECache <$> names f e <*> pure ec
    ETypeCache e tc -> ETypeCache <$> names f e <*> pure tc
    ELoc e sp       -> ELoc <$> names f e <*> pure sp
    EParens e       -> EParens <$> names f e
    ERef e          -> ERef <$> names f e
    EDeref e        -> EDeref <$> names f e
    ETuple e ne     -> ETuple <$> names f e <*> names f ne
    ECon _          -> pure ex
    EArray es       -> EArray <$> names f es
    EObj ob         -> EObj <$> names f ob
    EFree e         -> EFree <$> names f e
    EField _        -> pure ex
    EGet e fn       -> EGet <$> names f e <*> pure fn
    EGetI e e'      -> EGetI <$> names f e <*> names f e'
    ESet e e'       -> ESet <$> names f e <*> names f e'
    EUnOp uo e      -> EUnOp uo <$> names f e
    EBinOp bo e e'  -> EBinOp bo <$> names f e <*> names f e'
    EBlock e        -> EBlock <$> names f e
    ESeq e e'       -> ESeq <$> names f e <*> names f e'
    EBind p e sc    -> EBind <$> names f p <*> names f e <*> names f sc
    EIf e e' m_br   -> EIf <$> names f e <*> names f e' <*> names f m_br
    ECase e cb      -> ECase <$> names f e <*> names f cb
    EFor i c u e    -> EFor <$> names f i <*> names f c <*> names f u <*> names f e
    EWhile e e'     -> EWhile <$> names f e <*> names f e'
    EDoWhile e e'   -> EDoWhile <$> names f e <*> names f e'
    EReturn e       -> EReturn <$> names f e
    EBreak          -> pure ex
    EContinue       -> pure ex

instance HasNames Exp ConstructorName where
  names f ex = case ex of
    EVar _          -> pure ex
    ELit _          -> pure ex
    ECall e es      -> ECall <$> names f e <*> names f es
    ELam ps m_ty sc -> ELam <$> names f ps <*> pure m_ty <*> names f sc
    EType e ty      -> EType <$> names f e <*> pure ty
    ECast e ty      -> ECast <$> names f e <*> pure ty
    ECache e ec     -> ECache <$> names f e <*> pure ec
    ETypeCache e tc -> ETypeCache <$> names f e <*> pure tc
    ELoc e sp       -> ELoc <$> names f e <*> pure sp
    EParens e       -> EParens <$> names f e
    ERef e          -> ERef <$> names f e
    EDeref e        -> EDeref <$> names f e
    ETuple e ne     -> ETuple <$> names f e <*> names f ne
    ECon cn         -> ECon <$> f cn
    EArray es       -> EArray <$> names f es
    EObj ob         -> EObj <$> names f ob
    EFree e         -> EFree <$> names f e
    EField _        -> pure ex
    EGet e fn       -> EGet <$> names f e <*> pure fn
    EGetI e e'      -> EGetI <$> names f e <*> names f e'
    ESet e e'       -> ESet <$> names f e <*> names f e'
    EUnOp uo e      -> EUnOp uo <$> names f e
    EBinOp bo e e'  -> EBinOp bo <$> names f e <*> names f e'
    EBlock e        -> EBlock <$> names f e
    ESeq e e'       -> ESeq <$> names f e <*> names f e'
    EBind p e sc    -> EBind <$> names f p <*> names f e <*> names f sc
    EIf e e' m_br   -> EIf <$> names f e <*> names f e' <*> names f m_br
    ECase e cb      -> ECase <$> names f e <*> names f cb
    EFor i c u e    -> EFor <$> names f i <*> names f c <*> names f u <*> names f e
    EWhile e e'     -> EWhile <$> names f e <*> names f e'
    EDoWhile e e'   -> EDoWhile <$> names f e <*> names f e'
    EReturn e       -> EReturn <$> names f e
    EBreak          -> pure ex
    EContinue       -> pure ex

instance HasNames Exp FieldName where
  names f ex = case ex of
    EVar _          -> pure ex
    ELit _          -> pure ex
    ECall e es      -> ECall <$> names f e <*> names f es
    ELam ps m_ty sc -> ELam <$> names f ps <*> pure m_ty <*> names f sc
    EType e ty      -> EType <$> names f e <*> pure ty
    ECast e ty      -> ECast <$> names f e <*> pure ty
    ECache e ec     -> ECache <$> names f e <*> pure ec
    ETypeCache e tc -> ETypeCache <$> names f e <*> pure tc
    ELoc e sp       -> ELoc <$> names f e <*> pure sp
    EParens e       -> EParens <$> names f e
    ERef e          -> ERef <$> names f e
    EDeref e        -> EDeref <$> names f e
    ETuple e ne     -> ETuple <$> names f e <*> names f ne
    ECon _          -> pure ex
    EArray es       -> EArray <$> names f es
    EObj ob         -> EObj <$> names f ob
    EFree e         -> EFree <$> names f e
    EField fn       -> EField <$> f fn
    EGet e fn       -> EGet <$> names f e <*> f fn
    EGetI e e'      -> EGetI <$> names f e <*> names f e'
    ESet e e'       -> ESet <$> names f e <*> names f e'
    EUnOp uo e      -> EUnOp uo <$> names f e
    EBinOp bo e e'  -> EBinOp bo <$> names f e <*> names f e'
    EBlock e        -> EBlock <$> names f e
    ESeq e e'       -> ESeq <$> names f e <*> names f e'
    EBind p e sc    -> EBind <$> names f p <*> names f e <*> names f sc
    EIf e e' m_br   -> EIf <$> names f e <*> names f e' <*> names f m_br
    ECase e cb      -> ECase <$> names f e <*> names f cb
    EFor i c u e    -> EFor <$> names f i <*> names f c <*> names f u <*> names f e
    EWhile e e'     -> EWhile <$> names f e <*> names f e'
    EDoWhile e e'   -> EDoWhile <$> names f e <*> names f e'
    EReturn e       -> EReturn <$> names f e
    EBreak          -> pure ex
    EContinue       -> pure ex

instance HasNames Exp PatternName where
  names f ex = case ex of
    EVar _          -> pure ex
    ELit _          -> pure ex
    ECall e es      -> ECall <$> names f e <*> names f es
    ELam ps m_ty sc -> ELam <$> names f ps <*> pure m_ty <*> names f sc
    EType e ty      -> EType <$> names f e <*> pure ty
    ECast e ty      -> ECast <$> names f e <*> pure ty
    ECache e ec     -> ECache <$> names f e <*> pure ec
    ETypeCache e tc -> ETypeCache <$> names f e <*> pure tc
    ELoc e sp       -> ELoc <$> names f e <*> pure sp
    EParens e       -> EParens <$> names f e
    ERef e          -> ERef <$> names f e
    EDeref e        -> EDeref <$> names f e
    ETuple e ne     -> ETuple <$> names f e <*> names f ne
    ECon _          -> pure ex
    EArray es       -> EArray <$> names f es
    EObj ob         -> EObj <$> names f ob
    EFree e         -> EFree <$> names f e
    EField _        -> pure ex
    EGet e fn       -> EGet <$> names f e <*> pure fn
    EGetI e e'      -> EGetI <$> names f e <*> names f e'
    ESet e e'       -> ESet <$> names f e <*> names f e'
    EUnOp uo e      -> EUnOp uo <$> names f e
    EBinOp bo e e'  -> EBinOp bo <$> names f e <*> names f e'
    EBlock e        -> EBlock <$> names f e
    ESeq e e'       -> ESeq <$> names f e <*> names f e'
    EBind p e sc    -> EBind <$> names f p <*> names f e <*> names f sc
    EIf e e' m_br   -> EIf <$> names f e <*> names f e' <*> names f m_br
    ECase e cb      -> ECase <$> names f e <*> names f cb
    EFor i c u e    -> EFor <$> names f i <*> names f c <*> names f u <*> names f e
    EWhile e e'     -> EWhile <$> names f e <*> names f e'
    EDoWhile e e'   -> EDoWhile <$> names f e <*> names f e'
    EReturn e       -> EReturn <$> names f e
    EBreak          -> pure ex
    EContinue       -> pure ex

instance HasNames Exp TypeName where
  names f ex = case ex of
    EVar _          -> pure ex
    ELit _          -> pure ex
    ECall e es      -> ECall <$> names f e <*> names f es
    ELam ps m_ty sc -> ELam <$> names f ps <*> names f m_ty <*> names f sc
    EType e ty      -> EType <$> names f e <*> names f ty
    ECast e ty      -> ECast <$> names f e <*> names f ty
    ECache e ec     -> ECache <$> names f e <*> pure ec
    ETypeCache e tc -> ETypeCache <$> names f e <*> pure tc
    ELoc e sp       -> ELoc <$> names f e <*> pure sp
    EParens e       -> EParens <$> names f e
    ERef e          -> ERef <$> names f e
    EDeref e        -> EDeref <$> names f e
    ETuple e ne     -> ETuple <$> names f e <*> names f ne
    ECon _          -> pure ex
    EArray es       -> EArray <$> names f es
    EObj ob         -> EObj <$> names f ob
    EFree e         -> EFree <$> names f e
    EField _        -> pure ex
    EGet e fn       -> EGet <$> names f e <*> pure fn
    EGetI e e'      -> EGetI <$> names f e <*> names f e'
    ESet e e'       -> ESet <$> names f e <*> names f e'
    EUnOp uo e      -> EUnOp uo <$> names f e
    EBinOp bo e e'  -> EBinOp bo <$> names f e <*> names f e'
    EBlock e        -> EBlock <$> names f e
    ESeq e e'       -> ESeq <$> names f e <*> names f e'
    EBind p e sc    -> EBind <$> names f p <*> names f e <*> names f sc
    EIf e e' m_br   -> EIf <$> names f e <*> names f e' <*> names f m_br
    ECase e cb      -> ECase <$> names f e <*> names f cb
    EFor i c u e    -> EFor <$> names f i <*> names f c <*> names f u <*> names f e
    EWhile e e'     -> EWhile <$> names f e <*> names f e'
    EDoWhile e e'   -> EDoWhile <$> names f e <*> names f e'
    EReturn e       -> EReturn <$> names f e
    EBreak          -> pure ex
    EContinue       -> pure ex
