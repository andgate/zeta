{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.ExternDefn where

import           Language.Zeta.Syntax.Classy.HasNames.Class  (HasNames (..))
import           Language.Zeta.Syntax.Data.ExternDefn        (ExternDefn (getExternDefnName),
                                                              setExternDefnName)
import           Language.Zeta.Syntax.Data.Name.FunctionName (FunctionName)

instance HasNames ExternDefn FunctionName where
  names f defn = setExternDefnName defn <$> f (getExternDefnName defn)
