{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.Pat where

import                          Language.Zeta.Syntax.Classy.HasNames.Class       (HasNames (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasNames.Exp         ()
import                          Language.Zeta.Syntax.Classy.HasNames.Object      ()
import                          Language.Zeta.Syntax.Classy.HasNames.Traversable ()
import                          Language.Zeta.Syntax.Classy.HasNames.Type        ()
import                          Language.Zeta.Syntax.Data.Name.ClassName         (ClassName)
import                          Language.Zeta.Syntax.Data.Name.ConstructorName   (ConstructorName)
import                          Language.Zeta.Syntax.Data.Name.FieldName         (FieldName)
import                          Language.Zeta.Syntax.Data.Name.PatternName       (PatternName)
import                          Language.Zeta.Syntax.Data.Name.TypeName          (TypeName)
import                          Language.Zeta.Syntax.Data.Pat                    (Pat (..))

instance HasNames Pat ClassName where
  names f pat = case pat of
    PVar _          -> pure pat
    PCon cn ps      -> PCon cn <$> names f ps
    PTuple p ne     -> PTuple <$> names f p <*> names f ne
    PObj ob         -> PObj <$> names f ob
    PLabel pn p     -> PLabel pn <$> names f p
    PView e p       -> PView <$> names f e <*> names f p
    PWild           -> pure pat
    PType p ty      -> PType <$> names f p <*> names f ty
    PExpCache p ec  -> PExpCache <$> names f p <*> pure ec
    PTypeCache p tc -> PTypeCache <$> names f p <*> pure tc
    PLoc p sp       -> PLoc <$> names f p <*> pure sp
    PParens p       -> PParens <$> names f p

instance HasNames Pat ConstructorName where
  names f pat = case pat of
    PVar _          -> pure pat
    PCon cn ps      -> PCon <$> f cn <*> names f ps
    PTuple p ne     -> PTuple <$> names f p <*> names f ne
    PObj ob         -> PObj <$> names f ob
    PLabel pn p     -> PLabel pn <$> names f p
    PView e p       -> PView <$> names f e <*> names f p
    PWild           -> pure pat
    PType p ty      -> PType <$> names f p <*> pure ty
    PExpCache p ec  -> PExpCache <$> names f p <*> pure ec
    PTypeCache p tc -> PTypeCache <$> names f p <*> pure tc
    PLoc p sp       -> PLoc <$> names f p <*> pure sp
    PParens p       -> PParens <$> names f p

instance HasNames Pat FieldName where
  names f pat = case pat of
    PVar _          -> pure pat
    PCon cn ps      -> PCon cn <$> names f ps
    PTuple p ne     -> PTuple <$> names f p <*> names f ne
    PObj ob         -> PObj <$> names f ob
    PLabel pn p     -> PLabel pn <$> names f p
    PView e p       -> PView <$> names f e <*> names f p
    PWild           -> pure pat
    PType p ty      -> PType <$> names f p <*> pure ty
    PExpCache p ec  -> PExpCache <$> names f p <*> pure ec
    PTypeCache p tc -> PTypeCache <$> names f p <*> pure tc
    PLoc p sp       -> PLoc <$> names f p <*> pure sp
    PParens p       -> PParens <$> names f p

instance HasNames Pat PatternName where
  names f pat = case pat of
    PVar n          -> PVar <$> f n
    PCon cn ps      -> PCon cn <$> names f ps
    PTuple p ne     -> PTuple <$> names f p <*> names f ne
    PObj ob         -> PObj <$> names f ob
    PLabel pn p     -> PLabel <$> f pn <*> names f p
    PView e p       -> PView <$> names f e <*> names f p
    PWild           -> pure pat
    PType p ty      -> PType <$> names f p <*> pure ty
    PExpCache p ec  -> PExpCache <$> names f p <*> pure ec
    PTypeCache p tc -> PTypeCache <$> names f p <*> pure tc
    PLoc p sp       -> PLoc <$> names f p <*> pure sp
    PParens p       -> PParens <$> names f p

instance HasNames Pat TypeName where
  names f pat = case pat of
    PVar _          -> pure pat
    PCon cn ps      -> PCon cn <$> names f ps
    PTuple p ne     -> PTuple <$> names f p <*> names f ne
    PObj ob         -> PObj <$> names f ob
    PLabel pn p     -> PLabel pn <$> names f p
    PView e p       -> PView <$> names f e <*> names f p
    PWild           -> pure pat
    PType p ty      -> PType <$> names f p <*> names f ty
    PExpCache p ec  -> PExpCache <$> names f p <*> pure ec
    PTypeCache p tc -> PTypeCache <$> names f p <*> pure tc
    PLoc p sp       -> PLoc <$> names f p <*> pure sp
    PParens p       -> PParens <$> names f p
