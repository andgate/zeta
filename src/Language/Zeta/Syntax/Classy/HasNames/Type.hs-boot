{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.Type where

import           Language.Zeta.Syntax.Classy.HasNames.Class (HasNames)
import           Language.Zeta.Syntax.Data.Name.ClassName   (ClassName)
import           Language.Zeta.Syntax.Data.Name.TypeName    (TypeName)
import           Language.Zeta.Syntax.Data.Type             (Type)

instance HasNames Type ClassName
instance HasNames Type TypeName
