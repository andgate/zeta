{-# LANGUAGE MultiParamTypeClasses #-}
module Language.Zeta.Syntax.Classy.HasNames.Class where

import           Control.Lens.Traversal (Traversal')

-- | Classy name carriers.
class HasNames s n where
  -- | Traversal for some object that has some sort of name.
  names :: Traversal' s n
