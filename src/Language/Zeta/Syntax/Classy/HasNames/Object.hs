{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE FlexibleContexts      #-}
module Language.Zeta.Syntax.Classy.HasNames.Object where

import           Language.Zeta.Syntax.Classy.HasNames.Class       (HasNames (..))
import           Language.Zeta.Syntax.Classy.HasNames.Traversable ()
import           Language.Zeta.Syntax.Data.Name.FieldName         (FieldName)
import           Language.Zeta.Syntax.Data.Object                 (Object (..),
                                                                   ObjectField (..))

instance {-# OVERLAPS #-} (HasNames t FieldName) => HasNames (Object t) FieldName where
  names f (Object l fns) =
    Object l <$> names f fns

instance {-# OVERLAPS #-} (HasNames t FieldName) => HasNames (ObjectField t) FieldName where
  names f (ObjectField l n t) =
    ObjectField l <$> f n <*> names f t
