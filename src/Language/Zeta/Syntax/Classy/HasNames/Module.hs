{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.Module where

import           Language.Zeta.Syntax.Classy.HasNames.Class        (HasNames (..))
import           Language.Zeta.Syntax.Classy.HasNames.ModuleHeader ()
import           Language.Zeta.Syntax.Data.Module                  (Module (getModuleHeader),
                                                                    setModuleHeader)
import           Language.Zeta.Syntax.Data.Name.ModuleName         (ModuleName)

instance HasNames Module ModuleName where
  names f m =
    setModuleHeader m <$> names f (getModuleHeader m)
