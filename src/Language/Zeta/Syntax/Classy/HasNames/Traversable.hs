{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.Traversable where

import           Language.Zeta.Syntax.Classy.HasNames.Class (HasNames (..))

instance {-# OVERLAPPABLE #-} (Traversable f, HasNames a n) => HasNames (f a) n where
  names f = traverse (names f)
