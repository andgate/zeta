{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.ModuleHeader.ModuleName where

import           Language.Zeta.Syntax.Classy.HasNames.Class (HasNames (..))
import           Language.Zeta.Syntax.Data.ModuleHeader     (ModuleHeader (getModuleHeaderName),
                                                             setModuleHeaderName)
import           Language.Zeta.Syntax.Data.Name.ModuleName  (ModuleName)

instance HasNames ModuleHeader ModuleName where
  names f h =
    setModuleHeaderName h <$> f (getModuleHeaderName h)
