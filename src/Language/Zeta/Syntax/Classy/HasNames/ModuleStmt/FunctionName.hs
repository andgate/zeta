{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.ModuleStmt.FunctionName where

import           Language.Zeta.Syntax.Classy.HasNames.Class      (HasNames (..))
import           Language.Zeta.Syntax.Classy.HasNames.ExternDefn ()
import           Language.Zeta.Syntax.Classy.HasNames.FuncDefn   ()
import           Language.Zeta.Syntax.Data.ModuleStmt            (ModuleStmt (..))
import           Language.Zeta.Syntax.Data.Name.FunctionName     (FunctionName (..))

instance HasNames ModuleStmt FunctionName where
  names f = \case
    ModuleFuncDefn defn   -> ModuleFuncDefn <$> names f defn
    ModuleExternDefn defn -> ModuleExternDefn <$> names f defn
    stmt                  -> pure stmt
