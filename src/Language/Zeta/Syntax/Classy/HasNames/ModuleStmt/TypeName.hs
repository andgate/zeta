{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.ModuleStmt.TypeName where

import           Language.Zeta.Syntax.Classy.HasNames.Class             (HasNames (..))
import           Language.Zeta.Syntax.Classy.HasNames.TypeDefn.TypeName ()
import           Language.Zeta.Syntax.Data.ModuleStmt                   (ModuleStmt (..))
import           Language.Zeta.Syntax.Data.Name.TypeName                (TypeName (..))

instance HasNames ModuleStmt TypeName where
  names f = \case
    ModuleTypeDefn defn -> ModuleTypeDefn <$> names f defn
    stmt                -> pure stmt
