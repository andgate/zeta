{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.Clause where

import           Language.Zeta.Syntax.Classy.HasNames.Class       (HasNames (..))
import           Language.Zeta.Syntax.Classy.HasNames.Pat         ()
import           Language.Zeta.Syntax.Classy.HasNames.Traversable ()
import           Language.Zeta.Syntax.Data.Clause                 (Clause (..),
                                                                   ClauseBlock (..))
import           Language.Zeta.Syntax.Data.Name.ClassName         (ClassName)
import           Language.Zeta.Syntax.Data.Name.ConstructorName   (ConstructorName)
import           Language.Zeta.Syntax.Data.Name.FieldName         (FieldName)
import           Language.Zeta.Syntax.Data.Name.PatternName       (PatternName)
import           Language.Zeta.Syntax.Data.Name.TypeName          (TypeName)

instance HasNames ClauseBlock ClassName where
  names f (ClauseBlock m_sp cs) =
    ClauseBlock m_sp <$> names f cs

instance HasNames ClauseBlock ConstructorName where
  names f (ClauseBlock m_sp cs) =
    ClauseBlock m_sp <$> names f cs

instance HasNames ClauseBlock FieldName where
  names f (ClauseBlock m_sp cs) =
    ClauseBlock m_sp <$> names f cs

instance HasNames ClauseBlock PatternName where
  names f (ClauseBlock m_sp cs) =
    ClauseBlock m_sp <$> names f cs

instance HasNames ClauseBlock TypeName where
  names f (ClauseBlock m_sp cs) =
    ClauseBlock m_sp <$> names f cs

instance HasNames Clause ClassName where
  names f (Clause m_sp ps sc) =
    Clause m_sp <$> names f ps <*> names f sc

instance HasNames Clause ConstructorName where
  names f (Clause m_sp ps sc) =
    Clause m_sp <$> names f ps <*> names f sc

instance HasNames Clause FieldName where
  names f (Clause m_sp ps sc) =
    Clause m_sp <$> names f ps <*> names f sc

instance HasNames Clause PatternName where
  names f (Clause m_sp ps sc) =
    Clause m_sp <$> names f ps <*> names f sc

instance HasNames Clause TypeName where
  names f (Clause m_sp ps sc) =
    Clause m_sp <$> names f ps <*> names f sc
