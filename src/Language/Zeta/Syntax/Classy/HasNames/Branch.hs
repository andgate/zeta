{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.Branch where

import                          Language.Zeta.Syntax.Classy.HasNames.Class       (HasNames (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasNames.Exp         ()
import                          Language.Zeta.Syntax.Classy.HasNames.Traversable ()
import                          Language.Zeta.Syntax.Data.Branch                 (Branch (..))
import                          Language.Zeta.Syntax.Data.Name.ClassName         (ClassName)
import                          Language.Zeta.Syntax.Data.Name.ConstructorName   (ConstructorName)
import                          Language.Zeta.Syntax.Data.Name.FieldName         (FieldName)
import                          Language.Zeta.Syntax.Data.Name.PatternName       (PatternName)
import                          Language.Zeta.Syntax.Data.Name.TypeName          (TypeName)

instance HasNames Branch ClassName where
  names f = \case
    ElseBranch m_sp e ->
      ElseBranch m_sp <$> names f e
    ElifBranch m_sp e e' m_br ->
      ElifBranch m_sp <$> names f e <*> names f e' <*> names f m_br

instance HasNames Branch ConstructorName where
  names f = \case
    ElseBranch m_sp e ->
      ElseBranch m_sp <$> names f e
    ElifBranch m_sp e e' m_br ->
      ElifBranch m_sp <$> names f e <*> names f e' <*> names f m_br

instance HasNames Branch FieldName where
  names f = \case
    ElseBranch m_sp e ->
      ElseBranch m_sp <$> names f e
    ElifBranch m_sp e e' m_br ->
      ElifBranch m_sp <$> names f e <*> names f e' <*> names f m_br

instance HasNames Branch PatternName where
  names f = \case
    ElseBranch m_sp e ->
      ElseBranch m_sp <$> names f e
    ElifBranch m_sp e e' m_br ->
      ElifBranch m_sp <$> names f e <*> names f e' <*> names f m_br

instance HasNames Branch TypeName where
  names f = \case
    ElseBranch m_sp e ->
      ElseBranch m_sp <$> names f e
    ElifBranch m_sp e e' m_br ->
      ElifBranch m_sp <$> names f e <*> names f e' <*> names f m_br
