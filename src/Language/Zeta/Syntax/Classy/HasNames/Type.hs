{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.Type where

import           Language.Zeta.Syntax.Classy.HasNames.Class       (HasNames (..))
import           Language.Zeta.Syntax.Classy.HasNames.Traversable ()
import           Language.Zeta.Syntax.Classy.HasNames.TypeContext ()
import           Language.Zeta.Syntax.Data.Name.ClassName         (ClassName)
import           Language.Zeta.Syntax.Data.Name.TypeName          (TypeName)
import           Language.Zeta.Syntax.Data.Type                   (Type (..))

instance HasNames Type ClassName where
  names f typ = case typ of
    TVar _       -> pure typ
    TFunc tys ty -> TFunc <$> names f tys <*> names f ty
    TApp ty tys  -> TApp <$> names f ty <*> names f tys
    TCon _       -> pure typ
    TInt _       -> pure typ
    TUInt _      -> pure typ
    TFp _        -> pure typ
    TTuple ty ne -> TTuple <$> names f ty <*> names f ne
    TArray n ty  -> TArray n <$> names f ty
    TPtr ty      -> TPtr <$> names f ty
    TCtx ctx ty  -> TCtx <$> names f ctx <*> names f ty
    TCache ty tc -> TCache <$> names f ty <*> pure tc
    TLoc ty sp   -> TLoc <$> names f ty <*> pure sp
    TParens ty   -> TParens <$> names f ty

instance HasNames Type TypeName where
  names f typ = case typ of
    TVar _       -> pure typ
    TFunc tys ty -> TFunc <$> names f tys <*> names f ty
    TApp ty tys  -> TApp <$> names f ty <*> names f tys
    TCon tn      -> TCon <$> f tn
    TInt _       -> pure typ
    TUInt _      -> pure typ
    TFp _        -> pure typ
    TTuple ty ne -> TTuple <$> names f ty <*> names f ne
    TArray n ty  -> TArray n <$> names f ty
    TPtr ty      -> TPtr <$> names f ty
    TCtx ctx ty  -> TCtx <$> names f ctx <*> names f ty
    TCache ty tc -> TCache <$> names f ty <*> pure tc
    TLoc ty sp   -> TLoc <$> names f ty <*> pure sp
    TParens ty   -> TParens <$> names f ty
