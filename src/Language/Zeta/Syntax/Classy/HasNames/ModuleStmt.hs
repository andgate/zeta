module Language.Zeta.Syntax.Classy.HasNames.ModuleStmt where

import           Language.Zeta.Syntax.Classy.HasNames.ModuleStmt.FunctionName ()
import           Language.Zeta.Syntax.Classy.HasNames.ModuleStmt.TypeName     ()
