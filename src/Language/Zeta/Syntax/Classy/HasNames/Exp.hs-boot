{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.Exp where

import           Language.Zeta.Syntax.Classy.HasNames.Class     (HasNames)
import           Language.Zeta.Syntax.Data.Exp                  (Exp)
import           Language.Zeta.Syntax.Data.Name.ClassName       (ClassName)
import           Language.Zeta.Syntax.Data.Name.ConstructorName (ConstructorName)
import           Language.Zeta.Syntax.Data.Name.FieldName       (FieldName)
import           Language.Zeta.Syntax.Data.Name.PatternName     (PatternName)
import           Language.Zeta.Syntax.Data.Name.TypeName        (TypeName)

instance HasNames Exp ClassName
instance HasNames Exp ConstructorName
instance HasNames Exp FieldName
instance HasNames Exp PatternName
instance HasNames Exp TypeName
