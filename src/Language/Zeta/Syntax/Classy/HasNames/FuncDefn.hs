{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.FuncDefn where

import           Language.Zeta.Syntax.Classy.HasNames.Class  (HasNames (..))
import           Language.Zeta.Syntax.Data.FuncDefn          (FuncDefn (getFuncDefnName),
                                                              setFuncDefnName)
import           Language.Zeta.Syntax.Data.Name.FunctionName (FunctionName)

instance HasNames FuncDefn FunctionName where
  names f defn = setFuncDefnName defn <$> f (getFuncDefnName defn)
