module Language.Zeta.Syntax.Classy.HasNames.TypeDefn where

import Language.Zeta.Syntax.Classy.HasNames.TypeDefn.ConstructorName ()
import Language.Zeta.Syntax.Classy.HasNames.TypeDefn.TypeName ()
