{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances  #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.HasNames.Operator where

import           Language.Zeta.Syntax.Classy.HasNames.Class (HasNames (..))
import           Language.Zeta.Syntax.Data.Operator         (BinaryOperator,
                                                             UnaryOperator)

instance HasNames UnaryOperator String where
  names f op = op <$ f (show op)

instance HasNames BinaryOperator String where
  names f op = op <$ f (show op)
