{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE LambdaCase            #-}
module Language.Zeta.Syntax.Classy.HasNames.TypeContext where

import                          Language.Zeta.Syntax.Classy.HasNames.Class       (HasNames (..))
import                          Language.Zeta.Syntax.Classy.HasNames.Traversable ()
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasNames.Type        ()
import                          Language.Zeta.Syntax.Data.Name.ClassName         (ClassName)
import                          Language.Zeta.Syntax.Data.Name.TypeName          (TypeName)
import                          Language.Zeta.Syntax.Data.TypeContext            (ClassConstraint (..),
                                                                                  ClassInstance (..),
                                                                                  TypeContext (..),
                                                                                  TypeContextElement (..),
                                                                                  TypeSchema (..))

instance HasNames TypeContext ClassName where
  names f (TypeContext l es) =
    TypeContext l <$> names f es

instance HasNames TypeContextElement ClassName where
  names f = \case
    TypeContextSchema ts     -> TypeContextSchema <$> names f ts
    TypeContextConstraint cc -> TypeContextConstraint <$> names f cc

instance HasNames TypeSchema ClassName where
  names _ = pure

instance HasNames ClassConstraint ClassName where
  names f (ClassConstraint l cs) =
    ClassConstraint l <$> names f cs

instance HasNames ClassInstance ClassName where
  names f (ClassInstance l n ty) =
    ClassInstance l <$> f n <*> names f ty

instance HasNames TypeContext TypeName where
  names f (TypeContext l es) =
    TypeContext l <$> names f es

instance HasNames TypeContextElement TypeName where
  names f = \case
    TypeContextSchema ts     -> TypeContextSchema <$> names f ts
    TypeContextConstraint cc -> TypeContextConstraint <$> names f cc

instance HasNames TypeSchema TypeName where
  names _ = pure

instance HasNames ClassConstraint TypeName where
  names f (ClassConstraint l cs) =
    ClassConstraint l <$> names f cs

instance HasNames ClassInstance TypeName where
  names f (ClassInstance l n ty) =
    ClassInstance l n <$> names f ty
