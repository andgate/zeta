{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Object where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Class          (HasTypeCache (..))
import           Language.Zeta.Syntax.Classy.HasTypeCache.Foldable       ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Name.FieldName ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Type           ()
import           Language.Zeta.Syntax.Data.Object                        (Object (..),
                                                                          ObjectField (..))

instance (HasTypeCache t) => HasTypeCache (Object t) where
  getTypeCache (Object _ flds) =
    getTypeCache flds

instance (HasTypeCache t) => HasTypeCache (ObjectField t) where
  getTypeCache (ObjectField _ fn t) =
    getTypeCache fn <> getTypeCache t
