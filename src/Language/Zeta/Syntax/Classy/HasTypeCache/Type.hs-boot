{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Type where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Class (HasTypeCache)
import           Language.Zeta.Syntax.Data.Type                 (Type)

instance HasTypeCache Type
