{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.TVar where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Class (HasTypeCache (..))
import           Language.Zeta.Syntax.Data.TVar                 (TVar (..))
import           Language.Zeta.Syntax.Data.TypeCache            (newTypeCache)

instance HasTypeCache TVar where
  getTypeCache tv =
    case getTVarBinder tv of
      Nothing -> newTypeCache True 0
      Just b  -> newTypeCache False (b + 1)
