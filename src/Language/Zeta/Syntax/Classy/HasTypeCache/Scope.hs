{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Scope where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Class (HasTypeCache (..))
import           Language.Zeta.Syntax.Data.Scope                (Scope (..))

instance HasTypeCache t => HasTypeCache (Scope t) where
  getTypeCache = getTypeCache . getScopeBody
