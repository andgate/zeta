{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Type where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Class       (HasTypeCache (..))
import           Language.Zeta.Syntax.Classy.HasTypeCache.Foldable    ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.TVar        ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.TypeContext ()
import           Language.Zeta.Syntax.Data.Type                       (Type (..))

instance HasTypeCache Type where
  getTypeCache ty = case ty of
    TVar tv       -> getTypeCache tv
    TFunc tys ty' -> getTypeCache tys <> getTypeCache ty'
    TApp ty' tys  -> getTypeCache ty' <> getTypeCache tys
    TCon _        -> mempty
    TInt _        -> mempty
    TUInt _       -> mempty
    TFp _         -> mempty
    TTuple ty' ne -> getTypeCache ty' <> getTypeCache ne
    TArray _ ty'  -> getTypeCache ty'
    TPtr ty'      -> getTypeCache ty'
    TCtx tc ty'   -> getTypeCache tc <> getTypeCache ty'
    TCache _ tc   -> tc
    TLoc ty' _    -> getTypeCache ty'
    TParens ty'   -> getTypeCache ty'
