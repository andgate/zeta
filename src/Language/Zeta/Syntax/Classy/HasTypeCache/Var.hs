{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Var where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Class (HasTypeCache (..))
import           Language.Zeta.Syntax.Classy.HasTypeCache.Type  ()
import           Language.Zeta.Syntax.Data.Var                  (Var (..))

instance HasTypeCache Var where
  getTypeCache =
    foldMap getTypeCache . getVarType
