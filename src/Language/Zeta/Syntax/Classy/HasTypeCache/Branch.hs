{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Branch where

import                          Language.Zeta.Syntax.Classy.HasTypeCache.Class    (HasTypeCache (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasTypeCache.Exp      ()
import                          Language.Zeta.Syntax.Classy.HasTypeCache.Foldable ()
import                          Language.Zeta.Syntax.Data.Branch                  (Branch (..))

instance HasTypeCache Branch where
  getTypeCache = \case
    ElseBranch _ e ->
      getTypeCache e
    ElifBranch _ e e' m_br ->
      getTypeCache e <> getTypeCache e' <> getTypeCache m_br
