{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Pat where

import                          Language.Zeta.Syntax.Classy.HasTypeCache.Class                (HasTypeCache (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasTypeCache.Exp                  ()
import                          Language.Zeta.Syntax.Classy.HasTypeCache.Foldable             ()
import                          Language.Zeta.Syntax.Classy.HasTypeCache.Name.ConstructorName ()
import                          Language.Zeta.Syntax.Classy.HasTypeCache.Name.PatternName     ()
import                          Language.Zeta.Syntax.Classy.HasTypeCache.Object               ()
import                          Language.Zeta.Syntax.Classy.HasTypeCache.Type                 ()
import                          Language.Zeta.Syntax.Data.Pat                                 (Pat (..))

instance HasTypeCache Pat where
  getTypeCache pt =
    case pt of
      PVar pn         -> getTypeCache pn
      PCon cn pats    -> getTypeCache cn <> getTypeCache pats
      PTuple pat ne   -> getTypeCache pat <> getTypeCache ne
      PObj ob         -> getTypeCache ob
      PLabel pn pat   -> getTypeCache pn <> getTypeCache pat
      PView ex pat    -> getTypeCache ex <> getTypeCache pat
      PWild           -> mempty
      PType pat ty    -> getTypeCache pat <> getTypeCache ty
      PExpCache pat _ -> getTypeCache pat
      PTypeCache _ tc -> tc
      PLoc pat _      -> getTypeCache pat
      PParens pat     -> getTypeCache pat
