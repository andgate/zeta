{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Clause where

import                          Language.Zeta.Syntax.Classy.HasTypeCache.Class    (HasTypeCache (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasTypeCache.Exp      ()
import                          Language.Zeta.Syntax.Classy.HasTypeCache.Foldable ()
import                          Language.Zeta.Syntax.Classy.HasTypeCache.Pat      ()
import                          Language.Zeta.Syntax.Classy.HasTypeCache.Scope    ()
import                          Language.Zeta.Syntax.Data.Clause                  (Clause (..),
                                                                                   ClauseBlock (..))

instance HasTypeCache ClauseBlock where
  getTypeCache (ClauseBlock _ cs) =
    getTypeCache cs

instance HasTypeCache Clause where
  getTypeCache (Clause _ ps sc) =
    getTypeCache ps <> getTypeCache sc
