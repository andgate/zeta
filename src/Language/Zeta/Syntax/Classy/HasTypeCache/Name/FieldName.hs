{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Name.FieldName where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Class    (HasTypeCache (..))
import           Language.Zeta.Syntax.Classy.HasTypeCache.Foldable ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Type     ()
import           Language.Zeta.Syntax.Data.Name.FieldName          (FieldName (..))

instance HasTypeCache FieldName where
  getTypeCache = getTypeCache . getFieldNameType
