{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Name.ConstructorName where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Class    (HasTypeCache (..))
import           Language.Zeta.Syntax.Classy.HasTypeCache.Foldable ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Type     ()
import           Language.Zeta.Syntax.Data.Name.ConstructorName    (ConstructorName (..))

instance HasTypeCache ConstructorName where
  getTypeCache = getTypeCache . getConstructorNameType
