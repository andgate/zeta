{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Name.PatternName where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Class    (HasTypeCache (..))
import           Language.Zeta.Syntax.Classy.HasTypeCache.Foldable ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Type     ()
import           Language.Zeta.Syntax.Data.Name.PatternName        (PatternName (..))

instance HasTypeCache PatternName where
  getTypeCache = getTypeCache . getPatternNameType
