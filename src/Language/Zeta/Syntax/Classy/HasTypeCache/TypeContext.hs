{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.TypeContext where

import                          Language.Zeta.Syntax.Classy.HasTypeCache.Class    (HasTypeCache (..))
import                          Language.Zeta.Syntax.Classy.HasTypeCache.Foldable ()
import                          Language.Zeta.Syntax.Classy.HasTypeCache.TVar     ()
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.HasTypeCache.Type     ()
import                          Language.Zeta.Syntax.Data.TypeContext             (ClassConstraint (..),
                                                                                   ClassInstance (..),
                                                                                   TypeContext (..),
                                                                                   TypeContextElement (..),
                                                                                   TypeSchema (..))

instance HasTypeCache TypeContext where
  getTypeCache (TypeContext _ tcs) =
    foldMap getTypeCache tcs

instance HasTypeCache TypeContextElement where
  getTypeCache (TypeContextSchema sch)    = getTypeCache sch
  getTypeCache (TypeContextConstraint cc) = getTypeCache cc

instance HasTypeCache TypeSchema where
  getTypeCache (TypeSchema _ tvs) = getTypeCache tvs

instance HasTypeCache ClassConstraint where
  getTypeCache (ClassConstraint _ cs) = getTypeCache cs

instance HasTypeCache ClassInstance where
  getTypeCache (ClassInstance _ _ args) = getTypeCache args
