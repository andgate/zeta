{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Exp where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Branch               ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Class                (HasTypeCache (..))
import           Language.Zeta.Syntax.Classy.HasTypeCache.Clause               ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Foldable             ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Name.ConstructorName ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Name.FieldName       ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Pat                  ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Scope                ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Type                 ()
import           Language.Zeta.Syntax.Classy.HasTypeCache.Var                  ()
import           Language.Zeta.Syntax.Data.Exp                                 (Exp (..))

instance HasTypeCache Exp where
  getTypeCache ex = case ex of
      EVar var          -> getTypeCache var
      ELit _            -> mempty
      ECall e es        -> getTypeCache e <> getTypeCache es
      ELam pats m_ty sc -> getTypeCache pats <> getTypeCache m_ty <> getTypeCache sc
      EType e ty        -> getTypeCache e <> getTypeCache ty
      ECast e ty        -> getTypeCache e <> getTypeCache ty
      ECache e _        -> getTypeCache e
      ETypeCache _ tc   -> tc
      ELoc e _          -> getTypeCache e
      EParens e         -> getTypeCache e
      ERef e            -> getTypeCache e
      EDeref e          -> getTypeCache e
      ETuple e ne       -> getTypeCache e <> getTypeCache ne
      ECon cn           -> getTypeCache cn
      EArray es         -> getTypeCache es
      EObj ob           -> getTypeCache ob
      EFree e           -> getTypeCache e
      EField fn         -> getTypeCache fn
      EGet e fn         -> getTypeCache e <> getTypeCache fn
      EGetI e e'        -> getTypeCache e <> getTypeCache e'
      ESet e e'         -> getTypeCache e <> getTypeCache e'
      EUnOp _ e         -> getTypeCache e
      EBinOp _ e e'     -> getTypeCache e <> getTypeCache e'
      EBlock e          -> getTypeCache e
      ESeq e e'         -> getTypeCache e <> getTypeCache e'
      EBind p e sc      -> getTypeCache p <> getTypeCache e <> getTypeCache sc
      EIf e e' m_br     -> getTypeCache e <> getTypeCache e' <> getTypeCache m_br
      ECase e cb        -> getTypeCache e <> getTypeCache cb
      EFor i c u e      -> getTypeCache i <> getTypeCache c <> getTypeCache u <> getTypeCache e
      EWhile e e'       -> getTypeCache e <> getTypeCache e'
      EDoWhile e e'     -> getTypeCache e <> getTypeCache e'
      EReturn e         -> getTypeCache e
      EBreak            -> mempty
      EContinue         -> mempty
