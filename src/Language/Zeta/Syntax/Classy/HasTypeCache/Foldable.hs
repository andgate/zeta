{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Language.Zeta.Syntax.Classy.HasTypeCache.Foldable where

import           Language.Zeta.Syntax.Classy.HasTypeCache.Class (HasTypeCache (..))

instance {-# OVERLAPPABLE #-} (Foldable f, HasTypeCache t) => HasTypeCache (f t) where
  getTypeCache = foldMap getTypeCache
