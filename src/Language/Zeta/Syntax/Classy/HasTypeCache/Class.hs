module Language.Zeta.Syntax.Classy.HasTypeCache.Class where

import           Language.Zeta.Syntax.Data.TypeCache (TypeCache)

class HasTypeCache t where
  getTypeCache :: t -> TypeCache
