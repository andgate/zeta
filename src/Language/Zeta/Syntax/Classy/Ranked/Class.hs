{-# LANGUAGE MultiParamTypeClasses #-}
module Language.Zeta.Syntax.Classy.Ranked.Class where

-- | Classy rankable types.
class Ranked a r where
  -- | Get the rank of something.
  rank :: a -> r
