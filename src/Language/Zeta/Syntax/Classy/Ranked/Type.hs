{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Ranked.Type where

import           Language.Zeta.Syntax.Classy.Ranked.Class (Ranked (..))
import           Language.Zeta.Syntax.Data.Type           (Type)
import           Language.Zeta.Syntax.Data.TypeRank       (TypeRank, typeRank)

instance Ranked Type TypeRank where
  rank = typeRank
