{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Ranked.Exp where

import           Language.Zeta.Syntax.Classy.Ranked.Class (Ranked (..))
import           Language.Zeta.Syntax.Data.Exp            (Exp)
import           Language.Zeta.Syntax.Data.ExpRank        (ExpRank, expRank)

instance Ranked Exp ExpRank where
  rank = expRank
