{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Syntax.Classy.Ranked.Pat where

import           Language.Zeta.Syntax.Classy.Ranked.Class (Ranked (..))
import           Language.Zeta.Syntax.Data.Pat            (Pat)
import           Language.Zeta.Syntax.Data.PatRank        (PatRank, patRank)

instance Ranked Pat PatRank where
  rank = patRank
