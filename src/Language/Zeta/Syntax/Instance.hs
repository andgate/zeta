{-|
Module      : Language.Zeta.Syntax.Instance
Description : Zeta type class instance syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Instance (module X) where

import           Language.Zeta.Syntax.Data.InstanceDefn as X
