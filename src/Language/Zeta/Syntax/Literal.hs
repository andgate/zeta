{-|
Module      : Language.Zeta.Syntax.Literal
Description : Zeta literal syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Literal (module X) where

import           Language.Zeta.Syntax.Data.Lit as X
