module Language.Zeta.Syntax.Optics.Clause where

import           Control.Lens.Prism                       (Prism', prism)
import           Data.List.NonEmpty                       (NonEmpty)
import           Document.Data.Span                       (Span)
import           Language.Zeta.Syntax.Classy.Abstracts    (Abstracts (..))
import           Language.Zeta.Syntax.Classy.Instantiable (Instantiable (..))
import           Language.Zeta.Syntax.Data.Clause         (Clause (..))
import           Language.Zeta.Syntax.Data.Exp            (Exp)
import           Language.Zeta.Syntax.Data.Pat            (Pat)

_Clause :: Prism' Clause (Maybe Span, NonEmpty Pat, Exp)
_Clause = prism toClause fromClause
  where
    {-# INLINE toClause #-}
    toClause (m_sp, ps, e) =
      Clause m_sp ps (abstract ps e)
    {-# INLINE fromClause #-}
    fromClause (Clause m_sp ps sc) =
      Right (m_sp, ps, instantiate ps sc)
