module Language.Zeta.Syntax.Optics.FuncDefn where

import           Control.Lens.Lens                           (Lens', lens)
import           Document.Data.Span                          (Span)
import           Language.Zeta.Syntax.Data.FuncDefn          (FuncDefn (..),
                                                              FuncDefnBody,
                                                              setFuncDefnArgs,
                                                              setFuncDefnBody,
                                                              setFuncDefnLoc,
                                                              setFuncDefnName,
                                                              setFuncDefnReturnType,
                                                              setFuncDefnTypeCtx)
import           Language.Zeta.Syntax.Data.Name.FunctionName (FunctionName)
import           Language.Zeta.Syntax.Data.Pat               (Pat)
import           Language.Zeta.Syntax.Data.Type              (Type)
import           Language.Zeta.Syntax.Data.TypeContext       (TypeContext)

-- | Lens for the source location of a function definition.
funcDefnLoc :: Lens' FuncDefn (Maybe Span)
funcDefnLoc = lens getFuncDefnLoc setFuncDefnLoc

-- | Lens for the type context preceeding a function definition.
funcDefnTyCtx :: Lens' FuncDefn (Maybe TypeContext)
funcDefnTyCtx = lens getFuncDefnTyCtx setFuncDefnTypeCtx

-- | Lens for the 'FunctionName' of a function definition.
funcDefnName :: Lens' FuncDefn FunctionName
funcDefnName = lens getFuncDefnName setFuncDefnName

-- | Lens for the function parameters of a function definition.
funcDefnArgs :: Lens' FuncDefn [Pat]
funcDefnArgs = lens getFuncDefnArgs setFuncDefnArgs

-- | Lens for the return type of a function definition.
funcDefnReturnType :: Lens' FuncDefn (Maybe Type)
funcDefnReturnType = lens getFuncDefnReturnType setFuncDefnReturnType

-- | Lens for the body of a function definition.
funcDefnBody :: Lens' FuncDefn FuncDefnBody
funcDefnBody = lens getFuncDefnBody setFuncDefnBody
