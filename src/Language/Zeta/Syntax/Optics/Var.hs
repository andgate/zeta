{-# LANGUAGE OverloadedStrings #-}
module Language.Zeta.Syntax.Optics.Var where

import           Control.Lens.Fold                              (has)
import           Control.Lens.Getter                            (Getter, to)
import           Control.Lens.Lens                              (Lens, Lens',
                                                                 lens)
import           Control.Lens.Prism                             (_Just)
import           Data.Text                                      (Text)
import qualified Data.Text                                      as T
import           Document.Data.Span                             (Span)
import           Language.Zeta.Syntax.Data.Name.ConstructorName (ConstructorName,
                                                                 newConstructorName)
import           Language.Zeta.Syntax.Data.Name.FieldName       (FieldName,
                                                                 newFieldName)
import           Language.Zeta.Syntax.Data.Type                 (Type)
import           Language.Zeta.Syntax.Data.Var                  (Var (..),
                                                                 setVarBinder,
                                                                 setVarLoc,
                                                                 setVarStr,
                                                                 setVarType)

-- | Getter for the bounded string rep.
-- An unbounded var x looks like: x
-- A var x, bounded on 1, looks like: x@1
varBoundStr :: Getter Var Text
varBoundStr = to $
  \v -> case getVarBinder v of
    Nothing -> getVarStr v
    Just b  -> getVarStr v <> "@" <> T.pack (show b)

-- | Lens into the text representation of a variable.
varStr :: Lens' Var Text
varStr = lens getVarStr setVarStr

-- | Lens into the binder of a variable.
varBinder :: Lens' Var (Maybe Int)
varBinder = lens getVarBinder setVarBinder

-- | Lens into the type annotation of a variable.
varType :: Lens Var Var (Maybe Type) (Maybe Type)
varType = lens getVarType setVarType

-- | Lens into the source location annotation of a variable.
varLoc :: Lens' Var (Maybe Span)
varLoc = lens getVarLoc setVarLoc

-- | Test is a variable has a binder.
hasVarBinder
  :: Var  -- ^ Variable to test.
  -> Bool -- ^ Result of test, 'True' if variable has a binder, otherwise 'False'.
hasVarBinder = has (varBinder . _Just)

-- | Test is a variable has a type annotation.
hasVarType
  :: Var  -- ^ Variable to test.
  -> Bool -- ^ Result of test, 'True' if variable has a type annotation, otherwise 'False'.
hasVarType = has (varType . _Just)

-- | Test is a variable has a source location.
hasVarLoc
  :: Var  -- ^ Variable to test.
  -> Bool -- ^ Result of test, 'True' if variable has a source location, otherwise 'False'.
hasVarLoc = has (varLoc . _Just)

-- | Getter for a 'ConstructorName' from a 'Var'.
toConstructorName :: Getter Var ConstructorName
toConstructorName = to (\v -> newConstructorName (getVarLoc v) (getVarStr v))

-- | Getter for a 'FieldName' from a 'Var'.
toFieldName :: Getter Var FieldName
toFieldName = to (\v -> newFieldName (getVarLoc v) (getVarStr v))
