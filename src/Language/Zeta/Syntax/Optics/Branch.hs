{-# LANGUAGE LambdaCase #-}
module Language.Zeta.Syntax.Optics.Branch where

import           Control.Lens.Prism               (Prism', prism)
import           Document.Data.Span               (Span)
import           Language.Zeta.Syntax.Data.Branch (Branch (..))
import           Language.Zeta.Syntax.Data.Exp    (Exp)

_ElseBranch :: Prism' Branch (Maybe Span, Exp)
_ElseBranch = prism toElseBranch fromElseBranch
  where
    {-# INLINE toElseBranch #-}
    toElseBranch (m_sp, e) =
      ElseBranch m_sp e
    {-# INLINE fromElseBranch #-}
    fromElseBranch = \case
      ElseBranch m_sp e ->
        Right (m_sp, e)
      br -> Left br

_ElifBranch :: Prism' Branch (Maybe Span, Exp, Exp, Maybe Branch)
_ElifBranch = prism toElifBranch fromElifBranch
  where
    {-# INLINE toElifBranch #-}
    toElifBranch (m_sp, p, e, m_br) =
      ElifBranch m_sp p e m_br
    {-# INLINE fromElifBranch #-}
    fromElifBranch = \case
      ElifBranch m_sp p e m_br ->
        Right (m_sp, p, e, m_br)
      br -> Left br
