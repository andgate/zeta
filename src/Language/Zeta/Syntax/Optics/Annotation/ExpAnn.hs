module Language.Zeta.Syntax.Optics.Annotation.ExpAnn where

import           Control.Lens.Lens                           (Lens', lens)
import           Document.Data.Span                          (Span)
import           Language.Zeta.Syntax.Data.Annotation.ExpAnn (ExpAnn (..),
                                                              setExpAnnExpCache,
                                                              setExpAnnLoc,
                                                              setExpAnnType,
                                                              setExpAnnTypeCache)
import           Language.Zeta.Syntax.Data.ExpCache          (ExpCache)
import           Language.Zeta.Syntax.Data.Type              (Type)
import           Language.Zeta.Syntax.Data.TypeCache         (TypeCache)

-- | Lens into the source location annotation of an expression annotation.
expAnnLoc :: Lens' ExpAnn (Maybe Span)
expAnnLoc = lens getExpAnnLoc setExpAnnLoc

-- | Lens into the type of an expression annotation.
expAnnType :: Lens' ExpAnn (Maybe Type)
expAnnType = lens getExpAnnType setExpAnnType

-- | Lens into the expression cache annotation of an expression annotation.
expAnnExpCache :: Lens' ExpAnn (Maybe ExpCache)
expAnnExpCache = lens getExpAnnExpCache setExpAnnExpCache

-- | Lens into the type cache annotation of an expression annotation.
expAnnTypeCache :: Lens' ExpAnn (Maybe TypeCache)
expAnnTypeCache = lens getExpAnnTypeCache setExpAnnTypeCache
