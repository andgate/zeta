module Language.Zeta.Syntax.Optics.Annotation.TypeAnn where

import           Control.Lens.Lens                            (Lens', lens)
import           Document.Data.Span                           (Span)
import           Language.Zeta.Syntax.Data.Annotation.TypeAnn (TypeAnn (..),
                                                               setTypeAnnCache,
                                                               setTypeAnnLoc)
import           Language.Zeta.Syntax.Data.TypeCache          (TypeCache)

-- | Lens into the source location of a 'TypeAnn'.
typeAnnLoc :: Lens' TypeAnn (Maybe Span)
typeAnnLoc = lens getTypeAnnLoc setTypeAnnLoc

-- | Lens into the type cache annotation of a 'TypeAnn'.
typeAnnCache :: Lens' TypeAnn (Maybe TypeCache)
typeAnnCache = lens getTypeAnnCache setTypeAnnCache
