{-# LANGUAGE OverloadedStrings #-}
module Language.Zeta.Syntax.Optics.TVar where

import           Control.Lens.Fold                       (has)
import           Control.Lens.Getter                     (Getter, to)
import           Control.Lens.Lens                       (Lens, Lens', lens)
import           Control.Lens.Prism                      (_Just)
import           Data.Text                               (Text)
import qualified Data.Text                               as T
import           Document.Data.Span                      (Span)
import           Language.Zeta.Syntax.Data.Name.TypeName (TypeName, newTypeName)
import           Language.Zeta.Syntax.Data.TVar          (TVar (..),
                                                          setTVarBinder,
                                                          setTVarLoc,
                                                          setTVarStr,
                                                          setTVarType)
import           Language.Zeta.Syntax.Data.Type          (Type)

-- | Getter for the bounded string rep.
-- An unbounded tvar x looks like: x
-- A tvar x, bounded on 1, looks like: x@1
tvarBoundStr :: Getter TVar Text
tvarBoundStr = to $
  \v -> case getTVarBinder v of
    Nothing -> getTVarStr v
    Just b  -> getTVarStr v <> "@" <> T.pack (show b)

-- | Lens into the text representation of a type variable.
tvarStr :: Lens' TVar Text
tvarStr = lens getTVarStr setTVarStr

-- | Lens into the binder of a type variable.
tvarBinder :: Lens' TVar (Maybe Int)
tvarBinder = lens getTVarBinder setTVarBinder

-- | Lens into the type annotation of a type variable.
tvarType :: Lens TVar TVar (Maybe Type) Type
tvarType = lens getTVarType setTVarType

-- | Lens into the source location annotation of a type variable.
tvarLoc :: Lens' TVar (Maybe Span)
tvarLoc = lens getTVarLoc setTVarLoc

-- | Test is a type variable has a binder.
hasTVarBinder
  :: TVar  -- ^ Type variable to test.
  -> Bool -- ^ Result of test, 'True' if type variable has a binder, otherwise 'False'.
hasTVarBinder = has (tvarBinder . _Just)

-- | Getter to get a 'TypeName' from a 'TVar'.
toTypeName :: Getter TVar TypeName
toTypeName = to (\tv -> newTypeName (getTVarLoc tv) (getTVarStr tv))
