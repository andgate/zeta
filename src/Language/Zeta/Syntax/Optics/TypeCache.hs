module Language.Zeta.Syntax.Optics.TypeCache where

import           Control.Lens.Lens                   (Lens', lens)
import           Language.Zeta.Syntax.Data.TypeCache (TypeCache (..),
                                                      setTypeCacheHasFree,
                                                      setTypeCacheMaxBound)

-- | Lens into the has free variables flag of a type cache.
typeCacheHasFree :: Lens' TypeCache Bool
typeCacheHasFree = lens getTypeCacheHasFree setTypeCacheHasFree

-- | Lens into the max bound variable value of a type cache.
typeCacheMaxBound :: Lens' TypeCache Int
typeCacheMaxBound = lens getTypeCacheMaxBound setTypeCacheMaxBound
