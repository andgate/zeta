{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
module Language.Zeta.Syntax.Optics.Scope where

import           Control.Lens.Lens               (Lens, Lens', lens)
import           Language.Zeta.Syntax.Data.Scope (Scope (..), setScopeBody,
                                                  setScopeLevel)

-- | Lens into the binding level of some scope.
scopeLevel :: forall t. Lens' (Scope t) Int
scopeLevel = lens getScopeLevel setScopeLevel

-- | Lens into the term body of some scope.
scopeBody :: forall a b. Lens (Scope a) (Scope b) a b
scopeBody = lens getScopeBody setScopeBody
