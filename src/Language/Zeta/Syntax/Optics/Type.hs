{-# LANGUAGE LambdaCase #-}
module Language.Zeta.Syntax.Optics.Type where

import           Control.Lens.Lens                              (Lens', lens)
import           Control.Lens.Prism                             (Prism', _Just,
                                                                 prism')
import           Control.Lens.Review                            (Review, unto)
import           Control.Lens.Traversal                         (Traversal')
import           Data.Bifunctor                                 (Bifunctor (second))
import           Data.List.NonEmpty                             (NonEmpty)
import           Document.Data.Span                             (Span)
import           Language.Zeta.Syntax.Classy.HasTypeCache       (HasTypeCache (..))
import           Language.Zeta.Syntax.Data.Annotation.TypeAnn   (TypeAnn)
import           Language.Zeta.Syntax.Data.Name.TypeName        (TypeName)
import           Language.Zeta.Syntax.Data.TVar                 (TVar)
import           Language.Zeta.Syntax.Data.Type                 (Type (..),
                                                                 fromTLoc,
                                                                 getTypeAnn,
                                                                 getTypePure,
                                                                 setTypeAnn,
                                                                 toTLoc,
                                                                 transferTypeAnn)
import           Language.Zeta.Syntax.Data.TypeCache            (TypeCache)
import           Language.Zeta.Syntax.Data.TypeContext          (TypeContext)
import           Language.Zeta.Syntax.Optics.Annotation.TypeAnn (typeAnnCache,
                                                                 typeAnnLoc)

-- | Lens into the annotations of some type.
tyannotation :: Lens' Type TypeAnn
tyannotation = lens getTypeAnn setTypeAnn

-- | Lens into a pure form of some type, without any top-level annotations.
typure :: Lens' Type Type
typure = lens getTypePure transferTypeAnn

-- | Lens into the source location of a type.
tyloc :: Lens' Type (Maybe Span)
tyloc = tyannotation . typeAnnLoc

-- | Traversal into the source location of a type.
tylocOf :: Traversal' Type Span
tylocOf = tyloc . _Just

-- | Lens into the term cache annotation of a type.
tycache :: Lens' Type (Maybe TypeCache)
tycache = tyannotation . typeAnnCache

-- | Traversal into the term cache annotation of a type.
tycacheOf :: Traversal' Type TypeCache
tycacheOf = tycache . _Just

-- | Prism for 'TVar' types.
_TVar :: Prism' Type TVar
_TVar = prism' TVar $ \case
  TVar v -> Just v
  _      -> Nothing

-- | Prism for 'TApp'.
_TApp :: Prism' Type (Type, [Type])
_TApp = prism' (uncurry TApp) $ \case
  TApp c xs -> Just (c, xs)
  _         -> Nothing

-- | Prism for 'TCon'.
_TCon :: Prism' Type TypeName
_TCon = prism' TCon $ \case
  TCon n -> Just n
  _      -> Nothing

-- | Prism for 'TInt'.
_TInt :: Prism' Type Int
_TInt = prism' TInt $ \case
  TInt i -> Just i
  _      -> Nothing

-- | Prism for 'TUInt'.
_TUInt :: Prism' Type Int
_TUInt = prism' TUInt $ \case
  TUInt i -> Just i
  _       -> Nothing

-- | Prism for 'TFp'.
_TFp :: Prism' Type Int
_TFp = prism' TFp $ \case
  TFp i -> Just i
  _     -> Nothing

-- | Prism for 'TTuple'.
_TTuple :: Prism' Type (Type, NonEmpty Type)
_TTuple = prism' (uncurry TTuple) $ \case
  TTuple x1 xs -> Just (x1, xs)
  _            -> Nothing

-- | Prism for 'TArray'.
_TArray :: Prism' Type (Int, Type)
_TArray = prism' (uncurry TArray) $ \case
  TArray i ty -> Just (i, ty)
  _           -> Nothing

-- | Prism for 'TPtr'.
_TPtr :: Prism' Type Type
_TPtr = prism' TPtr $ \case
  TPtr t -> Just t
  _      -> Nothing

-- | Prism for 'TFunc'.
_TFunc :: Prism' Type ([Type], Type)
_TFunc = prism' (uncurry TFunc) $ \case
  TFunc xs y -> Just (xs, y)
  _          -> Nothing

-- | Prism for 'TCtx'.
_TCtx :: Prism' Type (TypeContext, Type)
_TCtx = prism' (uncurry TCtx) $ \case
  TCtx ctx t -> Just (ctx, t)
  _          -> Nothing

-- | Review for annotating types with cache information.
-- Will generate cache annotations for types.
_TCache :: Review Type Type
_TCache = unto buildCacheAnn
  where
    buildCacheAnn t = TCache t (getTypeCache t)
    {-# INLINE buildCacheAnn #-}

-- | Prism for 'TLoc'.
_TLoc :: Prism' Type (Type, Maybe Span)
_TLoc = prism' (uncurry toTLoc) (fmap (second Just) . fromTLoc)

-- | Prism for 'TParens'.
_TParens :: Prism' Type Type
_TParens = prism' TParens $ \case
  TParens t -> Just t
  _         -> Nothing

-- | Review for annotating types.
-- Will generate cache annotations for types.
-- Expects a type and possibly a source location.
_TAnn :: Review Type (Type, Maybe Span)
_TAnn = _TCache . _TLoc
