{-# LANGUAGE LambdaCase #-}
module Language.Zeta.Syntax.Optics.Pat where

import           Control.Lens.Iso                               (Iso', iso)
import           Control.Lens.Lens                              (Lens', lens)
import           Control.Lens.Prism                             (Prism', _Just,
                                                                 prism')
import           Control.Lens.Review                            (Review, unto)
import           Control.Lens.Traversal                         (Traversal')
import           Data.Bifunctor                                 (Bifunctor (second))
import           Data.List.NonEmpty                             (NonEmpty)
import           Document.Data.Span                             (Span)
import           Language.Zeta.Syntax.Classy.HasExpCache        (HasExpCache (..))
import           Language.Zeta.Syntax.Classy.HasTypeCache       (HasTypeCache (..))
import           Language.Zeta.Syntax.Data.Annotation.ExpAnn    (ExpAnn)
import           Language.Zeta.Syntax.Data.Exp                  (Exp)
import           Language.Zeta.Syntax.Data.ExpCache             (ExpCache)
import           Language.Zeta.Syntax.Data.Name.ConstructorName (ConstructorName)
import           Language.Zeta.Syntax.Data.Name.PatternName     (PatternName (..))
import           Language.Zeta.Syntax.Data.Object               (Object)
import           Language.Zeta.Syntax.Data.Pat                  (Pat (..),
                                                                 fromPLoc,
                                                                 getPatAnn,
                                                                 getPatPure,
                                                                 setPatAnn,
                                                                 toPLoc,
                                                                 transferPatAnn)
import           Language.Zeta.Syntax.Data.Type                 (Type)
import           Language.Zeta.Syntax.Data.TypeCache            (TypeCache)
import           Language.Zeta.Syntax.Data.Var                  (Var (..))
import           Language.Zeta.Syntax.Optics.Annotation.ExpAnn  (expAnnExpCache,
                                                                 expAnnLoc,
                                                                 expAnnType,
                                                                 expAnnTypeCache)

-- | Isomorphism between 'PatternName' and 'Var'.
isoPatternName2Var :: Iso' PatternName Var
isoPatternName2Var = iso fromPatternName toPatternName
  where
    fromPatternName (PatternName txt l ty) = V txt Nothing ty l
    toPatternName (V txt _ ty l) = PatternName txt l ty

-- | Lens into the expression annotation of a pattern.
pannotation :: Lens' Pat ExpAnn
pannotation = lens getPatAnn setPatAnn

-- | Lens into the pure form of a pattern, without any 'ExpAnn' annotations.
ppure :: Lens' Pat Pat
ppure = lens getPatPure transferPatAnn

-- | Lens into the source location annotation of a pattern.
ploc :: Lens' Pat (Maybe Span)
ploc = pannotation . expAnnLoc

-- | Traversal into the source location annotation of pattern.
plocOf :: Traversal' Pat Span
plocOf = ploc . _Just

-- | Lens into the type annotation of pattern.
ptype :: Lens' Pat (Maybe Type)
ptype = pannotation . expAnnType

-- | Traversal into the type annotation of pattern.
ptypeOf :: Traversal' Pat Type
ptypeOf = ptype . _Just

-- | Lens into the term cache annotation of pattern.
pexpCache :: Lens' Pat (Maybe ExpCache)
pexpCache = pannotation . expAnnExpCache

-- | Traversal into the term cache annotation of pattern.
pexpCacheOf :: Traversal' Pat ExpCache
pexpCacheOf = pexpCache . _Just

-- | Lens into the term cache annotation of pattern.
ptypeCache :: Lens' Pat (Maybe TypeCache)
ptypeCache = pannotation . expAnnTypeCache

-- | Traversal into the term cache annotation of pattern.
ptypeCacheOf :: Traversal' Pat TypeCache
ptypeCacheOf = ptypeCache . _Just

-- | Prism for 'PVar'.
_PVar :: Prism' Pat PatternName
_PVar = prism' PVar $ \case
  PVar n -> Just n
  _      -> Nothing

-- | Prism for 'PCon'.
_PCon :: Prism' Pat (ConstructorName, [Pat])
_PCon = prism' (uncurry PCon) $ \case
  PCon cn xs -> Just (cn, xs)
  _          -> Nothing

-- | Prism for 'PTuple'.
_PTuple :: Prism' Pat (Pat, NonEmpty Pat)
_PTuple = prism' (uncurry PTuple) $ \case
  PTuple x1 xs -> Just (x1, xs)
  _            -> Nothing

-- | Prism for 'PObj'.
_PObj :: Prism' Pat (Object Pat)
_PObj = prism' PObj $ \case
  PObj o -> Just o
  _      -> Nothing

-- | Prism for 'PLabel'.
_PLabel :: Prism' Pat (PatternName, Pat)
_PLabel = prism' (uncurry PLabel) $ \case
  PLabel n p -> Just (n, p)
  _          -> Nothing

-- | Prism for 'PView'.
_PView :: Prism' Pat (Exp, Pat)
_PView = prism' (uncurry PView) $ \case
  PView e p -> Just (e, p)
  _         -> Nothing

-- | Prism for 'PWild'.
_PWild :: Prism' Pat ()
_PWild = prism' (const PWild) $ \case
  PWild -> Just ()
  _     -> Nothing

-- | Prism for 'PType'.
_PType :: Prism' Pat (Pat, Type)
_PType = prism' (uncurry PType) $ \case
  PType p t -> Just (p, t)
  _         -> Nothing

-- | Review for pattern cache annotation types.
-- Will generate cache annotations for both expressions and types.
_PCache :: Review Pat Pat
_PCache = _PExpCache . _PTypeCache

-- | Review for pattern cache annotation types.
-- Will generate cache annotations for an expression cache.
_PExpCache :: Review Pat Pat
_PExpCache = unto buildExpCacheAnn
  where
    buildExpCacheAnn p = PExpCache p (getExpCache p)
    {-# INLINE buildExpCacheAnn #-}

-- | Review for pattern cache annotation types.
-- Will generate cache annotations for a type cache.
_PTypeCache :: Review Pat Pat
_PTypeCache = unto buildTypeCacheAnn
  where
    buildTypeCacheAnn p = PTypeCache p (getTypeCache p)
    {-# INLINE buildTypeCacheAnn #-}

-- | Prism for 'PLoc'.
_PLoc :: Prism' Pat (Pat, Maybe Span)
_PLoc = prism' (uncurry toPLoc) (fmap (second Just) . fromPLoc)

-- | Prism for 'PParens'.
_PParens :: Prism' Pat Pat
_PParens = prism' PParens $ \case
  PParens p -> Just p
  _         -> Nothing

-- | Review for annotating patterns.
-- Will generate cache annotations for both expressions and types.
-- Expects a pattern and possibly a source location.
_PAnn :: Review Pat (Pat, Maybe Span)
_PAnn = _PCache . _PLoc

-- | Traversal over the names in a pattern.
patternNames :: Traversal' Pat PatternName
patternNames f = \case
    PVar pn -> PVar <$> f pn
    PCon cn pats -> PCon cn <$> traverse (patternNames f) pats
    PTuple pat ne -> PTuple <$> patternNames f pat <*> traverse (patternNames f) ne
    PObj ob -> PObj <$> traverse (patternNames f) ob
    PLabel pn pat -> PLabel <$> f pn <*> patternNames f pat
    PView e pat -> PView e <$> patternNames f pat
    pat@PWild -> pure pat
    PType pat ty -> PType <$> patternNames f pat <*> pure ty
    PExpCache pat ec -> PExpCache <$> patternNames f pat <*> pure ec
    PTypeCache pat tc -> PTypeCache <$> patternNames f pat <*> pure tc
    PLoc pat sp -> PLoc <$> patternNames f pat <*> pure sp
    PParens pat -> PParens <$> patternNames f pat

-- | Traversal over the names in a pattern, but viewed as variables.
patternVars :: Traversal' Pat Var
patternVars = patternNames . isoPatternName2Var

patternExpOf :: Traversal' Pat Exp
patternExpOf f pat = case pat of
  PVar _ -> pure pat
  PCon cn ps -> PCon cn <$> traverse (patternExpOf f) ps
  PTuple p ne -> PTuple <$> patternExpOf f p <*> traverse (patternExpOf f) ne
  PObj ob -> PObj <$> traverse (patternExpOf f) ob
  PLabel pn p -> PLabel pn <$> patternExpOf f p
  PView e p -> PView <$> f e <*> patternExpOf f p
  PWild -> pure pat
  PType p ty -> PType <$> patternExpOf f p <*> pure ty
  PExpCache p ec -> PExpCache <$> patternExpOf f p <*> pure ec
  PTypeCache p tc -> PTypeCache <$> patternExpOf f p <*> pure tc
  PLoc p l -> PLoc <$> patternExpOf f p <*> pure l
  PParens p -> PParens <$> patternExpOf f p
