module Language.Zeta.Syntax.Optics.ExpCache where

import           Control.Lens.Lens                  (Lens', lens)
import           Language.Zeta.Syntax.Data.ExpCache (ExpCache (..),
                                                     setExpCacheHasFree,
                                                     setExpCacheMaxBound)

-- | Lens into the has free variables flag of a expression cache.
expCacheHasFree :: Lens' ExpCache Bool
expCacheHasFree = lens getExpCacheHasFree setExpCacheHasFree

-- | Lens into the max bound variable value of a expression cache.
expCacheMaxBound :: Lens' ExpCache Int
expCacheMaxBound = lens getExpCacheMaxBound setExpCacheMaxBound
