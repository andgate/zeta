{-# LANGUAGE LambdaCase    #-}
{-# LANGUAGE RankNTypes    #-}
{-# LANGUAGE TupleSections #-}
module Language.Zeta.Syntax.Optics.Exp where

import                          Control.Lens.Lens                              (Lens',
                                                                                lens)
import                          Control.Lens.Prism                             (Prism',
                                                                                _Just,
                                                                                prism')
import                          Control.Lens.Review                            (Review,
                                                                                unto)
import                          Control.Lens.Traversal                         (Traversal')
import                          Data.Bifunctor                                 (Bifunctor (second))
import                          Data.List.NonEmpty                             (NonEmpty)
import                          Document.Data.Span                             (Span)
import                          Language.Zeta.Syntax.Classy.Abstracts.Class    (Abstracts (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.Abstracts.Exp      ()
import                          Language.Zeta.Syntax.Classy.HasExpCache        (HasExpCache (..))
import                          Language.Zeta.Syntax.Classy.HasTypeCache       (HasTypeCache (..))
import                          Language.Zeta.Syntax.Classy.Instantiable.Class (Instantiable (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Classy.Instantiable.Exp   ()
import                          Language.Zeta.Syntax.Data.Annotation.ExpAnn    (ExpAnn)
import                          Language.Zeta.Syntax.Data.Exp                  (Exp (..),
                                                                                fromELam,
                                                                                fromELoc,
                                                                                getExpAnn,
                                                                                getExpPure,
                                                                                setExpAnn,
                                                                                toELoc,
                                                                                transferExpAnn)
import                          Language.Zeta.Syntax.Data.ExpCache             (ExpCache (..))
import                          Language.Zeta.Syntax.Data.Lit                  (Lit)
import                          Language.Zeta.Syntax.Data.Name.ConstructorName (ConstructorName)
import                          Language.Zeta.Syntax.Data.Name.FieldName       (FieldName)
import                          Language.Zeta.Syntax.Data.Object               (Object)
import                          Language.Zeta.Syntax.Data.Operator             (BinaryOperator,
                                                                                UnaryOperator)
import                          Language.Zeta.Syntax.Data.Pat                  (Pat)
import                          Language.Zeta.Syntax.Data.Type                 (Type)
import                          Language.Zeta.Syntax.Data.TypeCache            (TypeCache)
import                          Language.Zeta.Syntax.Data.Var                  (Var)
import                          Language.Zeta.Syntax.Optics.Annotation.ExpAnn  (expAnnExpCache,
                                                                                expAnnLoc,
                                                                                expAnnType,
                                                                                expAnnTypeCache)

-- | Lens into the 'ExpAnn' of an expression,
-- which can be extracted and placed onto an expression.
eannotation :: Lens' Exp ExpAnn
eannotation = lens getExpAnn setExpAnn

-- | Lens into the pure form of an expression,
-- that is, without any annotations.
epure :: Lens' Exp Exp
epure = lens getExpPure transferExpAnn

-- | Lens into the source location annotation of an expression.
eloc :: Lens' Exp (Maybe Span)
eloc = eannotation . expAnnLoc

-- | Traversal into the source location annotation of an expression.
elocOf :: Traversal' Exp Span
elocOf = eloc . _Just

-- | Lens into the type annotation of an expression.
etype :: Lens' Exp (Maybe Type)
etype = eannotation . expAnnType

-- | Traversal into the type annotation of an expression.
etypeOf :: Traversal' Exp Type
etypeOf = etype . _Just

-- | Lens into the expression cache annotation of an expression.
ecache :: Lens' Exp (Maybe ExpCache)
ecache = eannotation . expAnnExpCache

-- | Traversal into the expression cache annotation of an expression.
ecacheOf :: Traversal' Exp ExpCache
ecacheOf = ecache . _Just

-- | Lens into the type cache annotation of an expression.
etypeCache :: Lens' Exp (Maybe TypeCache)
etypeCache = eannotation . expAnnTypeCache

-- | Traversal into the type cache annotation of an expression.
etypeCacheOf :: Traversal' Exp TypeCache
etypeCacheOf = etypeCache . _Just

-- | Prism for 'EVar' expressions.
_EVar :: Prism' Exp Var
_EVar = prism' EVar $ \case
  EVar v -> Just v
  _      -> Nothing

-- | Prism for 'ELit' expressions.
_ELit :: Prism' Exp Lit
_ELit = prism' ELit $ \case
  ELit l -> Just l
  _      -> Nothing

-- | Prism for 'ECall' expressions.
_ECall :: Prism' Exp (Exp, [Exp])
_ECall = prism' (uncurry ECall) $ \case
  ECall f xs -> Just (f, xs)
  _          -> Nothing

-- | Prism for 'ELam' expressions.
_ELam :: Prism' Exp ([Pat], Maybe Type, Exp)
_ELam = prism' toELam' fromELam'
  where
    toELam' (ps, m_ty, e) =
      ELam ps m_ty (abstract ps e)
    fromELam' e = case fromELam e of
      Nothing             ->
        Nothing
      Just (ps, m_ty, sc) ->
        Just (ps, m_ty, instantiate ps sc)

-- | Prism for 'EType' expressions.
_EType :: Prism' Exp (Exp, Type)
_EType = prism' (uncurry EType) $ \case
  EType e ty -> Just (e, ty)
  _          -> Nothing

-- | Prism for 'ECast' expressions.
_ECast :: Prism' Exp (Exp, Type)
_ECast = prism' (uncurry ECast) $ \case
  ECast e ty -> Just (e, ty)
  _          -> Nothing

-- | Review for annotating an expression with both an expression cache and a type cache.
_ECache :: Review Exp Exp
_ECache = _EExpCache . _ETypeCache

-- | Review for annotating an expression with an expression cache.
_EExpCache :: Review Exp Exp
_EExpCache = unto buildExpCacheAnn
  where
    buildExpCacheAnn e = ECache e (getExpCache e)
    {-# INLINE buildExpCacheAnn #-}

-- | Review for annotating an expression with a type cache.
_ETypeCache :: Review Exp Exp
_ETypeCache = unto buildTypeCacheAnn
  where
    buildTypeCacheAnn e = ETypeCache e (getTypeCache e)
    {-# INLINE buildTypeCacheAnn #-}

-- | Prism for 'EParens' expressions.
_EParens :: Prism' Exp Exp
_EParens = prism' EParens $ \case
  EParens e -> Just e
  _         -> Nothing

-- | Prism for 'ELoc' expressions.
_ELoc :: Prism' Exp (Exp, Maybe Span)
_ELoc = prism' (uncurry toELoc) (fmap (second Just) . fromELoc)

-- | Prism for 'ERef' expressions.
_ERef :: Prism' Exp Exp
_ERef = prism' ERef $ \case
  ERef e -> Just e
  _      -> Nothing

-- | Prism for 'EDeref' expressions.
_EDeref :: Prism' Exp Exp
_EDeref = prism' EDeref $ \case
  EDeref e -> Just e
  _        -> Nothing

-- | Prism for 'ETuple' expressions.
_ETuple :: Prism' Exp (Exp, NonEmpty Exp)
_ETuple = prism' (uncurry ETuple) $ \case
  ETuple x1 xs -> Just (x1, xs)
  _            -> Nothing

-- | Prism for 'ECon' expressions.
_ECon :: Prism' Exp ConstructorName
_ECon = prism' ECon $ \case
  ECon cn -> Just cn
  _       -> Nothing

-- | Prism for 'EArray' expressions.
_EArray :: Prism' Exp [Exp]
_EArray = prism' EArray $ \case
  EArray xs -> Just xs
  _         -> Nothing

-- | Prism for 'EObj' expressions.
_EObj :: Prism' Exp (Object Exp)
_EObj = prism' EObj $ \case
  EObj o -> Just o
  _      -> Nothing

-- | Prism for 'EFree' expressions.
_EFree :: Prism' Exp Exp
_EFree = prism' EFree $ \case
  EFree x -> Just x
  _       -> Nothing

-- | Prism for 'EGet' expressions.
_EGet :: Prism' Exp (Exp, FieldName)
_EGet = prism' (uncurry EGet) $ \case
  EGet a x -> Just (a, x)
  _        -> Nothing

-- | Prism for 'EGetI' expressions.
_EGetI :: Prism' Exp (Exp, Exp)
_EGetI = prism' (uncurry EGetI) $ \case
  EGetI a x -> Just (a, x)
  _         -> Nothing

-- | Prism for 'ESet' expressions.
_ESet :: Prism' Exp (Exp, Exp)
_ESet = prism' (uncurry ESet) $ \case
  ESet a x -> Just (a, x)
  _        -> Nothing

-- | Prism for 'EUnOp' expressions.
_EUnOp :: Prism' Exp (UnaryOperator, Exp)
_EUnOp = prism' (uncurry EUnOp) $ \case
  EUnOp op x -> Just (op, x)
  _          -> Nothing

-- | Prism for 'EBinOp' expressions.
_EBinOp :: Prism' Exp (BinaryOperator, Exp, Exp)
_EBinOp = prism' (\(op, x, y) -> EBinOp op x y) $ \case
  EBinOp op x y -> Just (op, x, y)
  _             -> Nothing


-- | Review for annotating expressions.
-- Will generate cache annotations for both expressions and types.
-- Expects an expression and possibly a source location.
_EAnn :: Review Exp (Exp, Maybe Span)
_EAnn = _ECache . _ELoc
