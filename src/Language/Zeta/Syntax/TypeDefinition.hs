{-|
Module      : Language.Zeta.Syntax.TypeDefinition
Description : Zeta type definition syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.TypeDefinition (module X) where

import           Language.Zeta.Syntax.Data.TypeDefn as X
