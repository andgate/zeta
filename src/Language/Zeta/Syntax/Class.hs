{-|
Module      : Language.Zeta.Syntax.Class
Description : Zeta type class definition syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Class (module X) where

import           Language.Zeta.Syntax.Data.ClassDefn       as X
import           Language.Zeta.Syntax.Data.Name.ClassName  as X
import           Language.Zeta.Syntax.Data.Name.MethodName as X
