{-|
Module      : Language.Zeta.Syntax.Scope
Description : Zeta scope syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Scope (module X) where

import           Language.Zeta.Syntax.Data.Scope as X
