{-|
Module      : Language.Zeta.Syntax.Function
Description : Zeta import syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Function (module X) where

import           Language.Zeta.Syntax.Data.FuncDefn          as X
import           Language.Zeta.Syntax.Data.Name.FunctionName as X
