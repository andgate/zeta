{-|
Module      : Language.Zeta.Syntax.Operator
Description : Zeta operator syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Operator (module X) where

import           Language.Zeta.Syntax.Data.Operator                  as X
