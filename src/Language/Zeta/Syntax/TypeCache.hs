{-|
Module      : Language.Zeta.Syntax.TypeCache
Description : Zeta type cache for optimizing traversals.
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.TypeCache (module X) where

import           Language.Zeta.Syntax.Data.TypeCache as X
