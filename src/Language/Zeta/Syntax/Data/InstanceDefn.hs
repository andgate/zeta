module Language.Zeta.Syntax.Data.InstanceDefn where

import           Document.Data.Span                       (Span)
import           Language.Zeta.Syntax.Data.FuncDefn       (FuncDefn)
import           Language.Zeta.Syntax.Data.Name.ClassName (ClassName)
import           Language.Zeta.Syntax.Data.Type           (Type)
import           Language.Zeta.Syntax.Data.TypeContext    (TypeContext)

-- | Type class instance definition syntax.
data InstanceDefn =
  InstanceDefn          -- ^ Constructor for type class instance definitions.
    Span                -- ^ Source location of an instance definition.
    (Maybe TypeContext) -- ^ Type context of an instance definition.
    ClassName           -- ^ Class name that the instance implements.
    [Type]              -- ^ Instance signature.
    InstanceDefnBlock   -- ^ Instance definition block, the body of the instance.
  deriving(Show)

-- | A type class instance definition block, the body of an instance definition.
-- Consists of a sequence of function definitions which satisfy the target type class.
newtype InstanceDefnBlock =
  InstanceDefnBlock -- ^ Constructor for type class instance blocks.
    [FuncDefn]      -- ^ A list of function definitions.
  deriving(Show)
