{-# LANGUAGE RankNTypes #-}
module Language.Zeta.Syntax.Data.Scope where

-- | Term Scopes
-- Modified De Brujin Indices
--   The paper on Lean's elaborator suggests using a modified approach to Connor McBride's locally nameless approach.
--     1) Generalize abstract/instantiate to process sequences of free and bound variables
--     2) Annotate terms with the maxium level of binding B.
--        This is used to optimize the instantiation operation.
--     3) Annotate terms with a boolean flag where true indicates the presence of free variables.
--        This is used to optimize the abstract operation.
--
-- Sources:
--   - "I am not a Number -- I am a Free Variable"
--     http://www.cs.ru.nl/~james/RESEARCH/haskell2004.pdf
--   - "Elaboration in Dependent Type Theory"
--     https://arxiv.org/pdf/1505.04324.pdf
data Scope t =
  Scope -- ^ Term scope constructor, prefer 'newScope'.
  { -- | Get the scope level (ie. the variable it binds) of a scope.
    getScopeLevel :: Int,
    -- | Get the term body of a scope.
    getScopeBody  :: t
  } deriving (Show)

-- | Construct a new scope over some term.
newScope
  :: Int      -- ^ Scope level of new scope.
  -> t        -- ^ Some term to scope over.
  -> Scope t  -- ^ A new, empty scope over the term, with a level of 0.
newScope lvl t =
  Scope
    { getScopeLevel = lvl
    , getScopeBody = t
    }
{-# INLINE newScope #-}

-- | Construct an empty scope over some term.
-- An empty scope has a level of 0.
emptyScope
  :: t        -- ^ Some term to scope over.
  -> Scope t  -- ^ A new, empty scope over the term, with a level of 0.
emptyScope = newScope 0
{-# INLINE emptyScope #-}

-- | Set the level of some 'Scope'.
setScopeLevel
  :: forall t.
     Scope t    -- ^ Some scope to update.
  -> Int        -- ^ A new scope level to set.
  -> Scope t    -- ^ A new scope, updated with the given scope level.
setScopeLevel s i = s { getScopeLevel = i }
{-# INLINE setScopeLevel #-}

-- | Set the term body of some 'Scope'.
setScopeBody
  :: forall a b.
     Scope a    -- ^ Some scope to update.
  -> b          -- ^ Some new term.
  -> Scope b    -- ^ A new scope, updated with the new term body.
setScopeBody s tm = s { getScopeBody = tm }
{-# INLINE setScopeBody #-}

instance Functor Scope where
  fmap f s =
    setScopeBody s (f $ getScopeBody s)
  {-# INLINE fmap #-}

instance Foldable Scope where
  foldr f b s =
    f (getScopeBody s) b
  {-# INLINE foldr #-}

instance Traversable Scope where
  traverse f (Scope bnd body) =
    Scope bnd <$> f body
  {-# INLINE traverse #-}
