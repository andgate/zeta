module Language.Zeta.Syntax.Data.Annotation.TypeAnn where

import           Document.Data.Span                  (Span)
import           Language.Zeta.Syntax.Data.TypeCache (TypeCache)

-- | Type annotations.
-- These can be extracted from a type by unwrapping the annotations
-- from a 'Type', lifting them into an 'TypeAnn'.
-- Furthermore, with 'TypeAnn', we can write cleaner implementations of
-- lenses and traversals over type annotations.
data TypeAnn = TypeAnn {
  -- | Get a 'Span' that may annotate some type.
  getTypeAnnLoc   :: Maybe Span,
  -- | Get a 'TermCache' that may annotate some type.
  getTypeAnnCache :: Maybe TypeCache
} deriving (Show)

-- | Construct a new 'TypeAnn'.
newTypeAnn
  :: Maybe Span
  -> Maybe TypeCache
  -> TypeAnn
newTypeAnn ml mtc =
  TypeAnn {
    getTypeAnnLoc   = ml,
    getTypeAnnCache = mtc
  }
{-# INLINE newTypeAnn #-}

-- | Construct an empty type annotation.
-- This simply sets all the fields of 'TypeAnn' to 'Nothing'.
emptyTypeAnn :: TypeAnn
emptyTypeAnn =
  newTypeAnn Nothing Nothing
{-# INLINE emptyTypeAnn #-}

-- | Set the location annotation of an 'TypeAnn'.
setTypeAnnLoc
  :: TypeAnn    -- ^ Current type annotation.
  -> Maybe Span -- ^ New location annotation.
  -> TypeAnn    -- ^ New type annotation, updated with the new location annotation.
setTypeAnnLoc ann ml = ann { getTypeAnnLoc = ml }
{-# INLINE setTypeAnnLoc #-}

-- | Set the term cache annotation of an 'TypeAnn'.
setTypeAnnCache
  :: TypeAnn          -- ^ Current type annotation.
  -> Maybe TypeCache  -- ^ New term cache annotation.
  -> TypeAnn          -- ^ New type annotation, updated with the new term cache annotation.
setTypeAnnCache ann ml = ann { getTypeAnnCache = ml }
{-# INLINE setTypeAnnCache #-}
