module Language.Zeta.Syntax.Data.Annotation.ExpAnn where

import           Document.Data.Span                  (Span)
import           Language.Zeta.Syntax.Data.ExpCache  (ExpCache)
import           Language.Zeta.Syntax.Data.Type      (Type)
import           Language.Zeta.Syntax.Data.TypeCache (TypeCache)

-- | Expression annotations.
-- These can be extracted from an expression by unwrapping the annotations
-- from a 'Exp', lifting them into an 'ExpAnn'.
-- Furthermore, with 'ExpAnn', we can write cleaner implementations of
-- lenses and traversals over expression annotations.
data ExpAnn =
  ExpAnn
  { -- | Get a 'Span' that may annotate some expression.
    getExpAnnLoc       :: Maybe Span
    -- | Get a 'Type' that may annotate some expression.
  , getExpAnnType      :: Maybe Type
    -- | Get an 'ExpCache' that may annotate some expression.
  , getExpAnnExpCache  :: Maybe ExpCache
    -- | Get a 'TypeCache' that may annotate some expression.
  , getExpAnnTypeCache :: Maybe TypeCache
  } deriving (Show)

-- | Construct a new 'ExpAnn'.
newExpAnn
  :: Maybe Span      -- ^ 'Span' the resulting annotation may contain.
  -> Maybe Type      -- ^ 'Type' the resulting annotation may contain.
  -> Maybe ExpCache  -- ^ 'ExpCache' the resulting annotation may contain.
  -> Maybe TypeCache -- ^ 'TypeCache' the resulting annotation may contain.
  -> ExpAnn          -- ^ Resulting expression annotation, which contains all the give inputs.
newExpAnn ml mty mec mtc =
  ExpAnn {
    getExpAnnLoc   = ml,
    getExpAnnType  = mty,
    getExpAnnExpCache = mec,
    getExpAnnTypeCache = mtc
  }
{-# INLINE newExpAnn #-}

-- | Construct an empty expression annotation.
-- This simply sets all the fields of 'ExpAnn' to 'Nothing'.
emptyExpAnn :: ExpAnn
emptyExpAnn =
  newExpAnn Nothing Nothing Nothing Nothing
{-# INLINE emptyExpAnn #-}

-- | Set the location annotation of an 'ExpAnn'.
setExpAnnLoc
  :: ExpAnn     -- ^ Current expression annotation.
  -> Maybe Span -- ^ New location annotation.
  -> ExpAnn     -- ^ New expression annotation, updated with the new location annotation.
setExpAnnLoc ann ml = ann { getExpAnnLoc = ml }
{-# INLINE setExpAnnLoc #-}

-- | Set the type annotation of an 'ExpAnn'.
setExpAnnType
  :: ExpAnn     -- ^ Current expression annotation.
  -> Maybe Type -- ^ New type annotation.
  -> ExpAnn     -- ^ New expression annotation, updated with the new type annotation.
setExpAnnType ann mty = ann { getExpAnnType = mty }
{-# INLINE setExpAnnType #-}

-- | Set the expression cache annotation of an 'ExpAnn'.
setExpAnnExpCache
  :: ExpAnn         -- ^ Current expression annotation.
  -> Maybe ExpCache -- ^ New expression cache annotation.
  -> ExpAnn         -- ^ New expression annotation, updated with the new expression cache annotation.
setExpAnnExpCache ann mec = ann { getExpAnnExpCache = mec }
{-# INLINE setExpAnnExpCache #-}

-- | Set the expression cache annotation of an 'ExpAnn'.
setExpAnnTypeCache
  :: ExpAnn          -- ^ Current expression annotation.
  -> Maybe TypeCache -- ^ New type cache annotation.
  -> ExpAnn          -- ^ New expression annotation, updated with the new type cache annotation.
setExpAnnTypeCache ann mtc = ann { getExpAnnTypeCache = mtc }
{-# INLINE setExpAnnTypeCache #-}
