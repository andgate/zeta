module Language.Zeta.Syntax.Data.ModuleStmt where

import           Language.Zeta.Syntax.Data.ClassDefn    (ClassDefn)
import           Language.Zeta.Syntax.Data.ExternDefn   (ExternDefn)
import           Language.Zeta.Syntax.Data.FuncDefn     (FuncDefn)
import           Language.Zeta.Syntax.Data.ImportStmt   (ImportStmt)
import           Language.Zeta.Syntax.Data.InstanceDefn (InstanceDefn)
import           Language.Zeta.Syntax.Data.MorphismDefn (MorphismDefn)
import           Language.Zeta.Syntax.Data.TypeDefn     (TypeDefn)

-- | Module statement syntax,
-- which are the top-level statements in a source file.
data ModuleStmt
  = ModuleImportStmt    -- ^ Import module statement.
      ImportStmt        -- ^ Some import statement.
  | ModuleFuncDefn      -- ^ Function definition module statement.
      FuncDefn          -- ^ Some function definition.
  | ModuleExternDefn    -- ^ External definition module statement.
      ExternDefn        -- ^ Some external definition.
  | ModuleTypeDefn      -- ^ Type definition module statement.
      TypeDefn          -- ^ Some type definition.
  | ModuleClassDefn     -- ^ Type class definition module statement.
      ClassDefn         -- ^ Some type class definition.
  | ModuleInstanceDefn  -- ^ Type class instance definition module statement.
      InstanceDefn      -- ^ Some type class instance definition.
  | ModuleMorphismDefn  -- ^ Type class morphism definition module statement.
      MorphismDefn      -- ^ Some type class morphism definition.
  deriving(Show)
