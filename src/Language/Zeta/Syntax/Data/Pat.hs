{-# LANGUAGE LambdaCase #-}
module Language.Zeta.Syntax.Data.Pat where

import                          Data.List.NonEmpty                             (NonEmpty)
import                          Document.Data.Span                             (Span)
import                          Language.Zeta.Syntax.Data.Annotation.ExpAnn    (ExpAnn (..))
import {-# SOURCE #-}           Language.Zeta.Syntax.Data.Exp                  (Exp)
import                          Language.Zeta.Syntax.Data.ExpCache             (ExpCache)
import                          Language.Zeta.Syntax.Data.Name.ConstructorName (ConstructorName)
import                          Language.Zeta.Syntax.Data.Name.PatternName     (PatternName)
import                          Language.Zeta.Syntax.Data.Object               (Object)
import                          Language.Zeta.Syntax.Data.Type                 (Type)
import                          Language.Zeta.Syntax.Data.TypeCache            (TypeCache)

-- | Pattern syntax.
data Pat
  = PVar      -- ^ Variable pattern.
      PatternName                               -- ^ Some pattern name.
  | PCon      -- ^ Constructor pattern.
      ConstructorName                               -- ^ Some constructor name.
      [Pat]                                     -- ^ Pattern matches on the constructor arguments.
  | PTuple    -- ^ Tuple pattern.
      Pat                                       -- ^ First element pattern in the tuple pattern.
      (NonEmpty Pat)                            -- ^ The rest of the elements in the tuple pattern.
  | PObj      -- ^ Object pattern.
      (Object Pat)                              -- ^ Some object pattern.
  | PLabel    -- ^ Labelled pattern.
      PatternName                               -- ^ The label.
      Pat                                       -- ^ Some pattern to label.
  | PView     -- ^ View pattern.
      Exp                                       -- ^ Expression to run.
      Pat                                       -- ^ Pattern match on result of expression.
  | PWild     -- ^ Wildcard pattern.
  | PType     -- ^ Type-annotated pattern.
      Pat                                       -- ^ Pattern to annotate with a type.
      Type                                      -- ^ Type to annotate pattern with.
  | PExpCache    -- ^ Term-cache annotated pattern.
      Pat                                       -- ^ Pattern to annotated with a term cache.
      ExpCache                                  -- ^ Exp cache to annotate pattern with.
  | PTypeCache    -- ^ Term-cache annotated pattern.
      Pat                                       -- ^ Pattern to annotated with a term cache.
      TypeCache                                  -- ^ Type cache to annotate pattern with.
  | PLoc      -- ^ Source location annotated pattern.
      Pat                                       -- ^ Pattern to annotated with a source location.
      Span                                      -- ^ Source location to annotate pattern with.
  | PParens   -- ^ Parenthetical pattern.
      Pat                                       -- ^ Some pattern enclosed by parenthesis.
  deriving(Show)

-- | Smart constructor for 'PLoc'.
toPLoc
  :: Pat        -- ^ Some pattern to annotate.
  -> Maybe Span -- ^ Some source location annotation.
  -> Pat        -- ^ A new pattern annotated with the given source location.
toPLoc p = \case
  Just l  -> PLoc p l
  Nothing -> p

-- | Attempt to extract the ('Pat', 'Span') data from a 'PLoc'.
-- This only checks the root pattern, children are not searched.
fromPLoc
  :: Pat                -- ^ Some pattern to attempt to extract source location from.
  -> Maybe (Pat, Span)  -- ^ If the given 'Pat' is a 'PLoc', then its contents. Otherwise, 'Nothing'.
fromPLoc = \case
  PLoc p l -> Just (p, l)
  _        -> Nothing

-- | Get a pattern without any annotations (ie pure).
getPatPure
  :: Pat -- ^ Some impure pattern.
  -> Pat -- ^ Results in a purified pattern.
getPatPure = \case
  PLoc p _       -> getPatPure p
  PType p _      -> getPatPure p
  PExpCache p _  -> getPatPure p
  PTypeCache p _ -> getPatPure p
  PParens p      -> getPatPure p
  p              -> p

-- | Get the 'ExpAnn' annotations of an pattern.
getPatAnn
  :: Pat    -- ^ Attempts to extract expression annotations from a pattern.
  -> ExpAnn -- ^ Expression annotation extracted from the pattern.
getPatAnn = getPatAnn' (Nothing, Nothing, Nothing, Nothing)
  where
    getPatAnn' (ml, mty, mec, mtc) = \case
      PLoc p l        -> getPatAnn' (Just l, mty, mec, mtc) p
      PType p ty      -> getPatAnn' (ml, Just ty, mec, mtc) p
      PExpCache p ec  -> getPatAnn' (ml, mty, Just ec, mtc) p
      PTypeCache p tc -> getPatAnn' (ml, mty, mec, Just tc) p
      PParens p       -> getPatAnn' (ml, mty, mec, mtc) p
      _               -> ExpAnn ml mty mec mtc

-- | Set the 'ExpAnn' annotations of an pattern.
setPatAnn
  :: Pat    -- ^ Some pattern to be annotated.
  -> ExpAnn -- ^ An expression annotation to embed in the pattern.
  -> Pat    -- ^ New pattern, equipped with the given annotations.
setPatAnn p = \case
  ExpAnn (Just l) mty mec mtc ->
    setPatAnn (PLoc p l) (ExpAnn Nothing mty mec mtc)
  ExpAnn Nothing (Just ty) mec mtc ->
    setPatAnn (PType p ty) (ExpAnn Nothing Nothing mec mtc)
  ExpAnn Nothing Nothing (Just ec) mtc ->
    setPatAnn (PExpCache p ec) (ExpAnn Nothing Nothing Nothing mtc)
  ExpAnn Nothing Nothing Nothing (Just tc) ->
    setPatAnn (PTypeCache p tc) (ExpAnn Nothing Nothing Nothing Nothing)
  ExpAnn Nothing Nothing Nothing Nothing ->
    p
{-# INLINABLE setPatAnn #-}

-- | Transfer 'ExpAnn' from one pattern to another.
transferPatAnn
  :: Pat -- ^ Source pattern.
  -> Pat -- ^ Target pattern.
  -> Pat -- ^ Results in the target pattern wrapped in the 'ExpAnn' extract from the source pattern.
transferPatAnn e1 e2 = setPatAnn e2 (getPatAnn e1)
{-# INLINE transferPatAnn #-}

-- | Smartly attempts to extract the source location from a pattern.
-- This will traverse children a little bit, to recover any pattern annotations.
getPatLoc
  :: Pat          -- ^ Pattern to extract source location from.
  -> Maybe Span   -- ^ Results in either the source location of the pattern, or 'Nothing'.
getPatLoc = getExpAnnLoc . getPatAnn
{-# INLINE getPatLoc #-}

-- | Smartly attempts to extract the type annotation of a pattern.
-- This will traverse children a little bit, to recover any pattern annotations.
getPatType
  :: Pat          -- ^ Pattern to extract type annotation from.
  -> Maybe Type   -- ^ Results in either the type annotation of the pattern, or 'Nothing'.
getPatType = getExpAnnType . getPatAnn
{-# INLINE getPatType #-}

-- | Smartly attempts to extract the 'ExpCache' annotation of a pattern.
-- This will traverse children a little bit, to recover any pattern annotations.
getPatExpCache
  :: Pat             -- ^ Pattern to extract term cache annotation from.
  -> Maybe ExpCache -- ^ Results in either the term cache annotation of the pattern, or 'Nothing'.
getPatExpCache = getExpAnnExpCache . getPatAnn
{-# INLINE getPatExpCache #-}

-- | Smartly attempts to extract te 'TypeCache' annotation of a pattern.
-- This will traverse children a little bit, to recover any pattern annotations.
getPatTypeCache
  :: Pat             -- ^ Pattern to extract term cache annotation from.
  -> Maybe TypeCache -- ^ Results in either the term cache annotation of the pattern, or 'Nothing'.
getPatTypeCache = getExpAnnTypeCache . getPatAnn
{-# INLINE getPatTypeCache #-}
