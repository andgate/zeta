module Language.Zeta.Syntax.Data.Branch where

import                          Document.Data.Span            (Span)
import {-# SOURCE #-}           Language.Zeta.Syntax.Data.Exp (Exp)

-- | Conditional branch syntax.
-- This type covers else, else if, and elif branches.
data Branch
  = ElseBranch        -- ^ Constructor for else branches.
      (Maybe Span)      -- ^ Some source location.
      Exp               -- ^ Conditional block to execute.
  | ElifBranch        -- ^ Constructor for else if (or elif) branches.
      (Maybe Span)      -- ^ Some source location.
      Exp               -- ^ Branch condition.
      Exp               -- ^ Expression to execute when the condition is satisfied.
      (Maybe Branch)    -- ^ The next branch to try, when condition isn't satisfied.
  deriving(Show)
