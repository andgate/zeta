module Language.Zeta.Syntax.Data.ClassDefn where

import           Document.Data.Span                        (Span)
import           Language.Zeta.Syntax.Data.FuncDefn        (FuncDefn)
import           Language.Zeta.Syntax.Data.Name.ClassName  (ClassName)
import           Language.Zeta.Syntax.Data.Name.MethodName (MethodName)
import           Language.Zeta.Syntax.Data.Type            (Type)
import           Language.Zeta.Syntax.Data.TypeContext     (TypeContext)

-- | Datatype for type class definition syntax
data ClassDefn =
  ClassDefn -- ^ Type class definition constructor
  { -- | Get the source location of a type class definition.
    getClassDefnLoc    :: Span
    -- | Get the type context of a type class definition.
  , getClassDefnCtx    :: Maybe TypeContext
    -- | Get the name of a type class definition.
  , getClassDefnName   :: ClassName
    -- | Get the type parameters of a type class definition.
  , getClassDefnParams :: [Type]
    -- | Get the body of a type class definition.
  , getClassDefnBody   :: ClassDefnBlock
  } deriving(Show)

-- | Class definition block is a list of 'ClassDefnStmt',
-- which make up the body of a type class definition.
newtype ClassDefnBlock
  = ClassDefnBlock    -- ^ Constructor for type class definition blocks.
      [ClassDefnStmt] -- ^ List of type class definition statements.
  deriving(Show)

-- | A type class definition statement is either a method type signature
-- or a default method definition.
data ClassDefnStmt
  = ClassDefnSig  -- ^ Constructor for type class definition method type signature statements.
      Span        -- ^ Source location of type signature.
      MethodName  -- ^ Method name of type signature.
      [Type]      -- ^ Type parameters and return type of method signature.
  | ClassDefnMinimalMethod  -- ^ Constructor for type class default method implementation.
      FuncDefn              -- ^ A function definition.
  deriving (Show)
