{-# LANGUAGE LambdaCase #-}
module Language.Zeta.Syntax.Data.Operator where

-- | Unary operator syntax.
data UnaryOperator
  = OpNeg -- Negation operator.

-- | Binary operator syntax.
data BinaryOperator
  = OpAdd -- ^ Addition operator.
  | OpSub -- ^ Subtraction operator.
  | OpMul -- ^ Multiplication operator.
  | OpDiv -- ^ Division operator.
  | OpRem -- ^ Remainder (modulus) operator.

  | OpAnd -- ^ Logical and operator.
  | OpOr  -- ^ Logical or operator.
  | OpXor -- ^ Logical xor operator.
  | OpShR -- ^ Bitwise shift-right operator.
  | OpShL -- ^ Bitwise shift-left operator.

  | OpEq  -- ^ Equality operator.
  | OpNeq -- ^ Inequality operator.
  | OpLT  -- ^ Lesser-than operator.
  | OpLE  -- ^ Lesser than or equals to operator.
  | OpGT  -- ^ Greater than to operator.
  | OpGE  -- ^ Greater than or equals to operator.

instance Show UnaryOperator where
  show = \case
    OpNeg -> "neg"

instance Show BinaryOperator where
  show = \case
    OpAdd -> "add"
    OpSub -> "sub"
    OpMul -> "mul"
    OpDiv -> "div"
    OpRem -> "rem"
    OpAnd -> "and"
    OpOr  -> "or"
    OpXor -> "xor"
    OpShR -> "shr"
    OpShL -> "shl"
    OpEq  -> "eq"
    OpNeq -> "neq"
    OpLT  -> "lt"
    OpLE  -> "le"
    OpGT  -> "gt"
    OpGE  -> "ge"
