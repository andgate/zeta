module Language.Zeta.Syntax.Data.TypeCache where

-- | Type cache, used to optimize traversals for variable binding and such.
data TypeCache =
  TypeCache -- ^ The constructor for a type cache, prefer 'newTypeCache'.
  { -- | Get the has-free-varibles flag of a type cache.
    getTypeCacheHasFree  :: !Bool
    -- | Get the maxium bound of a type cache, B, such that
    -- B = maxBinder(t) + 1
  , getTypeCacheMaxBound :: !Int
  } deriving (Show)

-- | Construct a new type cache.
newTypeCache
  :: Bool      -- ^ Has free variables flag for type cache.
  -> Int       -- ^ Maximum bound depth.
  -> TypeCache -- ^ A new type cache.
newTypeCache = TypeCache
{-# INLINE newTypeCache #-}

-- | Construct an empty type cache.
-- To obey monoid laws, an empty type cache has free variables and a max bound of -1.
emptyTypeCache
  :: TypeCache -- ^ An empty type cache.
emptyTypeCache = newTypeCache True (-1)
{-# INLINE emptyTypeCache #-}

-- | Set the has free variables flag of a type cache.
setTypeCacheHasFree
  :: TypeCache -- ^ Type cache to update.
  -> Bool      -- ^ New has free variables flag.
  -> TypeCache -- ^ New type cache, updated with the new flag.
setTypeCacheHasFree tc p = tc { getTypeCacheHasFree = p }
{-# INLINE setTypeCacheHasFree #-}

-- | Set the maximum bound depth of a type cache.
setTypeCacheMaxBound
  :: TypeCache -- ^ Type cache to update.
  -> Int       -- ^ New maximum bound depth.
  -> TypeCache -- ^ New type cache, update with the new maximum bound depth.
setTypeCacheMaxBound tc n = tc { getTypeCacheMaxBound = n }
{-# INLINE setTypeCacheMaxBound #-}

-- | Test to see if a given cache has a max binder, \(b\), which is within the given bounds
-- \(b_min\) and \(b_max\), such that \(b \in [b_min, b_max)\).
-- Notice the minimum is an inclusive bound, while the maximum is an exclusive bound.
isInTypeCacheBounds
  :: TypeCache -- ^ Some type cache that may contain a binder maximum, \(b\).
  -> Int       -- ^ Inclusive minimum bound we're searching for, \(b_min\).
  -> Int       -- ^ Exclusive maximum bound we're searching for, \(b_max\).
  -> Bool      -- ^ True if the given type cache max binder is within the given range, such that \(b \in [b_min, b_max)\).
isInTypeCacheBounds tc minB maxB =
  minB <= b && b < maxB
  where
    b = getTypeCacheMaxBound tc
{-# INLINE isInTypeCacheBounds #-}

instance Semigroup TypeCache where
  (<>) c1 c2 = newTypeCache hasFree3 maxBound3
    where hasFree1 = getTypeCacheHasFree c1
          {-# INLINE hasFree1 #-}
          hasFree2 = getTypeCacheHasFree c2
          {-# INLINE hasFree2 #-}
          hasFree3 = hasFree1 || hasFree2
          {-# INLINE hasFree3 #-}
          maxBound1 = getTypeCacheMaxBound c1
          {-# INLINE maxBound1 #-}
          maxBound2 = getTypeCacheMaxBound c2
          {-# INLINE maxBound2 #-}
          maxBound3 = max maxBound1 maxBound2
          {-# INLINE maxBound3 #-}
  {-# INLINE (<>) #-}

instance Monoid TypeCache where
  mempty = emptyTypeCache
  {-# INLINE mempty #-}

