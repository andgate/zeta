module Language.Zeta.Syntax.Data.ModuleHeader where

import           Document.Data.Span                        (Span)
import           Language.Zeta.Syntax.Data.Name.ModuleName (ModuleName)

-- | A module header, the first statement in a file,
-- which declares the name of the module associated with that file.
data ModuleHeader
  = ModuleHeader -- Module header constructor, prefer 'newModuleHeader'.
  { -- | Get the name of the module declared by a module header.
    getModuleHeaderName :: ModuleName,
    -- | Get the source location of a module header.
    getModuleHeaderLoc  :: Maybe Span
  } deriving (Show)

-- | Construct a new module header.
newModuleHeader
  :: Maybe Span   -- ^ Source location for module header.
  -> ModuleName   -- ^ Name declared by module header.
  -> ModuleHeader -- ^ A new module header.
newModuleHeader l n =
  ModuleHeader {
    getModuleHeaderName = n,
    getModuleHeaderLoc = l
  }

-- | Set the name of a module header.
setModuleHeaderName
  :: ModuleHeader -- ^ Some module header to update.
  -> ModuleName   -- ^ A new module name.
  -> ModuleHeader -- ^ A new module header, updated with the new name.
setModuleHeaderName h n = h { getModuleHeaderName = n }

-- | Set the source location of a module header.
setModuleHeaderLoc
  :: ModuleHeader -- ^ Some module header to update.
  -> Maybe Span   -- ^ A new source location.
  -> ModuleHeader -- ^ A new module header, updated with the new source location.
setModuleHeaderLoc h l = h { getModuleHeaderLoc = l }
