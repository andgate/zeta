{-# LANGUAGE LambdaCase #-}
module Language.Zeta.Syntax.Data.Exp where

import           Data.List.NonEmpty                             (NonEmpty)
import           Document.Data.Span                             (Span)
import           Language.Zeta.Syntax.Data.Annotation.ExpAnn    (ExpAnn (..))
import           Language.Zeta.Syntax.Data.Branch               (Branch)
import           Language.Zeta.Syntax.Data.Clause               (ClauseBlock)
import           Language.Zeta.Syntax.Data.ExpCache             (ExpCache)
import           Language.Zeta.Syntax.Data.Lit                  (Lit)
import           Language.Zeta.Syntax.Data.Name.ConstructorName (ConstructorName)
import           Language.Zeta.Syntax.Data.Name.FieldName       (FieldName)
import           Language.Zeta.Syntax.Data.Object               (Object)
import           Language.Zeta.Syntax.Data.Operator             (BinaryOperator,
                                                                 UnaryOperator)
import           Language.Zeta.Syntax.Data.Pat                  (Pat)
import           Language.Zeta.Syntax.Data.Scope                (Scope)
import           Language.Zeta.Syntax.Data.Type                 (Type)
import           Language.Zeta.Syntax.Data.TypeCache            (TypeCache)
import           Language.Zeta.Syntax.Data.Var                  (Var)

-- | Expression syntax.
-- The blood of code.
data Exp
  = EVar            -- ^ Expression variable.
      Var                       -- ^ Represented by a variable.
  | ELit            -- ^ Literal expression.
      Lit                       -- ^ Represented by a literal.
  | ECall           -- ^ Function call expression.
      Exp                       -- ^ Function reference.
      [Exp]                     -- ^ Function arguments.
  | ELam            -- ^ Lambda expression (anonymous function).
      [Pat]                     -- ^ Parameter patterns.
      (Maybe Type)              -- ^ Optional return type.
      (Scope Exp)               -- ^ Expression body, scope by the lambda's pattern parameters.
  | EType           -- ^ Expression type annotation.
      Exp                       -- ^ Annotated expression.
      Type                      -- ^ Type of the annotated expression.
  | ECast           -- ^ Cast expression.
      Exp                       -- ^ Expression to cast.
      Type                      -- ^ Type to cast expression to.
  | ECache          -- ^ Expression cache annotation.
      Exp                       -- ^ Expression to annotate.
      ExpCache                  -- ^ Term cache to annotate expression with.
  | ETypeCache       -- ^ Type cache annotation, for types inside expressions.
      Exp                       -- ^ Expression to annotate.
      TypeCache                 -- ^ Term cache to annotate expression with.
  | ELoc            -- ^ Expression source location annotation.
      Exp                       -- ^ Expression to annotate.
      Span                      -- ^ Source location to annotate expression with.
  | EParens         -- ^ Expression parenthesis.
      Exp                       -- ^ Expression enclosed by parenthesis.
  | ERef            -- ^ Reference expression.
      Exp                       -- ^ Expression to get reference pointer to.
  | EDeref          -- ^ Derference expression.
      Exp                       -- ^ Pointer to get value of.
  | ETuple          -- ^ Tuple expression.
      Exp                       -- First value of tuple.
      (NonEmpty Exp)            -- Rest of the tuple values.
  | ECon            -- ^ Constructor expression.
      ConstructorName           -- ^ Name of constructor to call.
  | EArray          -- ^ Array expression.
      [Exp]                     -- ^ Contents of array.
  | EObj            -- ^ Object expression.
      (Object Exp)              -- ^ Object representation.
  | EFree           -- ^ Free heap allocated value.
      Exp                       -- ^ Pointer to heap object to free.
  | EField          -- ^ Field name expression.
      FieldName                 -- ^ Some field name.
  | EGet            -- ^ Get field of an expression.
      Exp                       -- ^ Object to access.
      FieldName                 -- ^ Name of field to access.
  | EGetI           -- ^ Get the index of an expression.
      Exp                       -- ^ An expression to index into.
      Exp                       -- ^ Some key to index the expression with.
  | ESet            -- ^ Update some location in memory.
      Exp                       -- ^ Reference to old value.
      Exp                       -- ^ New value.
  | EUnOp           -- ^ Unary operator expression.
      UnaryOperator             -- ^ Unary operator to apply.
      Exp                       -- ^ The one and only operand.
  | EBinOp          -- ^ Binary operator expression.
      BinaryOperator            -- ^ Binary operator to apply.
      Exp                       -- ^ Left operand.
      Exp                       -- ^ Right operand.
  | EBlock          -- ^ Block expression.
      Exp                       -- ^ Some expression enclosed within block.
  | ESeq            -- ^ Sequence expression.
      Exp                       -- ^ First expression in sequence.
      Exp                       -- ^ Second expression in sequence.
  | EBind           -- ^ Sequence two expressions, pattern matching the first and binding the names into the second.
      Pat                       -- ^ Some pattern to bind
      Exp                       -- ^ First expression in sequence, which is matched on.
      (Scope Exp)               -- ^ Second expression, scoped and bound by the names in the pattern.
  | EIf                  -- ^ If expression.
      Exp                   -- ^ Predicate expression.
      Exp                   -- ^ Conditional block.
      (Maybe Branch)        -- ^ If branch's
  | ECase                -- ^ Case expression.
      Exp                   -- ^ Expression to perform case-analysis on.
      ClauseBlock           -- ^ Clause block of cases.
  | EFor                 -- ^ For expression.
      Exp                   -- ^ Initializer expression.
      Exp                   -- ^ Loop condition.
      Exp                   -- ^ Post-loop update expression.
      Exp                   -- ^ Expression body to loop over.
  | EWhile               -- ^ While-loop statement.
      Exp                   -- ^ Loop condition.
      Exp                   -- ^ Expression body to loop over.
  | EDoWhile             -- ^ Do-While statement.
      Exp                   -- ^ Expression body to loop over.
      Exp                   -- ^ Loop condition.
  | EReturn              -- ^ Return statement.
      Exp                   -- ^ Some expression to return.
  | EBreak               -- ^ Break expression.
  | EContinue            -- ^ Continue expression.
  deriving (Show)


-- | Attempt to extract the value of 'ELam' from an expression,
-- if the given expression is one.
fromELam
  :: Exp
  -- ^ Expression to extract values from.
  -> Maybe ([Pat], Maybe Type, Scope Exp)
  -- ^ Successful extraction results in the values of 'ELam', otherwise 'Nothing'.
fromELam = \case
  ELam ps mty sc -> Just (ps, mty, sc)
  _              -> Nothing
{-# INLINE fromELam #-}

-- | Smart constructor for 'ELoc'.
toELoc
  :: Exp        -- ^ Expression to annotate with a source location.
  -> Maybe Span -- ^ A source location we want to annotation with.
  -> Exp        -- ^ An expression, newly annotated with a source location.
toELoc e = \case
  Just l  -> ELoc e l
  Nothing -> e
{-# INLINE toELoc #-}

-- | Attempt to extract the value of 'ELoc' from an expression,
-- if the given expression is one.
fromELoc
  :: Exp                -- ^ Expression to attempt extraction on.
  -> Maybe (Exp, Span)  -- ^ Sucessful extraction results in the values of 'ELoc', otherwise 'Nothing'.
fromELoc = \case
  ELoc e l -> Just (e, l)
  _        -> Nothing
{-# INLINE fromELoc #-}

-- | Get an expression without any annotations (ie. pure).
getExpPure
  :: Exp -- ^ Expression to purify.
  -> Exp -- ^ Purified expression.
getExpPure = \case
  ELoc e _       -> getExpPure e
  EType e _      -> getExpPure e
  ECache e _     -> getExpPure e
  ETypeCache e _ -> getExpPure e
  EParens e      -> getExpPure e
  e              -> e
{-# INLINE getExpPure #-}

-- | Get the 'ExpAnn' annotations of an expression.
getExpAnn
  :: Exp    -- ^ The expression to extract annotations from.
  -> ExpAnn -- ^ Annotations extracted from expression.
getExpAnn = getExpAnn' (Nothing, Nothing, Nothing, Nothing)
  where
    getExpAnn' (ml, mty, mec, mtc) = \case
      ELoc e l        -> getExpAnn' (Just l, mty, mec, mtc) e
      EType e ty      -> getExpAnn' (ml, Just ty, mec, mtc) e
      ECache e ec     -> getExpAnn' (ml, mty, Just ec, mtc) e
      ETypeCache e tc -> getExpAnn' (ml, mty, mec, Just tc) e
      EParens e       -> getExpAnn' (ml, mty, mec, mtc) e
      _               -> ExpAnn ml mty mec mtc
{-# INLINE getExpAnn #-}

-- | Set the 'ExpAnn' annotations of an expression.
-- This simply wraps the annotations stored in 'ExpAnn'
-- over a given expression. This will not replace existing
-- annotations.
setExpAnn
  :: Exp    -- ^ Expression to wrap annotations over.
  -> ExpAnn -- ^ New annotations.
  -> Exp    -- ^ Results in an expression wrapped in the annotations.
setExpAnn e = \case
  ExpAnn (Just l) mty mec mtc ->
    setExpAnn (ELoc e l) (ExpAnn Nothing mty mec mtc)
  ExpAnn Nothing (Just ty) mec mtc ->
    setExpAnn (EType e ty) (ExpAnn Nothing Nothing mec mtc)
  ExpAnn Nothing Nothing (Just ec) mtc ->
    setExpAnn (ECache e ec) (ExpAnn Nothing Nothing Nothing mtc)
  ExpAnn Nothing Nothing Nothing (Just tc) ->
    setExpAnn (ETypeCache e tc) (ExpAnn Nothing Nothing Nothing Nothing)
  ExpAnn Nothing Nothing Nothing Nothing ->
    e
{-# INLINE setExpAnn #-}

-- | Transfer annotations from one expression to another.
transferExpAnn
  :: Exp -- ^ Source expression where annotations are lifted from.
  -> Exp -- ^ Target expression, which is to be annotated.
  -> Exp -- ^ New expression, which is the lifted annotations wrapped around the target expression.
transferExpAnn e1 e2 = setExpAnn e2 (getExpAnn e1)
{-# INLINE transferExpAnn #-}

-- | Try to get the location annotation of an expression.
-- This will perform a deep search.
getExpLoc
  :: Exp        -- ^ The expression to attempt and extraction on.
  -> Maybe Span -- ^ 'Just' the source location annotation if it exists, otherwise 'Nothing'.
getExpLoc = getExpAnnLoc . getExpAnn
{-# INLINE getExpLoc #-}


-- | Attempt to extract the type annotation of an expression.
-- This will perform a deep search.
getExpType
  :: Exp        -- ^ The expression to attempt and extraction on.
  -> Maybe Type -- ^ 'Just' the type annotation if it exists, otherwise 'Nothing'.
getExpType = getExpAnnType . getExpAnn
{-# INLINE getExpType #-}

-- | Try to get the expression cache annotation of an expression.
-- This will perform a deep search.
getExpCacheAnn
  :: Exp              -- ^ The expression to attempt and extraction on.
  -> Maybe ExpCache  -- ^ 'Just' the expression cache annotation if it exists, otherwise 'Nothing'.
getExpCacheAnn = getExpAnnExpCache . getExpAnn
{-# INLINE getExpCacheAnn #-}

-- | Try to get the type cache annotation of an expression.
-- This will perform a deep search.
getExpTypeCache
  :: Exp              -- ^ The expression to attempt and extraction on.
  -> Maybe TypeCache  -- ^ 'Just' the type cache annotation if it exists, otherwise 'Nothing'.
getExpTypeCache = getExpAnnTypeCache . getExpAnn
{-# INLINE getExpTypeCache #-}

-- | Synonymous with 'getExpPure'.
exEAnn
  :: Exp -- ^ Impure expression.
  -> Exp -- ^ Purified expression.
exEAnn = getExpPure
{-# INLINE exEAnn #-}
