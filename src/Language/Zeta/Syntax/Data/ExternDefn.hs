module Language.Zeta.Syntax.Data.ExternDefn where

import           Document.Data.Span                          (Span)
import           Language.Zeta.Syntax.Data.Name.FunctionName (FunctionName)
import           Language.Zeta.Syntax.Data.Type              (Type)

-- | External function definition
data ExternDefn =
  ExternDefn -- ^ Constructor for external definitions, prefer 'newExternDefn'.
  { -- | Get the source location of an external definition.
    getExternDefnLoc        :: Maybe Span,
    -- | Get the function name of an external definition.
    getExternDefnName       :: FunctionName,
    -- | Get the function parameter types of an external definition.
    getExternDefnParamTypes :: [Type],
    -- | Get the return type of an external definition.
    getExternDefnReturnType :: Type
  } deriving(Show)

-- | Construct a new external definition.
newExternDefn
  :: Maybe Span   -- ^ Source location of the external definition.
  -> FunctionName -- ^ Name of the external definition.
  -> [Type]       -- ^ Function parameter types of the external definition.
  -> Type         -- ^ Return type of the external definition.
  -> ExternDefn   -- ^ Results in a new external definition.
newExternDefn l n paramtys retty =
  ExternDefn {
    getExternDefnLoc = l,
    getExternDefnName = n,
    getExternDefnParamTypes = paramtys,
    getExternDefnReturnType = retty
  }

-- | Set the 'FunctionName' of an external definition.
setExternDefnName
  :: ExternDefn    -- ^ External definition to update.
  -> FunctionName  -- ^ New function name for external definition.
  -> ExternDefn    -- ^ New external definition, updated with the function name.
setExternDefnName def n = def { getExternDefnName = n }
