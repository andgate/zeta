module Language.Zeta.Syntax.Data.MorphismDefn where

import           Document.Data.Span                       (Span)
import           Language.Zeta.Syntax.Data.FuncDefn       (FuncDefn)
import           Language.Zeta.Syntax.Data.Name.ClassName (ClassName)
import           Language.Zeta.Syntax.Data.Type           (Type)
import           Language.Zeta.Syntax.Data.TypeContext    (TypeContext)

-- Morphism definition syntax
data MorphismDefn =
  MorphismDefn -- ^ Morphism definition constructor.
  { -- | Get the source location of a morphism definition.
    getMorphismDefnLoc        :: Maybe Span
    -- | Get the type context preceding a morphism definition.
  , getMorphismDefnCtx        :: Maybe TypeContext
  -- | Get the name of the source type class in a morphism definition.
  , getMorphismDefnSourceName :: ClassName
  -- | Get the source arguments in a morphism definition.
  , getMorphismDefSourceArgs  :: [Type]
  -- | Get the name of the target type class in a morphism definition.
  , getMorphismDefnTarget     :: ClassName
  -- | Get the arguments of the target type class instance in a morphism definition.
  , getMorphismDefnSourceArgs :: [Type]
  -- | Get the body of a morphism definition.
  , getMorphismDefnBlock      :: MorphismDefnBlock
  } deriving(Show)

-- | Morphism definition block syntax, the body of a type class morphism definition.
newtype MorphismDefnBlock =
  MorphismDefnBlock -- ^ Constructor for a morphism definition block.
    [FuncDefn]      -- ^ A list of function definitions that make up a block.
  deriving(Show)
