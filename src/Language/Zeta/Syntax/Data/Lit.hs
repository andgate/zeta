module Language.Zeta.Syntax.Data.Lit where

import {-# SOURCE #-}           Language.Zeta.Syntax.Data.Exp (Exp)

-- Literal syntax.
data Lit
  = LNull         -- ^ The 'null' literal.
  | LInt          -- ^ An integer literal.
      Integer     -- ^ Some integer value.
  | LDouble       -- ^ A double floating point literal.
      Double      -- ^ Some double value.
  | LBool         -- ^ A boolean literal.
      Bool        -- ^ Some boolean value.
  | LChar         -- ^ Character literal.
      Char        -- ^ Some character value.
  | LString       -- ^ String literal.
      String      -- ^ Some string value.
  | LArray        -- ^ Array literal.
      [Exp]       -- ^ List of array elements.
  | LArrayI       -- ^ Sized, empty array literal.
      Integer     -- ^ Size of the empty array.
  | LGetI         -- ^ Static, indexed getter literal.
      Exp         -- ^ Expression of some object to access.
      Int         -- ^ The index to access.
  deriving(Show)
