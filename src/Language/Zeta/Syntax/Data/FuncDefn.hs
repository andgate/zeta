module Language.Zeta.Syntax.Data.FuncDefn where

import           Document.Data.Span                          (Span)
import           Language.Zeta.Syntax.Data.Exp               (Exp)
import           Language.Zeta.Syntax.Data.Name.FunctionName (FunctionName)
import           Language.Zeta.Syntax.Data.Pat               (Pat)
import           Language.Zeta.Syntax.Data.Type              (Type)
import           Language.Zeta.Syntax.Data.TypeContext       (TypeContext)

-- | Function definition syntax.
data FuncDefn
  = FuncDefn -- ^ Constructor for function definitions, prefer 'newFuncDefn'.
  { -- | Get the source location of a function definition.
    getFuncDefnLoc        :: Maybe Span,
    -- | Get the type context preceeding a function definition.
    getFuncDefnTyCtx      :: Maybe TypeContext,
    -- | Get the 'FunctionName' of a function definition.
    getFuncDefnName       :: FunctionName,
    -- | Get the function parameters of a function definition.
    getFuncDefnArgs       :: [Pat],
    -- | Get the return type of a function definition.
    getFuncDefnReturnType :: Maybe Type,
    -- | Get the body of a function definition.
    getFuncDefnBody       :: FuncDefnBody
  } deriving(Show)

-- | Construct a new function definition.
newFuncDefn
  :: Maybe Span         -- ^ Source location of the function definition.
  -> Maybe TypeContext  -- ^ Type context which precedes the function definition.
  -> FunctionName       -- ^ The name of the function definition.
  -> [Pat]              -- ^ The parameters of the function definition.
  -> Maybe Type         -- ^ The return type of the function definition.
  -> FuncDefnBody       -- ^ Function body of the new function definition.
  -> FuncDefn           -- ^ The new function definition.
newFuncDefn l ctx n args retty fnbody =
  FuncDefn {
    getFuncDefnLoc = l,
    getFuncDefnTyCtx = ctx ,
    getFuncDefnName = n ,
    getFuncDefnArgs = args,
    getFuncDefnReturnType = retty,
    getFuncDefnBody = fnbody
  }

-- | Set the source location of a function definition.
setFuncDefnLoc
  :: FuncDefn   -- ^ Function definition to modify.
  -> Maybe Span -- ^ New source location.
  -> FuncDefn   -- ^ New function definition, updated with the new source location.
setFuncDefnLoc def l = def { getFuncDefnLoc = l }

-- | Set the type context of a function definition.
setFuncDefnTypeCtx
  :: FuncDefn          -- ^ Function definition to modify.
  -> Maybe TypeContext -- ^ New type context.
  -> FuncDefn           -- ^ New function definition, updated with the new type context.
setFuncDefnTypeCtx def ctx = def { getFuncDefnTyCtx = ctx }

-- | Set the name of a function definition.
setFuncDefnName
  :: FuncDefn     -- ^ Function definition to modify.
  -> FunctionName -- ^ New function name.
  -> FuncDefn     -- ^ New function definition, updated with the new function name.
setFuncDefnName def n = def { getFuncDefnName = n }

-- | Set the arguments of a function definition.
setFuncDefnArgs
  :: FuncDefn -- ^ Function definition to modify.
  -> [Pat]    -- ^ New list of arguments.
  -> FuncDefn -- ^ New function definition, updated with the new list of arguments.
setFuncDefnArgs def args = def { getFuncDefnArgs = args }

-- | Set the return type of a function definition.
setFuncDefnReturnType
  :: FuncDefn   -- ^ Function definition to modify.
  -> Maybe Type -- ^ New return type.
  -> FuncDefn   -- ^ New function definition, updated with the new return type.
setFuncDefnReturnType def retty = def { getFuncDefnReturnType = retty }

-- | Set the body of a function definition.
setFuncDefnBody
  :: FuncDefn     -- ^ Function definition to modify.
  -> FuncDefnBody -- ^ New function definition body.
  -> FuncDefn     -- ^ New function definition, updated with the new body.
setFuncDefnBody def bd = def { getFuncDefnBody = bd }

-- | Function body syntax, which sits at the end of every function definition.
data FuncDefnBody
  = FuncDefnBodyBlock -- ^ Function body as a statement block.
      (Maybe Span)    -- ^ Source location.
      Exp             -- ^ Function body expression.
  | FuncDefnBodyExp   -- ^ A function body which consists of a single expression.
      (Maybe Span)    -- ^ Source location.
      Exp             -- ^ Expression to use as a function body.
  deriving(Show)
