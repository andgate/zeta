module Language.Zeta.Syntax.Data.TypeContext where

import                          Data.List.NonEmpty                       (NonEmpty)
import                          Document.Data.Span                       (Span)
import                          Language.Zeta.Syntax.Data.Name.ClassName (ClassName)
import                          Language.Zeta.Syntax.Data.TVar           (TVar)
import {-# SOURCE #-}           Language.Zeta.Syntax.Data.Type           (Type)

-- | Type context syntax.
data TypeContext =
  TypeContext             -- ^ Type context constructor.
    (Maybe Span)          -- ^ Source location of the type context.
    [TypeContextElement]  -- ^ Elements which make-up the type context.
  deriving(Show)

-- | A type context element,
-- which is either a type schema or some class constraints.
data TypeContextElement
  = TypeContextSchema     -- ^ Type context schema constructor.
      TypeSchema          -- ^ Some type schema.
  | TypeContextConstraint -- ^ Type context constraint constructor.
      ClassConstraint     -- ^ Some class constraint.
  deriving(Show)

-- | Type schema syntax.
data TypeSchema =
  TypeSchema        -- ^ Type schema constructor.
    (Maybe Span)    -- ^ Source location of type schema.
    (NonEmpty TVar) -- ^ Non-empty list of type variables bound by the schema.
  deriving(Show)

-- | Class constraint syntax.
data ClassConstraint =
  ClassConstraint            -- ^ Class constraint constructor.
    (Maybe Span)             -- ^ Source location of class constraint.
    (NonEmpty ClassInstance) -- ^ Non-empty list of class instances to use as constraints.
  deriving(Show)

-- | Class instance syntax.
data ClassInstance =
  ClassInstance  -- ^ Class instance constructor.
    (Maybe Span) -- ^ Source location of class instance.
    ClassName    -- ^ Name of instantiated class.
    [Type]       -- ^ Type arguments to instantiate type class with.
  deriving(Show)
