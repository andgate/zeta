{-# LANGUAGE LambdaCase   #-}
{-# LANGUAGE ViewPatterns #-}
module Language.Zeta.Syntax.Data.PatRank where

import           Language.Zeta.Syntax.Data.Pat (Pat (..))

-- | Pattern syntax term binding rank judement.
-- Ordered from A to B, with A being the lowest rank, and B being the highest.
data PatRank
  = APat -- ^ Rank-A patterns
  | BPat -- ^ Rank-B patterns

-- | Judge the rank of some pattern.
patRank
  :: Pat      -- ^ Pattern to judge.
  -> PatRank  -- ^ Judgement of the patterns rank.
patRank = \case
  PVar _         -> APat
  PCon _ []      -> APat
  PCon _ _       -> BPat
  PObj _         -> APat
  PLabel _ _     -> BPat
  PView _ _      -> BPat
  PTuple _ _     -> APat
  PWild          -> APat
  PType _ _      -> BPat
  PExpCache p _  -> patRank p
  PTypeCache p _ -> patRank p
  PLoc p _       -> patRank p
  PParens _      -> APat

-- | Test if a given expression is 'APat'.
isAPat
  :: Pat  -- ^ Pattern to test.
  -> Bool -- ^ 'True' if pattern is 'AExp', otherwise 'False'.
isAPat (patRank -> APat) = True
isAPat _                 = False

-- | Test if a given expression is 'BPat'.
isBPat
  :: Pat  -- ^ Pattern to test.
  -> Bool -- ^ 'True' if pattern is 'BExp', otherwise 'False'.
isBPat (patRank -> BPat) = True
isBPat _                 = False
