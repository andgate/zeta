module Language.Zeta.Syntax.Data.TVar where

import                          Data.Text                      (Text)
import                          Document.Data.Span             (Span)
import {-# SOURCE #-}           Language.Zeta.Syntax.Data.Type (Type)

-- | Type variable syntax.
data TVar =
  TV -- ^ Type variable constructor.
  { -- | Get the 'Text' representation of a type variable.
    getTVarStr    :: Text
    -- | Get the binder of a type variable, if there is one.
  , getTVarBinder :: Maybe Int
    -- | Get the type of a type variable.
  , getTVarType   :: Maybe Type
    -- | Get the source location of a type variable.
  , getTVarLoc    :: Maybe Span
  }

-- | Construct a new, free type variable.
newFreeTVar
  :: Maybe Span -- ^ A source location.
  -> Text       -- ^ A text identifier.
  -> TVar       -- ^ A new type variable.
newFreeTVar l txt =
  TV {
    getTVarStr    = txt,
    getTVarBinder = Nothing,
    getTVarType   = Nothing,
    getTVarLoc    = l
  }
{-# INLINE newFreeTVar #-}

-- | Given a list ordered list of vars,
-- apply a new set of binders to the vars.
-- The binder identifiers are generated from [0..(n-1)].
toBoundTVars :: [TVar] -> [TVar]
toBoundTVars = zipWith (flip setTVarBinder') [0..]
{-# INLINE toBoundTVars #-}

-- | Set the text identifier of a type variable.
setTVarStr
  :: TVar -- ^ A type variable to update.
  -> Text -- ^ New text identifier.
  -> TVar -- ^ New type variable, updated with new identifier.
setTVarStr v s = v { getTVarStr = s }
{-# INLINE setTVarStr #-}

-- | Set the binder of a type variable.
setTVarBinder
  :: TVar      -- ^ A type variable to update.
  -> Maybe Int -- ^ New binder, possibly Nothing.
  -> TVar      -- ^ New type variable, updated with new binder.
setTVarBinder v b = v { getTVarBinder = b }
{-# INLINE setTVarBinder #-}

-- | Set the binder of a type variable.
-- This is a weakened version of the setter, where the binder is non-optional.
setTVarBinder'
  :: TVar -- ^ A type variable to update.
  -> Int  -- ^ New binder id.
  -> TVar -- ^ New type variable, updated with new binder.
setTVarBinder' tv b = tv { getTVarBinder = Just b }
{-# INLINE setTVarBinder' #-}

-- | Set the type of a type variable.
setTVarType
  :: TVar -- ^ A type variable to update.
  -> Type -- ^ New type annotation.
  -> TVar -- ^ New type variable, updated with the new type annotation.
setTVarType v t = v { getTVarType = Just t }
{-# INLINE setTVarType #-}

-- | Set the source location of a type variable.
setTVarLoc
  :: TVar       -- ^ A type variable to update.
  -> Maybe Span -- ^ New source location.
  -> TVar       -- ^ New type variable, updated with the new source location.
setTVarLoc v l = v { getTVarLoc = l }
{-# INLINE setTVarLoc #-}

instance Eq TVar where
  (==) v u = getTVarStr v == getTVarStr u
  {-# INLINE (==) #-}

instance Show TVar where
  show (TV n _ _ _) = show n
