module Language.Zeta.Syntax.Data.Name.PatternName where

import           Data.Text                      (Text)
import           Document.Data.Span             (Span)
import           Language.Zeta.Syntax.Data.Type (Type)

-- | Datatype for pattern names.
data PatternName =
  PatternName -- ^ Pattern name constructor, use 'newPatternName' instead.
  { -- | Get the text represention of a pattern name.
    getPatternNameText :: Text,
    -- | Get the source location of a pattern name.
    getPatternNameLoc  :: Maybe Span,
    -- | Get the type of a pattern name.
    getPatternNameType :: Maybe Type
  }
  deriving (Show)

-- | Construct a new pattern name.
newPatternName
  :: Maybe Span  -- ^ Optional source location of new pattern name.
  -> Text        -- ^ Text representation of new pattern name.
  -> PatternName -- ^ A new pattern name.
newPatternName l txt =
  PatternName {
    getPatternNameText = txt,
    getPatternNameLoc = l,
    getPatternNameType = Nothing
  }

-- | Set the text representation of a pattern name.
setPatternNameText
  :: PatternName -- ^ Pattern name to update.
  -> Text        -- ^ New text representation of the pattern name.
  -> PatternName -- ^ New pattern name with the new text representation.
setPatternNameText n txt = n { getPatternNameText = txt }

setPatternNameLoc
  :: PatternName -- ^ Pattern name to update.
  -> Maybe Span  -- ^ New source location for the pattern name.
  -> PatternName -- ^ New pattern name with the new source location.
setPatternNameLoc n l = n { getPatternNameLoc = l }

setPatternNameType
  :: PatternName -- ^ Pattern name to update.
  -> Maybe Type  -- ^ New type for the pattern name.
  -> PatternName -- ^ New pattern name with a new type.
setPatternNameType n t = n { getPatternNameType = t }

instance Eq PatternName where
  (==) n1 n2 = getPatternNameText n1 == getPatternNameText n2

instance Ord PatternName where
  (<=) n1 n2 = getPatternNameText n1 <= getPatternNameText n2
