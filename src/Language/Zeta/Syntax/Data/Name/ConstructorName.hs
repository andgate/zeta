module Language.Zeta.Syntax.Data.Name.ConstructorName where

import           Data.Text                      (Text)
import           Document.Data.Span             (Span)
import           Language.Zeta.Syntax.Data.Type (Type)

-- | Data for constructor names.
data ConstructorName =
  ConstructorName -- ^ Constructor name constructor, use 'newConstructorName' instead.
  { getConstructorNameText :: Text,
    getConstructorNameLoc  :: Maybe Span,
    getConstructorNameType :: Maybe Type
  } deriving (Show)

-- | Construct a new constructor name.
newConstructorName
  :: Maybe Span       -- ^ Optional source location of the new constructor name.
  -> Text             -- ^ Text representation of the new constuctor name.
  -> ConstructorName  -- ^ A new constructor name.
newConstructorName l txt =
  ConstructorName {
    getConstructorNameText = txt,
    getConstructorNameLoc = l,
    getConstructorNameType = Nothing
  }

-- | Set the text representation of a constructor name.
setConstructorNameText
  :: ConstructorName -- ^ Constructor name to update.
  -> Text            -- ^ New text representation of the constructor name.
  -> ConstructorName -- ^ New constructor name with the new text representation.
setConstructorNameText n txt = n { getConstructorNameText = txt }

-- | Set the source location of a constructor name.
setConstructorNameLoc
  :: ConstructorName -- ^ Constructor name to update.
  -> Maybe Span      -- ^ New source location for the constructor name.
  -> ConstructorName -- ^ New constructor name with new source location.
setConstructorNameLoc n l = n { getConstructorNameLoc = l }

-- | Set the type annotation of a constructor name.
setConstructorNameType
  :: ConstructorName -- ^ Constructor name to update.
  -> Maybe Type      -- ^ New type annotation for the constructor name.
  -> ConstructorName -- ^ New constructor name with new type annotation.
setConstructorNameType n ty = n { getConstructorNameType = ty }

instance Eq ConstructorName where
  (==) n1 n2 = getConstructorNameText n1 == getConstructorNameText n2

instance Ord ConstructorName where
  (<=) n1 n2 = getConstructorNameText n1 <= getConstructorNameText n2
