module Language.Zeta.Syntax.Data.Name.FieldName where

import           Data.Text                      (Text)
import           Document.Data.Span             (Span)
import           Language.Zeta.Syntax.Data.Type (Type)

-- | Datatype for object field names.
data FieldName =
  FieldName -- ^ Field name constructor, use 'newFieldName' instead.
  { -- | Get the text representation of a field name.
    getFieldNameText :: Text,
    -- | Get the source location of a field name.
    getFieldNameLoc  :: Maybe Span,
    -- | Get the type of a field name.
    getFieldNameType :: Maybe Type
  } deriving (Show)

-- | Construct a new field name.
newFieldName
  :: Maybe Span -- ^ Optional source location of the new field name.
  -> Text       -- ^ Text representation of the field name.
  -> FieldName  -- ^ A new field name.
newFieldName l txt =
  FieldName {
    getFieldNameText = txt,
    getFieldNameLoc = l,
    getFieldNameType = Nothing
  }

-- | Set the text representation of a field name.
setFieldNameText
  :: FieldName -- ^ Field name to update.
  -> Text      -- ^ New text representation of the field name.
  -> FieldName -- ^ New field name with the new text representation.
setFieldNameText n txt = n { getFieldNameText = txt }

-- | Set the source location of a field name.
setFieldNameLoc
  :: FieldName  -- ^ Field name to update.
  -> Maybe Span -- ^ New source location for the field name.
  -> FieldName  -- ^ New field name with the new source location.
setFieldNameLoc n l = n { getFieldNameLoc = l }

-- | Set the type of a field name.
setFieldNameType
  :: FieldName  -- ^ Field name to update.
  -> Maybe Type -- ^ New type for the field name.
  -> FieldName  -- ^ New field name with a new type.
setFieldNameType n t = n { getFieldNameType = t }

instance Eq FieldName where
  (==) n1 n2 = getFieldNameText n1 == getFieldNameText n2

instance Ord FieldName where
  (<=) n1 n2 = getFieldNameText n1 <= getFieldNameText n2
