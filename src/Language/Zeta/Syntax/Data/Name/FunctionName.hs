module Language.Zeta.Syntax.Data.Name.FunctionName where

import           Data.Text          (Text)
import           Document.Data.Span (Span)

-- | Datatype for function names.
data FunctionName =
  FunctionName -- ^ Function name constructor, use 'newFunctionName' instead.
  { -- | Get the text representation of a function name.
    getFunctionNameText :: Text,
    -- | Get the source location of a function name.
    getFunctionNameLoc  :: Maybe Span
  } deriving (Show)

-- | Construct a new function name.
newFunctionName
  :: Maybe Span   -- ^ Optional source location of new function name.
  -> Text         -- ^ Text representation of new function name.
  -> FunctionName -- ^ A new function name.
newFunctionName l txt =
  FunctionName {
    getFunctionNameLoc = l,
    getFunctionNameText = txt
  }

-- | Set the text representation of a function name.
setFunctionNameText
  :: FunctionName -- ^ Function name to update.
  -> Text         -- ^ New text representation of the function name.
  -> FunctionName -- ^ New function name with the new text representation.
setFunctionNameText n txt = n { getFunctionNameText = txt }

-- | Set the source location of a function name.
setFunctionNameLoc
  :: FunctionName -- ^ Function name to update.
  -> Maybe Span   -- ^ New source location for the function name.
  -> FunctionName -- ^ New function name with the new source location.
setFunctionNameLoc n l = n { getFunctionNameLoc = l }

instance Eq FunctionName where
  (==) n1 n2 = getFunctionNameText n1 == getFunctionNameText n2

instance Ord FunctionName where
  (<=) n1 n2 = getFunctionNameText n1 <= getFunctionNameText n2
