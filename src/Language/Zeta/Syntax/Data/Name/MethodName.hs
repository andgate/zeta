module Language.Zeta.Syntax.Data.Name.MethodName where

import           Data.Text          (Text)
import           Document.Data.Span (Span)

-- | Datatype for type class instance method names.
data MethodName =
  MethodName -- ^ Method name constructor, use 'newMethodName' instead.
  { -- | Get the text representation of a method name.
    getMethodNameText :: Text,
    -- | Get the source location of a method name.
    getMethodNameLoc  :: Maybe Span
  } deriving (Show)

-- | Construct a new method name.
newMethodName
  :: Maybe Span -- ^ Optional source location of new method name.
  -> Text       -- ^ Text representation of new method name.
  -> MethodName -- ^ A new method name.
newMethodName l txt =
  MethodName {
    getMethodNameText = txt,
    getMethodNameLoc = l
  }

-- | Set the text representation of a method name.
setMethodNameText
  :: MethodName -- ^ Method name to update.
  -> Text       -- ^ A new text representation for the method name.
  -> MethodName -- ^ A new method name, updated with the new text representation.
setMethodNameText n txt = n { getMethodNameText = txt }

-- | Set the source location of a method name.
setMethodNameLoc
  :: MethodName -- ^ Method name to update.
  -> Maybe Span -- ^ A new source location for the method name.
  -> MethodName -- ^ A new method name, updated with the new source location.
setMethodNameLoc n l = n { getMethodNameLoc = l }

instance Eq MethodName where
  (==) n1 n2 = getMethodNameText n1 == getMethodNameText n2

instance Ord MethodName where
  (<=) n1 n2 = getMethodNameText n1 <= getMethodNameText n2
