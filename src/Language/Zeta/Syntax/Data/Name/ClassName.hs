module Language.Zeta.Syntax.Data.Name.ClassName where

import           Data.Text          (Text)
import           Document.Data.Span (Span)

-- | Datatype for class names.
data ClassName
  = ClassName  -- ^ Class name constructor, use 'newClassName' instead.
  { -- | Get the text of a class name.
    getClassNameText :: Text,
    -- | Get the source location of a class name.
    getClassNameLoc  :: Maybe Span
  } deriving (Show)

-- | Construct a new class name.
newClassName
  :: Maybe Span   -- ^ Optional source location of the new class name.
  -> Text         -- ^ Text string representation of the new class name.
  -> ClassName    -- ^ A new class name.
newClassName l txt =
  ClassName {
    getClassNameText = txt,
    getClassNameLoc = l
  }

-- | Set the text representation of a class name.
setClassNameText
  :: ClassName  -- ^ Class name to update.
  -> Text       -- ^ New text representation of the class name.
  -> ClassName  -- ^ New class name with the new text representation.
setClassNameText n txt = n { getClassNameText = txt }

-- | Set the source location of a class name.
setClassNameLoc
  :: ClassName   -- ^ Class name to update.
  -> Maybe Span  -- ^ New source location for the class name.
  -> ClassName   -- ^ New class name with the new source location.
setClassNameLoc n l = n { getClassNameLoc = l }

instance Eq ClassName where
  (==) n1 n2 = getClassNameText n1 == getClassNameText n2

instance Ord ClassName where
  (<=) n1 n2 = getClassNameText n1 <= getClassNameText n2
