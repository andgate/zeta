module Language.Zeta.Syntax.Data.Name.ImportName where

import           Data.Text          (Text)
import           Document.Data.Span (Span)

-- | Datatype for import names.
data ImportName =
  ImportName -- ^ Import name constructor, use 'newImportName' instead.
  { -- | Get the text representation of an import target name.
    getImportNameText       :: Text,
    -- | Get the list of name qualifiers preceeding an import target name.
    getImportNameQualifiers :: [Text],
    -- | Get the source location of an import name.
    getImportNameLoc        :: Maybe Span
  } deriving (Show)

-- | Construct a new import name.
newImportName
  :: Maybe Span
  -> [Text]
  -> Text
  -> ImportName
newImportName l qs txt =
  ImportName {
    getImportNameText = txt,
    getImportNameQualifiers = qs,
    getImportNameLoc = l
  }

-- | Set the text representation of an import target name.
setImportNameText
  :: ImportName -- ^ Import name to update.
  -> Text       -- ^ New text representation of the import target name.
  -> ImportName -- ^ New import name with the new text representation.
setImportNameText n txt = n { getImportNameText = txt }

-- | Set the list of name qualifiers preceeding an import target name.
setImportNameQualifiers
  :: ImportName -- ^ Import name to update.
  -> [Text]     -- ^ A new list of name qualifiers.
  -> ImportName -- ^ New import name, updated with list of name qualifiers.
setImportNameQualifiers n qs = n { getImportNameQualifiers = qs }

-- | Set the source location of an import name.
setImportNameLoc
  :: ImportName -- ^ Import name to update.
  -> Maybe Span -- ^ New source location for the import name.
  -> ImportName -- ^ New import name with the new source location.
setImportNameLoc n l = n { getImportNameLoc = l }

instance Eq ImportName where
  (==) n1 n2 = getImportNameText n1 == getImportNameText n2

instance Ord ImportName where
  (<=) n1 n2 = getImportNameText n1 <= getImportNameText n2
