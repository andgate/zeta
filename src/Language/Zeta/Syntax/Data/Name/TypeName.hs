module Language.Zeta.Syntax.Data.Name.TypeName where

import           Data.Text          (Text)
import           Document.Data.Span (Span)

-- | Datatype for type names.
data TypeName =
  TypeName -- ^ Type name constructor, use 'newTypeName' instead.
  { getTypeNameText :: Text,
    getTypeNameLoc  :: Maybe Span
  } deriving (Show)

-- | Construct a new type name.
newTypeName
  :: Maybe Span
  -> Text
  -> TypeName
newTypeName l txt =
  TypeName {
    getTypeNameText = txt,
    getTypeNameLoc = l
  }

-- | Set the text representation of a type name.
setTypeNameText
  :: TypeName -- ^ Type name to update.
  -> Text     -- ^ New text representation of the type name.
  -> TypeName -- ^ New type name with the new text representation.
setTypeNameText n txt = n { getTypeNameText = txt }

-- | Set the source location of a type name.
setTypeNameLoc
  :: TypeName   -- ^ Type name to update.
  -> Maybe Span -- ^ New source location for the type name.
  -> TypeName   -- ^ New type name with the new source location.
setTypeNameLoc n l = n { getTypeNameLoc = l }

instance Eq TypeName where
  (==) n1 n2 = getTypeNameText n1 == getTypeNameText n2

instance Ord TypeName where
  (<=) n1 n2 = getTypeNameText n1 <= getTypeNameText n2
