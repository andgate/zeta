module Language.Zeta.Syntax.Data.Name.ModuleName where

import           Data.Text          (Text)
import           Document.Data.Span (Span)

-- | Datatype for module names.
data ModuleName =
  ModuleName -- ^ Module name constructor, use 'newModuleName' instead.
  { -- | Get the text representation of a module name.
    getModuleNameText       :: Text,
    -- | Get the name qualifier of a module name.
    getModuleNameQualifiers :: [Text],
    -- | Get the source location of a module name.
    getModuleNameLoc        :: Maybe Span
  } deriving (Show)

-- | Construct a new module name.
newModuleName
  :: Maybe Span -- ^ Optional source location of new method name.
  -> [Text]     -- ^ Name qualifiers preceeding module name text.
  -> Text       -- ^ Text representation of new name.
  -> ModuleName -- ^ A new module name.
newModuleName l qs n =
  ModuleName {
    getModuleNameText = n,
    getModuleNameQualifiers = qs,
    getModuleNameLoc = l
  }

-- | Set the text representation of a module name.
setModuleNameText
  :: ModuleName -- ^ Module name to update.
  -> Text       -- ^ A new text representation for the module name.
  -> ModuleName -- ^ A new module name, updated with the new text representation.
setModuleNameText n txt = n { getModuleNameText = txt }

-- | Set the name qualifiers of a module name.
setModuleNameQualifiers
  :: ModuleName -- ^ Module name to update.
  -> [Text]     -- ^ A list of new name qualifiers for the module name.
  -> ModuleName -- ^ A new module name, updated with the new list of name qualifiers.
setModuleNameQualifiers n qs = n { getModuleNameQualifiers = qs }

-- | Set the source location of a module name.
setModuleNameLoc
  :: ModuleName -- ^ Module name to update.
  -> Maybe Span -- ^ A new source location for the module name.
  -> ModuleName -- ^ A new module name, updated with the new source location.
setModuleNameLoc n l = n { getModuleNameLoc = l }

instance Eq ModuleName where
  (==) n1 n2 = getModuleNameText n1 == getModuleNameText n2

instance Ord ModuleName where
  (<=) n1 n2 = getModuleNameText n1 <= getModuleNameText n2
