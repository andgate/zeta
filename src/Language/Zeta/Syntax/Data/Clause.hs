module Language.Zeta.Syntax.Data.Clause where

import                          Data.List.NonEmpty              (NonEmpty)
import                          Document.Data.Span              (Span)
import {-# SOURCE #-}           Language.Zeta.Syntax.Data.Exp   (Exp)
import                          Language.Zeta.Syntax.Data.Pat   (Pat)
import                          Language.Zeta.Syntax.Data.Scope (Scope)

-- | A block of clauses is just an ordered list of clauses.
data ClauseBlock =
  ClauseBlock -- ^ Constructor for a clause block.
    (Maybe Span)  -- ^ Some source location.
    [Clause]      -- ^ List of clauses.
  deriving (Show)

-- | A clause, which binds a list of patterns over an expression body.
data Clause
  = Clause
      (Maybe Span)   -- ^ Source location of clause
      (NonEmpty Pat) -- ^ List of patterns which the clause binds.
      (Scope Exp)    -- ^ Expression body bound by the clause.
  deriving (Show)
