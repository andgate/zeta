{-# LANGUAGE LambdaCase   #-}
{-# LANGUAGE ViewPatterns #-}
module Language.Zeta.Syntax.Data.Type where

import           Data.List.NonEmpty                           (NonEmpty)
import           Document.Data.Span                           (Span)
import           Language.Zeta.Syntax.Data.Annotation.TypeAnn (TypeAnn (..))
import           Language.Zeta.Syntax.Data.Name.TypeName      (TypeName)
import           Language.Zeta.Syntax.Data.TVar               (TVar)
import           Language.Zeta.Syntax.Data.TypeCache          (TypeCache)
import           Language.Zeta.Syntax.Data.TypeContext        (TypeContext)

-- | Type syntax.
data Type
  = TVar                      -- ^ Type variable.
      TVar                    -- ^ Some type variable.
  | TFunc                     -- ^ Type function.
      [Type]                  -- ^ Function argument types.
      Type                    -- ^ Function result type.
  | TApp                      -- ^ Type application.
      Type                    -- ^ Type to apply arguments to.
      [Type]                  -- ^ Type application arguments.
  | TCon                      -- ^ Type constuctor
      TypeName                -- ^ Some type name
  | TInt                      -- ^ Integer type.
      Int                     -- ^ Some size.
  | TUInt                     -- ^ Unsigned integer type.
      Int                     -- ^ Some size.
  | TFp                       -- ^ Double type.
      Int                     -- ^ Some size.
  | TTuple                    -- ^ Tuple type.
      Type                    -- ^ First element of type.
      (NonEmpty Type)         -- ^ Rest of tuple elements.
  | TArray                    -- ^ Array type.
      Int                     -- ^ Size of array.
      Type                    -- ^ Memebers in array.
  | TPtr                      -- ^ Pointer type.
      Type                    -- ^ Type of data pointer references.
  | TCtx                      -- ^ Type context.
      TypeContext             -- ^ Some type context.
      Type                    -- ^ Some type the type context is bound over.
  | TCache                    -- ^ Type term cache annoation.
      Type                    -- ^ Some type to annotate.
      TypeCache               -- ^ Some term cache to annotate a type with.
  | TLoc                      -- ^ Type source location annotation.
      Type                    -- ^ Some type to annotate.
      Span                    -- ^ Some source location to annotate a type with.
  | TParens                   -- ^ Parenthesis annotation.
      Type                    -- ^ Type enclosed by parenthesis annotation.
  deriving(Show)

-- | Constructor to build a 'TLoc'.
toTLoc
  :: Type       -- ^ Some type to annotate.
  -> Maybe Span -- ^ Some source location to annotate the type with.
  -> Type       -- ^ Results in the type annotated with some source location.
toTLoc t = \case
  Just l  -> TLoc t l
  Nothing -> t

-- | Attempt to extract the contents of a 'Type' if its a 'TLoc'.
fromTLoc
  :: Type               -- ^ Type to try to match a 'TLoc' on.
  -> Maybe (Type, Span) -- ^ If success, the contents of 'TLoc', otherwise 'Nothing'.
fromTLoc = \case
  TLoc t l -> Just (t, l)
  _        -> Nothing

-- | Get a type without any annotations (or pure).
-- Simply matches on a type, dropping any annotations,
-- until it encouters a 'Type' constructor that isn't an annotation.
getTypePure
  :: Type -- ^ Some type that may have top-level annotations.
  -> Type -- ^ That type, but without top-level annotations.
getTypePure = \case
  TLoc t _   -> getTypePure t
  TCache t _ -> getTypePure t
  TParens t  -> getTypePure t
  t          -> t

-- | Get the 'TypeAnn' annotations of a type.
getTypeAnn
  :: Type    -- ^ Some type to extract an annotation from.
  -> TypeAnn -- ^ Type annotation that was extract from the given type.
getTypeAnn = getTypeAnn' (Nothing, Nothing)
  where
    getTypeAnn' (ml, mtc) = \case
      TLoc t l    -> getTypeAnn' (Just l, mtc) t
      TCache t tc -> getTypeAnn' (ml, Just tc) t
      TParens t   -> getTypeAnn' (ml, mtc) t
      _           -> TypeAnn ml mtc

-- | Set the 'TypeAnn' annotations of a type.
setTypeAnn
  :: Type    -- ^ Some type to update.
  -> TypeAnn -- ^ Some new set of type annotations.
  -> Type    -- ^ A new type, wrapped in the given annotations.
setTypeAnn t = \case
  TypeAnn (Just l) mtc      -> setTypeAnn (TLoc t l) (TypeAnn Nothing mtc)
  TypeAnn Nothing (Just tc) -> setTypeAnn (TCache t tc) (TypeAnn Nothing Nothing)
  TypeAnn Nothing Nothing   -> t

-- | Transfer 'TypeAnn' from one 'Type' to another.
transferTypeAnn
  :: Type -- ^ Some type with annotations.
  -> Type -- ^ Some type to transfer those annotations too.
  -> Type -- ^ A new type, made from the wrapping the second type in the annotations from the first.
transferTypeAnn t1 t2 = setTypeAnn t2 (getTypeAnn t1)
{-# INLINE transferTypeAnn #-}

-- | Discard annotations embedded in a type.
-- Equivalent to 'getTypePure'.
exTyAnn
  :: Type -- ^ Impure type, possibly with top-level annotations.
  -> Type -- ^ Type with top-level annotations removed.
exTyAnn = \case
  TLoc t _  -> exTyAnn t
  TParens t -> exTyAnn t
  t         -> t

-- | Supported integer sizes.
intSizes :: [Int]
intSizes = [1, 8, 16, 32, 64]

-- | Supported unsigned integer sizes.
uintSizes :: [Int]
uintSizes = [8, 16, 32, 64]

-- | Supported double sizes.
floatSizes :: [Int]
floatSizes = [16, 32, 64, 128]

-- | Supported list of sized integer types.
intTypes :: [Type]
intTypes = TInt <$> intSizes

-- | Supported list of sized unsigned integer types.
uintTypes :: [Type]
uintTypes = TUInt <$> uintSizes

-- | Supported list of sized double types.
floatTypes :: [Type]
floatTypes = TFp <$> floatSizes

-- | Supported list of all numeric types.
numTypes :: [Type]
numTypes = intTypes <> uintTypes <> floatTypes

-- | Check to see if a given 'Type' is a 'TInt'.
isIntTy
  :: Type -- ^ Some type to test.
  -> Bool -- ^ 'True' when a 'TInt', otherwise 'False'.
isIntTy (getTypePure -> (TInt _)) = True
isIntTy _                         = False

-- | Check to see if a given 'Type' is a 'TUInt'.
isUIntTy
  :: Type -- ^ Some type to test.
  -> Bool -- ^ 'True' when a 'TUInt', otherwise 'False'.
isUIntTy (getTypePure -> TUInt _) = True
isUIntTy _                        = False

-- | Check to see if a given 'Type' is a 'TFp'.
isFloatTy
  :: Type -- ^ Some type to test.
  -> Bool -- ^ 'True' when a 'TFp', otherwise 'False'.
isFloatTy (getTypePure -> TFp _) = True
isFloatTy _                      = False

-- | Check to see if a given 'Type' is one of the numeric constructors.
-- These are 'TInt', 'TUInt', and 'TFp'.
isNumType
  :: Type -- ^ Some type to test.
  -> Bool -- ^ 'True' when a numeric type, otherwise 'False'.
isNumType ty = isIntTy ty || isUIntTy ty || isFloatTy ty

-- | Check to see if a given 'Type' can be operated on with bitwise operates.
-- Supported types are numeric types and pointer types.
isBitType
  :: Type -- ^ Some type to test.
  -> Bool -- ^ 'True' when type supports bitwise operations, otherwise 'False'.
isBitType ty = isNumType ty || isPtrTy ty

-- | Check to see if a given 'Type' is a 'TBool'.
isBoolType
  :: Type -- ^ Some type to test.
  -> Bool -- ^ 'True' when a 'TBool', otherwise 'False'.
isBoolType (getTypePure -> TInt 1) = True
isBoolType _                       = False

-- | Check to see if a given 'Type' is a pointer type.
-- Supported pointer types are 'TArray' and 'TPtr'.
isPtrTy
  :: Type -- ^ Some type to test.
  -> Bool -- ^ 'True' when a pointer type, otherwise 'False'.
isPtrTy (getTypePure -> ty) =
  case ty of
    TArray _ _ -> True
    TPtr _     -> True
    _          -> False

-- | Unsafely try to get the type of an array.
-- This should be replaced with a traversal.
exTyArrElem
  :: Type -- ^ Some type to perform extraction on.
  -> Type -- ^ Results in the type of an array, or throws a fatal error.
exTyArrElem (getTypePure -> TArray _ ty) = ty
exTyArrElem _                            = error "expected array type"

-- | Unsafely try to get the type of a pointer (includes arrays).
-- This should be replaced with a traversal.
exTyPtrElem
  :: Type -- ^ Some type to perform extraction on.
  -> Type -- ^ Results in the type of a pointer, or throws a fatal error.
exTyPtrElem (getTypePure -> TArray _ ety) = ety
exTyPtrElem (getTypePure -> TPtr ety)     = ety
exTyPtrElem _                             = error "expected pointer type"

-- | Split a function type
-- Technically, this isn't required, but some old code needs it.
-- It should be removed, and is just leftovers from the revival.
splitType
  :: Type           -- ^ Some type to perform extraction on.
  -> ([Type], Type) -- ^ A pair containing the parameter and return types of the given type.
splitType (getTypePure -> TFunc paramtys retty) = (paramtys, retty)
splitType ty                                    = ([], ty)
