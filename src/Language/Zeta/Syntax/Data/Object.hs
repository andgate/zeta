module Language.Zeta.Syntax.Data.Object where

import           Document.Data.Span                       (Span)
import           Language.Zeta.Syntax.Data.Name.FieldName (FieldName)

-- | Object syntax, generalized over field name type and object values type.
-- This is so objects can be shared across expressions, patterns, and types.
data Object t =
  Object -- ^ Constructor for an object.
    (Maybe Span)    -- ^ Source location of the object.
    [ObjectField t] -- ^ List of fields in the objct.
  deriving(Show)

-- | Object field syntax, generalized over object value type.
-- This is so object fields can be shared across expressions, patterns, and types.
data ObjectField t =
  ObjectField -- ^ Constructor for an object field.
  { getObjectFieldLoc  :: Maybe Span -- ^ Object field source location.
  , getObjectFieldName :: FieldName  -- ^ Object field name.
  , getObjectFieldTerm :: t          -- ^ Object field value.
  } deriving(Show)

instance Functor Object where
  fmap f (Object l fls) =
    Object l (fmap f <$> fls)

instance Functor ObjectField where
  fmap f (ObjectField l fn t) =
    ObjectField l fn (f t)

instance Foldable Object where
  foldr f i (Object _ fls) =
    foldr (flip (foldr f)) i fls

instance Foldable ObjectField where
  foldr f i (ObjectField _ _ t) = f t i

instance Traversable Object where
  traverse f (Object l fls) =
    Object l <$> traverse (traverse f) fls

instance Traversable ObjectField where
  traverse f (ObjectField l fn t) =
    ObjectField l fn <$> f t
