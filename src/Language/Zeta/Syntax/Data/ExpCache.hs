module Language.Zeta.Syntax.Data.ExpCache where

-- | Expression cache, used to optimize traversals for expression abstraction and instantiation.
data ExpCache =
  ExpCache -- ^ The constructor for an expression cache, prefer 'newExpCache'.
  { -- | Get the has-free-varibles flag of a expression cache.
    getExpCacheHasFree  :: !Bool
    -- | Get the maxium bound of a expression cache, B, such that
    -- B = maxBinder(t) + 1
  , getExpCacheMaxBound :: !Int
  } deriving (Show)

-- | Construct a new expression cache.
newExpCache
  :: Bool      -- ^ Has free variables flag for expression cache.
  -> Int       -- ^ Maximum bound depth.
  -> ExpCache -- ^ A new expression cache.
newExpCache = ExpCache
{-# INLINE newExpCache #-}

-- | Construct an empty expression cache.
-- To obey monoid laws, an empty expression cache has free variables and a max bound of -1.
emptyExpCache
  :: ExpCache -- ^ An empty expression cache.
emptyExpCache = newExpCache True (-1)
{-# INLINE emptyExpCache #-}

-- | Set the has free variables flag of a expression cache.
setExpCacheHasFree
  :: ExpCache -- ^ Expression cache to update.
  -> Bool     -- ^ New has free variables flag.
  -> ExpCache -- ^ New expression cache, updated with the new flag.
setExpCacheHasFree tc p = tc { getExpCacheHasFree = p }
{-# INLINE setExpCacheHasFree #-}

-- | Set the maximum bound depth of a expression cache.
setExpCacheMaxBound
  :: ExpCache -- ^ Expression cache to update.
  -> Int      -- ^ New maximum bound depth.
  -> ExpCache -- ^ New expression cache, update with the new maximum bound depth.
setExpCacheMaxBound tc n = tc { getExpCacheMaxBound = n }
{-# INLINE setExpCacheMaxBound #-}

-- | Test to see if a given cache has a max binder, \(b\), which is within the given bounds
-- \(b_min\) and \(b_max\), such that \(b \in [b_min, b_max)\).
-- Notice the minimum is an inclusive bound, while the maximum is an exclusive bound.
isInExpCacheBounds
  :: ExpCache -- ^ Some expression cache that may contain a binder maximum, \(b\).
  -> Int      -- ^ Inclusive minimum bound we're searching for, \(b_min\).
  -> Int      -- ^ Exclusive maximum bound we're searching for, \(b_max\).
  -> Bool     -- ^ True if the given expression cache max binder is within the given range, such that \(b \in [b_min, b_max)\).
isInExpCacheBounds ec minB maxB =
  minB <= b && b < maxB
  where
    b = getExpCacheMaxBound ec
{-# INLINE isInExpCacheBounds #-}

instance Semigroup ExpCache where
  (<>) c1 c2 = newExpCache hasFree3 maxBound3
    where hasFree1 = getExpCacheHasFree c1
          {-# INLINE hasFree1 #-}
          hasFree2 = getExpCacheHasFree c2
          {-# INLINE hasFree2 #-}
          hasFree3 = hasFree1 || hasFree2
          {-# INLINE hasFree3 #-}
          maxBound1 = getExpCacheMaxBound c1
          {-# INLINE maxBound1 #-}
          maxBound2 = getExpCacheMaxBound c2
          {-# INLINE maxBound2 #-}
          maxBound3 = max maxBound1 maxBound2
          {-# INLINE maxBound3 #-}
  {-# INLINE (<>) #-}

instance Monoid ExpCache where
  mempty = emptyExpCache
  {-# INLINE mempty #-}

