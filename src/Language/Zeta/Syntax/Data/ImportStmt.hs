module Language.Zeta.Syntax.Data.ImportStmt where

import           Document.Data.Span                        (Span)
import           Language.Zeta.Syntax.Data.Name.ImportName (ImportName)

-- | Syntax for import statements.
data ImportStmt =
  ImportStmt -- ^ Constructor for import statements.
  { -- | Get the name the import specified by an import statement.
    getImportStmtName :: ImportName,
    -- | Get the source location of an import statement.
    getImportStmtLoc  :: Maybe Span
  } deriving (Show)

-- | Set the name of an import statement.
setImportStmtName
  :: ImportStmt -- ^ A import statement to update.
  -> ImportName -- ^ A new name.
  -> ImportStmt -- ^ A new import statement, updated with the new name.
setImportStmtName i n = i { getImportStmtName = n }

-- | Set the source location of an import statement.
setImportStmtLoc
  :: ImportStmt -- ^ An import statement to update.
  -> Span       -- ^ A new source location.
  -> ImportStmt -- ^ A new import statement, updated with the new source location.
setImportStmtLoc i l = i { getImportStmtLoc = Just l }
