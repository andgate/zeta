module Language.Zeta.Syntax.Data.TypeDefn where

import           Document.Data.Span                             (Span)
import           Language.Zeta.Syntax.Data.Exp                  (Exp)
import           Language.Zeta.Syntax.Data.FuncDefn             (FuncDefn)
import           Language.Zeta.Syntax.Data.Name.ConstructorName (ConstructorName)
import           Language.Zeta.Syntax.Data.Name.TypeName        (TypeName)
import           Language.Zeta.Syntax.Data.Pat                  (Pat)
import           Language.Zeta.Syntax.Data.TVar                 (TVar)
import           Language.Zeta.Syntax.Data.Type                 (Type)
import           Language.Zeta.Syntax.Data.TypeContext          (TypeContext)

-- | Type definition syntax.
data TypeDefn =
  TypeDefn -- ^ Type definition constructor.
    Span                -- ^ Source location of a type definition.
    (Maybe TypeContext) -- ^ Type context of a type definition.
    TypeName            -- ^ Name of type defined by a type definition.
    [TVar]              -- ^ Type variable parameters of a type definition.
    TypeDefnBlock       -- ^ Block of statements which make-up the body of a type definition.
  deriving(Show)

-- | Type definition block syntax.
-- This is the body of a type definition.
newtype TypeDefnBlock =
  TypeDefnBlock    -- ^ Type definition block constructor.
    [TypeDefnStmt] -- ^ List of type definition body statements.
  deriving(Show)

-- | Type definition statement syntax.
data TypeDefnStmt
  = TypeDefnConstr    -- ^ Constructor definition.
      Span            -- ^ Source location of constructor definition.
      ConstructorName -- ^ Name of constructor.
      [Type]          -- ^ Type arguments of constructor.
  | TypeDefnProp      -- ^ Property definition.
      Span            -- ^ Source location of property definition.
      Pat             -- ^ Pattern that binds the property values to the type.
      Exp             -- ^ Some expression which defines the properties initial values.
  | TypeDefnMethod    -- ^ Method definition.
      FuncDefn        -- ^ Some function definition to define as a member of this type.
  deriving (Show)
