{-# LANGUAGE LambdaCase   #-}
{-# LANGUAGE ViewPatterns #-}
module Language.Zeta.Syntax.Data.ExpRank where

import           Language.Zeta.Syntax.Data.Exp (Exp (..))

-- | Expression syntax term binding rank judement.
-- Ordered from A to C, with A being the lowest rank, and C being the highest.
data ExpRank
  = AExp -- ^ Rank A expression judment.
  | BExp -- ^ Rank B expression judment.
  | CExp -- ^ Rank C expression judment.

-- | Judge the rank of some expression.
expRank
  :: Exp      -- ^ Expression to judge.
  -> ExpRank  -- ^ Judgement of the expressions rank.
expRank = \case
  EVar {}        -> AExp
  ELit {}        -> AExp
  ECall {}       -> BExp
  ELam {}        -> CExp

  EType {}       -> CExp
  ECast {}       -> CExp

  ECache e _     -> expRank e
  ETypeCache e _ -> expRank e
  ELoc e _       -> expRank e
  EParens _      -> AExp

  ERef _         -> AExp
  EDeref _       -> AExp

  ETuple {}      -> AExp
  ECon {}        -> AExp
  EArray {}      -> AExp
  EObj {}        -> AExp

  EFree {}       -> BExp

  EField _       -> AExp
  EGet e _       -> expRank e
  EGetI e _      -> expRank e
  ESet _ _       -> BExp

  EUnOp {}       -> AExp
  EBinOp {}      -> AExp
  EBlock {}      -> AExp
  ESeq {}        -> CExp
  EBind {}       -> CExp
  EIf {}         -> CExp
  ECase {}       -> CExp
  EFor {}        -> CExp
  EWhile {}      -> CExp
  EDoWhile {}    -> CExp
  EReturn {}     -> BExp
  EBreak         -> AExp
  EContinue      -> AExp

-- | Test if a given expression is 'AExp'.
isAExp
  :: Exp  -- ^ Expression to test.
  -> Bool -- ^ 'True' if expression is 'BExp', otherwise 'False'.
isAExp (expRank -> AExp) = True
isAExp _                 = False

-- | Test if a given expression is 'BExp'.
isBExp
  :: Exp  -- ^ Expression to test.
  -> Bool -- ^ 'True' if expression is 'BExp', otherwise 'False'.
isBExp (expRank -> BExp) = True
isBExp _                 = False

-- | Test if a given expression is 'CExp'.
isCExp
  :: Exp  -- ^ Expression to test.
  -> Bool -- ^ 'True' if expression is 'CExp', otherwise 'False'.
isCExp (expRank -> CExp) = True
isCExp _                 = False
