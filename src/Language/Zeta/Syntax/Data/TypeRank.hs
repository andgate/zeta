{-# LANGUAGE LambdaCase   #-}
{-# LANGUAGE ViewPatterns #-}
module Language.Zeta.Syntax.Data.TypeRank where

import           Language.Zeta.Syntax.Data.Type (Type (..))

-- | Rank of a type
data TypeRank
  = AType -- ^ Rank-a types.
  | BType -- ^ Rank-b types.
  | CType -- ^ Rank-c types.

-- | Get the rank of a type.
typeRank
  :: Type      -- ^ Some type to rank.
  -> TypeRank  -- ^ Rank of the type
typeRank = \case
  TVar _     -> AType
  TApp _ _   -> BType
  TCon _     -> AType
  TInt  _    -> AType
  TUInt _    -> AType
  TFp   _    -> AType
  TTuple _ _ -> AType
  TArray _ _ -> AType
  TPtr _     -> AType
  TFunc _ _  -> CType
  TCtx _ _   -> CType
  TCache t _ -> typeRank t
  TLoc t _   -> typeRank t
  TParens _  -> AType

-- | Check if a type is rank-a.
isAType
  :: Type -- ^ Some type to test.
  -> Bool -- ^ True if type rank is 'AType'.
isAType (typeRank -> AType) = True
isAType _                   = False

-- | Check if a type is rank-b.
isBType
  :: Type -- ^ Some type to test.
  -> Bool -- ^ True if type rank is 'BType'.
isBType (typeRank -> BType) = True
isBType _                   = False

-- | Check if a type is rank-c.
isCType
  :: Type -- ^ Some type to test.
  -> Bool -- ^ True if type rank is 'CType'.
isCType (typeRank -> CType) = True
isCType _                   = False
