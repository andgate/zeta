module Language.Zeta.Syntax.Data.Var where

import           Data.Text                      (Text)
import           Document.Data.Span             (Span)
import           Language.Zeta.Syntax.Data.Type (Type)

-- | Expression variable syntax.
data Var =
  V -- ^ Variable constructor, prefer 'newFreeVar'.
  { -- | Get the 'Text' representation of a variable.
    getVarStr    :: Text
    -- | Get the binder of a variable, if there is one.
  , getVarBinder :: Maybe Int
    -- | Get the type of a variable.
  , getVarType   :: Maybe Type
    -- | Get the source location of a variable.
  , getVarLoc    :: Maybe Span
  }

-- | Construct a new, free variable.
newFreeVar
  :: Maybe Span -- ^ A source location.
  -> Text       -- ^ A text identifier.
  -> Var        -- ^ A new variable.
newFreeVar l txt =
  V {
    getVarStr    = txt,
    getVarBinder = Nothing,
    getVarType   = Nothing,
    getVarLoc    = l
  }
{-# INLINE newFreeVar #-}

-- | Given a list ordered list of vars,
-- apply a new set of binders to the vars.
-- The binder identifiers are generated from [0..(n-1)].
toBoundVars :: [Var] -> [Var]
toBoundVars = zipWith (flip setVarBinder') [0..]
{-# INLINE toBoundVars #-}

-- | Set the text identifier of a variable.
setVarStr
  :: Var  -- ^ A variable to update.
  -> Text -- ^ New text identifier.
  -> Var  -- ^ New variable, updated with new identifier.
setVarStr v s = v { getVarStr = s }
{-# INLINE setVarStr #-}

-- | Set the binder of a variable.
setVarBinder
  :: Var       -- ^ A variable to update.
  -> Maybe Int -- ^ New binder, possibly Nothing.
  -> Var       -- ^ New variable, updated with new binder.
setVarBinder v b = v { getVarBinder = b }
{-# INLINE setVarBinder #-}

-- | Set the binder of a variable.
-- This is a weakened version of the setter, where the binder is non-optional.
setVarBinder'
  :: Var       -- ^ A variable to update.
  -> Int       -- ^ New binder id.
  -> Var       -- ^ New variable, updated with new binder.
setVarBinder' v b = v { getVarBinder = Just b }
{-# INLINE setVarBinder' #-}

-- | Set the type of a variable.
setVarType
  :: Var  -- ^ A variable to update.
  -> Maybe Type -- ^ New type annotation.
  -> Var  -- ^ New variable, updated with the new type annotation.
setVarType v t = v { getVarType = t }
{-# INLINE setVarType #-}

-- | Set the source location of a variable.
setVarLoc
  :: Var        -- ^ A variable to update.
  -> Maybe Span -- ^ New source location.
  -> Var        -- ^ New variable, updated with the new source location.
setVarLoc v l = v { getVarLoc = l }
{-# INLINE setVarLoc #-}

instance Eq Var where
  (==) v u = getVarStr v == getVarStr u
  {-# INLINE (==) #-}

instance Show Var where
  show (V n _ _ _) = show n
