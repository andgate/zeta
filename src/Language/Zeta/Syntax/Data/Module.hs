module Language.Zeta.Syntax.Data.Module where

import           Document.Data.Location                 (Loc (..))
import           Document.Data.Span                     (Span)
import           Language.Zeta.Syntax.Data.ModuleHeader (ModuleHeader)
import           Language.Zeta.Syntax.Data.ModuleStmt   (ModuleStmt)

-- | Module syntax.
data Module =
  Module -- ^ Module constructor, prefer 'newModule'.
  { -- | Get the source location of a module.
    getModuleLoc      :: Maybe Span,
    -- | Get the source filepath of a module.
    getModuleFilePath :: Maybe FilePath,
    -- | Get the header of a module.
    getModuleHeader   :: ModuleHeader,
    -- | Get the list of statements in a module.
    getModuleStmtList :: [ModuleStmt]
  }

-- | Construct a new module.
newModule
  :: Maybe Span   -- ^ Module's source location..
  -> ModuleHeader -- ^ Module's header.
  -> [ModuleStmt] -- ^ Module's statement list.
  -> Module       -- ^ Results in a new module.
newModule l h stmts = Module
  { getModuleLoc = l
  , getModuleFilePath = Nothing
  , getModuleHeader = h
  , getModuleStmtList = stmts
  }

-- | Set the source location of a module.
setModuleLoc
  :: Module     -- ^ Module to update.
  -> Maybe Span -- ^ New source location.
  -> Module     -- ^ New module, updated with source location.
setModuleLoc m l = m { getModuleLoc = l }

setModuleHeader
  :: Module       -- ^ Module to update.
  -> ModuleHeader -- ^ New module header.
  -> Module       -- ^ New module, update with a new header.
setModuleHeader m h = m { getModuleHeader = h }

setModuleStmtList
  :: Module       -- ^ Module to update.
  -> [ModuleStmt] -- ^ New module statement list.
  -> Module       -- ^ New module, update with a new module statement list.
setModuleStmtList m ms = m { getModuleStmtList = ms }

getModuleLocation :: Module -> Maybe Loc
getModuleLocation m =
  L <$> getModuleFilePath m <*> getModuleLoc m
