{-|
Module      : Language.Zeta.Syntax.TypeContext
Description : Zeta type context syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.TypeContext (module X) where

import           Language.Zeta.Syntax.Data.TypeContext as X
