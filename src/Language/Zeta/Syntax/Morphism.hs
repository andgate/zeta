{-|
Module      : Language.Zeta.Syntax.Morphism
Description : Zeta type class morphism syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Morphism (module X) where

import           Language.Zeta.Syntax.Data.MorphismDefn as X
