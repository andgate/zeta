{-|
Module      : Language.Zeta.Syntax.Optics
Description : Zeta syntax optics
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Optics (
    -- * Expression Syntax Optics
    module ExpOptics,

    -- * Pattern Syntax Optics
    module PatOptics,

    -- * Type Syntax Optics
    module TypeOptics,

    -- * Clause Syntax Optics
    module ClauseOptics,

    -- * Scope Optics
    module ScopeOptics
  ) where

import           Language.Zeta.Syntax.Optics.Annotation.ExpAnn  as ExpOptics
import           Language.Zeta.Syntax.Optics.Annotation.TypeAnn as TypeOptics
import           Language.Zeta.Syntax.Optics.Branch             as ExpOptics
import           Language.Zeta.Syntax.Optics.Clause             as ClauseOptics
import           Language.Zeta.Syntax.Optics.Exp                as ExpOptics
import           Language.Zeta.Syntax.Optics.ExpCache           as ScopeOptics
import           Language.Zeta.Syntax.Optics.Pat                as PatOptics
import           Language.Zeta.Syntax.Optics.Scope              as ScopeOptics
import           Language.Zeta.Syntax.Optics.TVar               as TypeOptics
import           Language.Zeta.Syntax.Optics.Type               as TypeOptics
import           Language.Zeta.Syntax.Optics.TypeCache          as ScopeOptics
import           Language.Zeta.Syntax.Optics.Var                as ExpOptics
