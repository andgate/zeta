{-|
Module      : Language.Zeta.Syntax.Type
Description : Zeta type syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Type (module X) where

import           Language.Zeta.Syntax.Data.Annotation.TypeAnn as X
import           Language.Zeta.Syntax.Data.Name.TypeName      as X
import           Language.Zeta.Syntax.Data.TVar               as X
import           Language.Zeta.Syntax.Data.Type               as X
import           Language.Zeta.Syntax.Data.TypeRank           as X
