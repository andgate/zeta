{-|
Module      : Language.Zeta.Syntax.Module
Description : Zeta module syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Module (module X) where

import           Language.Zeta.Syntax.Data.Module          as X
import           Language.Zeta.Syntax.Data.ModuleHeader    as X
import           Language.Zeta.Syntax.Data.ModuleStmt      as X
import           Language.Zeta.Syntax.Data.Name.ModuleName as X
