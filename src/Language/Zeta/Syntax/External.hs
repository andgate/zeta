{-|
Module      : Language.Zeta.Syntax.External
Description : Zeta external definition syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.External (module X) where

import           Language.Zeta.Syntax.Data.ExternDefn as X
