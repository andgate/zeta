{-|
Module      : Language.Zeta.Syntax.Object
Description : Zeta object syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Object (module X) where

import           Language.Zeta.Syntax.Data.Object as X
