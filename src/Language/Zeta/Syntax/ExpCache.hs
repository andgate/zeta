{-|
Module      : Language.Zeta.Syntax.ExpCache
Description : Zeta expression cache for optimizing traversals.
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.ExpCache (module X) where

import           Language.Zeta.Syntax.Data.ExpCache as X
