{-|
Module      : Language.Zeta.Syntax.Import
Description : Zeta import syntax
Copyright   : (c) Gabriel Anderson, 2021
License     : MIT
Maintainer  : gabe4k@email.com
Stability   : experimental
Portability : POSIX
-}
module Language.Zeta.Syntax.Import (module X) where

import           Language.Zeta.Syntax.Data.ImportStmt      as X
import           Language.Zeta.Syntax.Data.Name.ImportName as X
