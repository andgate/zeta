module Language.Zeta.Compiler.NameMap where

import           Data.Map.Strict      (Map)
import           Language.Zeta.Syntax (ConstructorName, FuncDefn, FunctionName,
                                       Type)

data NameMap = NameMap
  { getFunctionNameMap    :: Map FunctionName FuncDefn
  , getConstructorTypeMap :: Map ConstructorName Type
  }
