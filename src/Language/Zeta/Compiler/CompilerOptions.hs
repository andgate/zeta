module Language.Zeta.Compiler.CompilerOptions where

data CompilerOptions = CompilerOptions
  { optSourceDirectory :: FilePath
  , optInputs          :: [FilePath]
  , optOutput          :: FilePath
  , optBuildDir        :: FilePath
  } deriving (Show)

newCompilerOptions :: FilePath -> [FilePath] -> FilePath -> FilePath -> CompilerOptions
newCompilerOptions srcdir inputs output buildDir =
  CompilerOptions
  { optSourceDirectory = srcdir
  , optInputs = inputs
  , optOutput = output
  , optBuildDir = buildDir
  }
{-# INLINE newCompilerOptions #-}

defaultCompilerOptions :: CompilerOptions
defaultCompilerOptions =
  newCompilerOptions "./" ["main.z"] "a.out" "./"
{-# INLINE defaultCompilerOptions #-}
