module Language.Zeta.Compiler.State where

import           Language.Zeta.Syntax (Module)

data CompilerState = CompilerState
  { getCompilerSrcFiles :: [FilePath]
  , getCompilerModule   :: [Module]
  }
