{-# LANGUAGE FlexibleInstances #-}
module Language.Zeta.Compiler.CompilerPlugin where

import           Prelude                      hiding (id, (.))

import           Control.Category             (Category (..))
import           Data.Text                    (Text)
import           Language.Zeta.Compiler.Type
import           Language.Zeta.Document.Token (Token)
import           Language.Zeta.Syntax         (Module)

newtype CompilerPlugin m a = CompilerPlugin { runCompilerPlugin :: Compiler m a a }

type PreLexPlugin m    = CompilerPlugin m Text
type PostLexPlugin m   = CompilerPlugin m [Token]
type PreParsePlugin m  = CompilerPlugin m [Token]
type PostParsePlugin m = CompilerPlugin m Module
type PreGenPlugin m    = CompilerPlugin m Module
type PostGenPlugin m   = CompilerPlugin m Text

data CompilerPlugins m = CompilerPlugins
  { preLex    :: [PreLexPlugin m]
  , postLex   :: [PostLexPlugin m]
  , preParse  :: [PreParsePlugin m]
  , postParse :: [PostParsePlugin m]
  , preGen    :: [PreGenPlugin m]
  , postGen   :: [PostGenPlugin m]
  }

instance Monad m => Semigroup (CompilerPlugin m a) where
  (<>) (CompilerPlugin c1) (CompilerPlugin c2) = CompilerPlugin (c2 . c1)

instance Monad m => Monoid (CompilerPlugin m a) where
   mempty = CompilerPlugin id

-- instance Monad m => Functor (CompilerPlugin m) where
--   fmap f (CompilerPlugin ca) =
--     CompilerPlugin (f <$> ca)

-- instance Monad m => Applicative (CompilerPlugin m) where
--   pure = CompilerPlugin . pure
--   (<*>) (CompilerPlugin ma) cf =
--     Compiler (\a -> ma a >>= (bmf <*> ma))

-- instance Monad m => Monad (CompilerPlugin m) where
--   return = id
