{-# LANGUAGE FlexibleContexts #-}
module Language.Zeta.Compiler.Core.CodeGen where

import           Data.Text                          (Text)
import           Language.Zeta.Compiler.Monad.Class (MonadCompiler)
import           Language.Zeta.Compiler.Type        (Compiler)
import           Language.Zeta.Syntax               (Module)

type CodeGen m = Compiler m Module Text

codegen :: MonadCompiler m => CodeGen m
codegen = undefined
