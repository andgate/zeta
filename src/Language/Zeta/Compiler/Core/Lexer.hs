{-# LANGUAGE FlexibleContexts #-}
module Language.Zeta.Compiler.Core.Lexer where

import           Data.Text                          (Text)
import           Language.Zeta.Compiler.Monad.Class (MonadCompiler)
import           Language.Zeta.Compiler.Type        (Compiler)
import           Language.Zeta.Document.Token       (Token)

type Lexer m = Compiler m Text [Token]

lexer :: MonadCompiler m => Lexer m
lexer = undefined
