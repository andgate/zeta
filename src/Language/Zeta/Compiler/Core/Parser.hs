{-# LANGUAGE FlexibleContexts #-}
module Language.Zeta.Compiler.Core.Parser where

import           Language.Zeta.Compiler.Monad.Class (MonadCompiler)
import           Language.Zeta.Compiler.Type        (Compiler)
import           Language.Zeta.Document.Token       (Token)
import           Language.Zeta.Syntax               (Module)

type Parser m = Compiler m [Token] Module

parser :: MonadCompiler m => Parser m
parser = undefined
