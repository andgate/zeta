{-# LANGUAGE ConstraintKinds  #-}
{-# LANGUAGE FlexibleContexts #-}
module Language.Zeta.Compiler.Monad.Class where

import           Control.Monad.IO.Class       (MonadIO)
import           Control.Monad.State.Strict   (MonadState)
import           Language.Zeta.Compiler.State (CompilerState)

type MonadCompiler m = (MonadIO m, MonadState CompilerState m)
