{-# LANGUAGE RankNTypes #-}
module Language.Zeta.Compiler.Type where

import           Prelude          hiding (id, (.))

import           Control.Category (Category (..))
import           Control.Monad    ((>=>))
import           Data.Profunctor  (Profunctor (..))

newtype Compiler m a b = Compiler { runCompiler :: a -> m b }

instance Monad m => Category (Compiler m) where
  id = Compiler return
  (.) (Compiler g) (Compiler f) = Compiler (f >=> g)

instance Monad m => Functor (Compiler m a) where
  fmap f (Compiler ca) = Compiler (fmap f <$> ca)

instance Monad m => Profunctor (Compiler m) where
  dimap f g (Compiler ca) = Compiler (dimap f (fmap g) ca)
  lmap f (Compiler ca)    = Compiler (ca . f)
  rmap g (Compiler ca)    = Compiler (fmap g . ca)

instance Monad m => Applicative (Compiler m a) where
  pure b = Compiler (\_a -> return b)
  (<*>) (Compiler cf) (Compiler ca) =
    Compiler $ \x -> do
      f <- cf x
      a <- ca x
      return $ f a

instance forall m i. (Monad m) => Monad (Compiler m i) where
  return = pure
  (>>=) (Compiler ca) cf =
    Compiler $ \i -> do
      a <- ca i
      runCompiler (cf a) i
