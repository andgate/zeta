module Language.Zeta.Compiler.NameSet where

import           Control.Lens.Lens      (Lens', lens)
import           Control.Lens.Operators ((^.))
import           Data.Set               (Set)
import qualified Data.Set               as Set
import           Language.Zeta.Syntax   (ClassName, ConstructorName, FieldName,
                                         FunctionName, MethodName, ModuleName,
                                         PatternName, TypeName, Var,
                                         toConstructorName, toFieldName)

data NameSet =
  NameSet
  { getNameSetClassNames       :: Set ClassName
  , getNameSetConstructorNames :: Set ConstructorName
  , getNameSetFieldNames       :: Set FieldName
  , getNameSetFunctionNames    :: Set FunctionName
  , getNameSetMethodNames      :: Set MethodName
  , getNameSetModuleNames      :: Set ModuleName
  , getNameSetPatternNames     :: Set PatternName
  , getNameSetTypeNames        :: Set TypeName
  }

emptyNameSet :: NameSet
emptyNameSet =
  NameSet
  { getNameSetClassNames       = mempty
  , getNameSetConstructorNames = mempty
  , getNameSetFieldNames       = mempty
  , getNameSetFunctionNames    = mempty
  , getNameSetMethodNames      = mempty
  , getNameSetModuleNames      = mempty
  , getNameSetPatternNames     = mempty
  , getNameSetTypeNames        = mempty
  }

setNameSetClassNames :: NameSet -> Set ClassName -> NameSet
setNameSetClassNames ns s = ns { getNameSetClassNames = s }

setNameSetConstructorNames :: NameSet -> Set ConstructorName -> NameSet
setNameSetConstructorNames ns s = ns { getNameSetConstructorNames = s }

setNameSetFieldNames :: NameSet -> Set FieldName -> NameSet
setNameSetFieldNames ns s = ns { getNameSetFieldNames = s }

setNameSetFunctionNames :: NameSet -> Set FunctionName -> NameSet
setNameSetFunctionNames ns s = ns { getNameSetFunctionNames = s }

setNameSetMethodNames :: NameSet -> Set MethodName -> NameSet
setNameSetMethodNames ns s = ns { getNameSetMethodNames = s }

setNameSetModuleNames :: NameSet -> Set ModuleName -> NameSet
setNameSetModuleNames ns s = ns { getNameSetModuleNames = s }

setNameSetPatternNames :: NameSet -> Set PatternName -> NameSet
setNameSetPatternNames ns s = ns { getNameSetPatternNames = s }

setNameSetTypeNames :: NameSet -> Set TypeName -> NameSet
setNameSetTypeNames ns s = ns { getNameSetTypeNames = s }

nameSetClassNames :: Lens' NameSet (Set ClassName)
nameSetClassNames = lens getNameSetClassNames setNameSetClassNames

nameSetConstructorNames :: Lens' NameSet (Set ConstructorName)
nameSetConstructorNames = lens getNameSetConstructorNames setNameSetConstructorNames

nameSetFieldNames :: Lens' NameSet (Set FieldName)
nameSetFieldNames = lens getNameSetFieldNames setNameSetFieldNames

nameSetFunctionNames :: Lens' NameSet (Set FunctionName)
nameSetFunctionNames = lens getNameSetFunctionNames setNameSetFunctionNames

nameSetMethodNames :: Lens' NameSet (Set MethodName)
nameSetMethodNames = lens getNameSetMethodNames setNameSetMethodNames

nameSetModuleNames :: Lens' NameSet (Set ModuleName)
nameSetModuleNames = lens getNameSetModuleNames setNameSetModuleNames

nameSetPatternNames :: Lens' NameSet (Set PatternName)
nameSetPatternNames = lens getNameSetPatternNames setNameSetPatternNames

nameSetTypeNames :: Lens' NameSet (Set TypeName)
nameSetTypeNames = lens getNameSetTypeNames setNameSetTypeNames

data VarName
  = VarConstructorName ConstructorName
  | VarFieldName FieldName

lookupVar :: Var -> NameSet -> Maybe VarName
lookupVar v ns
  | p1        = Just $ VarConstructorName cn
  | p2        = Just $ VarFieldName fn
  | otherwise = Nothing
  where
    cn = v ^. toConstructorName
    fn = v ^. toFieldName
    p1 = Set.member cn (ns ^. nameSetConstructorNames)
    p2 = Set.member fn (ns ^. nameSetFieldNames)

instance Semigroup NameSet where
  (<>) ns1 ns2 =
    NameSet
    { getNameSetClassNames       = getNameSetClassNames ns1 <> getNameSetClassNames ns2
    , getNameSetConstructorNames = getNameSetConstructorNames ns1 <> getNameSetConstructorNames ns2
    , getNameSetFieldNames       = getNameSetFieldNames ns1 <> getNameSetFieldNames ns2
    , getNameSetFunctionNames    = getNameSetFunctionNames ns1 <> getNameSetFunctionNames ns2
    , getNameSetMethodNames      = getNameSetMethodNames ns1 <> getNameSetMethodNames ns2
    , getNameSetModuleNames      = getNameSetModuleNames ns1 <> getNameSetModuleNames ns2
    , getNameSetPatternNames     = getNameSetPatternNames ns1 <> getNameSetPatternNames ns2
    , getNameSetTypeNames        = getNameSetTypeNames ns1 <> getNameSetTypeNames ns2
    }

instance Monoid NameSet where
  mempty = emptyNameSet
