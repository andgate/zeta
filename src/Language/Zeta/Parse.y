{

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
module Language.Zeta.Parse (
    parse
  , parseTopLevel
  , parseExp
  , parseExpUnsafe
  , parseType
  , parseTypeUnsafe
  ) where

import Prelude hiding (span)

import Control.Comonad
import Control.Lens.Getter (to)
import Control.Lens.Review (re, (#))
import Control.Lens.Operators ((&), (.~), (^.), (^?), (^?!))
import Data.Maybe (fromJust)
import Language.Zeta.Lex
import Language.Zeta.Document.Token
import Language.Zeta.Syntax
import Document.Classy.HasSpan
import Document.Data.Span
import Control.Monad.Span

import Data.List.NonEmpty (NonEmpty)
import qualified Data.List.NonEmpty as NE

import Data.DList (DList)
import qualified Data.DList as DL

import Data.Text (Text)
import Prettyprinter

}

%name parse zeta_module
%name parseTopLevel module_stmt
%name parseExp standalone_exp
%name parseType standalone_type
%tokentype { Token }
-- %lexer { lexNonSpace >>= } { Token TokenEof _ _ }
-- %monad { P } { >>= } { return }
%errorhandlertype explist
%error { expParseError }


%token
  '\\'               { Token (TokenRsvp "\\") $$ }
  '->'               { Token (TokenRsvp "->") $$ }
  '=>'               { Token (TokenRsvp "=>") $$ }
  '<-'               { Token (TokenRsvp "<-") $$ }
  ':'                { Token (TokenRsvp ":") $$ }
  ';'                { Token (TokenRsvp ";") $$ }
  ','                { Token (TokenRsvp ",") $$ }
  '.'                { Token (TokenRsvp ".") $$ }
  '='                { Token (TokenRsvp "=") $$ }
  '_'                { Token (TokenRsvp "_") $$ }
  '*'                { Token (TokenRsvp "*") $$ }
  '&'                { Token (TokenRsvp "&") $$ }
  '~'                { Token (TokenRsvp "~") $$ }
  '|'                { Token (TokenRsvp "|") $$ }

  '('                { Token (TokenRsvp "(") $$ }
  ')'                { Token (TokenRsvp ")") $$ }
  '['                { Token (TokenRsvp "[") $$ }
  ']'                { Token (TokenRsvp "]") $$ }
  '{'                { Token (TokenRsvp "{") $$ }
  '}'                { Token (TokenRsvp "}") $$ }
  '<'                { Token (TokenRsvp "<") $$ }
  '>'                { Token (TokenRsvp ">") $$ }

  'forall'           { Token (TokenRsvp "forall") $$ }

  'i1'               { Token (TokenRsvp  "i1" ) $$ }
  'i8'               { Token (TokenRsvp  "i8" ) $$ }
  'i16'              { Token (TokenRsvp "i16" ) $$ }
  'i32'              { Token (TokenRsvp "i32" ) $$ }
  'i64'              { Token (TokenRsvp "i64" ) $$ }

  'u8'               { Token (TokenRsvp  "u8" ) $$ }
  'u16'              { Token (TokenRsvp "u16" ) $$ }
  'u32'              { Token (TokenRsvp "u32" ) $$ }
  'u64'              { Token (TokenRsvp "u64" ) $$ }

  'f16'              { Token (TokenRsvp  "f16" ) $$ }
  'f32'              { Token (TokenRsvp  "f32" ) $$ }
  'f64'              { Token (TokenRsvp  "f64" ) $$ }
  'f128'             { Token (TokenRsvp "f128" ) $$ }

  'null'             { Token (TokenRsvp   "null" ) $$ }

  'module'           { Token (TokenRsvp "module") $$ }
  'import'           { Token (TokenRsvp "import") $$ }

  'extern'           { Token (TokenRsvp "extern" ) $$ }
  'data'             { Token (TokenRsvp "type" ) $$ }

  'return'           { Token (TokenRsvp "return"  ) $$ }
  'break'            { Token (TokenRsvp "break"   ) $$ }
  'continue'         { Token (TokenRsvp "continue") $$ }

  'as'               { Token (TokenRsvp "as"   ) $$ }
  'case'             { Token (TokenRsvp "case"   ) $$ }

  'if'               { Token (TokenRsvp "if"  ) $$ }
  'else'             { Token (TokenRsvp "else") $$ }
  'elif'             { Token (TokenRsvp "elif") $$ }

  'while'            { Token (TokenRsvp "while") $$ }
  'do'               { Token (TokenRsvp "do"   ) $$ }
  'for'              { Token (TokenRsvp "for"  ) $$ }

  'free'           { Token (TokenRsvp "delete") $$ }

  varId              { Token (TokenVarId  _) _ }

  '#add'             { Token (TokenPrimId "#add") $$ }
  '#sub'             { Token (TokenPrimId "#sub") $$ }
  '#mul'             { Token (TokenPrimId "#mul") $$ }
  '#div'             { Token (TokenPrimId "#div") $$ }
  '#rem'             { Token (TokenPrimId "#rem") $$ }
  '#neg'             { Token (TokenPrimId "#nef") $$ }

  '#and'             { Token (TokenPrimId "#and") $$ }
  '#or'              { Token (TokenPrimId  "#or") $$ }
  '#xor'             { Token (TokenPrimId "#xor") $$ }
  '#shr'             { Token (TokenPrimId "#shr") $$ }
  '#shl'             { Token (TokenPrimId "#shl") $$ }

  '#eq'              { Token (TokenPrimId "#eq") $$ }
  '#neq'             { Token (TokenPrimId "#neq") $$ }
  '#lt'              { Token (TokenPrimId "#lt") $$ }
  '#le'              { Token (TokenPrimId "#le") $$ }
  '#gt'              { Token (TokenPrimId "#gt") $$ }
  '#ge'              { Token (TokenPrimId "#le") $$ }

  integer            { Token (TokenInteger _) _ }
  double             { Token (TokenDouble  _) _ }
  char               { Token (TokenChar    _) _ }
  string             { Token (TokenString  _) _ }
  boolean            { Token (TokenBool    _) _ }

  close_line         { Token TokenLineClose _ }
  open_block         { Token TokenBlockOpen _ }
  close_block        { Token TokenBlockClose _ }

  EOF                { Token TokenEof _ }

%%

-- Associativity

-- -----------------------------------------------------------------------------
-- | Helpers

some(p) -- :: { [_] }
  : some_dl(p)     { DL.toList $1 }

some_ne(p) -- :: { NonEmpty _ }
  : some(p)        { NE.fromList $1 }

some_dl(p) -- :: { DList _ }
  : some_dl(p) p   { DL.snoc $1 $2 }
  | p              { DL.singleton $1 }

-- | Zero or more occurences of 'p'
many(p) -- :: { [_] }
  : some(p)     { $1 }
  | {- empty -} { [] }

opt(p) -- :: { Maybe _ }
  : p           { Just $1 }
  | {- empty -} { Nothing }

parens(p) -- :: { SpanM _ }
  : '(' p ')' { include ($1 <++> $3) >> return $2 }

tupled(p) -- :: { [_] }
  : '(' sep_by1_dl(p, ',') ')' { DL.toList $2 }
  | '(' ')'                    { [] }

tupled_ne(p) -- :: { NonEmpty _ }
  : tupled(p) { NE.fromList $1 }

sep_by0(p,sep) -- :: { [_] }
  : sep_by1_dl(p,sep) { DL.toList $1 }
  | {- empty -}       { [] }

sep_by1(p,sep) -- :: { [_] }
  : sep_by1_dl(p,sep) { DL.toList $1 }

sep_by1_ne(p,sep) -- :: { NonEmpty _ }
  : sep_by1(p,sep) { NE.fromList $1 }

sep_by1_dl(p,sep) -- :: { DList _ }
  : sep_by1_dl(p,sep) sep p  { DL.snoc $1 $3 }
  | p                        { DL.singleton $1 }

linefold(p) -- :: { _ }
  : p close_line { $1 }

block(p) -- :: { [_] }
  : open_block many(linefold(p)) close_block { $2 }

block1(p) -- :: { NonEmpty _ }
  : open_block some_ne(linefold(p)) close_block { $2 }

-- -----------------------------------------------------------------------------
-- | Identifiers, Names and Literals

var_id :: { SpanM Text }
  : varId { include $1 >> return ($1 ^?! tokenVarId) }

var :: { Var }
  : var_id { newFreeVar ($1 ^? span) (extract $1) }

tvar :: { TVar }
  : var_id { newFreeTVar ($1 ^? span) (extract $1) }

con_name :: { ConstructorName }
  : var_id { newConstructorName ($1 ^? span) (extract $1) }

class_name :: { ClassName }
  : var_id { newClassName ($1 ^? span) (extract $1) }

field_name :: { FieldName }
  : var_id { newFieldName ($1 ^? span) (extract $1) }

func_name :: { FunctionName }
  : var_id { newFunctionName ($1 ^? span) (extract $1) }

import_name :: { ImportName }
  : var_id { newImportName ($1 ^? span) [] (extract $1) }

method_name :: { MethodName }
  : var_id { newMethodName ($1 ^? span) (extract $1) }

module_name :: { ModuleName }
  : var_id { newModuleName ($1 ^? span) [] (extract $1) }

pattern_name :: { PatternName }
  : var_id { newPatternName ($1 ^? span) (extract $1) }

type_name :: { TypeName }
  : var_id { newTypeName ($1 ^? span) (extract $1) }

literal :: { SpanM Lit }
  : 'null'   { include $1 >> return LNull }
  | integer  { include $1 >> (return . LInt . fromInteger) ($1 ^?! tokenInteger) }
  | double   { include $1 >> (return . LDouble) ($1 ^?! tokenDouble) }
  | char     { include $1 >> (return . LChar) ($1 ^?! tokenChar) }
  | boolean  { include $1 >> (return . LBool) ($1 ^?! tokenBool) }
  | string   { include $1 >> (return . LString) ($1 ^?! tokenString) }


-- -----------------------------------------------------------------------------
-- | Module

zeta_module :: { Module }
  : linefold(module_header) many(linefold(module_stmt)) mayeof { newModule ($1 <++> $2) $1 $2 }

module_header :: { ModuleHeader }
  : 'module' module_name { newModuleHeader ($1 <++> $2) $2 }

mayeof :: { Maybe Token }
  : opt(EOF) { $1 }

-- -----------------------------------------------------------------------------
-- | Top-Level Definition

module_stmt :: { ModuleStmt }
  : func_defn     { ModuleFuncDefn $1 }
  | extern_defn   { ModuleExternDefn $1 }


-- -----------------------------------------------------------------------------
-- | Function

func_defn :: { FuncDefn }
  : type_context_opt func_name arg_list0 type_sig_opt func_defn_body { newFuncDefn ($1 <++> $2 <++> $5) $1 $2 $3 $4 $5 }

arg_list0 :: { [Pat] }
  : pat_list0 { $1 }

func_defn_body :: { FuncDefnBody }
  : '{' exp '}' { FuncDefnBodyBlock ($1 <++> $3) $2 }
  | '=' exp     { FuncDefnBodyExp ($1 <++> $2) $2 }

-- -----------------------------------------------------------------------------
-- | External Definitions

extern_defn :: { ExternDefn }
  : 'extern' func_name '(' sep_by1(type, ',') ')' ':' type { newExternDefn ($1 <++> $7) $2 $4 $7 }

-- -----------------------------------------------------------------------------
-- | Patterns

pat_list0 :: { [Pat] }
  : tupled(pat) { $1 }

pat :: { Pat }
  : cpat { $1 }

cpat :: { Pat }
  : bpat ':' type { _PAnn # (PType $1 $3, $1 <++> $3) }
  | bpat          { $1 }

bpat :: { Pat }
  : con_name apats { _PAnn # (PCon $1 $2, $1 <++> $2) }
  | apat           { $1 }

apat :: { Pat }
  : pattern_name  { _PAnn # (PVar $1, $1 ^? span)  }
  | '_'           { _PAnn # (PWild, $1 ^? span) }
  | '(' pat ')'   { _PAnn # (PParens $2, $1 <++> $3) }
  | '(' pat ',' some_ne(pat) ')' { _PAnn # (PTuple $2 $4, $1 <++> $5) }

apats0 :: { [Pat] }
  : {- Empty -} { [] }
  | apats       { $1 }

apats :: { [Pat] }
  : apats_r      { reverse $1 }

apats_r :: { [Pat] }
  : apat { [$1] }
  | apats_r apat { $2:$1 }

-- -----------------------------------------------------------------------------
-- | Clauses

clauses :: { NonEmpty Clause }
  : block1(clause) { $1 }

clause :: { Clause }
  : pat '->' exp { _Clause # ($1 <++> $3, NE.fromList [$1], $3) }

-- -----------------------------------------------------------------------------
-- | Expressions

standalone_exp :: { Exp }
  : exp mayeof { $1 }

exp :: { Exp }
  : dexp { $1 }

dexp :: { Exp }
  : cexp ':' type  { _EAnn # (EType $1 $3, $1 <++> $3) }
  | cexp 'as' type { _EAnn # (ECast $1 $3, $1 <++> $3) }
  | cexp { $1 }

cexp :: { Exp }
  : arg_list0 type_sig_opt '->' exp  { _EAnn # (_ELam # ($1, $2, $4), $1 <++> $4) }
  | bexp '=' bexp                    { _EAnn # (ESet $1 $3, $1 <++> $3) }
  | bexp ';' cexp                    { _EAnn # (ESeq $1 $3, $1 <++> $3) }
  | 'return' exp ';'                 { _EAnn # (EReturn $2, $1 <++> $3)}
--  | pat '<-' exp ';' exp             { _EAnn # (_EBind $1 $3 $5, $1 <++> $5) }
  | 'while' '(' exp ')' exp          { _EAnn # (EWhile $3 $5, $1 <++> $5) }
  | 'do' exp 'while' '(' exp ')'     { _EAnn # (EDoWhile $2 $5, $1 <++> $6) }
  | 'for' '(' exp ';' exp ';' exp ')' exp
                                     { _EAnn # (EFor $3 $5 $7 $9, $1 <++> $9) }
  | bexp                             { $1 }

bexp :: { Exp }
  : aexp '(' sep_by1(aexp, ',') ')' { _EAnn # (ECall $1 $3, $1 <++> $4)  }
  | '[' sep_by1(exp, ',') ']'       { _EAnn # (EArray $2, $1 <++> $3) }
  | 'free' aexp                     { _EAnn # (EFree $2, $1 <++> $2) }
  | aexp                            { $1 }

aexp :: { Exp }
  : var                 { _EAnn # (EVar $1, $1 ^? span) }
  | literal             { _EAnn # (ELit (extract $1), $1 ^? span) }
  | unary_op '(' aexp ')'      
    { _EAnn # (EUnOp (extract $1) $3, $1 <++> $4) }
  | binary_op '(' aexp ',' aexp ')'
    { _EAnn # (EBinOp (extract $1) $3 $5, $1 <++> $6) }
  | '&' aexp            { _EAnn # (ERef $2, $1 <++> $2) }
  | '*' aexp            { _EAnn # (EDeref $2, $1 <++> $2) }
  | aexp '.' field_name { _EAnn # (EGet $1 $3, $1 <++> $3) }
  | aexp '[' exp ']'    { _EAnn # (EGetI $1 $3, $1 <++> $4) }
  | '[' sep_by1(exp, ',') ']'
                        { _EAnn # (ELit (LArray $2), $1 <++> $3) }
  | '[' aexp ']'
                        { _EAnn # (ELit (LArray [$2]), $1 <++> $3) }
  | '(' exp ',' sep_by1_ne(exp, ',') ')'
                        { _EAnn # (ETuple $2 $4, $1 <++> $5) }
  | '(' exp ')'         { _EAnn # ($2, $1 <++> $3) }
  | expObject           { _EAnn # (EObj $1, $1 ^? span) }
  | 'break' ';'         { _EAnn # (EContinue, $1 <++> $2) }
  | 'continue' ';'      { _EAnn # (EContinue, $1 <++> $2) }
  | '{' exp '}'         { _EAnn # (EBlock $2, $1 <++> $3) }

branch :: { Branch }
  : 'else' exp                              { ElseBranch ($1 <++> $2) $2 }
  | 'else' 'if' '(' exp ')' exp opt(branch) { ElifBranch ($1 <++> $7) $4 $6 $7 }
  | 'elif' '(' exp ')' exp opt(branch)      { ElifBranch ($1 <++> $6) $3 $5 $6 }

expObject :: { Object Exp }
  : '{' sep_by0(expObjectField, ',') '}' { Object ($1 <++> $3) $2 }

expObjectField :: { ObjectField Exp }
  : field_name ':' exp { ObjectField ($1 <++> $3) $1 $3 }

-- -----------------------------------------------------------------------------
-- | Operators

unary_op :: { SpanM UnaryOperator }
  : '#neg' { include $1 >> return OpNeg    }

binary_op :: { SpanM BinaryOperator }
  : '#add' { include $1 >> return OpAdd }
  | '#sub' { include $1 >> return OpSub }
  | '#mul' { include $1 >> return OpMul }
  | '#div' { include $1 >> return OpDiv }
  | '#rem' { include $1 >> return OpRem }

  | '#and' { include $1 >> return OpAnd }
  | '#or'  { include $1 >> return OpOr }
  | '#xor' { include $1 >> return OpXor }

  | '#shr' { include $1 >> return OpShR }
  | '#shl' { include $1 >> return OpShL }

  | '#eq'  { include $1 >> return OpEq }
  | '#neq' { include $1 >> return OpNeq }

  | '#lt'  { include $1 >> return OpLT }
  | '#le'  { include $1 >> return OpLE }
  | '#gt'  { include $1 >> return OpGT }
  | '#ge'  { include $1 >> return OpGE }

-- -----------------------------------------------------------------------------
-- | Types

standalone_type :: { Type }
  : type mayeof { $1 }

type_list :: { [Type] }
  : tupled(type) { $1 }

type_sig_opt :: { Maybe Type }
  : type_sig    { Just $1 }
  | {- empty -} { Nothing }

type_sig :: { Type }
  : ':' type { $2 }

type :: { Type }
  : btype { $1 }

btype :: { Type }
  : atype '->' btype                      { _TAnn # (TFunc [$1] $3, $1 <++> $3) }
  | '(' sep_by1(type, ',') ')' '->' btype { _TAnn # (TFunc $2 $5, $1 <++> $5) }
  | atype { $1 }

atype :: { Type }
  : tvar                  { _TAnn # (TVar $1, $1 ^? span) }
  | type some(type)       { _TAnn # (TApp $1 $2, $1 <++> $2) }
  | primType              { $1 }
  | '*' atype             { _TAnn # (TPtr $2, $1 <++> $2) }
  | '[' integer type ']'  { let i = $2 ^?! tokenInteger . to fromInteger
                            in _TAnn # (TArray i $3, $1 <++> $4) }
  | '(' type ')'          { _TAnn # (TParens $2, $1 <++> $3) }
  | '(' type ',' sep_by1_ne(type, ',') ')'
                          { _TAnn # (TTuple $2 $4, $1 <++> $5) }


primType :: { Type }
  : 'i1'  { _TAnn # (TInt  1, $1 ^? span) }
  | 'i8'  { _TAnn # (TInt  8, $1 ^? span) }
  | 'i16' { _TAnn # (TInt 16, $1 ^? span) }
  | 'i32' { _TAnn # (TInt 32, $1 ^? span) }
  | 'i64' { _TAnn # (TInt 64, $1 ^? span) }

  | 'u8'  { _TAnn # (TUInt  8, $1 ^? span) }
  | 'u16' { _TAnn # (TUInt 16, $1 ^? span) }
  | 'u32' { _TAnn # (TUInt 32, $1 ^? span) }
  | 'u64' { _TAnn # (TUInt 64, $1 ^? span) }

  | 'f16'  { _TAnn # (TFp  16, $1 ^? span) }
  | 'f32'  { _TAnn # (TFp  32, $1 ^? span) }
  | 'f64'  { _TAnn # (TFp  64, $1 ^? span) }
  | 'f128' { _TAnn # (TFp 128, $1 ^? span) }


-- -----------------------------------------------------------------------------
-- | Type Context

type_context_opt :: { Maybe TypeContext }
  : type_context { Just $1 }
  | {- empty -}  { Nothing }

type_context :: { TypeContext }
  : many(type_context_element) { TypeContext ($1 ^? span) $1 }

type_context_element :: { TypeContextElement }
  : type_schema       { TypeContextSchema $1 }
  | class_constraint  { TypeContextConstraint $1 }

type_schema :: { TypeSchema }
  : 'forall' tvar_typed_list_ne '.' { TypeSchema ($1 <++> $3) $2 }

tvar_typed :: { TVar }
  : '(' tvar ':' type ')' { $2 & tvarType .~ $4
                               & tvarLoc .~ ($1 <++> $5) }

tvar_typed_opt :: { TVar }
  : tvar       { $1 }
  | tvar_typed { $1 }

tvar_typed_list_ne :: { NonEmpty TVar }
  : some_ne(tvar_typed_opt) { $1 }

class_constraint :: { ClassConstraint }
  : tupled_ne(class_instance) '=>' { ClassConstraint ($1 <++> $2) $1 }

class_instance :: { ClassInstance }
  : class_name type_list { ClassInstance ($1 <++> $2) $1 $2 }

{

parseError :: [Token] -> a
parseError [] = error "Parse error, no tokens left!!"
parseError (t:_) = error $ "Parse error on token: " ++ show (pretty t)

expParseError :: ([Token], [String]) -> a
expParseError ([], s)  = error $ "Parse error:"
                               ++ "Expected: " ++ show s
expParseError (ts, s) = error $ "Parse error on tokens: " ++ show (pretty $ head ts) ++ "\n"
                               ++ "Expected: " ++ show (punctuate "," $ pretty <$> s)

parseExpUnsafe :: Text -> Exp
parseExpUnsafe = parseExp . lexUnsafe

parseTypeUnsafe :: Text -> Type
parseTypeUnsafe = parseType . lexUnsafe

}
