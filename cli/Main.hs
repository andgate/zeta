{-# OPTIONS_GHC -fno-warn-unused-imports #-}
{-# LANGUAGE ApplicativeDo     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
module Main where

import           Language.Zeta.Compiler                 (runCompiler)
import           Language.Zeta.Compiler.CompilerOptions (CompilerOptions (..))
import           Options.Applicative

options :: Parser CompilerOptions
options = do
  optSourceDirectory <-
    strOption
      ( long "src"
      <> metavar "FILE"
      <> value "./"
      <> help "Write build output to FILE" )
  optOutput <-
    strOption
      ( long "output"
      <> short 'o'
      <> metavar "FILE"
      <> value "a.out"
      <> help "Write output to FILE" )
  optBuildDir <-
    strOption
      ( long "build-dir"
      <> metavar "FILE"
      <> value "./"
      <> help "Write build output to FILE" )
  optInputs <-
    many (argument str idm)
  pure CompilerOptions {..}

compilerOpts :: ParserInfo CompilerOptions
compilerOpts = info (options <**> helper) idm

main :: IO ()
main = execParser compilerOpts >>= runCompiler
