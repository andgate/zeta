module Main where

import Control.Monad
import Data.List

import Test.Tasty (defaultMain, TestTree, testGroup)
import Test.Tasty.Golden (goldenVsFile, findByExtension)

import System.Directory (listDirectory, removeDirectoryRecursive, createDirectoryIfMissing, doesDirectoryExist)
import System.FilePath (takeBaseName, replaceExtension, (</>))
import System.Process (callCommand)


sourceCodeExt :: String
sourceCodeExt = ".z"

testSuiteRootDir :: FilePath
testSuiteRootDir = "test"

testSuiteBuildDir :: FilePath
testSuiteBuildDir = testSuiteRootDir </> "build"

testSuiteOutDir :: FilePath
testSuiteOutDir = testSuiteRootDir </> "out"

testSuiteBinDir :: FilePath
testSuiteBinDir = testSuiteRootDir </> "bin"

singleFileTestsRootDir :: FilePath
singleFileTestsRootDir = testSuiteRootDir </> "single"

multiFileTestsRootDir :: FilePath
multiFileTestsRootDir = testSuiteRootDir </> "multi"

main :: IO ()
main = goldenTests >>= defaultMain 

ifM :: Monad m => m Bool -> m () -> m ()
ifM mp mt = do
  p <- mp
  if p then mt else return ()

removeOldTests :: IO ()
removeOldTests = do
  ifM (doesDirectoryExist testSuiteOutDir)
      (removeDirectoryRecursive testSuiteOutDir)
  ifM (doesDirectoryExist testSuiteBuildDir)
      (removeDirectoryRecursive testSuiteBuildDir)
  ifM (doesDirectoryExist testSuiteBinDir)
      (removeDirectoryRecursive testSuiteBinDir)

goldenTests :: IO TestTree
goldenTests = do
  removeOldTests
  singleFileTests <- goldenTestSingle
  multiFileTests <- mempty -- goldenTestMulti
  return $ testGroup "Zeta Golden Tests" (singleFileTests <> multiFileTests)

goldenTestSingle :: IO [TestTree]
goldenTestSingle = do
  let testsDir = singleFileTestsRootDir
      outDir = testSuiteBinDir </> "single"
      binDir = testSuiteOutDir </> "single"
  srcPaths <- findByExtension [sourceCodeExt] testsDir
  createDirectoryIfMissing True outDir
  createDirectoryIfMissing True binDir

  return $
    [ goldenVsFile
        ("Golden Test " ++ takeBaseName srcPath ++ " (Single-File)")
        goldPath
        outPath
        (compileFile srcPath outPath)
    | srcPath <- srcPaths
    , let outPath = outDir </> takeBaseName srcPath <> ".out"
          goldPath = replaceExtension srcPath "gold"
    ]


goldenTestMulti :: IO [TestTree]
goldenTestMulti = do
  let testsDir = multiFileTestsRootDir
      outDir = testSuiteBinDir </> "multi"
      binDir = testSuiteOutDir </> "multi"
  srcDirs <- filterM doesDirectoryExist . map (testsDir </>) =<< listDirectory testsDir
  createDirectoryIfMissing True outDir
  createDirectoryIfMissing True binDir
  return $
    [ goldenVsFile
        ("Golden Test " ++ takeBaseName srcDir ++ " (Multi-File)")
        goldPath
        outPath
        (compileDir srcDir outPath)
    | srcDir <- srcDirs
    , let outPath = outDir </> takeBaseName srcDir <> ".out"
          goldPath = replaceExtension srcDir ".gold"
    ]


compileFile :: FilePath -> FilePath -> IO ()
compileFile srcPath outPath = do
  let exePath = "test/bin/single/" <> takeBaseName srcPath
  let buildPath = "test/build/single/" <> takeBaseName srcPath <> "/"
  callCommand $ "zeta " 
             ++ srcPath ++ " -o " ++ exePath
             ++ " --build-dir " ++ buildPath
  callCommand $ exePath ++ " > " ++ outPath

compileDir :: FilePath -> FilePath -> IO ()
compileDir srcDir outPath = do
  let exePath = "test/bin/multi/" <> takeBaseName srcDir
  srcPaths <- findByExtension [sourceCodeExt] srcDir
  callCommand $ "zeta " ++ intercalate " " srcPaths ++ " -o " ++ exePath
  callCommand $ exePath ++ " > " ++ outPath