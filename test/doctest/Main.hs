module Main where

import           System.FilePath.Glob (glob)
import           Test.DocTest

main = do
  srcFiles <- glob "src/**/*.hs"
  let ghcOpts = ["-fdefer-typed-holes", "-Wwarn=typed-holes"]
      opts = srcFiles <> ghcOpts
  doctest opts
