module Main where

import           Test.Control.Monad.Fresh          (freshTests)
import           Test.Control.Monad.Report         (reportTests)
import           Test.Control.Monad.Span           (spanMonadTests)
import           Test.Data.Result                  (resultTests)
import           Test.Document.Classy.HasLocation  (hasLocationTests)
import           Test.Document.Classy.HasPosition  (hasPositionTests)
import           Test.Document.Classy.HasSpan      (hasSpanTests)
import           Test.Document.Data.Location       (locationTests)
import           Test.Document.Data.Position       (positionTests)
import           Test.Document.Data.Span           (spanTests)
import           Test.Language.Zeta.Compiler       (compilerTests)
import           Test.Language.Zeta.Document.Token (tokenTests)
import           Test.Language.Zeta.Renamer        (renamerTests)
import           Test.Language.Zeta.Syntax         (syntaxTests)
import           Test.Tasty                        (defaultMain, testGroup)

main :: IO ()
main = defaultMain $ testGroup "Zeta Tests"
  [ positionTests
  , spanTests
  , locationTests
  , hasPositionTests
  , hasSpanTests
  , hasLocationTests
  , spanMonadTests
  , tokenTests
  , freshTests
  , resultTests
  , reportTests
  , syntaxTests
  , renamerTests
  , compilerTests
  ]
