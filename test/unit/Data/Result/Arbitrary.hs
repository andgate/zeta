{-# OPTIONS_GHC -fno-warn-orphans #-}
module Data.Result.Arbitrary where

import           Data.Result     (Result (..))
import Data.DList.Arbitrary ()
import           Test.QuickCheck (Arbitrary (..), chooseInt)

instance (Arbitrary e, Arbitrary a) => Arbitrary (Result e a) where
  arbitrary = do
    k <- chooseInt (1, 3)
    case k of
      1 -> Success <$> arbitrary
      2 -> Failure <$> arbitrary
      _ -> Continue <$> arbitrary <*> arbitrary
