{-# OPTIONS_GHC -fno-warn-orphans #-}
module Data.DList.Arbitrary where

import           Data.DList      (DList)
import qualified Data.DList      as DL
import           Test.QuickCheck (Arbitrary (..))

instance (Arbitrary a) => Arbitrary (DList a) where
  arbitrary = DL.fromList <$> arbitrary
  {-# INLINE arbitrary #-}
