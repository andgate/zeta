{-# OPTIONS_GHC -fno-warn-orphans #-}
module Data.Text.Arbitrary where

import           Data.Text       (Text, pack)
import           Test.QuickCheck (Arbitrary (..))

instance Arbitrary Text where
  arbitrary = pack <$> arbitrary
  {-# INLINABLE arbitrary #-}
