{-# OPTIONS_GHC -fno-warn-orphans #-}
module Control.Monad.Report.Arbitrary where

import           Control.Monad.Trans.Report (ReportT (..))
import           Data.Result.Arbitrary      ()
import           Test.QuickCheck            (Arbitrary (..))

instance (Arbitrary e, Monad m, Arbitrary a) => Arbitrary (ReportT e m a) where
  arbitrary = do
    r <- arbitrary
    return . ReportT $ \_ -> return r
