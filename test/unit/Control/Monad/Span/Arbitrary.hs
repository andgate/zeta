{-# OPTIONS_GHC -fno-warn-orphans #-}
module Control.Monad.Span.Arbitrary where

import           Control.Monad.Span           (SpanM (..))
import           Document.Data.Span.Arbitrary ()
import           Test.QuickCheck              (Arbitrary (..))

instance (Arbitrary a) => Arbitrary (SpanM a) where
  arbitrary =
    SpanM <$> ((,) <$> arbitrary <*> arbitrary)
