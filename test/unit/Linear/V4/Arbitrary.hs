{-# OPTIONS_GHC -fno-warn-orphans #-}
module Linear.V4.Arbitrary where

import           Linear.V4       (V4 (..))
import           Test.QuickCheck (Arbitrary (..))

instance (Arbitrary a) => Arbitrary (V4 a) where
  arbitrary = V4 <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary
  {-# INLINE arbitrary #-}
