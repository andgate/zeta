{-# OPTIONS_GHC -fno-warn-orphans #-}
module Linear.V2.Arbitrary where

import           Linear.V2       (V2 (..))
import           Test.QuickCheck (Arbitrary (..))

instance (Arbitrary a) => Arbitrary (V2 a) where
  arbitrary = V2 <$> arbitrary <*> arbitrary
  {-# INLINE arbitrary #-}
