{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Span.Arbitrary where

import           Document.Data.Span        (Span (..))
import           Linear.V4.Arbitrary       ()
import           Numeric.Natural.Arbitrary ()
import           Test.QuickCheck           (Arbitrary (..))

instance Arbitrary Span where
  arbitrary = S <$> arbitrary
  {-# INLINE arbitrary #-}
