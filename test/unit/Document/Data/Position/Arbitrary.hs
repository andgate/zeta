{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Position.Arbitrary where

import           Document.Data.Position    (Position (..))
import           Linear.V2.Arbitrary       ()
import           Numeric.Natural.Arbitrary ()
import           Test.QuickCheck           (Arbitrary (..))

instance Arbitrary Position where
  arbitrary = P <$> arbitrary
  {-# INLINE arbitrary #-}
