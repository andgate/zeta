{-# OPTIONS_GHC -fno-warn-orphans #-}
module Document.Data.Location.Arbitrary where

import           Document.Data.Location       (Loc (..))
import           Document.Data.Span.Arbitrary ()
import           Test.QuickCheck              (Arbitrary (..))

instance Arbitrary Loc where
  arbitrary = L <$> arbitrary <*> arbitrary
