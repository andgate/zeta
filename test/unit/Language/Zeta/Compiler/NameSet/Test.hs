{-# LANGUAGE OverloadedStrings #-}
module Language.Zeta.Compiler.NameSet.Test (testNameSet) where

import           Control.Lens.Operators         ((&), (.~))
import qualified Data.Set                       as Set
import           Language.Zeta.Compiler.NameSet (NameSet,
                                                 nameSetConstructorNames,
                                                 nameSetFieldNames, nameSetTypeNames)
import           Language.Zeta.Syntax           (newConstructorName,
                                                 newFieldName, newTypeName)

testNameSet :: NameSet
testNameSet =
  mempty
    & nameSetConstructorNames .~ Set.fromList (fmap (newConstructorName Nothing)
      ["Just"])
    & nameSetFieldNames .~ Set.fromList (fmap (newFieldName Nothing)
      ["a"])
    & nameSetTypeNames .~ Set.fromList (fmap (newTypeName Nothing)
      ["Maybe"])
