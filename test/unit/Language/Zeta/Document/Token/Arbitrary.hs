{-# OPTIONS_GHC -fno-warn-orphans #-}
module Language.Zeta.Document.Token.Arbitrary where

import qualified Data.Text                    as T
import           Document.Data.Span.Arbitrary ()
import           Language.Zeta.Document.Token (Token (..), TokenClass (..))
import           Test.Tasty.QuickCheck        (Arbitrary (..), Gen, chooseInt)

instance Arbitrary Token where
  arbitrary = Token <$> arbitrary <*> arbitrary

instance Arbitrary TokenClass where
  arbitrary = do
    n <- chooseInt (1, 13)
    case n of
      1  -> arbitraryTokenRsvp
      2  -> arbitraryTokenVarId
      3  -> arbitraryTokenPrimId
      4  -> arbitraryTokenInteger
      5  -> arbitraryTokenDouble
      6  -> arbitraryTokenChar
      7  -> arbitraryTokenString
      8  -> arbitraryTokenBool
      9  -> arbitraryTokenLineOpen
      10 -> arbitraryTokenLineClose
      11 -> arbitraryTokenBlockOpen
      12 -> arbitraryTokenBlockClose
      13 -> arbitraryTokenEof
      _  -> error ("Invalid TokenClass index: " <> show n)


arbitraryTokenRsvp :: Gen TokenClass
arbitraryTokenRsvp = TokenRsvp . T.pack <$> (arbitrary :: Gen String)

arbitraryTokenVarId :: Gen TokenClass
arbitraryTokenVarId = TokenVarId . T.pack <$> (arbitrary :: Gen String)

arbitraryTokenPrimId :: Gen TokenClass
arbitraryTokenPrimId = TokenPrimId . T.pack <$> (arbitrary :: Gen String)

arbitraryTokenInteger :: Gen TokenClass
arbitraryTokenInteger = TokenInteger <$> arbitrary

arbitraryTokenDouble :: Gen TokenClass
arbitraryTokenDouble = TokenDouble <$> arbitrary

arbitraryTokenChar :: Gen TokenClass
arbitraryTokenChar = TokenChar <$> arbitrary

arbitraryTokenString :: Gen TokenClass
arbitraryTokenString = TokenString <$> arbitrary

arbitraryTokenBool :: Gen TokenClass
arbitraryTokenBool = TokenBool <$> arbitrary

arbitraryTokenLineOpen :: Gen TokenClass
arbitraryTokenLineOpen = pure TokenLineOpen

arbitraryTokenLineClose :: Gen TokenClass
arbitraryTokenLineClose = pure TokenLineClose

arbitraryTokenBlockOpen :: Gen TokenClass
arbitraryTokenBlockOpen = pure TokenBlockOpen

arbitraryTokenBlockClose :: Gen TokenClass
arbitraryTokenBlockClose = pure TokenBlockClose

arbitraryTokenEof :: Gen TokenClass
arbitraryTokenEof = pure TokenEof
