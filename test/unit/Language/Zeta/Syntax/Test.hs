module Language.Zeta.Syntax.Test where

import           Prelude                   hiding (lex)

import           Control.Lens.Each         (each)
import           Control.Lens.Operators    ((^..))
import           Data.Bifunctor            (Bifunctor (..))
import qualified Data.Set                  as Set
import           Data.Text                 (Text)
import           Language.Zeta.Parse       (parseExpUnsafe, parseTypeUnsafe)
import           Language.Zeta.Syntax      (Exp, HasVars (vars), Scope,
                                            Type (TVar), abstract, getScopeBody,
                                            newFreeTVar, subst, toBoundTVars,
                                            tvarBoundStr, tvarStr, varBoundStr)
import           Prettyprinter             (Pretty (pretty),
                                            defaultLayoutOptions, layoutPretty)
import           Prettyprinter.Render.Text (renderStrict)
import           Test.Tasty.HUnit          ((@?=))

renderResult :: Pretty r => r -> Text
renderResult = renderStrict . layoutPretty defaultLayoutOptions . pretty

parsePrintExp :: Text -> Text
parsePrintExp =
  renderResult . parseExpUnsafe

parsePrintType :: Text -> Text
parsePrintType =
  renderResult . parseTypeUnsafe

testExp :: Text -> IO ()
testExp src =
  parsePrintExp src @?= src

testType :: Text -> IO ()
testType src =
  parsePrintType src @?= src

getExpVars :: Text -> [Text]
getExpVars src =
  parseExpUnsafe src ^.. vars . varBoundStr

getTypeVars :: Text -> [Text]
getTypeVars src =
  parseTypeUnsafe src ^.. vars . tvarBoundStr

abstractExp :: Text -> [Text] -> Scope Exp
abstractExp src vs =
  abstract vs (parseExpUnsafe src)

applyExp :: Text -> [(Text, Text)] -> Exp
applyExp src eqsSrc = subst eqs e
  where
    e = parseExpUnsafe src
    eqs = second parseExpUnsafe <$> eqsSrc

abstractType :: Text -> [Text] -> Scope Type
abstractType src vs =
  abstract vs (parseTypeUnsafe src)

testTypeAbstractAll :: Text -> IO ()
testTypeAbstractAll src =
  tvs' @?= expected
  where
    t = parseTypeUnsafe src
    ns = (Set.toList . Set.fromList) (t ^.. vars . tvarStr)
    tvs = newFreeTVar Nothing <$> ns
    t' = getScopeBody $ abstract tvs t
    tvs' = t' ^.. vars . tvarBoundStr
    expected = toBoundTVars tvs ^.. each . tvarBoundStr

testTypeBindUnbind :: Text -> IO ()
testTypeBindUnbind src =
  renderResult t @?= renderResult t'
  where
    t = parseTypeUnsafe src
    tvs = t ^.. vars
    eqs = zip tvs (TVar <$> tvs)
    t' = subst eqs t

applyType :: Text -> [(Text, Text)] -> Type
applyType src eqsSrc = subst eqs e
  where
    e = parseTypeUnsafe src
    eqs = second parseTypeUnsafe <$> eqsSrc
