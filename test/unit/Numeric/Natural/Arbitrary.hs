{-# OPTIONS_GHC -fno-warn-orphans #-}
module Numeric.Natural.Arbitrary where

import           Numeric.Natural (Natural)
import           Test.QuickCheck (Arbitrary (..), Gen)

instance Arbitrary Natural where
  arbitrary = fromIntegral . (+1) . abs <$> (arbitrary :: Gen Int)
  {-# INLINE arbitrary #-}
