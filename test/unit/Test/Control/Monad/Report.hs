{-# OPTIONS_GHC -Wno-unused-do-bind #-}
module Test.Control.Monad.Report (reportTests) where

import           Control.Monad.Report                (Report, runReport)
import           Control.Monad.Report.Class          (MonadReport (..))
import           Control.Monad.Report.Arbitrary ()
import           Test.Data.Result                 ()
import           Test.Tasty                          (TestTree, testGroup)
import           Test.Tasty.HUnit                    (testCase, (@?=))
import qualified Test.Tasty.QuickCheck               as QC
import           Test.TestError                      (TestError)

type TestReport a = Report TestError a

runTestReport :: TestReport a -> Either [TestError] a
runTestReport = runReport

reportTests :: TestTree
reportTests = testGroup "Control.Monad.Report Tests"
  [ testCase "runReport (fmap id (return 1)) == 1" $
      runTestReport (fmap id (return 1)) @?= Right (1 :: Int)
  , QC.testProperty "Functor Identity Law: fmap id r == r" $
      \r -> runTestReport (fmap id r) == runTestReport (r :: TestReport Int)
  , QC.testProperty "Applicative Identity Law: pure id <*> r == r" $
      \r -> runTestReport (pure id <*> r) == runTestReport (r :: TestReport Int)
  , QC.testProperty "Monad Left Identity Law: return a >>= id == a" $
      \a -> runTestReport ((return (a :: Int) >>= (return . id)) :: TestReport Int) == Right a
  , QC.testProperty "Monad Right Identity Law: ma >>= return == ma" $
      \r -> runTestReport ((r :: TestReport Int) >>= return) == runTestReport r
  , QC.testProperty "return handles values correctly" $
      \i ->
        let m :: Report TestError Int
            m = return (i + 1)
        in runReport m == Right (i + 1)
  , QC.testProperty "double witness" $
      \i err1 err2 ->
        let m :: Report TestError Int
            m = do
              witness err1
              j <- return (i + 1)
              witness err2
              return (j + 1)
        in runReport m == Right (i + 2)
  , QC.testProperty "allFatal w/ double witness" $
      \i err1 err2 ->
        let m :: Report TestError Int
            m = allFatal $ do
              witness err1
              witness err2
              return (i + 1)
        in runReport m == Left [err1, err2]
  , QC.testProperty "double nonfatal" $
      \i err1 err2 ->
        let m :: Report TestError Int
            m = do
              j <- nonfatal err1 (i + 1)
              nonfatal err2 (j + 2)
        in runReport m == Right (i + 3)
  , QC.testProperty "allFatal w/ double nonfatal" $
      \i err1 err2 ->
        let m :: Report TestError Int
            m = allFatal $ do
              j <- nonfatal err1 (i + 1)
              nonfatal err2 (j + 2)
        in runReport m == Left [err1, err2]
  , QC.testProperty "double fatal" $
      \i err1 err2 ->
        let m :: Report TestError Int
            m = allFatal $ do
              j <- return (i + 1)
              fatal err1
              k <- return (j + 2)
              fatal err2
              return k
        in runReport m == Left [err1]
  ]
