module Test.Control.Monad.Span where

import           Control.Monad.Span           (MonadSpan (..), SpanM (..))
import           Control.Monad.Span.Arbitrary ()
import           Document.Data.Span           (Span)
import           Document.Data.Span.Arbitrary ()
import           Test.Tasty                   (TestTree, testGroup)
import           Test.Tasty.HUnit             (testCase, (@?=))
import qualified Test.Tasty.QuickCheck        as QC

spanMonadTests :: TestTree
spanMonadTests = testGroup "Span Monad Tests"
  [ testCase "First test" $
      (1 :: Int) @?= 1
  , QC.testProperty "Functor Identity Law: fmap id ma == ma" $
      \ma -> fmap id ma == (ma :: SpanM Int)
  , QC.testProperty "Applicative Identity Law: pure id <*> ma == ma" $
      \ma -> (pure id <*> ma) == (ma :: SpanM Int)
  , QC.testProperty "Monad Left Identity Law: return a >>= id == a" $
      \a -> runSpan (return a >>= (return . id)) == (Nothing, a :: Int)
  , QC.testProperty "Monad Right Identity Law: ma >>= return == ma" $
      \ma -> (ma >>= return) == (ma :: SpanM Int)
  , QC.testProperty "(runSpan . include) s == (s, ())" $
      \s -> (runSpan . include) s == (s :: Maybe Span, ())
  ]
