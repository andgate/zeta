{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Test.Control.Monad.Fresh (freshTests) where

import           Classy.HasGeneration  (HasGeneration (..))
import           Classy.HasName        (HasName (..))
import           Control.Lens.Lens     (lens)
import           Control.Monad.Fresh   (FreshT (..), MonadFresh (..), runFresh)
import           Control.Monad.State   (MonadState (..))
import           Data.Functor.Identity (Identity (..))
import           Data.String           (IsString (..))
import           Data.Text             (Text)
import qualified Data.Text             as T
import           Data.Text.Arbitrary   ()
import           Test.Tasty            (TestTree, testGroup)
import           Test.Tasty.HUnit      (testCase, (@?=))
import           Test.Tasty.QuickCheck as QC

data TestName = TestName Text Integer

newTestName :: Text -> TestName
newTestName n = TestName n 0

instance Eq TestName where
  (==) (TestName n1 g1) (TestName n2 g2) =
    n1 == n2 && g1 == g2

instance Show TestName where
  show (TestName n g) = show n <> "@" <> show g

instance IsString TestName where
  fromString s = newTestName (fromString s)

instance HasName TestName where
  _name =
    lens
      (\(TestName n _) -> n)
      (\(TestName _ i) n -> TestName n i)

instance HasGeneration TestName where
  generation =
    lens
      (\(TestName _ i) -> i)
      (\(TestName n _) i -> TestName n i)

instance Arbitrary TestName where
  arbitrary = TestName <$> (T.pack <$> arbitrary) <*> arbitrary

instance Arbitrary a => Arbitrary (FreshT Identity a) where
  arbitrary = do
    ns <- arbitrary
    a <- arbitrary
    return . FreshT $ do
      put ns
      return a

example1Results :: [TestName]
example1Results =
  [ TestName "a" 0
  , TestName "a" 1
  , TestName "b" 0
  , TestName "a" 2
  ]

example2Results :: [TestName]
example2Results =
  example1Results <>
  [ TestName "b" 1
  , TestName "c" 0
  ]

freshTests :: TestTree
freshTests = testGroup "Control.Monad.Fresh Tests"
  [ testCase "Example 1" $
      let
        r = runFresh $ do
          n1 <- fresh "a"
          n2 <- fresh "a"
          n3 <- fresh "b"
          n4 <- fresh "a"
          return [n1, n2, n3, n4]
      in
        r @?= example1Results
  , testCase "Example 2" $
      let
        r = runFresh $ do
          n1 <- fresh "a"
          n2 <- fresh "a"
          n3 <- fresh "b"
          n4 <- fresh "a"
          n5 <- fresh "b"
          n6 <- fresh "c"
          return [n1, n2, n3, n4, n5, n6]
      in
        r @?= example2Results
  , QC.testProperty "When applying fresh, old name generation data is discarded." $
      \c1 c2 c3 c4 ->
        let
          r = runFresh $ do
            n1 <- fresh (TestName "a" c1)
            n2 <- fresh (TestName "a" c2)
            n3 <- fresh (TestName "b" c3)
            n4 <- fresh (TestName "a" c4)
            return [n1, n2, n3, n4]
        in
          r == example1Results
  ]
