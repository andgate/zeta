module Test.Data.Result (resultTests) where

import qualified Data.DList            as DL
import           Data.Result           (Result (..))
import           Data.Result.Arbitrary   ()
import           Test.Tasty            (TestTree, testGroup)
import           Test.Tasty.HUnit      (testCase, (@?=))
import qualified Test.Tasty.QuickCheck as QC
import           Test.TestError        (TestError (..))

resultTests :: TestTree
resultTests = testGroup "Data.Result Tests"
  [ testCase "fmap (+1) (Success 1) == Success 2" $
      fmap (+1) (Success 1) @?= (Success 2 :: Result TestError Int)
  , testCase "fmap (+1) (Failure [ErrA]) == Failure [ErrA]" $
      fmap (+1) (Failure (DL.fromList [ErrA])) @?= (Failure (DL.fromList [ErrA]) :: Result TestError Int)
  , testCase "fmap (+1) (Continue [ErrA] 1) == Continue [ErrA] 2" $
      fmap (+1) (Continue (DL.fromList [ErrA]) 1) @?= (Continue (DL.fromList [ErrA]) 2 :: Result TestError Int)
  , QC.testProperty "Functor Identity Law: fmap id r == r" $
      \r -> fmap id r == (r :: Result TestError Int)
  , QC.testProperty "fmap (+1) s == s (w/ result += 1)" $
      \r -> case (r :: Result TestError Int) of
        Success x       -> fmap (+1) (Success x) == (Success (x + 1) :: Result TestError Int)
        Failure errs    -> fmap (+1) (Failure errs) == (Failure errs :: Result TestError Int)
        Continue errs x -> fmap (+1) (Continue errs x) == Continue errs (x + 1)
  ]
