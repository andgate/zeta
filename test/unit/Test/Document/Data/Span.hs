module Test.Document.Data.Span (spanTests) where

import           Prelude                          hiding (getLine)

import           Control.Lens.Operators           ((^.))
import           Document.Data.Position           (Position (..), getColumn,
                                                   getLine, initialPosition,
                                                   newPosition)
import           Document.Data.Position.Arbitrary ()
import           Document.Data.Span               (Span (..), _end, _start,
                                                   containsPosition,
                                                   containsSpan, getEnd,
                                                   getSpan, getSpanV, getStart,
                                                   growLineSpan, growSpan,
                                                   initialSpan, moveSpan,
                                                   newSpan, newSpan', setEnd,
                                                   setSpanV, setStart,
                                                   startNextLineSpan, stretch)
import           Document.Data.Span.Arbitrary     ()
import           Linear.V2                        (V2 (..))
import           Linear.V4                        (V4 (..))
import           Linear.V4.Arbitrary              ()
import           Numeric.Natural.Arbitrary        ()
import           Prettyprinter                    (Pretty (pretty))
import           Test.Tasty                       (TestTree, testGroup)
import           Test.Tasty.HUnit                 (testCase, (@?=))
import qualified Test.Tasty.QuickCheck            as QC


spanTests :: TestTree
spanTests = testGroup "Span Tests"
  [ testCase "The initial span starts on line 1" $
      (getLine . getStart) initialSpan @?= 1
  , testCase "The initial position starts on column 1" $
      (getColumn . getStart) initialSpan @?= 1
  , testCase "The initial span ends on line 1" $
      (getLine . getEnd) initialSpan @?= 1
  , testCase "The initial position ends on column 1" $
      (getColumn . getEnd) initialSpan @?= 1
  , testCase "The initial span prettyprints as \"1:1\"" $
      show (pretty initialSpan) @?= "1:1"
  , testCase "A span starting at 1:1 and ending at 1:2 prettyprints as \"1:1-1:2\"" $
      show (pretty $ newSpan' 1 1 1 2) @?= "1:1-1:2"
  , testCase "stretch initialPosition 2 == newSpan' 1 1 1 3" $
      stretch initialPosition 2 @?= newSpan' 1 1 1 3
  , testCase "moveSpan initialSpan 2 == newSpan' 1 3 1 3" $
      moveSpan (newSpan' 1 2 3 4) 2 @?= newSpan' 3 4 3 6
  , testCase "growSpan initialSpan 2 == newSpan' 1 1 1 3" $
      growSpan initialSpan 2 @?= newSpan' 1 1 1 3
  , testCase "startNextLineSpan (newSpan' 2 2 2 6) == newSpan' 3 1 3 1" $
      startNextLineSpan (newSpan' 2 2 2 6) @?= newSpan' 3 1 3 1
  , testCase "growLineSpan (newSpan' 2 2 2 6) 2 == newSpan' 2 2 3 1" $
      growLineSpan (newSpan' 2 2 2 6) @?= newSpan' 2 2 3 1
  , QC.testProperty "newSpan (newPosition l1 c2) (newPosition l2 c2) == newSpan' l1 c1 l2 c2" $
      \s -> (s :: Span) == s
  , QC.testProperty "newSpan (newPosition l1 c2) (newPosition l2 c2) == newSpan' l1 c1 l2 c2" $
      \l1 c1 l2 c2 -> newSpan (newPosition l1 c1) (newPosition l2 c2) == newSpan' l1 c1 l2 c2
  , QC.testProperty "show (pretty (newSpan p p)) == show (pretty p)" $
      \p -> show (pretty $ newSpan p p) == show (pretty p)
  , QC.testProperty "getSpan (newSpan p q) == (p, q)" $
      \p q -> getSpan (newSpan p q) == (p, q)
  , QC.testProperty "getSpanV (S v) == v" $
      \v -> getSpanV (S v) == v
  , QC.testProperty "getSpanV (S v) == v" $
      \s v -> getSpanV (setSpanV s v) == v
  , QC.testProperty "getStart (setStart s p) == p" $
      \s p -> getStart (setStart s p) == p
  , QC.testProperty "getEnd (setEnd s p) == p" $
      \s p -> getEnd (setEnd s p) == p
  , QC.testProperty "(newSpan s e) ^. _start == s" $
      \s e -> newSpan s e ^. _start == s
  , QC.testProperty "(newSpan s e) ^. _end == e" $
      \s e -> newSpan s e ^. _end == e
  , QC.testProperty "Monoid Associativity: (s1 <> s2) <> s3 == s1 <> (s2 <> s3)" $
      \s1 s2 s3 -> ((s1 :: Span) <> s2) <> s3 == s1 <> (s2 <> s3)
  , testCase "containsPosition (S (V4 1 1 2 1)) (P (V2 1 3)) == True" $
      containsPosition (S (V4 1 1 2 1)) (P (V2 1 3)) @?= True
  , QC.testProperty "containsSpan s s" $
      \s -> containsSpan (s :: Span) s
  , QC.testProperty "containsPosition s (s ^. _start)" $
      \s -> containsPosition (s :: Span) (s ^. _start)
  , QC.testProperty "containsPosition s (s ^. _end)" $
      \s -> containsPosition (s :: Span) (s ^. _end)
  , QC.testProperty "containsSpan (s1 <> s2) s1" $
      \s1 s2 -> containsSpan ((s1 :: Span) <> s2) s1
  , QC.testProperty "containsSpan (s1 <> s2) s2" $
      \s1 s2 -> containsSpan ((s1 :: Span) <> s2) s2
  ]
