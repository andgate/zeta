module Test.Document.Data.Location (locationTests) where

import           Control.Lens.Operators           ((&), (.~), (^.))
import           Document.Data.Location           (Loc (..), _locFilePath,
                                                   _locSpan, doesLocContainSpan,
                                                   newLocation, setLocFilePath,
                                                   setLocSpan)
import           Document.Data.Location.Arbitrary ()
import           Document.Data.Span               (newSpan')
import           Prettyprinter                    (Pretty (pretty))
import           Test.Tasty                       (TestTree, testGroup)
import           Test.Tasty.HUnit                 (testCase, (@?=))
import qualified Test.Tasty.QuickCheck            as QC

locationTests :: TestTree
locationTests = testGroup "Document.Data.Location Tests"
  [ testCase "pretty location" $
      (show . pretty . L "testfile.txt") (newSpan' 1 2 3 4) @?= "testfile.txt:1:2-3:4"
  , QC.testProperty "newLocation fp s == L fp s" $
      \fp sp -> newLocation fp sp == L fp sp
  , QC.testProperty "getLocFilePath (L fp sp) == fp" $
      \fp sp -> getLocFilePath (L fp sp) == fp
  , QC.testProperty "getLocSpan (L fp sp) == sp" $
      \fp sp -> getLocSpan (L fp sp) == sp
  , QC.testProperty "setLocFilePath l fp == L fp (getLocSpan l)" $
      \l fp -> setLocFilePath l fp == L fp (getLocSpan l)
  , QC.testProperty "setLocSpan l sp == L (getLocFilePath l) sp" $
      \l sp -> setLocSpan l sp == L (getLocFilePath l) sp
  , QC.testProperty "getLocSpan l == L (getLocFilePath l) sp" $
      \l sp -> setLocSpan l sp == L (getLocFilePath l) sp
  , QC.testProperty "l ^. _locFilePath == getLocFilePath l" $
      \l -> l ^. _locFilePath == getLocFilePath l
  , QC.testProperty "l ^. _locSpan == getLocSpan l" $
      \l -> l ^. _locSpan == getLocSpan l
  , QC.testProperty "(l & _locFilePath .~ fp) == setLocFilePath l fp" $
      \l fp -> (l & _locFilePath .~ fp) == setLocFilePath l fp
  , QC.testProperty "(l & _locSpan .~ sp) == setLocSpan l sp" $
      \l sp -> (l & _locSpan .~ sp) == setLocSpan l sp
  , QC.testProperty "getLocFilePath (l1 <> l2) == getLocFilePath l1" $
      \l1 l2 -> getLocFilePath (l1 <> l2) == getLocFilePath l1
  , QC.testProperty "doesLocContainSpan (l1 <> l2) (getLocSpan l1)" $
      \l1 l2 -> doesLocContainSpan (l1 <> l2) (getLocSpan l1)
  ]
