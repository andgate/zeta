{-# LANGUAGE OverloadedStrings #-}
module Test.Document.Data.Position (positionTests) where

import           Prelude                          hiding (getLine)

import           Control.Lens.Operators           ((&), (+~), (.~), (^.))
import           Document.Data.Position           (Position (..), _column,
                                                   _line, _position, getColumn,
                                                   getLine, getPosition,
                                                   getPositionV,
                                                   initialPosition,
                                                   movePositionColumn,
                                                   movePositionLine,
                                                   newPosition, setColumn,
                                                   setLine, setPositionV,
                                                   toColumnStartPosition,
                                                   toNextLinePosition,
                                                   toNextLineStartPosition)
import           Document.Data.Position.Arbitrary ()
import           Prettyprinter                    (Pretty (pretty))
import           Test.Tasty                       (TestTree, testGroup)
import           Test.Tasty.HUnit                 (testCase, (@?=))
import qualified Test.Tasty.QuickCheck            as QC

positionTests :: TestTree
positionTests = testGroup "Position Tests"
  [ testCase "The initial position has line 1" $
      getLine initialPosition @?= 1
  , testCase "The initial position has column 1" $
      getColumn initialPosition @?= 1
  , testCase "The initial position is equal to itself." $
      initialPosition == initialPosition @?= True
  , testCase "The initial position is less than or equal to itself." $
      initialPosition <= initialPosition @?= True
  , testCase "A pretty initial position is \"1:1\"" $
      show (pretty initialPosition) @?= "1:1"
  , testCase "1:1 < 1:10" $
      initialPosition < newPosition 1 10 @?= True
  , testCase "1:1 < 10:1" $
      initialPosition < newPosition 10 1 @?= True
  , testCase "movePositionLine initialPosition 1 == newPosition 2 1" $
      movePositionLine initialPosition 1 @?= newPosition 2 1
  , testCase "movePositionColumn initialPosition 1 == newPosition 1 2" $
      movePositionColumn initialPosition 1 @?= newPosition 1 2
  , testCase "toNextLinePosition (newPosition 2 2) == newPosition 3 2" $
      toNextLinePosition (newPosition 2 2) @?= newPosition 3 2
  , testCase "toColumnStartPosition (newPosition 2 2) == newPosition 2 1" $
      toColumnStartPosition (newPosition 2 2) @?= newPosition 2 1
  , testCase "toNextLineStartPosition (newPosition 2 2) == newPosition 3 1" $
      toNextLineStartPosition (newPosition 2 2) @?= newPosition 3 1
  , QC.testProperty "A position must always be equal to itself" $
      \p -> (p :: Position) == p
  , QC.testProperty "A position line must always be 1 or greater" $
      \p -> getLine p >= 1
  , QC.testProperty "A position column must always be 1 or greater" $
      \p -> getColumn p >= 1
  , QC.testProperty "getPosition (newPosition ln c) == (ln, c)" $
      \ln c -> getPosition (newPosition ln c) == (ln, c)
  , QC.testProperty "getPositionV (P v) == v" $
      \v -> getPositionV (P v) == v
  , QC.testProperty "getPositionV (setPositionV p v) == v" $
      \p v -> getPositionV (setPositionV p v) == v
  , QC.testProperty "getLine (setLine p ln) == ln" $
      \p ln -> getLine (setLine p ln) == ln
  , QC.testProperty "getColumn (setColumn p c) == c" $
      \p c -> getColumn (setColumn p c) == c
  , QC.testProperty "(p & _position .~ lnc) ^. _position == lnc" $
      \p lnc -> (p & _position .~ lnc) ^. _position == lnc
  , QC.testProperty "(p & _column +~ x) ^. _column == (p ^. _column) + x" $
      \p x -> (p & _column +~ x) ^. _column == (p ^. _column) + x
  , QC.testProperty "(p & _line +~ x) ^. _line == (p ^. _line) + x" $
      \p x -> (p & _line +~ x) ^. _line == (p ^. _line) + x
  , QC.testProperty "getLine (movePositionLine p x) == getLine p + x" $
      \p x -> getLine (movePositionLine p x) == getLine p + x
  , QC.testProperty "getColumn (movePositionLine p x) == getColumn p" $
      \p x -> getColumn (movePositionLine p x) == getColumn p
  , QC.testProperty "getLine (movePositionColumn p x) == getLine p" $
      \p x -> getLine (movePositionColumn p x) == getLine p
  , QC.testProperty "getColumn (movePositionColumn p x) == getColumn p + x" $
      \p x -> getColumn (movePositionColumn p x) == getColumn p + x
  ]
