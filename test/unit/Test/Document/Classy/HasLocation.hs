module Test.Document.Classy.HasLocation (hasLocationTests) where

import           Control.Lens.Operators           ((&), (.~), (^?))
import           Document.Classy.HasLocation      (HasLocation (..),
                                                   locFilePath, locSpan)
import           Document.Data.Location           (Loc (..), setLocFilePath,
                                                   setLocSpan)
import           Document.Data.Location.Arbitrary ()
import           Document.Data.Span.Arbitrary     ()
import           Test.Tasty                       (TestTree, testGroup)
import           Test.Tasty.HUnit                 (testCase, (@?=))
import qualified Test.Tasty.QuickCheck            as QC

hasLocationTests :: TestTree
hasLocationTests = testGroup "HasLocation Tests"
  [ QC.testProperty "l ^? location == Just l" $
      \l -> (l :: Loc) ^? location == Just l
  , QC.testProperty "(Just l) ^? location == Just l" $
      \l -> Just (l :: Loc) ^? location == Just l
  , testCase "(Nothing :: Maybe Loc) ^? location == Nothing" $
      (Nothing :: Maybe Loc) ^? location @?= Nothing
  , QC.testProperty "l ^? locFilePath == Just (getLocFilePath l)" $
      \l -> l ^? locFilePath == Just (getLocFilePath l)
  , QC.testProperty "l ^? locSpan == Just (getLocSpan l)" $
      \l -> l ^? locSpan == Just (getLocSpan l)
  , QC.testProperty "(l & locFilePath .~ fp) == setLocFilePath l fp" $
      \l fp -> (l & locFilePath .~ fp) == setLocFilePath l fp
  , QC.testProperty "(l & locSpan .~ sp) == setLocSpan l sp" $
      \l sp -> (l & locSpan .~ sp) == setLocSpan l sp
  ]
