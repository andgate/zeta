module Test.Document.Classy.HasPosition where

import           Control.Lens.Operators           ((^.), (^?))
import           Document.Classy.HasPosition      (HasPosition (..), column,
                                                   line)
import           Document.Data.Position           (Position, newPosition)
import           Document.Data.Position.Arbitrary ()
import           Linear.V2                        (V2, _x, _y)
import           Linear.V2.Arbitrary              ()
import           Numeric.Natural                  (Natural)
import           Test.Tasty                       (TestTree, testGroup)
import qualified Test.Tasty.QuickCheck            as QC

hasPositionTests :: TestTree
hasPositionTests = testGroup "HasPosition Tests"
  [ QC.testProperty "p ^? position == Just p" $
      \p -> (p :: Position) ^? position == Just p
  , QC.testProperty "newPosition ln c ^? line == Just ln" $
      \ln c -> newPosition ln c ^? line == Just ln
  , QC.testProperty "newPosition ln c ^? column == Just c" $
      \ln c -> newPosition ln c ^? column == Just c
  , QC.testProperty "(ln, c) ^? line == Just ln" $
      \ln c -> (ln :: Natural, c :: Natural) ^? line == Just ln
  , QC.testProperty "(ln, c) ^? column == Just c" $
      \ln c -> (ln :: Natural, c :: Natural) ^? column == Just c
  , QC.testProperty "v ^? line == Just (v ^. _x)" $
      \v -> (v :: V2 Natural) ^? line == Just (v ^. _x)
  , QC.testProperty "v ^? column == Just (v ^. _y)" $
      \v -> (v :: V2 Natural) ^? column == Just (v ^. _y)
  ]
