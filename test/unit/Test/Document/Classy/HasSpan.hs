module Test.Document.Classy.HasSpan where

import           Prelude                          hiding (span)

import           Control.Lens.Operators           ((^?))
import           Document.Classy.HasLocation      ()
import           Document.Classy.HasSpan          (HasSpan (..), end, start)
import           Document.Data.Location           (Loc (..))
import           Document.Data.Location.Arbitrary ()
import           Document.Data.Position           (Position)
import           Document.Data.Position.Arbitrary ()
import           Document.Data.Span               (Span (..), containsSpan,
                                                   newSpan)
import           Document.Data.Span.Arbitrary     ()
import           Linear.V4                        (V4)
import           Linear.V4.Arbitrary              ()
import           Numeric.Natural                  (Natural)
import           Numeric.Natural.Arbitrary        ()
import           Test.Tasty                       (TestTree, testGroup)
import           Test.Tasty.HUnit                 (testCase, (@?=))
import qualified Test.Tasty.QuickCheck            as QC

hasSpanTests :: TestTree
hasSpanTests = testGroup "HasSpan Tests"
  [ QC.testProperty "s ^? span == Just s" $
      \s -> (s :: Span) ^? span == Just s
  , QC.testProperty "l ^? span == Just s" $
      \l -> (l :: Loc) ^? span == Just (getLocSpan l)
  , QC.testProperty "s ^? span == Just s" $
      \s e -> (s :: Position, e :: Position) ^? span == Just (newSpan s e)
  , QC.testProperty "s ^? span == Just s" $
      \v -> (v :: V4 Natural) ^? span == Just (S v)
  , QC.testProperty "newSpan s e ^? start == Just s" $
      \s e -> newSpan s e ^? start == Just s
  , QC.testProperty "newSpan s e ^? end == Just e" $
      \s e -> newSpan s e ^? end == Just e
  , QC.testProperty "Just s ^? span == Just s" $
      \s -> Just (s :: Span) ^? span == Just s
  , testCase "(Nothing :: Maybe Span) ^? span == Nothing" $
      (Nothing :: Maybe Span) ^? span @?= Nothing
  , testCase "mconcat ([] :: [Maybe Span]) ^? span == Nothing" $
      mconcat ([] :: [Maybe Span]) ^? span @?= Nothing
  , testCase "mconcat [Nothing :: Maybe Span] ^? span == Nothing" $
      mconcat [Nothing :: Maybe Span] ^? span @?= Nothing
  , QC.testProperty "mconcat [Just (s :: Span)] ^? span == Just s" $
      \s -> mconcat [Just (s :: Span)] ^? span == Just s
  , QC.testProperty "mconcat [Nothing, Just (s :: Span)] ^? span == Just s" $
      \s -> mconcat [Nothing, Just (s :: Span)] ^? span == Just s
  , QC.testProperty "mconcat [Just (s :: Span), Nothing] ^? span == Just s" $
      \s -> mconcat [Just (s :: Span), Nothing] ^? span == Just s
  , QC.testProperty "mconcat on a list of spans" $
      \ss ->
        let ms' = mconcat (Just <$> ss) ^? span
        in case ms' of
          Just s' -> all (containsSpan s') ss
          Nothing -> null (ss :: [Span])
  ]
