{-# LANGUAGE OverloadedStrings #-}
module Test.Language.Zeta.Renamer.Exp (renamerExpTests) where

import           Control.Lens.Getter                 (to)
import           Control.Lens.Operators              ((^..))
import           Language.Zeta.Compiler.NameSet.Test (testNameSet)
import           Language.Zeta.Parse                 (parseExpUnsafe)
import           Language.Zeta.Renamer               (renameExp)
import           Language.Zeta.Syntax                (ConstructorName (..),
                                                      FieldName (..),
                                                      HasNames (..),
                                                      getConstructorNameText,
                                                      getFieldNameText)
import           Test.Tasty                          (TestTree, testGroup)
import           Test.Tasty.HUnit                    (testCase, (@?=))

renamerExpTests :: TestTree
renamerExpTests =
  testGroup "Renamer Tests (on expressions)"
    [ testCase "renameExp \"Just(n)\" ^.. names == [\"Just\"]" $
        let
          src = "Just(n)"
          e = parseExpUnsafe src
          e' = renameExp "" testNameSet e
          ns = e' ^.. names . to getConstructorNameText
        in
          ns @?= ["Just"]
    , testCase "renameExp \"{ a: 1 }\" ^.. names == [\"a\"]" $
        let
          src = "{ a: 1 }"
          e = parseExpUnsafe src
          e' = renameExp "" testNameSet e
          ns = e' ^.. names . to getFieldNameText
        in
          ns @?= ["a"]
    ]
