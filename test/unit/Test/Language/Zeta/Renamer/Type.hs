{-# LANGUAGE OverloadedStrings #-}
module Test.Language.Zeta.Renamer.Type (renamerTypeTests) where

import           Control.Lens.Getter                 (to)
import           Control.Lens.Operators              ((^..))
import           Language.Zeta.Compiler.NameSet.Test (testNameSet)
import           Language.Zeta.Parse                 (parseTypeUnsafe)
import           Language.Zeta.Renamer               (renameType)
import           Language.Zeta.Syntax                (HasNames (..),
                                                      getTypeNameText)
import           Test.Tasty                          (TestTree, testGroup)
import           Test.Tasty.HUnit                    (testCase, (@?=))

renamerTypeTests :: TestTree
renamerTypeTests =
  testGroup "Renamer Tests (on types)"
    [ testCase "renameType \"Maybe a\" ^.. names . getTypeNameText == [\"Maybe\"]" $
        let
          src = "Maybe a"
          t = parseTypeUnsafe src
          t' = renameType "" testNameSet t
          ns = t' ^.. names . to getTypeNameText
        in
          ns @?= ["Maybe"]
    ]
