module Test.Language.Zeta.Renamer (renamerTests) where

import           Test.Language.Zeta.Renamer.Exp  (renamerExpTests)
import           Test.Language.Zeta.Renamer.Type (renamerTypeTests)
import Test.Tasty (TestTree, testGroup)

renamerTests :: TestTree
renamerTests =
  testGroup "Renamer Tests"
    [ renamerTypeTests
    , renamerExpTests
    ]
