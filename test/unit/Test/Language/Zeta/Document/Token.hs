{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns      #-}
module Test.Language.Zeta.Document.Token (tokenTests) where

import           Prelude                                hiding (span)

import           Control.Lens.Operators                 ((#), (&), (.~), (^.),
                                                         (^?))
import           Data.Maybe                             (isJust, isNothing)
import qualified Data.Text                              as T
import           Document.Classy.HasSpan                (HasSpan (..))
import           Document.Data.Span                     (newSpan')
import           Language.Zeta.Document.Token           (Token (..),
                                                         TokenClass (..),
                                                         _TokenBlockClose,
                                                         _TokenBlockOpen,
                                                         _TokenBool, _TokenChar,
                                                         _TokenDouble,
                                                         _TokenEof,
                                                         _TokenInteger,
                                                         _TokenLineClose,
                                                         _TokenLineOpen,
                                                         _TokenPrimId,
                                                         _TokenRsvp,
                                                         _TokenString,
                                                         _TokenVarId,
                                                         getTokenBlockClose,
                                                         getTokenBlockOpen,
                                                         getTokenBool,
                                                         getTokenChar,
                                                         getTokenDouble,
                                                         getTokenEof,
                                                         getTokenInteger,
                                                         getTokenLineClose,
                                                         getTokenLineOpen,
                                                         getTokenPrimId,
                                                         getTokenRsvp,
                                                         getTokenString,
                                                         getTokenVarId,
                                                         hasTokenBlockClose,
                                                         hasTokenBlockOpen,
                                                         hasTokenBool,
                                                         hasTokenChar,
                                                         hasTokenDouble,
                                                         hasTokenEof,
                                                         hasTokenInteger,
                                                         hasTokenLineClose,
                                                         hasTokenLineOpen,
                                                         hasTokenPrimId,
                                                         hasTokenRsvp,
                                                         hasTokenString,
                                                         hasTokenVarId,
                                                         setTokenClass,
                                                         setTokenSpan,
                                                         tokenBlockClose,
                                                         tokenBlockOpen,
                                                         tokenBool, tokenChar,
                                                         tokenClass,
                                                         tokenDouble, tokenEof,
                                                         tokenInteger,
                                                         tokenLineClose,
                                                         tokenLineOpen,
                                                         tokenPrimId, tokenRsvp,
                                                         tokenSpan, tokenString,
                                                         tokenVarId)
import           Language.Zeta.Document.Token.Arbitrary ()
import           Prettyprinter                          (Pretty (pretty))
import           Test.Tasty                             (TestTree, testGroup)
import           Test.Tasty.HUnit                       (testCase, (@?=))
import qualified Test.Tasty.QuickCheck                  as QC

tokenTests :: TestTree
tokenTests = testGroup "Language.Zeta.Document.Token Tests"
  [ QC.testProperty "getTokenClass (Token tc l) == tc" $
      \tc l -> getTokenClass (Token tc l) == tc
  , QC.testProperty "getTokenSpan (Token tc l) == l" $
      \tc l -> getTokenSpan (Token tc l) == l
  , QC.testProperty "Token tc l ^? span == Just l" $
      \tc l -> Token tc l ^? span == Just l
  , testCase "pretty (Token (TokenRsvp \"foo\") (newSpan' 1 1 2 1)) == \"TokenRsvp \\\"foo\\\"@1:1-2:1\"" $
      (show . pretty) (Token (TokenRsvp "foo") (newSpan' 1 1 2 1)) @?= "TokenRsvp \"foo\"@1:1-2:1"
  , QC.testProperty "Pretty representation preserves length" $
        \tc l -> (length . show . pretty) (Token tc l) == ((+1) . length . show) (pretty tc <> pretty l)
  , QC.testProperty "getTokenRsvp (TokenRsvp n) == Just n (or Nothing)" $ \case
      TokenRsvp n -> getTokenRsvp (TokenRsvp n) == Just n
      t           -> isNothing (getTokenRsvp t)
  , QC.testProperty "getTokenVarId (TokenVarId n) == Just n (or Nothing)" $ \case
      TokenVarId n -> getTokenVarId (TokenVarId n) == Just n
      t            -> isNothing (getTokenVarId t)
  , QC.testProperty "getTokenPrimId (TokenPrimId n) == Just n (or Nothing)" $ \case
      TokenPrimId n -> getTokenPrimId (TokenPrimId n) == Just n
      t             -> isNothing (getTokenPrimId t)
  , QC.testProperty "getTokenInteger (TokenPrimId i) == Just i (or Nothing)" $ \case
      TokenInteger i -> getTokenInteger (TokenInteger i) == Just i
      t              -> isNothing (getTokenInteger t)
  , QC.testProperty "getTokenDouble (TokenDouble d) == Just d (or Nothing)" $ \case
      TokenDouble d -> getTokenDouble (TokenDouble d) == Just d
      t             -> isNothing (getTokenDouble t)
  , QC.testProperty "getTokenChar (TokenChar c) == Just c (or Nothing)" $ \case
      TokenChar c -> getTokenChar (TokenChar c) == Just c
      t           -> isNothing (getTokenChar t)
  , QC.testProperty "getTokenString (TokenString s) == Just s (or Nothing)" $ \case
      TokenString s -> getTokenString (TokenString s) == Just s
      t             -> isNothing (getTokenString t)
  , QC.testProperty "getTokenBool (TokenBool b) == Just b (or Nothing)" $ \case
      TokenBool b -> getTokenBool (TokenBool b) == Just b
      t           -> isNothing (getTokenBool t)
  , testCase "getTokenLineOpen TokenLineOpen == Just ()" $
      getTokenLineOpen TokenLineOpen @?= Just ()
  , testCase "getTokenLineClose TokenLineClose == Just ()" $
      getTokenLineClose TokenLineClose @?= Just ()
  , testCase "getTokenBlockOpen TokenBlockOpen == Just ()" $
      getTokenBlockOpen TokenBlockOpen @?= Just ()
  , testCase "getTokenBlockClose TokenBlockClose == Just ()" $
      getTokenBlockClose TokenBlockClose @?= Just ()
  , testCase "getTokenEof TokenEof == Just ()" $
      getTokenEof TokenEof @?= Just ()
  , QC.testProperty "getTokenLineOpen TokenLineOpen == Just () (or Nothing)" $ \case
      TokenLineOpen -> getTokenLineOpen TokenLineOpen == Just ()
      t             -> isNothing (getTokenLineOpen t)
  , QC.testProperty "getTokenLineClose TokenLineClose == Just () (or Nothing)" $ \case
      TokenLineClose -> getTokenLineClose TokenLineClose == Just ()
      t              -> isNothing (getTokenLineClose t)
  , QC.testProperty "getTokenBlockOpen TokenBlockOpen == Just () (or Nothing)" $ \case
      TokenBlockOpen -> getTokenBlockOpen TokenBlockOpen == Just ()
      t              -> isNothing (getTokenBlockOpen t)
  , QC.testProperty "getTokenBlockClose TokenBlockClose == Just () (or Nothing)" $ \case
      TokenBlockClose -> getTokenBlockClose TokenBlockClose == Just ()
      t               -> isNothing (getTokenBlockClose t)
  , QC.testProperty "getTokenEof TokenEof == Just () (or Nothing)" $ \case
      TokenEof -> getTokenEof TokenEof == Just ()
      t        -> isNothing (getTokenEof t)
  , QC.testProperty "t ^. tokenClass == getTokenClass t" $
      \t -> t ^. tokenClass == getTokenClass t
  , QC.testProperty "(t & tokenClass .~ tc) == setTokenClass t tc" $
      \t tc -> (t & tokenClass .~ tc) == setTokenClass t tc
  , QC.testProperty "t ^. tokenSpan == getTokenSpan t" $
      \t -> t ^. tokenSpan == getTokenSpan t
  , QC.testProperty "(t & tokenSpan .~ l) == setTokenSpan t l" $
      \t l -> (t & tokenSpan .~ l) == setTokenSpan t l
  , QC.testProperty "t ^? _TokenRsvp == getTokenRsvp t" $
      \t -> t ^? _TokenRsvp == getTokenRsvp t
  , QC.testProperty "t ^? _TokenVarId == getTokenVarId t" $
      \t -> t ^? _TokenVarId == getTokenVarId t
  , QC.testProperty "t ^? _TokenPrimId == getTokenPrimId t" $
      \t -> t ^? _TokenPrimId == getTokenPrimId t
  , QC.testProperty "t ^? _TokenInteger == getTokenInteger t" $
      \t -> t ^? _TokenInteger == getTokenInteger t
  , QC.testProperty "t ^? _TokenDouble == getTokenDouble t" $
      \t -> t ^? _TokenDouble == getTokenDouble t
  , QC.testProperty "t ^? _TokenChar == getTokenChar t" $
      \t -> t ^? _TokenChar == getTokenChar t
  , QC.testProperty "t ^? _TokenString == getTokenString t" $
      \t -> t ^? _TokenString == getTokenString t
  , QC.testProperty "t ^? _TokenBool == getTokenBool t" $
      \t -> t ^? _TokenBool == getTokenBool t
  , QC.testProperty "t ^? _TokenLineOpen == getTokenLineOpen t" $
      \t -> t ^? _TokenLineOpen == getTokenLineOpen t
  , QC.testProperty "t ^? _TokenLineClose == getTokenLineClose t" $
      \t -> t ^? _TokenLineClose == getTokenLineClose t
  , QC.testProperty "t ^? _TokenBlockOpen == getTokenBlockOpen t" $
      \t -> t ^? _TokenBlockOpen == getTokenBlockOpen t
  , QC.testProperty "t ^? _TokenBlockClose == getTokenBlockClose t" $
      \t -> t ^? _TokenBlockClose == getTokenBlockClose t
  , QC.testProperty "t ^? _TokenEof == getTokenEof t" $
      \t -> t ^? _TokenEof == getTokenEof t
  , QC.testProperty "_TokenRsvp # txt == TokenRsvp txt" $
      \(T.pack -> txt) -> _TokenRsvp # txt == TokenRsvp txt
  , QC.testProperty "_TokenVarId # n == TokenVarId n" $
      \(T.pack -> n) -> _TokenVarId # n == TokenVarId n
  , QC.testProperty "_TokenPrimId # n == TokenPrimId n" $
      \(T.pack -> n) -> _TokenPrimId # n == TokenPrimId n
  , QC.testProperty "_TokenInteger # i == TokenInteger i" $
      \i -> _TokenInteger # i == TokenInteger i
  , QC.testProperty "_TokenDouble d == TokenDouble d" $
      \d -> _TokenDouble # d == TokenDouble d
  , QC.testProperty "_TokenChar c == TokenChar c" $
      \c -> _TokenChar # c == TokenChar c
  , QC.testProperty "_TokenString # s == TokenString s" $
      \s -> _TokenString # s == TokenString s
  , QC.testProperty "t ^? _TokenBool == getTokenBool t" $
      \b -> _TokenBool # b == TokenBool b
  , testCase "_TokenLineOpen # () == TokenLineOpen" $
      _TokenLineOpen # () @?= TokenLineOpen
  , testCase "_TokenLineClose # () == TokenLineClose" $
      _TokenLineClose # () @?= TokenLineClose
  , testCase "_TokenBlockOpen # () == TokenBlockOpen" $
      _TokenBlockOpen # () @?= TokenBlockOpen
  , testCase "t ^? _TokenBlockClose == getTokenBlockClose" $
      _TokenBlockClose # () @?= TokenBlockClose
  , testCase "_TokenEof # () == TokenEof" $
      _TokenEof # () @?= TokenEof
  , QC.testProperty "t ^? tokenRsvp == (getTokenRsvp . getTokenClass) t" $
      \t -> t ^? tokenRsvp == (getTokenRsvp . getTokenClass) t
  , QC.testProperty "t ^? tokenVarId == (getTokenVarId . getTokenClass) t" $
      \t -> t ^? tokenVarId == (getTokenVarId . getTokenClass) t
  , QC.testProperty "t ^? tokenPrimId == (getTokenPrimId . getTokenClass) t" $
      \t -> t ^? tokenPrimId == (getTokenPrimId . getTokenClass) t
  , QC.testProperty "t ^? tokenInteger == (getTokenInteger . getTokenClass) t" $
      \t -> t ^? tokenInteger == (getTokenInteger . getTokenClass) t
  , QC.testProperty "t ^? tokenDouble == (getTokenDouble . getTokenClass) t" $
      \t -> t ^? tokenDouble == (getTokenDouble . getTokenClass) t
  , QC.testProperty "t ^? tokenChar == (getTokenChar . getTokenClass) t" $
      \t -> t ^? tokenChar == (getTokenChar . getTokenClass) t
  , QC.testProperty "t ^? tokenString == (getTokenString . getTokenClass) t" $
      \t -> t ^? tokenString == (getTokenString . getTokenClass) t
  , QC.testProperty "t ^? tokenBool == (getTokenBool . getTokenClass) t" $
      \t -> t ^? tokenBool == (getTokenBool . getTokenClass) t
  , QC.testProperty "t ^? tokenLineOpen == (getTokenLineOpen . getTokenClass) t" $
      \t -> t ^? tokenLineOpen == (getTokenLineOpen . getTokenClass) t
  , QC.testProperty "t ^? tokenLineClose == (getTokenLineClose . getTokenClass) t" $
      \t -> t ^? tokenLineClose == (getTokenLineClose . getTokenClass) t
  , QC.testProperty "t ^? tokenBlockOpen == (getTokenBlockOpen . getTokenClass) t" $
      \t -> t ^? tokenBlockOpen == (getTokenBlockOpen . getTokenClass) t
  , QC.testProperty "t ^? tokenBlockClose == (getTokenBlockClose . getTokenClass) t" $
      \t -> t ^? tokenBlockClose == (getTokenBlockClose . getTokenClass) t
  , QC.testProperty "t ^? tokenEof == (getTokenEof . getTokenClass) t" $
      \t -> t ^? tokenEof == (getTokenEof . getTokenClass) t
  , QC.testProperty "hasTokenRsvp t == (isJust . getTokenRsvp . getTokenClass) t" $
      \t -> hasTokenRsvp t == (isJust . getTokenRsvp . getTokenClass) t
  , QC.testProperty "hasTokenVarId t == (isJust . getTokenVarId . getTokenClass) t" $
      \t -> hasTokenVarId t == (isJust . getTokenVarId . getTokenClass) t
  , QC.testProperty "hasTokenPrimId t == (isJust . getTokenPrimId . getTokenClass) t" $
      \t -> hasTokenPrimId t == (isJust . getTokenPrimId . getTokenClass) t
  , QC.testProperty "hasTokenInteger t == (isJust . getTokenInteger . getTokenClass) t" $
      \t -> hasTokenInteger t == (isJust . getTokenInteger . getTokenClass) t
  , QC.testProperty "hasTokenDouble t == (isJust . getTokenDouble . getTokenClass) t" $
      \t -> hasTokenDouble t == (isJust . getTokenDouble . getTokenClass) t
  , QC.testProperty "hasTokenChar t == (isJust . getTokenChar . getTokenClass) t" $
      \t -> hasTokenChar t == (isJust . getTokenChar . getTokenClass) t
  , QC.testProperty "hasTokenString t == (isJust . getTokenString . getTokenClass) t" $
      \t -> hasTokenString t == (isJust . getTokenString . getTokenClass) t
  , QC.testProperty "hasTokenBool t == (isJust . getTokenBool . getTokenClass) t" $
      \t -> hasTokenBool t == (isJust . getTokenBool . getTokenClass) t
  , QC.testProperty "hasTokenLineOpen t == (isJust . getTokenLineOpen . getTokenClass) t" $
      \t -> hasTokenLineOpen t == (isJust . getTokenLineOpen . getTokenClass) t
  , QC.testProperty "t ^? hasTokenLineClose == (isJust . getTokenLineClose . getTokenClass) t" $
      \t -> hasTokenLineClose t == (isJust . getTokenLineClose . getTokenClass) t
  , QC.testProperty "hasTokenBlockOpen t == (isJust . getTokenBlockOpen . getTokenClass) t" $
      \t -> hasTokenBlockOpen t == (isJust . getTokenBlockOpen . getTokenClass) t
  , QC.testProperty "hasTokenBlockClose t == (isJust . getTokenBlockClose . getTokenClass) t" $
      \t -> hasTokenBlockClose t == (isJust . getTokenBlockClose . getTokenClass) t
  , QC.testProperty "hasTokenEof t == (isJust . getTokenEof . getTokenClass) t" $
      \t -> hasTokenEof t == (isJust . getTokenEof . getTokenClass) t
  ]
