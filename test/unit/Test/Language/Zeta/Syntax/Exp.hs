{-# LANGUAGE OverloadedStrings #-}
module Test.Language.Zeta.Syntax.Exp (expSyntaxTests) where

import           Prelude                   hiding (lex)

import           Control.Lens.Fold         (has)
import           Control.Lens.Operators    ((^..))
import           Control.Lens.Prism        (_Just, _Nothing)
import           Language.Zeta.Parse       (parseExpUnsafe)
import           Language.Zeta.Syntax      (HasVars (vars),
                                            Scope (getScopeBody), varBinder,
                                            varBoundStr)
import           Language.Zeta.Syntax.Test (abstractExp, applyExp, getExpVars,
                                            renderResult, testExp)
import           Test.Tasty                (TestTree, testGroup)
import           Test.Tasty.HUnit          (testCase, (@?=))

expSyntaxTests :: TestTree
expSyntaxTests = testGroup "Expression Syntax Tests"
  [ testCase "#add(1, 2)" $
      testExp "#add(1, 2)"
  , testCase "#add(x, y)" $
      testExp "#add(x, y)"
  , testCase "(x) -> #add(x, y)" $
      testExp "(x) -> #add(x, y)"
  , testCase "(x, y) -> #add(x, y)" $
      testExp "(x, y) -> #add(x, y)"
  , testCase "a: i32" $
      testExp "a: i32"
  , testCase "a as i32" $
      testExp "a as i32"
  , testCase "#mul(#add(x, y), z)" $
      getExpVars "#mul(#add(x, y), z)" @?= ["x", "y", "z"]
  , testCase "(x) -> #add(x@0, y)" $
      getExpVars "(x) -> #add(x, y)" @?= ["x@0", "y"]
  , testCase "(x, y) -> #add(x@0, y@1)" $
      getExpVars "(x, y) -> #add(x, y)" @?= ["x@0", "y@1"]
  , testCase "(x, y, z) -> #mul(#add(x@0, y@1), z@2)" $
      getExpVars "(x, y, z) -> #mul(#add(x, y), z)" @?= ["x@0", "y@1", "z@2"]
  , testCase "(x) -> (y) -> #add(x@1, y@0)" $
      getExpVars "(x) -> (y) -> #add(x, y)" @?= ["x@1", "y@0"]
  , testCase "(y) -> (x) -> #add(x@0, y@1)" $
      getExpVars "(y) -> (x) -> #add(x, y)" @?= ["x@0", "y@1"]
  , testCase "(x) -> (y) -> (z) -> #mul(#add(x@2, y@1), z@0)" $
      getExpVars "(x) -> (y) -> (z) -> #mul(#add(x, y), z)" @?= ["x@2", "y@1", "z@0"]
  , testCase "hasFree #mul(#add(x, y), z) == True" $
      let src = "#mul(#add(x, y), z)"
          e = parseExpUnsafe src
          r = all (has (varBinder . _Nothing)) (e ^.. vars)
      in r @?= True
  , testCase "hasFree (abstractExp #mul(#add(x, y), z)) == False" $
      let src = "#mul(#add(x, y), z)"
          vs = [ "x", "y", "z" ]
          e = getScopeBody $ abstractExp src vs
          r = all (has (varBinder . _Just)) (e ^.. vars)
      in r @?= True
  , testCase "abstractExp #mul(#add(x, y), z) [x, y, z]" $
      let src = "#mul(#add(x, y), z)"
          vs = [ "x", "y", "z" ]
          e = getScopeBody $ abstractExp src vs
      in e ^.. vars . varBoundStr @?= ["x@0", "y@1", "z@2"]
  , testCase "applyExp #mul(#add(x, y), z) such that x=1, y=2, and z=3." $
      let src = "#mul(#add(x, y), z)"
          eqs = [ ("x", "1"), ("y", "2"), ("z", "3") ]
      in renderResult (applyExp src eqs) @?= "#mul(#add(1, 2), 3)"
  ]
