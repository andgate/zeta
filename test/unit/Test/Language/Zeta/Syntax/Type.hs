{-# LANGUAGE OverloadedStrings #-}
module Test.Language.Zeta.Syntax.Type (typeSyntaxTests) where

import           Prelude                   hiding (lex)

import           Language.Zeta.Syntax.Test (applyType, renderResult, testType,
                                            testTypeAbstractAll,
                                            testTypeBindUnbind)
import           Test.Tasty                (TestTree, testGroup)
import           Test.Tasty.HUnit          (testCase, (@?=))

typeSyntaxTests :: TestTree
typeSyntaxTests = testGroup "Type Syntax Tests"
  [ testCase "T" $
      testType "T"
  , testCase "T a" $
      testType "T a"
  , testCase "T a b" $
      testType "T a b"
  , testCase "A -> B" $
      testType "A -> B"
  , testCase "A -> B -> C" $
      testType "A -> B -> C"
  , testCase "i1" $
      testType "i1"
  , testCase "i8" $
      testType "i8"
  , testCase "i16" $
      testType "i16"
  , testCase "i32" $
      testType "i32"
  , testCase "i64" $
      testType "i64"
  , testCase "u1" $
      testType "u1"
  , testCase "u8" $
      testType "u8"
  , testCase "u16" $
      testType "u16"
  , testCase "u32" $
      testType "u32"
  , testCase "u64" $
      testType "u64"
  , testCase "f1" $
      testType "f1"
  , testCase "f8" $
      testType "f8"
  , testCase "f16" $
      testType "f16"
  , testCase "f32" $
      testType "f32"
  , testCase "f64" $
      testType "f64"
  , testCase "testTypeAbstractAll (T a b)" $
      testTypeAbstractAll "T a b"
  , testCase "testTypeBindUnbind (T a b)" $
      testTypeBindUnbind "T a b"
  , testCase "applyType (T a b) [(a, i32), (b, f64)] == T i32 f64" $
      let
        t = applyType "T a b" [("a", "i32"), ("b", "f64")]
      in
        renderResult t @?= "T i32 f64"
  ]
