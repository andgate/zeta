module Test.Language.Zeta.Syntax (syntaxTests) where

import           Test.Language.Zeta.Syntax.Exp  (expSyntaxTests)
import           Test.Language.Zeta.Syntax.Type (typeSyntaxTests)
import           Test.Tasty                     (TestTree, testGroup)

syntaxTests :: TestTree
syntaxTests = testGroup "Syntax Tests"
  [ expSyntaxTests
  , typeSyntaxTests
  ]
