{-# LANGUAGE LambdaCase #-}
module Test.TestError where

import           Test.Tasty.QuickCheck (Arbitrary (..), chooseInt)

data TestError = ErrA | ErrB | ErrC
  deriving (Show)

instance Eq TestError where
  (==) e1 e2 = case (e1, e2) of
    (ErrA, ErrA) -> True
    (ErrB, ErrB) -> True
    (ErrC, ErrC) -> True
    _            -> False

instance Enum TestError where
  fromEnum = \case
    ErrA -> 1
    ErrB -> 2
    ErrC -> 3

  toEnum = \case
    1 -> ErrA
    2 -> ErrB
    _ -> ErrC

instance Arbitrary TestError where
  arbitrary =
    toEnum <$> chooseInt (1, 3)
