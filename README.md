# Zeta Programming Language

## About

Compiler for the Zeta programming language. Zeta is a low-level, strongly typed systems language that targets LLVM. Statements and expressions are strictly evaluated, and functions are call-by-value. Zeta has syntax similar to C, but adjusted to handle a heavier type system.

Zeta provides primitives for pointers, arrays, and c-strings. It even includes
stack and heap managment, and allows for mutation and unchecked side-effects.
Zeta provides a type system similar to SystemF. Dependent types may be included in the future.

This code is in early development, and many pieces are unimplemented/broken.
In the future, I'd like to have tests passing and a website up. Currently, there is no set of milestones that designate a 1.0 release.


## Requirements

You will need llvm-8 and the latest ghc/cabal for compiling haskell.
Also, you should run `cabal update` to ensure you have the freshest
set of packages for haskell.

## Running tests

To run tests, run the command `cabal new-test golden-testing`.

## Building files

Currently, only `.z` files are supported. To build files,
use the command:
```
cabal new-run zetta -- [-o outfile] infile..
```
If the build succeeds, you may execute the outfile.
The default outfile is `a.out`.

## Language Features
  
  - Typesystem: basically System-f
    - type constructors
    - type variables
    - higher-rank polymorphism
  - Targets LLVM
    - Base types/values mirror the types offered by LLVM
    - Transforms syntax into a-normal form lambda calculus (ANF)
    - ANF syntax corresponds strictly to Single-Static Assignment (SSA), which LLVM IR is based on
    - So from ANF, generating llvm ir is a straightforward exercise.
  - Algebraic Data Types
  - Pattern Matching, Destructuring Values
  - Statements
  - Structured programming (if statements, for-loops, while-loops, mutable variables)
  - Garbage collected heap values, closures
    - Would be nice to have garbage collected heap values/closures
    - This would allow for truly high-level functional programming
    - LLVM supports custom garbage collectors, could be some optimizations there
    - GC is acceptable for low-level languages now
      - for instance, Nim and Go feature garbage collected runtimes
  - Type classes
    - type class morphisms?
      - Cool feature, but might not be doable in our timeframe
  - Objects? Getters/setters? Inheritance/subclassing?
    - With mutable values, feels like object-oriented features are necessary
    - Might not be necessary, they introduce complexity we don't really need
    - Just convenience syntax for passing objects into methods LUL
    - Though dot-syntax is nice
    

## Milestones
   
  - Tests working with 2021 syntax proposal
  - Base package is written with documentation and tests
  - 2021 Zeta Language Standard is written
  - Main Website is published
    - Language Overview
    - Documentation
      - Beginners Guide
      - Language Standard
    - Download page for compiler executable
      - Hosts most recent build executables for windows, mac osx, and linux.
      - Releases should be an automated process.
    - Link to Package DB
    - Link to gitlab
  - Package DB website
    - Some website to host packages for the community
    - Needs a name
    - Inspired by hackage/npm

## Useful Links

Some links to help with development


[Git development model](https://nvie.com/posts/a-successful-git-branching-model/)

### Parsing
https://www.haskell.org/alex/doc/html/index.html

https://www.haskell.org/happy/doc/html/index.html (anyone have a link for v1.19 docs?)

https://github.com/harpocrates/language-rust/blob/0e02e21c389cb56c77f71b902288b195ed273af6/src/Language/Rust/Parser/Internal.y#L74


### Pattern Matching Compiler
http://l-lang.org/blog/Compiling-pattern-matching/


### LLVM
http://hackage.haskell.org/package/llvm-hs

http://hackage.haskell.org/package/llvm-hs-pure

https://mapping-high-level-constructs-to-llvm-ir.readthedocs.io/en/latest/README.html


### Destination-Passing Style
https://www.microsoft.com/en-us/research/wp-content/uploads/2016/11/dps-fhpc17.pdf


### Monomorphization
https://reasonablypolymorphic.com/blog/algorithmic-sytc/


### Arbitrary Rank Polymorphic Type Checking
https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/putting.pdf

https://github.com/namin/higher-rank

https://github.com/garyb/infer-rank-n-types


### Bidirectional Type Checking
https://www.cl.cam.ac.uk/~nk480/bidir.pdf

https://github.com/ollef/Bidirectional

http://davidchristiansen.dk/tutorials/bidirectional.pdf

https://github.com/sweirich/pi-forall

### Typeclasses

[Improving typeclasses by being open](https://www.fceia.unr.edu.ar/~mauro/pubs/cm-conf.pdf)